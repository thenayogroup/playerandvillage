# Todo List:

  - Logging system
  - Config of some kind (Change a setting, for example, camera sensitivity, and remember it in a file)
  - Create a test project that test the math lib with glm!
  - Shader list that you can append to and use.
  - "Rewind" button to go back. (For ex. press a button and go back a minute)
  - There is a lot of problems with using the `static` keyword in the code with the Hot-Loading feature,
    to solve this I suggest that we will use something like cayse did, create another memory block 
    seperatly from the one we have right now, and use it for stuff that should be acted like `static`.
    For example, in `Noise.h` we wanted to create a static random engine but couldn't, if we could have
    created the engine and store it in memory that would have worked.
  - More soon..