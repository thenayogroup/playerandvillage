#pragma once

namespace Color
{
    static const v3 Red = V3(1, 0, 0);
    static const v3 Green = V3(0, 1, 0);
    static const v3 Blue = V3(0, 0, 1);
    static const v3 Yellow = V3(1,1,0);
    static const v3 Cyan = V3(0, 1, 1);
    static const v3 Magenta = V3(1, 0, 1);
    static const v3 White = V3(1);
    static const v3 Black = V3(0);
    
    // These grays are useful for fine-tuning lighting color values
    // and for other areas where subtle variations of grays are needed.
    // PERCENTAGE GRAYS:
    static const v3 Gray05 = White*0.05f;
    static const v3 Gray10 = White*0.10f;
    static const v3 Gray15 = White*0.15f;
    static const v3 Gray20 = White*0.20f;
    static const v3 Gray25 = White*0.25f;
    static const v3 Gray30 = White*0.30f;
    static const v3 Gray35 = White*0.35f;
    static const v3 Gray40 = White*0.40f;
    static const v3 Gray45 = White*0.45f;
    static const v3 Gray50 = White*0.50f;
    static const v3 Gray55 = White*0.55f;
    static const v3 Gray60 = White*0.60f;
    static const v3 Gray65 = White*0.65f;
    static const v3 Gray70 = White*0.70f;
    static const v3 Gray75 = White*0.75f;
    static const v3 Gray80 = White*0.80f;
    static const v3 Gray85 = White*0.85f;
    static const v3 Gray90 = White*0.90f;
    static const v3 Gray95 = White*0.95f;
    
    // OTHER GRAYS
    static const v3 DimGray = V3(0.329412f, 0.329412f, 0.329412f);
    static const v3 Grey = V3(0.752941f, 0.752941f, 0.752941f);
    static const v3 LightGrey = V3(0.658824f, 0.658824f, 0.658824f);
    static const v3 VLightGray = V3(0.80f, 0.80f, 0.80f);
    
    static const v3 Aquamarine = V3(0.439216f, 0.858824f, 0.576471f);
    static const v3 BlueViolet = V3(0.62352f, 0.372549f, 0.623529f);
    static const v3 Brown = V3(0.647059f, 0.164706f, 0.164706f);
    static const v3 CadetBlue = V3(0.372549f, 0.623529f, 0.623529f);
    static const v3 Coral = V3(1.0f, 0.498039f, 0.0f);
    static const v3 CornflowerBlue = V3(0.258824f, 0.258824f, 0.435294f);
    static const v3 DarkGreen = V3(0.184314f, 0.309804f, 0.184314f);
    static const v3 DarkOliveGreen = V3(0.309804f, 0.309804f, 0.184314f);
    static const v3 DarkOrchid = V3(0.6f, 0.196078f, 0.8f);
    static const v3 DarkSlateBlue = V3(0.419608f, 0.137255f, 0.556863f);
    static const v3 DarkSlateGray = V3(0.184314f, 0.309804f, 0.309804f);
    static const v3 DarkSlateGrey = V3(0.184314f, 0.309804f, 0.309804f);
    static const v3 DarkTurquoise = V3(0.439216f, 0.576471f, 0.858824f);
    static const v3 Firebrick = V3(0.556863f, 0.137255f, 0.137255f);
    static const v3 ForestGreen = V3(0.137255f, 0.556863f, 0.137255f);
    static const v3 Gold = V3(0.8f, 0.498039f, 0.196078f);
    static const v3 Goldenrod = V3(0.858824f, 0.858824f, 0.439216f);
    static const v3 GreenYellow = V3(0.576471f, 0.858824f, 0.439216f);
    static const v3 IndianRed = V3(0.309804f, 0.184314f, 0.184314f);
    static const v3 Khaki = V3(0.623529f, 0.623529f, 0.372549f);
    static const v3 LightBlue = V3(0.74902f, 0.847059f, 0.847059f);
    static const v3 LightSteelBlue = V3(0.560784f, 0.560784f, 0.737255f);
    static const v3 LimeGreen = V3(0.196078f, 0.8f, 0.196078f);
    static const v3 Maroon = V3(0.556863f, 0.137255f, 0.419608f);
    static const v3 MediumAquamarine = V3(0.196078f, 0.8f, 0.6f);
    static const v3 MediumBlue = V3(0.196078f, 0.196078f, 0.8f);
    static const v3 MediumForestGreen = V3(0.419608f, 0.556863f, 0.137255f);
    static const v3 MediumGoldenrod = V3(0.917647f, 0.917647f, 0.678431f);
    static const v3 MediumOrchid = V3(0.576471f, 0.439216f, 0.858824f);
    static const v3 MediumSeaGreen = V3(0.258824f, 0.435294f, 0.258824f);
    static const v3 MediumSlateBlue = V3(0.498039f, 0.0f, 1.0f);
    static const v3 MediumSpringGreen = V3(0.498039f, 1.0f,0.0f);
    static const v3 MediumTurquoise = V3(0.439216f, 0.858824f, 0.858824f);
    static const v3 MediumVioletRed = V3(0.858824f, 0.439216f, 0.576471f);
    static const v3 MidnightBlue = V3(0.184314f, 0.184314f, 0.309804f);
    static const v3 Navy = V3(0.137255f, 0.137255f, 0.556863f);
    static const v3 NavyBlue = V3(0.137255f, 0.137255f, 0.556863f);
    static const v3 Orange = V3(1.0f, 0.5f, 0.0f);
    static const v3 OrangeRed = V3(1.0f, 0.25f, 0.0f);
    static const v3 Orchid = V3(0.858824f, 0.439216f, 0.858824f);
    static const v3 PaleGreen = V3(0.560784f, 0.737255f, 0.560784f);
    static const v3 Pink = V3(0.737255f, 0.560784f, 0.560784f);
    static const v3 Plum = V3(0.917647f, 0.678431f, 0.917647f);
    static const v3 Salmon = V3(0.435294f, 0.258824f, 0.258824f);
    static const v3 SeaGreen = V3(0.137255f, 0.556863f, 0.419608f);
    static const v3 Sienna = V3(0.556863f, 0.419608f, 0.137255f);
    static const v3 SkyBlue = V3(0.196078f, 0.6f, 0.8f);
    static const v3 SlateBlue = V3(0.0f, 0.498039f, 1.0f);
    static const v3 SpringGreen = V3(0.0f, 1.0f, 0.498039f);
    static const v3 SteelBlue = V3(0.137255f, 0.419608f, 0.556863f);
    static const v3 Tan = V3(0.858824f, 0.576471f, 0.439216f);
    static const v3 Thistle = V3(0.847059f, 0.74902f, 0.847059f);
    static const v3 Turquoise = V3(0.678431f, 0.917647f, 0.917647f);
    static const v3 Violet = V3(0.309804f, 0.184314f, 0.309804f);
    static const v3 VioletRed = V3(0.8f, 0.196078f, 0.6f);
    static const v3 Wheat = V3(0.847059f, 0.847059f, 0.74902f);
    static const v3 YellowGreen = V3(0.6f, 0.8f, 0.196078f);
    static const v3 SummerSky = V3(0.22f, 0.69f, 0.87f);
    static const v3 RichBlue = V3(0.35f, 0.35f, 0.67f);
    static const v3 Brass = V3(0.71f, 0.65f, 0.26f);
    static const v3 Copper = V3(0.72f, 0.45f, 0.20f);
    static const v3 Bronze = V3(0.55f, 0.47f, 0.14f);
    static const v3 Bronze2 = V3(0.65f, 0.49f, 0.24f);
    static const v3 Silver = V3(0.90f, 0.91f, 0.98f);
    static const v3 BrightGold = V3(0.85f, 0.85f, 0.10f);
    static const v3 OldGold = V3(0.81f, 0.71f, 0.23f);
    static const v3 Feldspar = V3(0.82f, 0.57f, 0.46f);
    static const v3 Quartz = V3(0.85f, 0.85f, 0.95f);
    static const v3 NeonPink = V3(1.00f, 0.43f, 0.78f);
    static const v3 DarkPurple = V3(0.53f, 0.12f, 0.47f);
    static const v3 NeonBlue = V3(0.30f, 0.30f, 1.00f);
    static const v3 CoolCopper = V3(0.85f, 0.53f, 0.10f);
    static const v3 MandarinOrange = V3(0.89f, 0.47f, 0.20f);
    static const v3 LightWood = V3(0.91f, 0.76f, 0.65f);
    static const v3 MediumWood = V3(0.65f, 0.50f, 0.39f);
    static const v3 DarkWood = V3(0.52f, 0.37f, 0.26f);
    static const v3 SpicyPink = V3(1.00f, 0.11f, 0.68f);
    static const v3 SemiSweetChoc = V3(0.42f, 0.26f, 0.15f);
    static const v3 BakersChoc = V3(0.36f, 0.20f, 0.09f);
    static const v3 Flesh = V3(0.96f, 0.80f, 0.69f);
    static const v3 NewTan = V3(0.92f, 0.78f, 0.62f);
    static const v3 NewMidnightBlue = V3(0.00f, 0.00f, 0.61f);
    static const v3 VeryDarkBrown = V3(0.35f, 0.16f, 0.14f);
    static const v3 DarkBrown = V3(0.36f, 0.25f, 0.20f);
    static const v3 DarkTan = V3(0.59f, 0.41f, 0.31f);
    static const v3 GreenCopper = V3(0.32f, 0.49f, 0.46f);
    static const v3 DkGreenCopper = V3(0.29f, 0.46f, 0.43f);
    static const v3 DustyRose = V3(0.52f, 0.39f, 0.39f);
    static const v3 HuntersGreen = V3(0.13f, 0.37f, 0.31f);
    static const v3 Scarlet = V3(0.55f, 0.09f, 0.09f);
};