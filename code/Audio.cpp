#include "Audio.h"

static void InitializeAudio(audio_manager& Audio)
{
    Audio.Device = alcOpenDevice(nullptr);
    
    if(!Audio.Device)
    {
        // TODO(Naor): Add error message and crash the game?
        std::cout << "[Error:Audio] Couldn't load the audio system!\n";
        return;
    }
    
    Audio.Context = alcCreateContext(Audio.Device, nullptr);
    alcMakeContextCurrent(Audio.Context);
    
    ALCenum error = alGetError();
    if(error != AL_NO_ERROR)
    {
        std::cout << error << "\n";
    }
}

static void UpdateListener(audio_manager& Audio)
{
    alListenerfv(AL_POSITION, Audio.Listener.Position.Data());
    alListenerfv(AL_VELOCITY, Audio.Listener.Velocity.Data());
    // TODO(Naor): Find better solution for this mess
    r32 Orientation[] = {Audio.Listener.Forward.x, Audio.Listener.Forward.y, Audio.Listener.Forward.z,
        Audio.Listener.Up.x, Audio.Listener.Up.y, Audio.Listener.Up.z,};
    alListenerfv(AL_ORIENTATION, Orientation);
}

inline void sound_source::SetPosition(const v3& NewPosition)
{
    alSourcefv(ID, AL_POSITION, NewPosition.Data());
    Position = NewPosition;
}

inline void sound_source::SetVelocity(const v3& NewVelocity)
{
    alSourcefv(ID, AL_VELOCITY, NewVelocity.Data());
    Velocity = NewVelocity;
}

inline void sound_source::SetDirection(const v3& NewDirection)
{
    alSourcefv(ID, AL_DIRECTION, NewDirection.Data());
    Direction = NewDirection;
}

inline void sound_source::SetGain(r32 NewGain)
{
    alSourcef(ID, AL_GAIN, NewGain);
    Gain = NewGain;
}

inline void sound_source::SetPitch(r32 NewPitch)
{
    alSourcef(ID, AL_PITCH, NewPitch);
    Pitch = NewPitch;
}

inline void sound_source::SetLoop(b32 NewLoop)
{
    alSourcei(ID, AL_LOOPING, NewLoop);
    Loop = NewLoop;
}

// NOTE(Naor): Using this with a paused source will resume the source.
// If the source is already playing, the source will restart.
inline void sound_source::Play()
{
    alSourcePlay(ID);
    IsPlaying = true;
}

inline void sound_source::Pause()
{
    if(IsPlaying)
    {
        alSourcePause(ID);
        IsPlaying = false;
    }
}

inline void sound_source::Stop()
{
    if(IsPlaying)
    {
        // NOTE(Naor): There is another function called alSourceStop, but this will not "rewind" the source.
        alSourceRewind(ID);
        IsPlaying = false;
    }
}

// TODO(Naor): The if(Source.ID == 0) is wrong! alGenSources can return 0 as an id.
static void GenerateSoundSource(sound_source& Source, sound_buffer& Buffer, b32 Loop)
{
    if(Source.ID == 0)
        alGenSources(1, &Source.ID);
    else
        Source.Stop();
    
    alSourcei(Source.ID, AL_BUFFER, Buffer.ID);
    Source.SetLoop(Loop);
    
    Source.Loop = Loop;
    Source.IsPlaying = false;
    Source.IsDeleted = false;
}

static void GenerateSoundSource(sound_source& Source, sound_buffer& Buffer, b32 Loop, r32 Gain, r32 Pitch)
{
    if(Source.ID == 0)
        alGenSources(1, &Source.ID);
    else
        Source.Stop();
    
    alSourcei(Source.ID, AL_BUFFER, Buffer.ID);
    Source.SetLoop(Loop);
    Source.SetGain(Gain);
    Source.SetPitch(Pitch);
    
    Source.Gain = Gain;
    Source.Pitch = Pitch;
    Source.Loop = Loop;
    Source.IsPlaying = false;
    Source.IsDeleted = false;
}

static void GenerateSoundSource(sound_source& Source, sound_buffer& Buffer, b32 Loop,
                                const v3& Position, const v3& Velocity, const v3& Direction)
{
    if(Source.ID == 0)
        alGenSources(1, &Source.ID);
    else
        Source.Stop();
    
    alSourcei(Source.ID, AL_BUFFER, Buffer.ID);
    Source.SetLoop(Loop);
    Source.SetPosition(Position);
    Source.SetVelocity(Velocity);
    Source.SetDirection(Direction);
    
    Source.Position = Position;
    Source.Velocity = Velocity;
    Source.Direction = Direction;
    Source.Loop = Loop;
    Source.IsPlaying = false;
    Source.IsDeleted = false;
}

static void GenerateSoundSource(sound_source& Source, sound_buffer& Buffer, b32 Loop, r32 Gain, r32 Pitch,
                                const v3& Position, const v3& Velocity, const v3& Direction)
{
    if(Source.ID == 0)
        alGenSources(1, &Source.ID);
    else
        Source.Stop();
    
    alSourcei(Source.ID, AL_BUFFER, Buffer.ID);
    Source.SetLoop(Loop);
    Source.SetGain(Gain);
    Source.SetPitch(Pitch);
    Source.SetPosition(Position);
    Source.SetVelocity(Velocity);
    Source.SetDirection(Direction);
    
    Source.Gain = Gain;
    Source.Pitch = Pitch;
    Source.Position = Position;
    Source.Velocity = Velocity;
    Source.Direction = Direction;
    Source.Loop = Loop;
    Source.IsPlaying = false;
    Source.IsDeleted = false;
}

static void DeleteSoundSource(sound_source& Source)
{
    if(!Source.IsDeleted)
    {
        // This will result in a NOP if the source is already stopped
        Source.Stop();
        
        alDeleteSources(1, &Source.ID);
        Source.IsDeleted = true;
        Source.IsPlaying = false;
    }
}