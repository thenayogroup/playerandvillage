#pragma once

static void UpdateAndRenderBuilding(entity& Building, mesh& Mesh, texture_index Texture)
{
    m4 ModelMatrix = Math::YRotation(Math::Radians(Building.Angle)); 
    ModelMatrix = Translate(ModelMatrix, Building.Position);
    
    v4 Color = V4(0.0f, 0.0f, 0.0f, 1.0f);
    
    switch(Building.Type)
    {
        case EntityType_FixedBuilding:
        {
            if(Building.IsFirstPlaced)
            {
                Building.IsFirstPlaced = false;
                
                Building.BoundingBox = Mesh.BoundingBox * ModelMatrix;
                CalculateTilesLocations(Building.BoundingBox, GameState->BuildingsTilesLocations,
                                        GameState->BuildingsTilesLocationsCount, TileDimension);
                
            }
            
            CheckRayCollisionWithEntity(GameState->SelectedBuilding, GameState->MainCamera,
                                        GameState->MousePicker, GameState->Entities, Building,
                                        SelectionMaxDistance);
        }
        break;
        case EntityType_PlacingBuilding:
        {
            GameState->PlacingBuilding->TilesLocationsCount = 0;
            
            CalculateTilesLocations(Mesh.BoundingBox * ModelMatrix,
                                    Building.TilesLocations,
                                    Building.TilesLocationsCount,
                                    TileDimension);
            Color = Building.Color;
        }
        break;
    }
    
    RenderModel(GameState->RenderGroup, GameState->Assets.Shader_TexturedModel, Mesh, Texture, GameState->MVP, ModelMatrix, Color);
}

static void UpdateAndRenderBuildingType(entity& Building, building_type BuildingType)
{
    switch(BuildingType)
    {
        case BuildingType_Farmhouse:
        {
            UpdateAndRenderBuilding(Building,
                                    GameState->Assets.Mesh_Farmhouse,
                                    TEXTURE_FARMHOUSE_INDEX);
        }
        break;
        case BuildingType_Cottage:
        {
            UpdateAndRenderBuilding(Building,
                                    GameState->Assets.Mesh_Cottage,
                                    TEXTURE_COTTAGE_INDEX);
        }
        break;
        case BuildingType_Wall:
        {
            UpdateAndRenderBuilding(Building,
                                    GameState->Assets.Mesh_Wall,
                                    TEXTURE_WALL_INDEX);
        }
        break;
        
        case BuildingType_Outpost:
        {
            UpdateAndRenderBuilding(Building,
                                    GameState->Assets.Mesh_Outpost,
                                    TEXTURE_OUTPOST_INDEX);
        }
        break;
        
        case BuildingType_CampFire:
        {
            UpdateAndRenderBuilding(Building,
                                    GameState->Assets.Mesh_FireCamp,
                                    TEXTURE_CAMPFIRE_INDEX);
        }
        break;
        
    }
}

static void CheckCollisionPlacingBuilding(entity& Entity)
{
    
    v3 StartRay = GameState->MainCamera.Position; 
    v3 EndRay = GameState->MainCamera.Position + GameState->MousePicker.Distance * GameState->MousePicker.Ray;
    
    DEBUG_LINE(StartRay, EndRay, V3(1, 1, 1));
    
    v3 StartPlane =  NumOfPlacingTiles * GameState->PlacingTileCoord.RelWorld;
    v3 EndPlane = NumOfPlacingTiles * V3(GameState->PlacingTileCoord.RelWorld.x + TileDimension, 0,
                                         GameState->PlacingTileCoord.RelWorld.z + TileDimension);
    
    DEBUG_PLANE(StartPlane, EndPlane, V3(1, 1, 1));
    
    // TODO(Jonathan): Move this to another function?
    
    // Check if it's okay to place a building in that location
    bool Found = false;
    for(int i = 0; i < Entity.TilesLocationsCount && !Found; ++i)
    {
        for(int j = 0; j < GameState->BuildingsTilesLocationsCount && !Found; ++j)
        {
            if(Entity.TilesLocations[i] == GameState->BuildingsTilesLocations[j])
            {
                Found = true;
            }
        }
        
        for(u32 EntityIndex = 0; EntityIndex < GameState->EntitiesCount; ++EntityIndex)
        {
            for(int j = 0; j < GameState->Entities[EntityIndex].TilesLocationsCount && !Found; ++j)
            {
                if(GameState->Entities[EntityIndex].Type != EntityType_PlacingBuilding)
                {
                    if(Entity.TilesLocations[i] == GameState->Entities[EntityIndex].TilesLocations[j])
                    {
                        Found = true;
                    }
                    
                    if(!Found && (GameState->Entities[EntityIndex].TilesLocations[j].x >= GameState->PlacingTileCoord.X * NumOfPlacingTiles &&
                                  GameState->Entities[EntityIndex].TilesLocations[j].x < GameState->PlacingTileCoord.X * NumOfPlacingTiles + NumOfPlacingTiles) &&
                       (GameState->Entities[EntityIndex].TilesLocations[j].y >= GameState->PlacingTileCoord.Z * NumOfPlacingTiles &&
                        GameState->Entities[EntityIndex].TilesLocations[j].y < GameState->PlacingTileCoord.Z * NumOfPlacingTiles + NumOfPlacingTiles))
                    {
                        Found = true;
                    }
                }
            }
        }
        // Note: limiting the player to place buildings until 3 tiles in placing coords
        // TODO(Jonathan): When going to negative tiles it's not working properly!!!!!
        v2 MaxTile = GameState->Player->TilesLocations[GameState->Player->TilesLocationsCount - 1];
        if(!Found &&
           (MaxTile.x / NumOfPlacingTiles + 3 <  GameState->PlacingTileCoord.X ||
            MaxTile.y / NumOfPlacingTiles + 3 <  GameState->PlacingTileCoord.Z))
        {
            Found = true;
        }
        
        // If the player can't build there do these
        if(Found)
        {
            GameState->CanBuild = false;
            Entity.Color = V4(1.0f, 0.0f, 0.0f, 0.5f);
        }
        else
        {
            GameState->CanBuild = true;
            Entity.Color = V4(0.0f, 0.0f, 0.0f, 0.5f);
        }
        
    }
}

static void UpdateAndRenderPlacingBuilding(entity& Entity)
{
    if(GameState->BuildMode)
    {
        Entity.Position = NumOfPlacingTiles * V3(GameState->PlacingTileCoord.RelWorld.x  + TileDimension / 2,
                                                 0.0f, GameState->PlacingTileCoord.RelWorld.z  + TileDimension / 2);
        
        Entity.Position.y = GetWorldHeight(Entity.Position.x,
                                           Entity.Position.z);
        
        if(GameState->BuildingType != BuildingType_None)
        {
            if(Entity.Rotate)
            {
                Entity.Angle += 90.0f;
                if(Entity.Angle >= 360)
                {
                    Entity.Angle = 0;
                }
                Entity.Rotate = false;
                
                // TODO(Jonathan): Rotate according to where the player is looking
            }
            
            // Drawing the placing building
            UpdateAndRenderBuildingType(*GameState->PlacingBuilding, (building_type)GameState->BuildingType);
        }
        
        CheckCollisionPlacingBuilding(Entity);
    }
}
\
// TODO(Jonathan): Maybe change place
static void AddBuildingByPlayer(entity& Player, building_type BuildingType)
{
    if(GameState->CanBuild)
    {
        recipe_index RecipeIndex = GetRecipeIndexByInventoryItemType(GetInventoryTypeByBuildingType(BuildingType));
        recipe* Recipe = &Player.Inventory.Recipes[RecipeIndex];
        
        if(IsRecipeCraftable(Player.Inventory, *Recipe))
        {
            CraftRecipe(Player.Inventory, *Recipe);
            AddBuilding(GameState->PlacingBuilding->BuildingType,
                        GameState->PlacingBuilding->Position,
                        GameState->PlacingBuilding->Angle);
            
            if(Recipe->CraftedItem.Type == InventoryItemType_CampFire)
            {
                GameState->DrawCircle = true;
                GameState->CirclePos = GameState->PlacingBuilding->Position;
                GameState->CirclePos.y += 0.2f;
            }
            ModifyInventoryItem(Player.Inventory, GetInventoryTypeByBuildingType(BuildingType), -1);
        }
    }
}

static void ChooseBuildingWithMouseWheel(s32 EventY)
{
    if(GameState->BuildMode)
    {
        // TODO: Implement this instead because Event->y can be more than 1 or less than -1.
        // GameState->BuildingType += Event->y;
        
        if(EventY == 1) // scroll up
        {
            if((++GameState->BuildingType) > BuildingType_NumOfBuildings - 1)
            {
                GameState->BuildingType = BuildingType_None;
            }
        }
        else if(EventY == -1) // scroll down
        {
            if((--GameState->BuildingType) < BuildingType_None)
            {
                GameState->BuildingType = BuildingType_NumOfBuildings - 1;
            }
        }
        
        if(GameState->PlacingBuilding->BuildingType != GameState->BuildingType)
        {
            GameState->PlacingBuilding->BuildingType = (building_type)GameState->BuildingType;
            GameState->PlacingBuilding->Angle = 0;
            GameState->PlacingBuilding->Rotate = false;
        }
        
    }
}