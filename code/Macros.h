#pragma once

#define Kilobyte(Value) ((Value)*1024LL)
#define Megabyte(Value) (Kilobyte(Value)*1024LL)
#define Gigabyte(Value) (Megabyte(Value)*1024LL)
#define Terabyte(Value) (Gigabyte(Value)*1024LL)

#define Minimum(A, B) ((A < B) ? (A) : (B))
#define Maximum(A, B) ((A > B) ? (A) : (B))

#define ToVoid(X) reinterpret_cast<void*>(##X)
#define ToReal32(X) static_cast<real32>(##X)
#define ToInt32(X) static_cast<int32>(##X)

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

// TODO(casey): Complete assertion macro - don't worry everyone!
#ifdef NAYO_DEBUG
#define Assert(Expression) if(!(Expression)) {*(int *)0 = 0;}
#else
#define Assert(E) // TODO(Naor): We can display user friendly message here instead
#endif