#include "Renderer.h"

static void
PushRender(render_group& RenderGroup, const material& Material, mesh* Mesh, u32 Texture,
           render_draw_type DrawType, bool Element, u32 Instances, render_draw_mode DrawMode)
{
    render_instance& Render = RenderGroup.Renders[RenderGroup.Count++];
    Render.Material = Material;
    Render.DrawType = DrawType;
    Render.DrawMode = DrawMode;
    Render.Mesh = Mesh;
    Render.Texture = Texture;
    Render.Instances = Instances;
}

void SetUniform(material& Material, const uniform& NewUniform)
{
    Assert(Material.UniformsCount + 1 < UNIFORMS_SIZE);
    uniform& Uniform = Material.Uniforms[Material.UniformsCount++];
    Uniform = NewUniform;
}

void SetBool(material& Material, char* Name, bool Value, shader_uniform_index Index)
{
    uniform Uniform;
    
    Uniform.Name = Name;
    Uniform.Index = Index;
    Uniform.Value.Type = ShaderUniformType_Bool;
    Uniform.Value.Bool = Value;
    
    SetUniform(Material, Uniform);
}

void SetInt(material& Material, char* Name, s32 Value, shader_uniform_index Index)
{
    uniform Uniform;
    
    Uniform.Name = Name;
    Uniform.Index = Index;
    Uniform.Value.Type = ShaderUniformType_Int;
    Uniform.Value.Int = Value;
    
    SetUniform(Material, Uniform);
}

void SetFloat(material& Material, char* Name, r32 Value, shader_uniform_index Index)
{
    uniform Uniform;
    
    Uniform.Name = Name;
    Uniform.Index = Index;
    Uniform.Value.Type = ShaderUniformType_Float;
    Uniform.Value.Float = Value;
    
    SetUniform(Material, Uniform);
}

void SetVector(material& Material, char* Name, const v2& Value, shader_uniform_index Index)
{
    uniform Uniform;
    
    Uniform.Name = Name;
    Uniform.Index = Index;
    Uniform.Value.Type = ShaderUniformType_Vec2;
    Uniform.Value.Vec2 = Value;
    
    SetUniform(Material, Uniform);
}

void SetVector(material& Material, char* Name, const v3& Value, shader_uniform_index Index)
{
    uniform Uniform;
    
    Uniform.Name = Name;
    Uniform.Index = Index;
    Uniform.Value.Type = ShaderUniformType_Vec3;
    Uniform.Value.Vec3 = Value;
    
    SetUniform(Material, Uniform);
}

void SetVector(material& Material, char* Name, const v4& Value, shader_uniform_index Index)
{
    uniform Uniform;
    
    Uniform.Name = Name;
    Uniform.Index = Index;
    Uniform.Value.Type = ShaderUniformType_Vec4;
    Uniform.Value.Vec4 = Value;
    
    SetUniform(Material, Uniform);
}

void SetMatrix(material& Material, char* Name, const m2& Value, shader_uniform_index Index)
{
    uniform Uniform;
    
    Uniform.Name = Name;
    Uniform.Index = Index;
    Uniform.Value.Type = ShaderUniformType_Mat2;
    Uniform.Value.Mat2 = Value;
    
    SetUniform(Material, Uniform);
}

void SetMatrix(material& Material, char* Name, const m3& Value, shader_uniform_index Index)
{
    uniform Uniform;
    
    Uniform.Name = Name;
    Uniform.Index = Index;
    Uniform.Value.Type = ShaderUniformType_Mat3;
    Uniform.Value.Mat3 = Value;
    
    SetUniform(Material, Uniform);
}

void SetMatrix(material& Material, char* Name, const m4& Value, shader_uniform_index Index)
{
    uniform Uniform;
    
    Uniform.Name = Name;
    Uniform.Index = Index;
    Uniform.Value.Type = ShaderUniformType_Mat4;
    Uniform.Value.Mat4 = Value;
    
    SetUniform(Material, Uniform);
}

static void RenderScene(render_group& RenderGroup)
{
    for(u32 Index = 0; Index < RenderGroup.Count; ++Index)
    {
        render_instance& Render = RenderGroup.Renders[Index];
        
        // TODO(Jonathan): One use for each shader and for the global uniforms
        //if(Index ==  0 || (GameState->RenderGroup.Renders[Index - 1].Material.Shader->Id != Render.Material.Shader->Id
        //&& Index > 0))
        //{
        shader_program& Shader = *Render.Material.Shader;
        Shader.Use();
        
        
        for (u32 UniformIndex = 0; UniformIndex < Render.Material.UniformsCount; ++UniformIndex)
        {
            uniform& Uniform = Render.Material.Uniforms[UniformIndex];
            switch (Uniform.Value.Type)
            {
                case ShaderUniformType_Bool:
                {
                    glUniform1i(Shader.Uniforms[Render.Material.Uniforms[UniformIndex].Index], Uniform.Value.Bool);
                }
                break;
                case ShaderUniformType_Int:
                {
                    glUniform1i(Shader.Uniforms[Render.Material.Uniforms[UniformIndex].Index], Uniform.Value.Int);
                }
                break;
                case ShaderUniformType_Float:
                {
                    glUniform1f(Shader.Uniforms[Render.Material.Uniforms[UniformIndex].Index], Uniform.Value.Float);
                }
                break;
                case ShaderUniformType_Vec2:
                {
                    glUniform2fv(Shader.Uniforms[Render.Material.Uniforms[UniformIndex].Index], 1, Uniform.Value.Vec2.Data());
                }
                break;
                case ShaderUniformType_Vec3:
                {
                    glUniform3fv(Shader.Uniforms[Render.Material.Uniforms[UniformIndex].Index], 1, Uniform.Value.Vec3.Data());
                }
                break;
                case ShaderUniformType_Vec4:
                {
                    glUniform4fv(Shader.Uniforms[Render.Material.Uniforms[UniformIndex].Index], 1, Uniform.Value.Vec4.Data());
                }
                break;
                case ShaderUniformType_Mat2:
                {
                    glUniformMatrix2fv(Shader.Uniforms[Render.Material.Uniforms[UniformIndex].Index], 1,
                                       GL_FALSE, Uniform.Value.Mat2.Data());
                }
                break;
                case ShaderUniformType_Mat3:
                {
                    glUniformMatrix3fv(Shader.Uniforms[Render.Material.Uniforms[UniformIndex].Index], 1,
                                       GL_FALSE, Uniform.Value.Mat3.Data());
                }
                break;
                case ShaderUniformType_Mat4:
                {
                    glUniformMatrix4fv(Shader.Uniforms[Render.Material.Uniforms[UniformIndex].Index], 1,
                                       GL_FALSE, Uniform.Value.Mat4.Data());
                }
                break;
                default:
                {
                    Assert(!"Unrecognized Uniform type set.");
                }
                break;
            }
            //}
            
        }
        
        if(Render.Texture != GL_TEXTURE_NULL_ID)
        {
            glBindTexture(GL_TEXTURE_2D, Render.Texture);
        }
        
        glBindVertexArray(Render.Mesh->Vao);
        switch(Render.DrawType)
        {
            case RenderDrawType_Arrays:
            {
                glDrawArrays(Render.DrawMode, 0, Render.Mesh->Count);
            }
            break;
            case RenderDrawType_Elements:
            {
                glDrawElements(Render.DrawMode, Render.Mesh->Count, GL_UNSIGNED_INT, nullptr);
            }
            break;
            case RenderDrawType_ArraysInstanced:
            {
                glDrawArraysInstanced(Render.DrawMode, 0, Render.Mesh->Count, Render.Instances);
            }
            break;
        }
        glBindVertexArray(0);
    }
}