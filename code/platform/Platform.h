#pragma once
// TODO(Naor): When we get here, move all the OS stuff to here (see handmade hero @handmade_platform.h near the bottom)
struct platform_memory_block
{
    u8* Base;
    // u64 Flags; // See handmade_platform.h @674
    u64 Size;
    uptr Used;
    platform_memory_block* ArenaPrev;
    platform_memory_block* ArenaNext;
};

// Platform stuff
#define PLATFORM_ALLOCATE_MEMORY(name) platform_memory_block* name(size_t Size)
typedef PLATFORM_ALLOCATE_MEMORY(platform_allocate_memory);

// TODO(Naor): Search where, when and how casey use this!
#define PLATFORM_DEALLOCATE_MEMORY(name) void name(platform_memory_block* Block)
typedef PLATFORM_DEALLOCATE_MEMORY(platform_deallocate_memory);

struct platform_api
{
    uptr MemoryBaseOffset = 0;
    platform_allocate_memory* AllocateMemory;
    platform_deallocate_memory* DeallocateMemory;
};
// TODO(Naor): Read more about extern and why it is needed here
extern platform_api Platform;

struct scoped_memory_block
{
    platform_memory_block* Memory;
    
    scoped_memory_block(size_t SizeInit)
    {
        Memory = Platform.AllocateMemory(SizeInit);
    }
    
    ~scoped_memory_block()
    {
        Platform.DeallocateMemory(Memory);
    }
    
    // Just for convenience (Block.Base() instead of Block.Memory->Base)
    inline u8* Base()
    {
        u8* Result = Memory->Base;
        return Result;
    }
};

#define DEBUG_MAX_ENTRIES 1024
#define DEBUG_MAX_STRING_LENGTH 64
struct debug_entry
{
    u64 CycleCount;
    char FileName[DEBUG_MAX_STRING_LENGTH];
    char FunctionName[DEBUG_MAX_STRING_LENGTH];
    
    u32 LineNumber;
    u32 HitCount;
};

struct debug_entry_marker
{
    debug_entry Entries[DEBUG_MAX_ENTRIES];
    u64 TotalCycles = 0;
};

#define DEBUG_MAX_MARKERS 256
struct debug_profiler
{
    debug_entry_marker Markers[DEBUG_MAX_MARKERS];
    debug_entry SortedEntries[DEBUG_MAX_ENTRIES];
    debug_entry_marker SelectedMarker;
    u64 MaxTotalCycles = 0;
    u32 CurrentEntryMarker = 0;
    u32 EntriesCount = 0;
    s32 SelectedMarkerIndex = -1;
    bool IsPaused = false;
};

enum debug_log_level
{
    DebugLogLevel_Info = 0x1,
    DebugLogLevel_Warning = 0x2,
    DebugLogLevel_Error = 0x4,
    DebugLogLevel_Critical = 0x8
};

#define LOG_MAX_LENGTH 512
#define LOG_MAX_FILTER_LENGTH 256
#define LOG_MAX_NAME_LENGTH 64
struct debug_log
{
    char Buffer[LOG_MAX_LENGTH];
    char FunctionName[LOG_MAX_NAME_LENGTH];
    char FileName[LOG_MAX_NAME_LENGTH];
    char Time[16];
    ImVec4 Color;
    debug_log_level Level;
    s32 Count = 1;
    s32 Line;
};

#include "array.h"
struct debug_logger
{
    char QuoteOfTheDay[512];
    char Filter[LOG_MAX_FILTER_LENGTH];
    dynamic_array<debug_log> Log;
    bool ScrollToBottom = true;
    u32 FilterFlag = 0xF;
};

// NOTE(Naor): When adding/removing variables here, you need to
// restart the game! we allocate the memory in main so we wont have any room
// for additional space. (Theoretically, we can allocate more memory than we need
// and when adding variables they SHOULD work... without any problems, considering
// that they wont go out of the memory block)
struct debug_state
{
    debug_profiler Profiler;
    debug_logger Logger;
    
    ImGuiContext* Context = 0;
    
    // Start imgui info stuff
    u64 Time = 0;
    bool MousePressed[3] = { false, false, false };
    u32 FontTexture = 0;
    
    s32 ShaderHandle = 0;
    s32 VertHandle = 0;
    s32 FragHandle = 0;
    
    s32 AttribLocationTex = 0;
    s32 AttribLocationProjMtx = 0;
    s32 AttribLocationPosition = 0;
    s32 AttribLocationUV = 0;
    s32 AttribLocationColor = 0;
    
    u32 VboHandle = 0;
    u32 VaoHandle = 0;
    u32 ElementsHandle = 0;
    
    SDL_Cursor* SdlCursors[ImGuiMouseCursor_Count_] = { 0 };
    bool IsCursorActive = false; // True if ImGui will get the input.
    
    bool IsAnyWindowDisplayed = false;
    
    ImFont* FontCode;
    ImFont* FontBold;
    ImFont* FontRegular;
    // End imgui info stuff
    
    // NOTE(Naor): From here downward, you can add anything you want for the debugging.
    bool DisplayProfiler = false;
    bool DisplayLogger = false;
    
    v2 DrawableSize;
    v2 WindowSize;
};
extern debug_state* DebugState;

struct game_memory
{
    void* MemoryBlock;
    size_t SizeInBytes;
    bool IsInitialized;
    
    platform_api PlatformAPI;
    SDL_Window* Window;
#ifdef NAYO_DEBUG
    debug_state* DebugStateAPI;
#endif
};

// Game stuff
#define GAME_UPDATE_AND_RENDER(name) void name(r32 Delta, game_memory& GameMemory, std::queue<SDL_Event> GameInput, const u8* GameInputContinuous)
typedef GAME_UPDATE_AND_RENDER(game_update_and_render);

#define GAME_INIT(name) void name(const gl_version& OpenGLVersion, game_memory& GameMemory)
typedef GAME_INIT(game_init);

#define GAME_INPUT_WHILE_PLAYBACK(name) void name(game_memory& GameMemory, std::queue<SDL_Event> GameInput, const u8* GameInputContinuous, bool First)
typedef GAME_INPUT_WHILE_PLAYBACK(game_input_while_playback);

#define GAME_DEBUG_RENDER(name) void name(r32 Delta)
typedef GAME_DEBUG_RENDER(game_debug_render);

// SDL Platform stuff

// TODO(Naor): There is a bug in the fullscreen in window mode, 
// we want the user to be able to interact with other windows outside the
// game without the game minimizing, try to figure out how we can achieve that.
// TODO(Naor): Change resolution to fit every monitor (for now, only full resolution)
// NOTE(Naor): SDL_WINDOW_FULLSCREEN_DESKTOP cant change the resolution, it uses the
// max resolution of the monitor, so in the option menu dont allow the player
// to change the resolution in that mode, only in window mode + fullscreen mode.
static void SDLToggleFullscreen(SDL_Window* Window)
{
    u32 Flags = SDL_GetWindowFlags(Window);
    
    if(Flags & SDL_WINDOW_FULLSCREEN_DESKTOP)
    {
        SDL_SetWindowFullscreen(Window, 0);
    }
    else
    {
        SDL_SetWindowFullscreen(Window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    }
    
    s32 DrawableWidth;
    s32 DrawableHeight;
    SDL_GL_GetDrawableSize(Window, &DrawableWidth, &DrawableHeight);
    
    glViewport(0, 0, DrawableWidth, DrawableHeight);
}
