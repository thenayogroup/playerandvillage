#include "ShaderProgram.h"

inline void shader_program::Use()
{
    glUseProgram(Id);
}

void shader_program::Delete()
{
    if(Id != 0)
    {
        glDeleteProgram(Id);
        Id = 0;
    }
}

void shader_program::Compile()
{
    Assert(Id == 0);
    Id = glCreateProgram();
    
    for(s32 ShaderIndex = 0;
        ShaderIndex < NUM_OF_SHADER_TYPE_COUNT;
        ++ShaderIndex)
    {
        if(Shaders[ShaderIndex] != 0)
        {
            glAttachShader(Id, Shaders[ShaderIndex]);
        }
    }
    
    glLinkProgram(Id);
    
#ifdef NAYO_DEBUG
    
    GLint IsLinked = 0;
    glGetProgramiv(Id, GL_LINK_STATUS, &IsLinked);
    if (IsLinked == GL_FALSE)
    {
        GLint MaxLength = 0;
        glGetProgramiv(Id, GL_INFO_LOG_LENGTH, &MaxLength);
        
        // The MaxLength includes the NULL character
        std::vector<char> InfoLog(MaxLength);
        glGetProgramInfoLog(Id, MaxLength, &MaxLength, &InfoLog[0]);
        
        // We don't need the Id anymore. (link failed)
        glDeleteProgram(Id);
        
        // Don't leak shaders either.
        for(s32 ShaderIndex = 0;
            ShaderIndex < NUM_OF_SHADER_TYPE_COUNT;
            ++ShaderIndex)
        {
            if(Shaders[ShaderIndex] != 0)
            {
                glDeleteShader(Shaders[ShaderIndex]);
            }
        }
        std::string ConvertedErrorLog(InfoLog.begin(), InfoLog.end());
        LogError("Couldn't compile the shader program with the following error: %s",ConvertedErrorLog.c_str());
        
        return;
    }
#endif
    
    for(s32 ShaderIndex = 0;
        ShaderIndex < NUM_OF_SHADER_TYPE_COUNT;
        ++ShaderIndex)
    {
        if(Shaders[ShaderIndex] != 0)
        {
            glDetachShader(Id, Shaders[ShaderIndex]);
            glDeleteShader(Shaders[ShaderIndex]);
        }
        Shaders[ShaderIndex] = 0; // Clear
    }
    
    for(s32 ShaderFileIndex = 0;
        ShaderFileIndex < NUM_OF_SHADER_TYPE_COUNT;
        ++ShaderFileIndex)
    {
        if(ShaderFiles[ShaderFileIndex].Exists)
            ShaderFiles[ShaderFileIndex].LastWriteTime = Utility::GetLastWriteTime(ShaderFiles[ShaderFileIndex].Filename);
    }
    
    // Getting all the uniforms
    for(s32 UniformIndex  = 0; 
        UniformIndex < ArrayCount(Uniforms); 
        UniformIndex++)
    {
        const char* UniformName = AssetsLookup::Uniforms[UniformIndex];
        Uniforms[UniformIndex] = glGetUniformLocation(Id, UniformName);
    }
    
}


// TODO: This is a copy paste of the other (AddShader) function, there is duplications here, fix it.
// TODO(Naor): !!!!!!!!THIS WILL NOT WORK WITH THE RECOMILER FEATURE!!!!!!!!!!!!
void shader_program::AddShaderBySource(type ShaderType, const char* ShaderSource)
{
    u32 NewShader = glCreateShader(static_cast<GLenum>(ShaderType));
    glShaderSource(NewShader, 1, &ShaderSource, nullptr);
    glCompileShader(NewShader);
    
#ifdef NAYO_DEBUG
    s32 IsCompiled = 0;
    glGetShaderiv(NewShader, GL_COMPILE_STATUS, &IsCompiled);
    if (IsCompiled == GL_FALSE)
    {
        s32 MaxLength = 0;
        glGetShaderiv(NewShader, GL_INFO_LOG_LENGTH, &MaxLength);
        
        // The maxLength includes the NULL character
        std::vector<char> ErrorLog(MaxLength);
        glGetShaderInfoLog(NewShader, MaxLength, &MaxLength, &ErrorLog[0]);
        
        std::string ConvertedErrorLog(ErrorLog.begin(), ErrorLog.end());
        LogError("The shader couldn't compile with the following error: %s", ConvertedErrorLog.c_str());
        
        glDeleteShader(NewShader); // Don't leak the shader.
        return;
    }
#endif
    Shaders[GetTypeIndex(ShaderType)] = NewShader;
}

void shader_program::AddShader(type ShaderType, const char* ShaderPath)
{
    u32 NewShader = glCreateShader(static_cast<GLenum>(ShaderType));
    std::string ShaderSource = GetShaderSource(ShaderPath);
    const char *ShaderSourcePointer = ShaderSource.c_str();
    glShaderSource(NewShader, 1, &ShaderSourcePointer, nullptr);
    glCompileShader(NewShader);
    
#ifdef NAYO_DEBUG
    s32 IsCompiled = 0;
    glGetShaderiv(NewShader, GL_COMPILE_STATUS, &IsCompiled);
    if (IsCompiled == GL_FALSE)
    {
        
        s32 MaxLength = 0;
        glGetShaderiv(NewShader, GL_INFO_LOG_LENGTH, &MaxLength);
        
        // The maxLength includes the NULL character
        std::vector<char> ErrorLog(MaxLength);
        glGetShaderInfoLog(NewShader, MaxLength, &MaxLength, &ErrorLog[0]);
        
        std::string ConvertedErrorLog(ErrorLog.begin(), ErrorLog.end());
        LogError("The shader %s couldn't compile with the following error: %s", ShaderPath, ConvertedErrorLog.c_str());
        
        glDeleteShader(NewShader); // Don't leak the shader.
        return;
    }
#endif
    
    type_index ShaderTypeIndex = GetTypeIndex(ShaderType);
    
    Shaders[ShaderTypeIndex] = NewShader;
    
    ShaderFiles[ShaderTypeIndex].Type = ShaderType;
    if(ShaderFiles[ShaderTypeIndex].Exists && ShaderFiles[ShaderTypeIndex].Filename != nullptr)
    {
        free(ShaderFiles[ShaderTypeIndex].Filename);
    }
    
    ShaderFiles[ShaderTypeIndex].Filename = (char*)malloc(sizeof(char) * (Utility::CStringLength(ShaderPath) + 1));
    Utility::CCopyString(ShaderFiles[ShaderTypeIndex].Filename, ShaderPath);
    ShaderFiles[ShaderTypeIndex].LastWriteTime = Utility::GetLastWriteTime(ShaderPath);
}

// TODO(Naor): Change the implementation to remove std
std::string shader_program::GetShaderSource(const char* ShaderPath) const
{
    std::string SourceCode;
    std::stringstream SourceStream;
    std::ifstream FileHandle(ShaderPath);
    
    while(!FileHandle.is_open())
    {
        FileHandle.open(ShaderPath);
    }
    
    SourceStream << FileHandle.rdbuf();
    SourceCode = SourceStream.str();
    FileHandle.close();
    
    return SourceCode;
}

void shader_program::Recompile()
{
    Delete();
    
    for(s32 ShaderFileIndex = 0;
        ShaderFileIndex < NUM_OF_SHADER_TYPE_COUNT;
        ++ShaderFileIndex)
    {
        if(ShaderFiles[ShaderFileIndex].Exists)
        {
            if(ShaderFiles[ShaderFileIndex].Type == shader_program::type::Multiple)
            {
                ParseAndCompileShader(*this, ShaderFiles[ShaderFileIndex].Filename);
                return;
            }
            else
            {
                AddShader(ShaderFiles[ShaderFileIndex].Type, ShaderFiles[ShaderFileIndex].Filename);
            }
        }
    }
    
    Compile();
}

void shader_program::UpdateIfModified()
{
    for(s32 ShaderFileIndex = 0;
        ShaderFileIndex < NUM_OF_SHADER_TYPE_COUNT;
        ++ShaderFileIndex)
    {
        if(ShaderFiles[ShaderFileIndex].Exists)
        {
            FILETIME NewWriteTime = Utility::GetLastWriteTime(ShaderFiles[ShaderFileIndex].Filename);
            if(CompareFileTime(&(ShaderFiles[ShaderFileIndex].LastWriteTime), &NewWriteTime) != 0)
            {
                Recompile();
                return;
            }
        }
    }
}

inline shader_program::type_index shader_program::GetTypeIndex(type ShaderType)
{
    type_index ShaderIndex = NUM_OF_SHADER_TYPE_COUNT;
    switch(ShaderType)
    {
        case Vertex:
        {
            ShaderIndex = Vertex_Index;
        }break;
        case Fragment:
        {
            ShaderIndex = Fragment_Index;
        }break;
        case Geometry:
        {
            ShaderIndex = Geometry_Index;
        }break;
        case Compute:
        {
            ShaderIndex = Compute_Index;
        }break;
        case TessControl:
        {
            ShaderIndex = TessControl_Index;
        }break;
        case TessEvaluation:
        {
            ShaderIndex = TessEvaluation_Index;
        }break;
    }
    Assert(ShaderIndex != NUM_OF_SHADER_TYPE_COUNT);
    
    return ShaderIndex;
}

void shader_program::SetSingleFileShader(const char* Filename)
{
    // Clearing all files
    for(s32 ShaderFileIndex = 0;
        ShaderFileIndex < NUM_OF_SHADER_TYPE_COUNT;
        ++ShaderFileIndex)
    {
        ShaderFiles[ShaderFileIndex] = {};
    }
    
    // We only want to set the first index because we only use one file
    ShaderFiles[0].Exists = 1;
    ShaderFiles[0].Type = shader_program::type::Multiple;
    ShaderFiles[0].LastWriteTime = Utility::GetLastWriteTime(Filename);;
    ShaderFiles[0].Filename = (char*)malloc(sizeof(char) * (Utility::CStringLength(Filename) + 1));
    Utility::CCopyString(ShaderFiles[0].Filename, Filename);
}