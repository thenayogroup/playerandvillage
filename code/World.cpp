#include "World.h"

static void _CreateNormals(dynamic_array<vertex_pn>& Vertices, const dynamic_array<u32>& Indices)
{
    s32 IndicesSize = ToInt32(Indices.Count);
    for(s32 Index = 0; Index < IndicesSize - 2; Index+=3)
    {
        vertex_pn& V = Vertices[Indices[Index]];
        V.Normal = Math::CalculateNormal(V.Position, 
                                         Vertices[Indices[Index+1]].Position, 
                                         Vertices[Indices[Index+2]].Position);
    }
}

static void CreateWorld(world& W)
{
#ifdef NAYO_DEBUG
    if(W.Mesh.Vbo != 0)
    {
        glDeleteBuffers(1, &W.Mesh.Vbo);
        glDeleteBuffers(1, &W.Mesh.Ebo);
        glDeleteVertexArrays(1, &W.Mesh.Vao);
    }
    
#endif
    
    dynamic_array<vertex_pn> Vertices;
    dynamic_array<u32> Indices;
    
    Reserve(&Vertices, ToInt32(Math::Square(WORLD_RADIUS*2.0f)) * 6);
    Reserve(&Indices, WORLD_RADIUS*2*2*(WORLD_RADIUS*2-1) + ((WORLD_RADIUS*2+1))*2);
    
    u32 IndexCount = 0;
    
    u32 BottomIndex = WORLD_RADIUS*2*2*(WORLD_RADIUS*2-1);
    u32 SecondBottomIndex = BottomIndex;
    
    W.MinHeight = 10000.0f;
    W.MaxHeight = -10000.0f;
    
    // NOTE(Naor): The random needs to be between -+(floor(Half TerrainScale))
    v3 LastOffset = V3(Math::Clamp(Math::Random<-1,1>(), -0.5f, 0.5f),
                       0.0f,
                       Math::Clamp(Math::Random<-1,1>(), -0.5f, 0.5f));
    for(s32 Z = -WORLD_RADIUS; Z < WORLD_RADIUS+1; ++Z)
    {
        for(s32 X = -WORLD_RADIUS; X < WORLD_RADIUS; ++X)
        {
            const v3 TerrainScale = V3(TERRAIN_SCALE, 1.0f, TERRAIN_SCALE);
            
            v3 Vertex = V3(X+LastOffset.x, 0.0f, Z+LastOffset.z) * TerrainScale;
            Vertex.y = GetWorldHeight(Vertex.x, Vertex.z);
            
            if(Vertex.y > W.MaxHeight)
                W.MaxHeight = Vertex.y;
            if(Vertex.y < W.MinHeight)
                W.MinHeight = Vertex.y;
            
            Vertices.PushBack(vertex_pn(Vertex, V3(1.0f)));
            
            // NOTE(Naor): This section takes MOST of the provoking vertex and we need to change the color for 
            // each triangle.
            if(Z < WORLD_RADIUS-1 || (X == WORLD_RADIUS-1 && Z >= WORLD_RADIUS-1))
            {
                // TODO(Naor): Add Clamp to Vector
                LastOffset = V3(Math::Clamp(Math::Random<-1,1>(), -0.2f, 0.2f),
                                0.0f,
                                Math::Clamp(Math::Random<-1,1>(), -0.2f, 0.2f));
                Vertex = V3(X+1.0f+LastOffset.x, 0.0f, Z+LastOffset.z) * TerrainScale;
                Vertex.y = GetWorldHeight(Vertex.x, Vertex.z);
                
                if(Vertex.y > W.MaxHeight)
                    W.MaxHeight = Vertex.y;
                if(Vertex.y < W.MinHeight)
                    W.MinHeight = Vertex.y;
                
                Vertices.PushBack(vertex_pn(Vertex, V3(1.0f)));
            }
            
            if(Z < WORLD_RADIUS-2)
            {
                const u32 Below = WORLD_RADIUS*2*2+IndexCount;
                if((X+WORLD_RADIUS) % 2 == 0)
                {
                    Indices.PushBack(IndexCount);
                    Indices.PushBack(Below);
                    Indices.PushBack(Below+1);
                    
                    Indices.PushBack(IndexCount+1);
                    Indices.PushBack(IndexCount);
                    Indices.PushBack(Below+1);
                }
                else
                {
                    Indices.PushBack(IndexCount);
                    Indices.PushBack(Below);
                    Indices.PushBack(IndexCount+1);
                    
                    Indices.PushBack(IndexCount+1);
                    Indices.PushBack(Below);
                    Indices.PushBack(Below+1);
                }
                IndexCount+=2;
            }
            
            else if(Z < WORLD_RADIUS-1)
            {
                const u32 Below = SecondBottomIndex;
                if((X+WORLD_RADIUS) % 2 == 0)
                {
                    Indices.PushBack(IndexCount);
                    Indices.PushBack(Below);
                    Indices.PushBack(Below+1);
                    
                    Indices.PushBack(IndexCount+1);
                    Indices.PushBack(IndexCount);
                    Indices.PushBack(Below+1);
                }
                else
                {
                    Indices.PushBack(IndexCount);
                    Indices.PushBack(Below);
                    Indices.PushBack(IndexCount+1);
                    
                    Indices.PushBack(IndexCount+1);
                    Indices.PushBack(Below);
                    Indices.PushBack(Below+1);
                }
                IndexCount+=2;
                SecondBottomIndex++;
            }
            else if(Z < WORLD_RADIUS)
            {
                const u32 Below = BottomIndex + WORLD_RADIUS*2+1;
                if((X+WORLD_RADIUS) % 2 == 0)
                {
                    Indices.PushBack(BottomIndex);
                    Indices.PushBack(Below);
                    Indices.PushBack(BottomIndex+1);
                    
                    Indices.PushBack(BottomIndex+1);
                    Indices.PushBack(Below);
                    Indices.PushBack(Below+1);
                }
                else
                {
                    Indices.PushBack(Below);
                    Indices.PushBack(Below+1);
                    Indices.PushBack(BottomIndex);
                    
                    Indices.PushBack(Below+1);
                    Indices.PushBack(BottomIndex+1);
                    Indices.PushBack(BottomIndex);
                }
                BottomIndex++;
            }
        }
    }
    W.Mesh.Count = ToInt32(Indices.Count);
    
    _CreateNormals(Vertices, Indices);
    
    glGenVertexArrays(1, &W.Mesh.Vao);
    glGenBuffers(1, &W.Mesh.Vbo);
    glGenBuffers(1, &W.Mesh.Ebo);
    
    glBindVertexArray(W.Mesh.Vao);
    glBindBuffer(GL_ARRAY_BUFFER, W.Mesh.Vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, W.Mesh.Ebo);
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_pn) * Vertices.Count, Vertices.Data(), GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(u32) * W.Mesh.Count, Indices.Data(), GL_STATIC_DRAW);
    
    glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(vertex_pn), nullptr);
    glVertexAttribPointer(1, 3, GL_FLOAT, false, sizeof(vertex_pn), reinterpret_cast<void*>(sizeof(v3)));
    
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    // TODO(Naor): Remove this and insert the renderer here instead
    FreeDynamicArray(&Vertices);
    FreeDynamicArray(&Indices);
}

static void UpdateAndRenderWorld(r32 Delta, world& W)
{
    TIMED_FUNCTION();
    
    material Material = {};
    Material.Shader = &GameState->Assets.Shader_World;
    SetMatrix(Material, "ProjectionView", GameState->MVP, SHADER_UNIFORM_VP_INDEX);
    SetFloat(Material, "Time", GameState->CurrentTime, SHADER_UNIFORM_TIME_INDEX);
    // TODO(Naor): Move this to the creation of the world, this should only
    // be initialized once.
    SetFloat(Material, "MinHeight", W.MinHeight, SHADER_UNIFORM_MIN_HEIGHT_INDEX);
    SetFloat(Material, "MaxHeight", W.MaxHeight, SHADER_UNIFORM_MAX_HEIGHT_INDEX);
    
    PushRender(GameState->RenderGroup, Material, &W.Mesh, GameState->Assets.Textures[TEXTURE_GRASS_INDEX], RenderDrawType_Elements, true);
}

static void InitEnv()
{
    for(s32 i = 0; i < 300; ++i)
    {
        v2 XZPos = Math::RandomVec2<-WORLD_RADIUS, WORLD_RADIUS>() * TERRAIN_SCALE;
        r32 YPos = GetWorldHeight(XZPos.x, XZPos.y);
        
        for(u32 j = 0; j < GameState->EntitiesCount; ++j)
        {
            entity& Entity = GameState->Entities[j];
            if(Entity.Type == EntityType_Tree)
            {
                if(Math::Distance(V3(XZPos.x, YPos, XZPos.y), Entity.Position) < 1.0f)
                {
                    // Get a new location
                    XZPos = Math::RandomVec2<-WORLD_RADIUS, WORLD_RADIUS>() * TERRAIN_SCALE;
                    YPos = GetWorldHeight(XZPos.x, XZPos.y);
                    // next loop we get ++i so we want to make it -1.
                    j=0;
                }
            }
        }
        v3 Pos = V3(XZPos.x, YPos-0.2f, XZPos.y);
        AddEnvEntity(EntityType_Tree, Pos);
    }
    
    
    for(s32 i = -WORLD_RADIUS; i < WORLD_RADIUS;++i)
    {
        for(s32 j = -WORLD_RADIUS; j < WORLD_RADIUS;++j)
        {
            s32 X = Math::RandomInt32<1, 35>();
            s32 Y = Math::RandomInt32<1, 35>();
            if(j % X == 0 && i % Y == 0)
            {
                v3 Pos = V3(TERRAIN_SCALE * i, GetWorldHeight(i * TERRAIN_SCALE, j * TERRAIN_SCALE), TERRAIN_SCALE * j);
                AddEnvEntity(EntityType_Bush, Pos);
            }
        }
    }
    
    for(s32 i = -WORLD_RADIUS; i < WORLD_RADIUS;++i)
    {
        for(s32 j = -WORLD_RADIUS; j < WORLD_RADIUS;++j)
        { 
            s32 X = Math::RandomInt32<1, 20>();
            s32 Y = Math::RandomInt32<1, 20>();
            if(j % X == 0 && i % Y == 0)
            {
                v3 Pos = V3(TERRAIN_SCALE * i,
                            GetWorldHeight(i * TERRAIN_SCALE, j * TERRAIN_SCALE),
                            TERRAIN_SCALE * j);
                AddEnvEntity(EntityType_Rock, Pos);
                
            }
        }
    }
    
    
    for(u32 i = 0; i < 30; ++i)
    {
        r32 X = 100.0f + 30.0f * (i / 30.0f);
        r32 Z = 100.0f;//+ 30.0f * (i / 30.0f);
        v3 Pos = V3(X, GetWorldHeight(X, Z), Z);
        AddEnvEntity(EntityType_Steel, Pos);
    }
    
    
    for(u32 i = 0; i < 20; ++i)
    {
        r32 X = 0.0f + 30.0f * (i / 30.0f);
        r32 Z = 0.0f;//+ 30.0f * (i / 30.0f);
        v3 Pos = V3(X, GetWorldHeight(X, Z), Z);
        AddEnvEntity(EntityType_Flint, Pos);
    }
    
    
}

static void DestroyResource(entity& Entity)
{
    TIMED_FUNCTION();
    if(Entity.Type == EntityType_Tree || Entity.Type == EntityType_Rock)
    {
        Entity.Health -= GameState->Player->Power;
        LogInfo("Health: %f", Entity.Health);
        if(Entity.Health <= 0)
        {
            v3 Pos = Entity.Position;
            s32 Amount = Math::RandomInt32<1,5>();
            // NOTE(Jonathan): Temporary until we'll find better solution
            for(s32 i = 0; i < Amount;++i)
            {
                s32 Offset = Math::RandomInt32<0, 5>();
                Pos.x += (Offset / 5.0f);
                Pos.z += (Offset / 5.0f);
                Pos.y = GetWorldHeight(Pos.x, Pos.z) + 0.2f;
                
                if(Entity.Type == EntityType_Tree)
                    AddEnvEntity(EntityType_Wood, Pos);
                else if(Entity.Type == EntityType_Rock)
                    AddEnvEntity(EntityType_Stone, Pos);
            }
            
            RemoveEntity(Entity);
            ResetSelectedEntity(GameState->SelectedEnv);
        }
    }
}

static bool IsTypeCollectible(entity_type Type)
{
    return(Type == EntityType_Wood ||
           Type == EntityType_Stone ||
           Type == EntityType_Steel);
}

// TODO(Jonathan): We need to add every entity that is collectible to here,
// not so comfortable make something more easy to use like giving entity Collectible flag 
// TODO(Jonathan): Instead of checking each type, Maybe add some flag COLLECTABLE
static void CollectResource(entity& Entity)
{
    if(Entity.Type == EntityType_Wood ||
       Entity.Type == EntityType_Stone ||
       Entity.Type == EntityType_Steel || 
       Entity.Type == EntityType_Flint)
    {
        
        switch(Entity.Type)
        {
            case EntityType_Wood:
            {
                AddInventoryItem(GameState->Player->Inventory, InventoryItemType_Wood, InventoryItemState_Resource, WOOD_VALUE);
            }
            break;
            case EntityType_Stone:
            {
                AddInventoryItem(GameState->Player->Inventory, InventoryItemType_Stone, 
                                 InventoryItemState_Resource, STONE_VALUE);
            }
            break;
            case EntityType_Steel:
            {
                AddInventoryItem(GameState->Player->Inventory, InventoryItemType_Steel,
                                 InventoryItemState_Resource, STEEL_VALUE);
            }
            break;
            case EntityType_Flint:
            {
                AddInventoryItem(GameState->Player->Inventory, InventoryItemType_Flint, 
                                 InventoryItemState_Resource, 1);
            }
            break;
        }
        
        RemoveEntity(Entity);
    }
}