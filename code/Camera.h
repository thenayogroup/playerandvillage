#pragma once

// TODO(Naor): Move this a better place to make it configureable
static constexpr real32 DEFAULT_SENSITIVITY = 0.001f;
static constexpr real32 DEFAULT_DEADZONE = 0.0f; // 0.2f
static constexpr real32 DEFAULT_SPEED = 50.0f;//5.0f;

enum direction
{
    Left,
    Right,
    Forward,
    Backward,
    Up,
    Down
};

struct camera
{
    bool HasMoved = false;
    
    m4 Projection;
    m4 ProjectionInverse;
    m4 View;
    
    quat Orientation;
    
    v3 Position;
    v3 Velocity;
    v3 Up;
    v3 Right;
    v3 Forward;
    
    r32 Speed;
    
    // For Third-Person
    real32 Distance;
    v3* Attachment;
};

// World, function prototype
static r32 GetWorldHeight(r32 X, r32 Z);

enum frustum_sides
{
    FRUSTUM_NEAR,
    FRUSTUM_FAR,
    FRUSTUM_LEFT,
    FRUSTUM_RIGHT,
    FRUSTUM_UP,
    FRUSTUM_DOWN,
    
    FRUSTUM_COUNT
};

struct frustum
{
    plane Planes[FRUSTUM_COUNT];
};

static frustum Frustum(m4 VP)
{
    frustum Result;
    // TODO(Jonathan): Duplications
    Result.Planes[FRUSTUM_RIGHT].Normal.x = VP.E[0][3] - VP.E[0][0];
    Result.Planes[FRUSTUM_RIGHT].Normal.y = VP.E[1][3] - VP.E[1][0];
    Result.Planes[FRUSTUM_RIGHT].Normal.z = VP.E[2][3] - VP.E[2][0];
    Result.Planes[FRUSTUM_RIGHT].Distance = VP.E[3][3] - VP.E[3][0];
    
    Result.Planes[FRUSTUM_LEFT].Normal.x = VP.E[0][3] + VP.E[0][0];
    Result.Planes[FRUSTUM_LEFT].Normal.y = VP.E[1][3] + VP.E[1][0];
    Result.Planes[FRUSTUM_LEFT].Normal.z = VP.E[2][3] + VP.E[2][0];
    Result.Planes[FRUSTUM_LEFT].Distance = VP.E[3][3] + VP.E[3][0];
    
    Result.Planes[FRUSTUM_DOWN].Normal.x = VP.E[0][3] + VP.E[0][1];
    Result.Planes[FRUSTUM_DOWN].Normal.y = VP.E[1][3] + VP.E[1][1];
    Result.Planes[FRUSTUM_DOWN].Normal.z = VP.E[2][3] + VP.E[2][1];
    Result.Planes[FRUSTUM_DOWN].Distance = VP.E[3][3] + VP.E[3][1];
    
    Result.Planes[FRUSTUM_UP].Normal.x = VP.E[0][3] - VP.E[0][1];
    Result.Planes[FRUSTUM_UP].Normal.y = VP.E[1][3] - VP.E[1][1];
    Result.Planes[FRUSTUM_UP].Normal.z = VP.E[2][3] - VP.E[2][1];
    Result.Planes[FRUSTUM_UP].Distance = VP.E[3][3] - VP.E[3][1];
    
    Result.Planes[FRUSTUM_FAR].Normal.x = VP.E[0][3] - VP.E[0][2];
    Result.Planes[FRUSTUM_FAR].Normal.y = VP.E[1][3] - VP.E[1][2];
    Result.Planes[FRUSTUM_FAR].Normal.z = VP.E[2][3] - VP.E[2][2];
    Result.Planes[FRUSTUM_FAR].Distance = VP.E[3][3] - VP.E[3][2];
    
    Result.Planes[FRUSTUM_NEAR].Normal.x = VP.E[0][3] + VP.E[0][2];
    Result.Planes[FRUSTUM_NEAR].Normal.y = VP.E[1][3] + VP.E[1][2];
    Result.Planes[FRUSTUM_NEAR].Normal.z = VP.E[2][3] + VP.E[2][2];
    Result.Planes[FRUSTUM_NEAR].Distance = VP.E[3][3] + VP.E[3][2];
    
    for(s32 i = 0; i < FRUSTUM_COUNT; ++i)
    {
        Normalize(Result.Planes[i]);
    }
    
    return Result;
}

static bool FrustumSphereIntersection(const frustum& Frustum, v3 EntityCenter, r32 EntityRadius)
{
    for (int i = 0; i < FRUSTUM_COUNT; i++)
    {
        if (EntityCenter.Dot(Frustum.Planes[i].Normal) + Frustum.Planes[i].Distance + EntityRadius <= 0)
            return false;
    }
    
    return true;
}

// TODO(Jonathan): Understand better frustum culling
bool FrustumIntersection(const frustum& Frustum, const v3& Min, const v3& Max)
{
    for (u32 i = 0; i < FRUSTUM_COUNT; ++i)
    {
        v3 positive = Min;
        if (Frustum.Planes[i].Normal.x >= 0.0f)
        {
            positive.x = Max.x;
        }
        if (Frustum.Planes[i].Normal.y >= 0.0f)
        {
            positive.y = Max.y;
        }
        if (Frustum.Planes[i].Normal.z >= 0.0f)
        {
            positive.z = Max.z;
        }
        if(Dot(Frustum.Planes[i].Normal, positive) + Frustum.Planes[i].Distance  < 0.0f)
        {
            return false;
        }
    }
    return true;
}
