#pragma once

// TODO(Naor): Change the enums to the standard that we use (Mesh_Dave_Index) or something similar.
// + We may need to change the names a bit, for example, NUM_OF_SHADER or NUM_OF_TEXTURES could 
// conflict with other names by mistake, try to figure out solution (first without enum class).

enum mesh_index : s32
{
    MESH_DAVE_INDEX,
    MESH_FARMHOUSE_INDEX,
    MESH_COTTAGE_INDEX,
    MESH_WALL_INDEX,
    MESH_OUTPOST_INDEX,
    MESH_TREE_INDEX,
    MESH_BUSH_INDEX,
    MESH_ROCK_INDEX,
    MESH_GOLEM_INDEX,
    MESH_TREE1_INDEX,
    MESH_TREE2_INDEX,
    MESH_TREE3_INDEX,
    MESH_TRUNK_INDEX,
    MESH_STONE_INDEX,
    MESH_STEEL_INDEX,
    MESH_FIRECAMP_INDEX,
    MESH_QUAD_INDEX,
    
    NUM_OF_MESHS
};

enum shader_index : s32
{
    SHADER_WORLD_INDEX,
    SHADER_TEXTURED_MODEL_INDEX,
    SHADER_TEXTURED_MODEL_INSTANCED_INDEX,
    SHADER_ORTHO_INDEX,
    
    NUM_OF_SHADERS
};

constexpr static u32 GL_TEXTURE_NULL_ID = 9999999;
enum texture_index : s32
{
    TEXTURE_NULL_INDEX,
    
    TEXTURE_DAVE_INDEX,
    TEXTURE_FARMHOUSE_INDEX,
    TEXTURE_COTTAGE_INDEX,
    TEXTURE_WALL_INDEX,
    TEXTURE_GOLEM_INDEX,
    TEXTURE_OUTPOST_INDEX,
    TEXTURE_CROSSHAIR_INDEX,
    TEXTURE_TREE_INDEX,
    TEXTURE_TREE_2_INDEX,
    TEXTURE_TERRAIN_INDEX,
    TEXTURE_GRASS_INDEX,
    TEXTURE_INVENTORYITEM_INDEX,
    TEXTURE_CAMPFIRE_INDEX,
    TEXTURE_STONE_SLOT_INDEX,
    TEXTURE_WOOD_SLOT_INDEX,
    TEXTURE_STEEL_SLOT_INDEX,
    TEXTURE_FLINT_SLOT_INDEX,
    TEXTURE_FIRESTONE_SLOT_INDEX,
    TEXTURE_CAMPFIRE_SLOT_INDEX,
    
    NUM_OF_TEXTURES
};


enum shader_uniform_index : s32
{
    SHADER_UNIFORM_VP_INDEX,
    SHADER_UNIFORM_PROJECTION_INDEX,
    SHADER_UNIFORM_VIEW_INDEX,
    SHADER_UNIFORM_MODEL_INDEX,
    SHADER_UNIFORM_VIEW_POSITION_INDEX,
    SHADER_UNIFORM_TIME_INDEX,
    SHADER_UNIFORM_COLOR_INDEX,
    SHADER_UNIFORM_ALPHA_INDEX,
    
    // Temp uniforms
    SHADER_UNIFORM_MIN_HEIGHT_INDEX,
    SHADER_UNIFORM_MAX_HEIGHT_INDEX,
    
    NUM_OF_SHADER_UNIFORMS
};

// NOTE(Naor): This !!MUST!! match the order + values of the index enums!!!
namespace AssetsLookup
{
    const char* Shaders[]=
    {
        "shaders/world.glsl",
        "shaders/textured_model.glsl",
        "shaders/textured_model_instanced.glsl",
        "shaders/ortho.glsl",
    };
    
    const char* Uniforms[] = 
    {
        "ProjectionView",
        "Projection",
        "View",
        "Model",
        "ViewPosition",
        "Time",
        "Color",
        "Alpha",
        
        // Temp
        "MinHeight",
        "MaxHeight",
        0 // NOTE(Naor): This will indicate that we finished the array in-case we iterate over it
    };
};
