#pragma once

#ifdef NAYO_DEBUG

// TODO(Naor): Add node based profiler, so we can see where everything is came from
// + we want to be able to count the cycles based on the nodes so it will not
// accumulate.

#define TIMED_BLOCK__(Name, Number, ...) timed_block TimedBlock_##Number(__COUNTER__, __FILE__, Name, __LINE__)
#define TIMED_BLOCK_(Name, Number, ...)  TIMED_BLOCK__(Name, Number, ## __VA_ARGS__)
#define TIMED_BLOCK(Name, ...) TIMED_BLOCK_(Name, __COUNTER__, ## __VA_ARGS__)

#define TIMED_FUNCTION(...) TIMED_BLOCK((char *)__FUNCTION__)

struct timed_block
{
    debug_entry* Record;
    
    timed_block(s32 Counter, char* FileName, char* FunctionName, u32 LineNumber)
    {
        Record = DebugState->Profiler.Markers[DebugState->Profiler.CurrentEntryMarker].Entries + Counter;
        char* FileNameCopy = FileName + Utility::CStringFindLast(FileName, "\\code\\");
        Assert(Utility::CStringLength(FileNameCopy) < DEBUG_MAX_STRING_LENGTH);
        Utility::CCopyString(Record->FileName, FileNameCopy);
        Utility::CCopyString(Record->FunctionName, FunctionName);
        Record->LineNumber = LineNumber;
        Record->HitCount++; // TODO(Naor): Casey added an option to add more than 1, if we need it checkout day 177.
        Record->CycleCount -= __rdtsc();
    }
    
    ~timed_block()
    {
        Record->CycleCount += __rdtsc();
        
        if(DebugState->Profiler.IsPaused)
            Record->CycleCount = 0;
    }
};

#else

#define TIMED_BLOCK__(...)
#define TIMED_BLOCK_(...)
#define TIMED_BLOCK(...)
#define TIMED_FUNCTION(...)

#endif

namespace debug
{
    static void PrepareProfiler_()
    {
        // Count max total count of the markers
        u64 MaxTotalCycles = 0;
        u64 CurrentTotalCycles = 0;
        for(s32 MarkerIndex = 0;
            MarkerIndex < DEBUG_MAX_MARKERS;
            ++MarkerIndex)
        {
            CurrentTotalCycles = DebugState->Profiler.Markers[MarkerIndex].TotalCycles;
            if(MaxTotalCycles < CurrentTotalCycles)
            {
                MaxTotalCycles = CurrentTotalCycles;
            }
        }
        DebugState->Profiler.MaxTotalCycles = MaxTotalCycles;
    }
    
    static void PrepareMarker_(debug_entry_marker* Marker)
    {
        debug_entry* Entry;
        for(s32 EntryIndex = 0;
            EntryIndex < DEBUG_MAX_ENTRIES;
            ++EntryIndex)
        {
            Entry = Marker->Entries + EntryIndex;
            Marker->TotalCycles += Entry->CycleCount;
        }
    }
    
    static void PrepareUsedMarker_(debug_entry_marker* Marker)
    {
        debug_entry* Entry;
        debug_entry* SecondEntry;
        debug_entry* MaxEntry;
        s32 SortedIndex = 0;
        for(s32 EntryIndex = 0;
            EntryIndex < DEBUG_MAX_ENTRIES;
            ++EntryIndex)
        {
            Entry = Marker->Entries + EntryIndex;
            if(Entry->CycleCount == 0)
                continue;
            
            MaxEntry = Entry;
            for(s32 SecondIndex = EntryIndex+1;
                SecondIndex < DEBUG_MAX_ENTRIES;
                ++SecondIndex)
            {
                SecondEntry = Marker->Entries + SecondIndex;
                if(SecondEntry->CycleCount > MaxEntry->CycleCount)
                {
                    MaxEntry = SecondEntry;
                }
            }
            
            DebugState->Profiler.SortedEntries[SortedIndex++] = *MaxEntry;
            
            Utility::Swap(*MaxEntry, *Entry);
        }
        
        DebugState->Profiler.EntriesCount = SortedIndex;
    }
    
    static void RenderTable_(debug_entry_marker* Marker)
    {
        ImGui::Columns(4, 0);
        ImGui::Separator();
        ImGui::Text("FileName"); ImGui::NextColumn();
        ImGui::Text("Function"); ImGui::NextColumn();
        ImGui::Text("Cycles"); ImGui::NextColumn();
        ImGui::Text("Precent"); ImGui::NextColumn();
        ImGui::Separator();
        
        debug_entry* Entry;
        r32 OneOverTotal = 1.0f / Marker->TotalCycles;
        static u32 Selected = 999999;
        const u32 EntriesCount = DebugState->Profiler.EntriesCount;
        for(u32 EntryIndex = 0;
            EntryIndex < EntriesCount;
            ++EntryIndex)
        {
            Entry = DebugState->Profiler.SortedEntries + EntryIndex;
            
            if (ImGui::Selectable(Entry->FileName, Selected == EntryIndex, ImGuiSelectableFlags_SpanAllColumns))
                Selected = EntryIndex;
            ImGui::NextColumn();
            ImGui::Text(Entry->FunctionName); ImGui::NextColumn();
            ImGui::Text("%d", Entry->CycleCount); ImGui::NextColumn();
            ImGui::Text("%.3f", Entry->CycleCount * OneOverTotal * 100.0f); ImGui::NextColumn();
        }
        
        ImGui::Columns(1);
        ImGui::Separator();
    }
    
    static void CopyEntry_(debug_entry* Target, debug_entry* Source)
    {
        Target->CycleCount = Source->CycleCount;
        Target->LineNumber = Source->LineNumber;
        Target->HitCount = Source->HitCount;
        
        Utility::CCopyString(Target->FileName, Source->FileName);
        Utility::CCopyString(Target->FunctionName, Source->FunctionName);
    }
    
    static r32 ValuesGetter_(void* Data, s32 Index)
    {
        return DebugState->Profiler.Markers[Index].TotalCycles / (r32)DebugState->Profiler.MaxTotalCycles;
    }
    
    static void ValuesHover_(void* Data, s32 Index)
    {
        ImGui::SetTooltip("%d", DebugState->Profiler.Markers[Index].TotalCycles);
    }
    
    static void ValuesClick_(void* Data, s32 Index)
    {
        debug_entry_marker* Marker = &DebugState->Profiler.Markers[Index];
        debug_entry_marker* Selected = &DebugState->Profiler.SelectedMarker;
        DebugState->Profiler.SelectedMarkerIndex = Index;
        Selected->TotalCycles= Marker->TotalCycles;
        
        for(s32 EntryIndex = 0;
            EntryIndex < DEBUG_MAX_ENTRIES;
            ++EntryIndex)
        {
            CopyEntry_(&Selected->Entries[EntryIndex], &Marker->Entries[EntryIndex]);
        }
    }
    
    static void RenderGraph_()
    {
        ImDrawList* DrawList = ImGui::GetWindowDrawList();
        ImVec2 WindowSize = ImGui::GetWindowSize();
        
        ImGui::PlotClickableColored("", ValuesGetter_, ValuesHover_, ValuesClick_, 
                                    DebugState->Profiler.Markers, DEBUG_MAX_MARKERS, 0,
                                    0, 0.0f, 1.0f, ImVec2(WindowSize.x - 20.0f, 100.0f));
    }
    
    static void RenderProfiler()
    {
        ImGui::ShowDemoWindow();
        debug_entry_marker* Marker;
        if(DebugState->Profiler.SelectedMarkerIndex != -1)
        {
            Marker = &DebugState->Profiler.SelectedMarker;
        }
        else
        {
            s32 MarkerIndex = DebugState->Profiler.CurrentEntryMarker;
            if(DebugState->Profiler.IsPaused)
            {
                MarkerIndex--;
                if(MarkerIndex < 0)
                {
                    MarkerIndex = DEBUG_MAX_MARKERS -1;
                }
            }
            Marker = &DebugState->Profiler.Markers[MarkerIndex];
        }
        
        PrepareUsedMarker_(Marker);
        PrepareMarker_(&DebugState->Profiler.Markers[DebugState->Profiler.CurrentEntryMarker]);
        PrepareProfiler_();
        
        ImGui::SetNextWindowPos(ImVec2(20.0f, 30.0f), ImGuiCond_FirstUseEver);
        ImGui::SetNextWindowSize(ImVec2(550,500), ImGuiCond_FirstUseEver);
        if(ImGui::Begin("Profiler"))
        {
            RenderGraph_();
            if(ImGui::Button("Clear Marker Selected"))
            {
                DebugState->Profiler.SelectedMarkerIndex = -1;
            }
            ImGui::SameLine();
            ImGui::Checkbox("Pause", &DebugState->Profiler.IsPaused);
            
            RenderTable_(Marker);
        }
        
        ImGui::End();
        
        // TODO(Naor): When we pause, the current marker (new) is erased and all of it is 0.
        if(!DebugState->Profiler.IsPaused)
        {
            if(++DebugState->Profiler.CurrentEntryMarker >= DEBUG_MAX_MARKERS)
            {
                DebugState->Profiler.CurrentEntryMarker = 0;
            }
            
            DebugState->Profiler.Markers[DebugState->Profiler.CurrentEntryMarker] = {};
        }
        
        for(s32 EntryIndex = 0;
            EntryIndex < DEBUG_MAX_ENTRIES;
            ++EntryIndex)
        {
            DebugState->Profiler.SortedEntries[EntryIndex] = {};
        }
    }
};