#pragma once

#ifdef NAYO_DEBUG
#define DEBUG_LINE(P1, P2, Color) debug::AddLine(GameState->Primitives, ##P1, ##P2, ##Color)
#define DEBUG_POINT(P, Color) debug::AddPoint(GameState->Primitives, ##P, ##Color)
#define DEBUG_AABB(Min, Max, Color) debug::AddAABB(GameState->Primitives, ##Min, ##Max, ##Color)
#define DEBUG_SPHERE(P, R, S, Color) debug::AddSphere(GameState->Primitives, ##P, ##R, ##S, ##Color)
#define DEBUG_PLANE(Min, Max, Color) debug::AddPlane(GameState->Primitives, ##Min, ##Max, ##Color)

#else
#define DEBUG_LINE(...) 
#define DEBUG_POINT(...) 
#define DEBUG_AABB(...) 
#define DEBUG_SPHERE(...)
#define DEBUG_PLANE(...)
#endif

// TODO(Naor): Add ortho feature
// TODO(Naor): There is a HUGE problem with this implementation, when we render about 1000+- points,
// the FPS goes down A LOT! we need to fix this and find the source of the problem, it might be
// the fact that we build the buffer every frame(!).

namespace debug
{
    static const char* LinePointVertexShaderSrc =R"(
    #version 410 core
    
    layout(location=0) in vec3 Position;
    layout(location=1) in vec4 ColorPointSize;
    
    out vec4 FragColor;
    uniform mat4 ProjectionView;
    
    void main()
    {
    gl_Position = ProjectionView * vec4(Position, 1.0);
    gl_PointSize = ColorPointSize.w;
    FragColor = vec4(ColorPointSize.xyz, 1.0);
    }
    )";
    
    static const char* LinePointFragmentShaderSrc =R"(
    #version 410 core
    
    in vec4 FragColor;
    
    out vec4 OutColor;
    
    void main()
    {
    OutColor = FragColor;
    }
    )";
    
    
    // TODO(Naor): Add warning/error if we try to use more than this amount of primitives!
    static constexpr s32 MAX_LINE_BUFFER_SIZE =  4000000;//40000;//4000000;
    
    
    struct point
    {
        v3 Position;
        v4 ColorSize; // xyz = color (rgb), w = size
    };
    
    struct primitives
    {
        point* LineMapping;
        mesh Mesh;
        shader_program LinePointShader;
    };
    
    void AddLine(primitives& Primitives, const v3& P1, const v3& P2, const v3& Color);
    // TODO: Read: https://www.khronos.org/opengl/wiki/Buffer_Object_Streaming
    void Initialize(primitives& Primitives)
    {
        // NOTE: This is to enable point size modification
        glEnable(GL_PROGRAM_POINT_SIZE);
        
        Primitives.LinePointShader.AddShaderBySource(shader_program::type::Vertex, LinePointVertexShaderSrc);
        Primitives.LinePointShader.AddShaderBySource(shader_program::type::Fragment, LinePointFragmentShaderSrc);
        Primitives.LinePointShader.Compile();
        
        // Vao
        glGenVertexArrays(1, &Primitives.Mesh.Vao);
        
        // Vbo
        glGenBuffers(1, &Primitives.Mesh.Vbo);
        
        // Initialize Line Data
        glBindVertexArray(Primitives.Mesh.Vao);
        glBindBuffer(GL_ARRAY_BUFFER, Primitives.Mesh.Vbo);
        
        glBufferData(GL_ARRAY_BUFFER, 2 * MAX_LINE_BUFFER_SIZE  * sizeof(point), nullptr, GL_DYNAMIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(point), nullptr);
        glVertexAttribPointer(1, 4, GL_FLOAT, false, sizeof(point), reinterpret_cast<void*>(sizeof(v3)));
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        Primitives.LineMapping = reinterpret_cast<point*>(glMapBufferRange(GL_ARRAY_BUFFER, 0, MAX_LINE_BUFFER_SIZE * sizeof(point),
                                                                           GL_MAP_WRITE_BIT | GL_MAP_FLUSH_EXPLICIT_BIT | GL_MAP_UNSYNCHRONIZED_BIT));
        
        
        // Cleaning Up
        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        Primitives.Mesh.Count = 0;
    }
    
    void AddLine(primitives& Primitives, const v3& P1, const v3& P2, const v3& Color)
    {
        // @Cleanup: Maybe move that if to the begininig of each fucntion that uses that(AddAABB, AddPlane)
        // because every time we call AddLine it will call that if (the purpose of this fucntion is to add line
        if(Primitives.LineMapping == nullptr)
        {
            glBindBuffer(GL_ARRAY_BUFFER, Primitives.Mesh.Vbo);
            Primitives.LineMapping = reinterpret_cast<point*>(glMapBufferRange(GL_ARRAY_BUFFER, 0, MAX_LINE_BUFFER_SIZE * sizeof(point),
                                                                               GL_MAP_WRITE_BIT | GL_MAP_FLUSH_EXPLICIT_BIT | GL_MAP_UNSYNCHRONIZED_BIT));
            
        }
        
        Primitives.LineMapping[Primitives.Mesh.Count*2] = point{P1, V4(Color.x, Color.y, Color.z, 1.0f)};
        Primitives.LineMapping[Primitives.Mesh.Count*2+1] = point{P2, V4(Color.x, Color.y, Color.z, 1.0f)};
        Primitives.Mesh.Count++;
    }
    
    void AddSphere(primitives& Primitives, const v3& Center, r32 Radius, int32 Segments, const v3& Color)
    {
        // NOTE(Naor): We want at least 4 segments
        Segments = Math::Max(Segments, 4);
        
        // NOTE(Naor): The segments needs to be even
        if(Segments % 2 != 0)
            Segments++;
        
        v3 vv1, vv2, vv3, vv4;
        const r32 AngleInc = Math::TwoPi / Segments;
        int32 NumSegmentsY = Segments;
        r32 Latitude = AngleInc;
        int32 NumSegmentsX;
        r32 Longitude;
        r32 SinY1 = 0.0f, CosY1 = 1.0f, SinY2, CosY2;
        r32 SinX, CosX;
        
        while(NumSegmentsY--)
        {
            SinY2 = Math::Sin(Latitude);
            CosY2 = Math::Cos(Latitude);
            
            vv1 = V3(SinY1, 0.0f, CosY1) * Radius + Center;
            vv3 = V3(SinY2, 0.0f, CosY2) * Radius + Center;
            
            Longitude = AngleInc;
            
            NumSegmentsX = Segments;
            while(NumSegmentsX--)
            {
                SinX = Math::Sin(Longitude);
                CosX = Math::Cos(Longitude);
                
                vv2 = V3((CosX * SinY1), (SinX * SinY1), CosY1) * Radius + Center;
                vv4 = V3((CosX * SinY2), (SinX * SinY2), CosY2) * Radius + Center;
                SinX = Math::Sin(Longitude);
                CosX = Math::Cos(Longitude);
                
                AddLine(Primitives, vv1, vv2, Color);
                AddLine(Primitives, vv1, vv3, Color);
                
                vv1 = vv2;
                vv3 = vv4;
                Longitude += AngleInc;
            }
            SinY1 = SinY2;
            CosY1 = CosY2;
            Latitude += AngleInc;
        }
    }
    
    void AddHackyCircle(primitives& Primitives, const v3& Center, r32 Radius, int32 Segments, const v3& Color)
    {
        // NOTE(Naor): We want at least 4 segments
        Segments = Math::Max(Segments, 4);
        
        // NOTE(Naor): The segments needs to be even
        if(Segments % 2 != 0)
            Segments++;
        
        v3 vv1, vv2, vv3, vv4;
        const r32 AngleInc = Math::TwoPi / Segments;
        int32 NumSegmentsY = Segments;
        r32 Latitude = AngleInc;
        int32 NumSegmentsX;
        r32 Longitude;
        r32 SinY1 = 0.0f, CosY1 = 1.0f, SinY2, CosY2;
        r32 SinX, CosX;
        
        while(NumSegmentsY--)
        {
            SinY2 = Math::Sin(Latitude);
            CosY2 = Math::Cos(Latitude);
            
            vv1 = V3(SinY1, 0.0f, CosY1) * Radius + Center;
            vv3 = V3(SinY2, 0.0f, CosY2) * Radius + Center;
            
            Longitude = AngleInc;
            
            NumSegmentsX = Segments;
            while(NumSegmentsX--)
            {
                SinX = Math::Sin(Longitude);
                CosX = Math::Cos(Longitude);
                
                vv2 = V3((CosX * SinY1), 0, CosY1) * Radius + Center;
                vv4 = V3((CosX * SinY2), 0, CosY2) * Radius + Center;
                SinX = Math::Sin(Longitude);
                CosX = Math::Cos(Longitude);
                
                AddLine(Primitives, vv1, vv2, Color);
                AddLine(Primitives, vv1, vv3, Color);
                
                vv1 = vv2;
                vv3 = vv4;
                Longitude += AngleInc;
            }
            SinY1 = SinY2;
            CosY1 = CosY2;
            Latitude += AngleInc;
        }
    }
    
    
    
    void AddPoint(primitives& Primitives, const v3& Position, const v3& Color)
    {
        AddSphere(Primitives, Position, 0.5f, 6, Color);
    }
    
    void AddPlane(primitives& Primitives, const v3& Min, const v3& Max, const v3& Color)
    {
        AddLine(Primitives, Min, POINT_COMPRESSION(Min, Min, Max), Color);
        AddLine(Primitives, POINT_COMPRESSION(Min, Min, Max), POINT_COMPRESSION(Max, Min, Max), Color);
        AddLine(Primitives, POINT_COMPRESSION(Max, Min, Max), POINT_COMPRESSION(Max, Min, Min), Color);
        AddLine(Primitives, POINT_COMPRESSION(Max, Min, Min), Min, Color);
    }
    
    void AddAABB(primitives& Primitives, const v3& Min, const v3& Max, const v3& Color)
    {
        // Bottom
        AddLine(Primitives, Min, POINT_COMPRESSION(Min, Min, Max), Color);
        AddLine(Primitives, POINT_COMPRESSION(Min, Min, Max), POINT_COMPRESSION(Max, Min, Max), Color);
        AddLine(Primitives, POINT_COMPRESSION(Max, Min, Max), POINT_COMPRESSION(Max, Min, Min), Color);
        AddLine(Primitives, POINT_COMPRESSION(Max, Min, Min), Min, Color);
        // Upward lines
        AddLine(Primitives, Min, POINT_COMPRESSION(Min, Max, Min), Color);
        AddLine(Primitives, POINT_COMPRESSION(Min, Min, Max), POINT_COMPRESSION(Min, Max, Max), Color);
        AddLine(Primitives, POINT_COMPRESSION(Max, Min, Max), Max, Color);
        AddLine(Primitives, POINT_COMPRESSION(Max, Min, Min), POINT_COMPRESSION(Max, Max, Min), Color);
        // Top
        AddLine(Primitives, POINT_COMPRESSION(Min, Max, Min), POINT_COMPRESSION(Min, Max, Max), Color);
        AddLine(Primitives, POINT_COMPRESSION(Min, Max, Max), POINT_COMPRESSION(Max, Max, Max), Color);
        AddLine(Primitives, POINT_COMPRESSION(Max, Max, Max), POINT_COMPRESSION(Max, Max, Min), Color);
        AddLine(Primitives, POINT_COMPRESSION(Max, Max, Min), POINT_COMPRESSION(Min, Max, Min), Color);
    }
    
    inline void NextFrame(primitives& Primitives)
    {
        Primitives.Mesh.Count = 0;
    }
    
    // TODO(Naor): Add LineWidth
    void RenderFrame(render_group& RenderGroup, primitives& Primitives, const m4& MVP)
    {
        glBindBuffer(GL_ARRAY_BUFFER, Primitives.Mesh.Vbo);
        glFlushMappedBufferRange(GL_ARRAY_BUFFER, 0, Primitives.Mesh.Count * sizeof(point) * 2);
        glUnmapBuffer(GL_ARRAY_BUFFER);
        Primitives.LineMapping = nullptr;
        
        material Material = {};
        Material.Shader = &Primitives.LinePointShader;
        SetMatrix(Material, "ProjectionView", MVP, SHADER_UNIFORM_VP_INDEX);
        
        Primitives.Mesh.Count = 2 * Primitives.Mesh.Count;
        PushRender(RenderGroup, Material, &Primitives.Mesh, GL_TEXTURE_NULL_ID, RenderDrawType_Arrays, false, 0,RenderDrawMode_Lines);
    }
};