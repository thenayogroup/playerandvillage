#pragma once

#include <stdio.h>
#include <time.h>
#include <string>

void exec(const char* cmd, char* buffer) {
    std::string result = "";
    FILE* pipe = _popen(cmd, "r");
    if (!pipe) return;
    while (!feof(pipe)) {
        if (fgets(buffer, 512, pipe) != NULL)
            result += buffer;
    }
    _pclose(pipe);
}

#ifdef NAYO_DEBUG

inline void FormatText(char* Buffer, const char* Text, va_list Args)
{
    stbsp_vsprintf(Buffer, Text, Args); 
}

inline bool IsPrevLogDuplicate(const char* Text)
{
    s32 Index = (s32)DebugState->Logger.Log.Count-1;
    if(Index < 0)
        return false;
    
    if(Utility::CStringEqual(Text,
                             DebugState->Logger.Log[Index].Buffer))
    {
        return true;
    }
    return false;
}

inline void Log_(debug_log_level Level, const ImVec4& Color, char* FunctionName,
                 char* FileName, s32 LineNumber, const char* Text, ...)
{
    va_list Args;
    va_start(Args, Text);
    char Buffer[LOG_MAX_LENGTH];
    FormatText(Buffer, Text, Args); 
    
    time_t CurrentTime;
    tm* TimeInfo;
    
    time(&CurrentTime);
    TimeInfo = localtime(&CurrentTime);
    
    if(IsPrevLogDuplicate(Buffer))
    {
        debug_log* Log = &DebugState->Logger.Log[(s32)DebugState->Logger.Log.Count-1];
        Log->Count++;
        strftime(Log->Time, sizeof(Log->Time), "%H:%M:%S", TimeInfo);
    }
    else
    {
        debug_log NewLog = {}; 
        NewLog.Level = Level;
        NewLog.Color = Color;
        NewLog.Count = 1;
        Utility::CCopyString(NewLog.FunctionName, FunctionName);
        Utility::CCopyString(NewLog.FileName, FileName + Utility::CStringFindLast(FileName, "\\code\\"));
        NewLog.Line = LineNumber;
        strftime(NewLog.Time, sizeof(NewLog.Time), "%H:%M:%S", TimeInfo);
        Utility::CCopyString(NewLog.Buffer, Buffer);
        DebugState->Logger.Log.PushBack(NewLog);
    }
    
    va_end(Args);
}

// NOTE(Naor): Critical is for stuff that should NEVER 
// happen, and may cause the game to crash, Error is for
// stuff that SHOULDN'T happen, but we can still continue without it.
#define LogWarning(Text, ...) Log_(DebugLogLevel_Warning, (ImVec4)ImColor(1.0f, 0.8f, 0.3f), \
__FUNCTION__, __FILE__, __LINE__, "[WARNING] " ##Text, ## __VA_ARGS__)
#define LogCritical(Text, ...) Log_(DebugLogLevel_Critical, (ImVec4)ImColor(1.0f, 0.2f, 0.3f),\
__FUNCTION__, __FILE__, __LINE__,  "[CRITICAL] " ##Text,  ## __VA_ARGS__)
#define LogError(Text, ...) Log_(DebugLogLevel_Error, (ImVec4)ImColor(0.7f, 0.4f, 0.3f), \
__FUNCTION__, __FILE__, __LINE__, "[ERROR] " ##Text, ## __VA_ARGS__)
#define LogInfo(Text, ...) Log_(DebugLogLevel_Info, (ImVec4)ImColor(0.8f, 0.6f, 0.8f), \
__FUNCTION__, __FILE__, __LINE__, "[INFO] " ##Text ,## __VA_ARGS__)
#else
#define Log_(...)
#define LogWarning(...)
#define LogCritical(...)
#define LogError(...)
#define LogInfo(...)
#endif

namespace debug
{
    inline void UpdateQuoteOfTheDay()
    {
        //exec("start /B quote_of_the_day.py", DebugState->Logger.QuoteOfTheDay);
    }
    
    inline void RenderQuoteOfTheDay()
    {
        ImGui::PushStyleColor(ImGuiCol_Text, (ImVec4)ImColor(0.2f, 0.6f, 0.8f));
        ImGui::TextWrapped(DebugState->Logger.QuoteOfTheDay);
        ImGui::PopStyleColor();
        
        if(ImGui::Button("New Quote!"))
        {
            UpdateQuoteOfTheDay();
        }
    }
    
    static bool FilterLog(const char* Log)
    {
        char* FilterBegin = DebugState->Logger.Filter;
        char* Filter = FilterBegin;
        
        // In case there is not filter
        if(*Filter == 0)
            return true;
        
        while(*Log)
        {
            if(*Filter == 0)
                return true;
            
            char A = *Log++;
            char B = *Filter++;
            if(A >= 'A' && A <= 'Z')
                A += 'a'-'A';
            if(B >= 'A' && B <= 'Z')
                B += 'a'-'A';
            if(A != B)
                Filter = FilterBegin;
        }
        
        return (*Filter == 0);
    }
    
    inline void ClearLogs()
    {
        *DebugState->Logger.Filter = 0;
        DebugState->Logger.Log.Clear();
    }
    
    // TODO(Naor): Save log to file when crash / when asked (think what is best)
    // TODO(Naor): Log time
    static void RenderLogger()
    {
        ImGuiIO& IO = ImGui::GetIO();
        
        ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
        
        //ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f), ImGuiCond_Always);
        //ImGui::SetNextWindowSize(ImVec2(IO.DisplaySize.x,400), ImGuiCond_Always); 
        ImGui::SetNextWindowBgAlpha(0.8f);
        if(ImGui::Begin("Logger"))
        {
            // NOTE(Naor): This takes time to execute... we maybe bring it back
            // when we use threading.. (and figure out how the hell to open this
            // in the background!!)
            //RenderQuoteOfTheDay();
            
            ImGui::Text("Filter: "); ImGui::SameLine();
            ImGui::InputText("", DebugState->Logger.Filter, LOG_MAX_FILTER_LENGTH);
            
            u32* FilterFlags = &DebugState->Logger.FilterFlag;
            ImGui::CheckboxFlags("Info", FilterFlags, DebugLogLevel_Info); ImGui::SameLine();
            ImGui::CheckboxFlags("Warning", FilterFlags, DebugLogLevel_Warning); ImGui::SameLine();
            ImGui::CheckboxFlags("Error", FilterFlags, DebugLogLevel_Error); ImGui::SameLine();
            ImGui::CheckboxFlags("Critical", FilterFlags, DebugLogLevel_Critical);
            if(ImGui::Button("Clear"))
            {
                ClearLogs();
            }
            ImGui::Columns(1);
            ImGui::Separator();
            
            ImGui::BeginChild("Logger Region");
            const size_t LogCount = DebugState->Logger.Log.Count;
            debug_log* CurrentLog;
            for(s32 LogIndex = 0;
                LogIndex < LogCount;
                ++LogIndex)
            {
                CurrentLog = &DebugState->Logger.Log[LogIndex];
                if((CurrentLog->Level & DebugState->Logger.FilterFlag) != 0)
                {
                    if(FilterLog(CurrentLog->Buffer))
                    {
                        ImGui::PushFont(DebugState->FontCode);
                        ImGui::Text("[%s]", CurrentLog->Time);
                        ImGui::PopFont();
                        ImGui::SameLine();
                        
                        bool IsHover = false;
                        if(CurrentLog->Count > 1)
                        {
                            ImGui::TextColored(CurrentLog->Color, CurrentLog->Buffer); ImGui::SameLine();
                            IsHover = ImGui::IsItemHovered();
                            ImGui::TextColored(CurrentLog->Color, "(x%d)", CurrentLog->Count);
                        }
                        else
                        {
                            ImGui::TextColored(CurrentLog->Color, CurrentLog->Buffer);
                            IsHover = ImGui::IsItemHovered();
                        }
                        
                        ImGui::PushFont(DebugState->FontCode);
                        if(IsHover)
                        {
                            ImGui::SetTooltip("%s | %s | @%d", CurrentLog->FileName, CurrentLog->FunctionName, CurrentLog->Line); 
                        }
                        ImGui::PopFont();
                    }
                }
            }
            
            DebugState->Logger.ScrollToBottom = ImGui::GetScrollY() == ImGui::GetScrollMaxY();
            
            if(DebugState->Logger.ScrollToBottom)
                ImGui::SetScrollHere();
            
            ImGui::EndChild();
        }
        ImGui::End();
        
        ImGui::PopStyleVar();
    }
};


