#include <SDL.h>
#include <SDL_syswm.h>

// TODO(Naor): For now, there is no shutdown process, do we really need it?
// TODO(Naor): When the event is caused in ImGui, we want to disable it on the game.

// This is the main rendering function that you have to implement and provide to ImGui (via setting up 'RenderDrawListsFn' in the ImGuiIO structure)
// Note that this implementation is little overcomplicated because we are saving/setting up/restoring every OpenGL state explicitly, in order to be able to run within any OpenGL engine that doesn't do so. 
// If text or lines are blurry when integrating ImGui in your engine: in your Render function, try translating your projection matrix by (0.5f,0.5f) or (0.375f,0.375f)
void ImGui_SDL_RenderDrawData(ImDrawData* DrawData)
{
    // Avoid rendering when minimized, scale coordinates for retina displays (screen coordinates != framebuffer coordinates)
    ImGuiIO& io = ImGui::GetIO();
    s32 fb_width = (s32)(io.DisplaySize.x * io.DisplayFramebufferScale.x);
    s32 fb_height = (s32)(io.DisplaySize.y * io.DisplayFramebufferScale.y);
    if (fb_width == 0 || fb_height == 0)
        return;
    DrawData->ScaleClipRects(io.DisplayFramebufferScale);
    
    // Backup GL state
    glActiveTexture(GL_TEXTURE0);
    GLint last_polygon_mode[2]; glGetIntegerv(GL_POLYGON_MODE, last_polygon_mode);
    GLint last_viewport[4]; glGetIntegerv(GL_VIEWPORT, last_viewport);
    GLint last_scissor_box[4]; glGetIntegerv(GL_SCISSOR_BOX, last_scissor_box);
    GLenum last_blend_src_rgb; glGetIntegerv(GL_BLEND_SRC_RGB, (GLint*)&last_blend_src_rgb);
    GLenum last_blend_dst_rgb; glGetIntegerv(GL_BLEND_DST_RGB, (GLint*)&last_blend_dst_rgb);
    GLenum last_blend_src_alpha; glGetIntegerv(GL_BLEND_SRC_ALPHA, (GLint*)&last_blend_src_alpha);
    GLenum last_blend_dst_alpha; glGetIntegerv(GL_BLEND_DST_ALPHA, (GLint*)&last_blend_dst_alpha);
    GLenum last_blend_equation_rgb; glGetIntegerv(GL_BLEND_EQUATION_RGB, (GLint*)&last_blend_equation_rgb);
    GLenum last_blend_equation_alpha; glGetIntegerv(GL_BLEND_EQUATION_ALPHA, (GLint*)&last_blend_equation_alpha);
    GLboolean last_enable_blend = glIsEnabled(GL_BLEND);
    GLboolean last_enable_cull_face = glIsEnabled(GL_CULL_FACE);
    GLboolean last_enable_depth_test = glIsEnabled(GL_DEPTH_TEST);
    GLboolean last_enable_scissor_test = glIsEnabled(GL_SCISSOR_TEST);
    
    // Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled, polygon fill
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_SCISSOR_TEST);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    
    // Setup viewport, orthographic projection matrix
    glViewport(0, 0, (GLsizei)fb_width, (GLsizei)fb_height);
    const r32 ortho_projection[4][4] =
    {
        { 2.0f/io.DisplaySize.x, 0.0f,                   0.0f, 0.0f },
        { 0.0f,                  2.0f/-io.DisplaySize.y, 0.0f, 0.0f },
        { 0.0f,                  0.0f,                  -1.0f, 0.0f },
        {-1.0f,                  1.0f,                   0.0f, 1.0f },
    };
    glUseProgram(DebugState->ShaderHandle);
    glUniform1i(DebugState->AttribLocationTex, 0);
    glUniformMatrix4fv(DebugState->AttribLocationProjMtx, 1, GL_FALSE, &ortho_projection[0][0]);
    glBindVertexArray(DebugState->VaoHandle);
    glBindSampler(0, 0); // Rely on combined texture/sampler state.
    
    for (s32 n = 0; n < DrawData->CmdListsCount; n++)
    {
        const ImDrawList* cmd_list = DrawData->CmdLists[n];
        const ImDrawIdx* idx_buffer_offset = 0;
        
        glBindBuffer(GL_ARRAY_BUFFER, DebugState->VboHandle);
        glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)cmd_list->VtxBuffer.Size * sizeof(ImDrawVert), (const GLvoid*)cmd_list->VtxBuffer.Data, GL_STREAM_DRAW);
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, DebugState->ElementsHandle);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizeiptr)cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx), (const GLvoid*)cmd_list->IdxBuffer.Data, GL_STREAM_DRAW);
        
        for (s32 cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
        {
            const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
            if (pcmd->UserCallback)
            {
                pcmd->UserCallback(cmd_list, pcmd);
            }
            else
            {
                glBindTexture(GL_TEXTURE_2D, (GLuint)(intptr_t)pcmd->TextureId);
                glScissor((s32)pcmd->ClipRect.x, (s32)(fb_height - pcmd->ClipRect.w), (s32)(pcmd->ClipRect.z - pcmd->ClipRect.x), (s32)(pcmd->ClipRect.w - pcmd->ClipRect.y));
                glDrawElements(GL_TRIANGLES, (GLsizei)pcmd->ElemCount, sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT, idx_buffer_offset);
            }
            idx_buffer_offset += pcmd->ElemCount;
        }
    }
    
    // Restore modified GL state
    glBlendEquationSeparate(last_blend_equation_rgb, last_blend_equation_alpha);
    glBlendFuncSeparate(last_blend_src_rgb, last_blend_dst_rgb, last_blend_src_alpha, last_blend_dst_alpha);
    if (last_enable_blend) glEnable(GL_BLEND); else glDisable(GL_BLEND);
    if (last_enable_cull_face) glEnable(GL_CULL_FACE); else glDisable(GL_CULL_FACE);
    if (last_enable_depth_test) glEnable(GL_DEPTH_TEST); else glDisable(GL_DEPTH_TEST);
    if (last_enable_scissor_test) glEnable(GL_SCISSOR_TEST); else glDisable(GL_SCISSOR_TEST);
    glPolygonMode(GL_FRONT_AND_BACK, last_polygon_mode[0]);
    glViewport(last_viewport[0], last_viewport[1], (GLsizei)last_viewport[2], (GLsizei)last_viewport[3]);
    glScissor(last_scissor_box[0], last_scissor_box[1], (GLsizei)last_scissor_box[2], (GLsizei)last_scissor_box[3]);
}

void ImGui_SDL_CreateFontsTexture()
{
    // Build texture atlas
    ImGuiIO& io = ImGui::GetIO();
    unsigned char* Pixels;
    s32 Width, Height;
    io.Fonts->GetTexDataAsRGBA32(&Pixels, &Width, &Height);  // Load as RGBA 32-bits for OpenGL3 demo because it is more likely to be compatible with user's existing shader.
    
    // Upload texture to graphics system
    GLint LastTexture;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &LastTexture);
    glGenTextures(1, &DebugState->FontTexture);
    glBindTexture(GL_TEXTURE_2D, DebugState->FontTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Width, Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, Pixels);
    
    // Store our identifier
    io.Fonts->TexID = (void *)(uptr)DebugState->FontTexture;
    
    // Restore state
    glBindTexture(GL_TEXTURE_2D, LastTexture);
}

static bool ImGui_SDL_CreateDeviceObjects()
{
    // Backup GL state
    s32 LastTexture, LastArrayBuffer, LastVertexArray;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &LastTexture);
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &LastArrayBuffer);
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &LastVertexArray);
    
    const GLchar *VertexShader =
        "#version 150\n"
        "uniform mat4 ProjMtx;\n"
        "in vec2 Position;\n"
        "in vec2 UV;\n"
        "in vec4 Color;\n"
        "out vec2 Frag_UV;\n"
        "out vec4 Frag_Color;\n"
        "void main()\n"
        "{\n"
        "	Frag_UV = UV;\n"
        "	Frag_Color = Color;\n"
        "	gl_Position = ProjMtx * vec4(Position.xy,0,1);\n"
        "}\n";
    
    const GLchar* FragmentShader =
        "#version 150\n"
        "uniform sampler2D Texture;\n"
        "in vec2 Frag_UV;\n"
        "in vec4 Frag_Color;\n"
        "out vec4 Out_Color;\n"
        "void main()\n"
        "{\n"
        "	Out_Color = Frag_Color * texture( Texture, Frag_UV.st);\n"
        "}\n";
    
    DebugState->ShaderHandle = glCreateProgram();
    DebugState->VertHandle = glCreateShader(GL_VERTEX_SHADER);
    DebugState->FragHandle = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(DebugState->VertHandle, 1, &VertexShader, 0);
    glShaderSource(DebugState->FragHandle, 1, &FragmentShader, 0);
    glCompileShader(DebugState->VertHandle);
    glCompileShader(DebugState->FragHandle);
    glAttachShader(DebugState->ShaderHandle, DebugState->VertHandle);
    glAttachShader(DebugState->ShaderHandle, DebugState->FragHandle);
    glLinkProgram(DebugState->ShaderHandle);
    
    DebugState->AttribLocationTex = glGetUniformLocation(DebugState->ShaderHandle, "Texture");
    DebugState->AttribLocationProjMtx = glGetUniformLocation(DebugState->ShaderHandle, "ProjMtx");
    DebugState->AttribLocationPosition = glGetAttribLocation(DebugState->ShaderHandle, "Position");
    DebugState->AttribLocationUV = glGetAttribLocation(DebugState->ShaderHandle, "UV");
    DebugState->AttribLocationColor = glGetAttribLocation(DebugState->ShaderHandle, "Color");
    
    glGenBuffers(1, &DebugState->VboHandle);
    glGenBuffers(1, &DebugState->ElementsHandle);
    
    glGenVertexArrays(1, &DebugState->VaoHandle);
    glBindVertexArray(DebugState->VaoHandle);
    glBindBuffer(GL_ARRAY_BUFFER, DebugState->VboHandle);
    glEnableVertexAttribArray(DebugState->AttribLocationPosition);
    glEnableVertexAttribArray(DebugState->AttribLocationUV);
    glEnableVertexAttribArray(DebugState->AttribLocationColor);
    
    glVertexAttribPointer(DebugState->AttribLocationPosition, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)IM_OFFSETOF(ImDrawVert, pos));
    glVertexAttribPointer(DebugState->AttribLocationUV, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)IM_OFFSETOF(ImDrawVert, uv));
    glVertexAttribPointer(DebugState->AttribLocationColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*)IM_OFFSETOF(ImDrawVert, col));
    
    ImGui_SDL_CreateFontsTexture();
    
    // Restore modified GL state
    glBindTexture(GL_TEXTURE_2D, LastTexture);
    glBindBuffer(GL_ARRAY_BUFFER, LastArrayBuffer);
    glBindVertexArray(LastVertexArray);
    
    return true;
}

static const char* ImGui_SDL_GetClipboardText(void*)
{
    return SDL_GetClipboardText();
}

static void ImGui_SDL_SetClipboardText(void*, const char* Text)
{
    SDL_SetClipboardText(Text);
}

static bool ImGui_SDL_Initialize(SDL_Window* Window)
{
    // Keyboard mapping. ImGui will use those indices to peek into the io.KeyDown[] array.
    ImGuiIO& io = ImGui::GetIO();
    io.KeyMap[ImGuiKey_Tab] = SDL_SCANCODE_TAB;
    io.KeyMap[ImGuiKey_LeftArrow] = SDL_SCANCODE_LEFT;
    io.KeyMap[ImGuiKey_RightArrow] = SDL_SCANCODE_RIGHT;
    io.KeyMap[ImGuiKey_UpArrow] = SDL_SCANCODE_UP;
    io.KeyMap[ImGuiKey_DownArrow] = SDL_SCANCODE_DOWN;
    io.KeyMap[ImGuiKey_PageUp] = SDL_SCANCODE_PAGEUP;
    io.KeyMap[ImGuiKey_PageDown] = SDL_SCANCODE_PAGEDOWN;
    io.KeyMap[ImGuiKey_Home] = SDL_SCANCODE_HOME;
    io.KeyMap[ImGuiKey_End] = SDL_SCANCODE_END;
    io.KeyMap[ImGuiKey_Insert] = SDL_SCANCODE_INSERT;
    io.KeyMap[ImGuiKey_Delete] = SDL_SCANCODE_DELETE;
    io.KeyMap[ImGuiKey_Backspace] = SDL_SCANCODE_BACKSPACE;
    io.KeyMap[ImGuiKey_Space] = SDL_SCANCODE_SPACE;
    io.KeyMap[ImGuiKey_Enter] = SDL_SCANCODE_RETURN;
    io.KeyMap[ImGuiKey_Escape] = SDL_SCANCODE_ESCAPE;
    io.KeyMap[ImGuiKey_A] = SDL_SCANCODE_A;
    io.KeyMap[ImGuiKey_C] = SDL_SCANCODE_C;
    io.KeyMap[ImGuiKey_V] = SDL_SCANCODE_V;
    io.KeyMap[ImGuiKey_X] = SDL_SCANCODE_X;
    io.KeyMap[ImGuiKey_Y] = SDL_SCANCODE_Y;
    io.KeyMap[ImGuiKey_Z] = SDL_SCANCODE_Z;
    
    io.SetClipboardTextFn = ImGui_SDL_SetClipboardText;
    io.GetClipboardTextFn = ImGui_SDL_GetClipboardText;
    io.ClipboardUserData = NULL;
    
    DebugState->SdlCursors[ImGuiMouseCursor_Arrow] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW);
    DebugState->SdlCursors[ImGuiMouseCursor_TextInput] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_IBEAM);
    DebugState->SdlCursors[ImGuiMouseCursor_ResizeAll] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZEALL);
    DebugState->SdlCursors[ImGuiMouseCursor_ResizeNS] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENS);
    DebugState->SdlCursors[ImGuiMouseCursor_ResizeEW] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZEWE);
    DebugState->SdlCursors[ImGuiMouseCursor_ResizeNESW] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENESW);
    DebugState->SdlCursors[ImGuiMouseCursor_ResizeNWSE] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENWSE);
    
#ifdef _WIN32
    SDL_SysWMinfo wmInfo;
    SDL_VERSION(&wmInfo.version);
    SDL_GetWindowWMInfo(Window, &wmInfo);
    io.ImeWindowHandle = wmInfo.info.win.window;
#else
    (void)Window;
#endif
    
    // NOTE(Naor): Here we initialize all the opengl stuff only once, once we created
    // them, we dont need to recreate them when hot-swapping..
    ImGui_SDL_CreateDeviceObjects();
    
    return true;
}

static void ImGui_SDL_ProcessEvent(SDL_Event* Event)
{
    ImGuiIO& io = ImGui::GetIO();
    
    switch (Event->type)
    {
        case SDL_MOUSEWHEEL:
        {
            if (Event->wheel.x > 0) io.MouseWheelH += 1;
            if (Event->wheel.x < 0) io.MouseWheelH -= 1;
            if (Event->wheel.y > 0) io.MouseWheel += 1;
            if (Event->wheel.y < 0) io.MouseWheel -= 1;
            
            break;
        }
        case SDL_MOUSEBUTTONDOWN:
        {
            if (Event->button.button == SDL_BUTTON_LEFT) DebugState->MousePressed[0] = true;
            if (Event->button.button == SDL_BUTTON_RIGHT) DebugState->MousePressed[1] = true;
            if (Event->button.button == SDL_BUTTON_MIDDLE) DebugState->MousePressed[2] = true;
            break;
        }
        case SDL_TEXTINPUT:
        {
            io.AddInputCharactersUTF8(Event->text.text);
            break;
        }
        case SDL_KEYDOWN:
        case SDL_KEYUP:
        {
            int key = Event->key.keysym.scancode;
            IM_ASSERT(key >= 0 && key < IM_ARRAYSIZE(io.KeysDown));
            io.KeysDown[key] = (Event->type == SDL_KEYDOWN);
            io.KeyShift = ((SDL_GetModState() & KMOD_SHIFT) != 0);
            io.KeyCtrl = ((SDL_GetModState() & KMOD_CTRL) != 0);
            io.KeyAlt = ((SDL_GetModState() & KMOD_ALT) != 0);
            io.KeySuper = ((SDL_GetModState() & KMOD_GUI) != 0);
            break;
        }
    }
    
    ImGuiContext* Context = ImGui::GetCurrentContext();
    DebugState->IsAnyWindowDisplayed = Context->WindowsActiveCount > 1;
    
    if(!DebugState->IsAnyWindowDisplayed || (!Context->HoveredWindow && DebugState->MousePressed[0]))
        DebugState->IsCursorActive = false;
    else if(Context->ActiveIdWindow && DebugState->IsAnyWindowDisplayed)
        DebugState->IsCursorActive = true;
}

static void ImGui_SDL_NewFrame(SDL_Window* Window)
{
    ImGuiIO& io = ImGui::GetIO();
    
    // Setup display size (every frame to accommodate for window resizing)
    int w, h;
    int display_w, display_h;
    SDL_GetWindowSize(Window, &w, &h);
    SDL_GL_GetDrawableSize(Window, &display_w, &display_h);
    io.DisplaySize = ImVec2((float)w, (float)h);
    io.DisplayFramebufferScale = ImVec2(w > 0 ? ((float)display_w / w) : 0, h > 0 ? ((float)display_h / h) : 0);
    
    // Setup time step (we don't use SDL_GetTicks() because it is using millisecond resolution)
    static Uint64 frequency = SDL_GetPerformanceFrequency();
    Uint64 current_time = SDL_GetPerformanceCounter();
    io.DeltaTime = DebugState->Time > 0 ? (float)((double)(current_time - DebugState->Time) / frequency) : (float)(1.0f / 60.0f);
    DebugState->Time = current_time;
    
    // NOTE(Naor): We only want to update input if we have a cursor active
    if(DebugState->IsCursorActive)
    {
        // Setup mouse inputs (we already got mouse wheel, keyboard keys & characters from our event handler)
        int mx, my;
        Uint32 mouse_buttons = SDL_GetMouseState(&mx, &my);
        io.MousePos = ImVec2(-FLT_MAX, -FLT_MAX);
        io.MouseDown[0] = DebugState->MousePressed[0] || (mouse_buttons & SDL_BUTTON(SDL_BUTTON_LEFT)) != 0;  // If a mouse press event came, always pass it as "mouse held this frame", so we don't miss click-release events that are shorter than 1 frame.
        io.MouseDown[1] = DebugState->MousePressed[1] || (mouse_buttons & SDL_BUTTON(SDL_BUTTON_RIGHT)) != 0;
        io.MouseDown[2] = DebugState->MousePressed[2] || (mouse_buttons & SDL_BUTTON(SDL_BUTTON_MIDDLE)) != 0;
        DebugState->MousePressed[0] = DebugState->MousePressed[1] = DebugState->MousePressed[2] = false;
        
        // We need to use SDL_CaptureMouse() to easily retrieve mouse coordinates outside of the client area. This is only supported from SDL 2.0.4 (released Jan 2016)
#if (SDL_MAJOR_VERSION >= 2) && (SDL_MINOR_VERSION >= 0) && (SDL_PATCHLEVEL >= 4)   
        if ((SDL_GetWindowFlags(Window) & (SDL_WINDOW_MOUSE_FOCUS | SDL_WINDOW_MOUSE_CAPTURE)) != 0)
            io.MousePos = ImVec2((float)mx, (float)my);
        bool any_mouse_button_down = false;
        for (int n = 0; n < IM_ARRAYSIZE(io.MouseDown); n++)
            any_mouse_button_down |= io.MouseDown[n];
        if (any_mouse_button_down && (SDL_GetWindowFlags(Window) & SDL_WINDOW_MOUSE_CAPTURE) == 0)
            SDL_CaptureMouse(SDL_TRUE);
        if (!any_mouse_button_down && (SDL_GetWindowFlags(Window) & SDL_WINDOW_MOUSE_CAPTURE) != 0)
            SDL_CaptureMouse(SDL_FALSE);
#else
        if ((SDL_GetWindowFlags(Window) & SDL_WINDOW_INPUT_FOCUS) != 0)
            io.MousePos = ImVec2((float)mx, (float)my);
#endif
        
    }
    else
    {
        io.MousePos = ImVec2(-FLT_MAX, -FLT_MAX);
        io.MouseDown[0] = false;  // If a mouse press event came, always pass it as "mouse held this frame", so we don't miss click-release events that are shorter than 1 frame.
        io.MouseDown[1] = false;
        io.MouseDown[2] = false;
        DebugState->MousePressed[0] = DebugState->MousePressed[1] = DebugState->MousePressed[2] = false;
    }
    
    // Hide OS mouse cursor if ImGui is drawing it
    ImGuiMouseCursor cursor = ImGui::GetMouseCursor();
    if (io.MouseDrawCursor || cursor == ImGuiMouseCursor_None)
    {
        SDL_ShowCursor(0);
    }
    else
    {
        SDL_SetCursor(DebugState->SdlCursors[cursor]);
        SDL_ShowCursor(1);
    }
    
    // Start the frame. This call will update the io.WantCaptureMouse, io.WantCaptureKeyboard flag that you can use to dispatch inputs (or not) to your application.
    ImGui::NewFrame();
}