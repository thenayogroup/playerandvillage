#include "Recording.h"

namespace Recording
{
    bool IsRecording(s32 Slot)
    {
        return RecordMemorySlots[Slot].IsRecording;
    }
    
    bool IsThereRecordingInProgress()
    {
        for(s32 Slot = 0; Slot < NUM_OF_RECORDING_SLOTS; ++Slot)
        {
            if(IsRecording(Slot))
            {
                return true;
            }
        }
        return false;
    }
    
    // NOTE: We dont want a reference to the events!
    void RecordInput(std::queue<SDL_Event> GameInput)
    {
        for(s32 Slot = 0; Slot < NUM_OF_RECORDING_SLOTS; ++Slot)
        {
            if(IsRecording(Slot))
            {
                // If the memory already has NOT been initialized it
                if(!RecordMemorySlots[Slot].IsInitialized)
                {
                    RecordMemorySlots[Slot].PlayingMemoryBlock = VirtualAlloc(0, MainMemory.SizeInBytes,
                                                                              MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
                    RecordMemorySlots[Slot].MemoryBlock = VirtualAlloc(0, MainMemory.SizeInBytes,
                                                                       MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
                    CopyMemory(RecordMemorySlots[Slot].MemoryBlock, MainMemory.MemoryBlock, MainMemory.SizeInBytes);
                    CopyMemory(RecordMemorySlots[Slot].PlayingMemoryBlock, MainMemory.MemoryBlock, MainMemory.SizeInBytes);
                    
                    RecordMemorySlots[Slot].IsInitialized = true;
                }
                
                RecordMemorySlots[Slot].InputRecord.push_back(GameInput);
                
                // NOTE(Naor): This should return SDL_NUM_SCANCODES but we stick with 
                // this for now, maybe they will change that. (Remember that we use SDL_NUM_SCANCODES
                // in the array length so be careful!)
                
                // We put static here because this will be only in main.cpp and it will not change (the pointer)
                s32 InputKeyLength;
                const u8* CurrentContinuousInput = SDL_GetKeyboardState(&InputKeyLength);
                
                std::array<u8, SDL_NUM_SCANCODES> NewContinuousInput = {};
                
                // copy the content of the keyboard state to a new container
                for(s32 Index = 0; Index < InputKeyLength; ++Index)
                {
                    NewContinuousInput[Index] = CurrentContinuousInput[Index];
                }
                
                // And save the container.
                RecordMemorySlots[Slot].InputRecordContinuous.push_back(NewContinuousInput);
                RecordMemorySlots[Slot].InputSize++;
            }
        }
    }
    
    void BeginRecord(s32 Slot)
    {
        RecordMemorySlots[Slot].IsRecording = true;
    }
    
    void EndRecord(s32 Slot)
    {
        RecordMemorySlots[Slot].IsRecording = false;
    }
    
    void RestartPlayback(s32 Slot)
    {
        if(RecordMemorySlots[Slot].MemoryBlock != nullptr)
        {
            Recording::JustStartedPlayback = true;
            CopyMemory(RecordMemorySlots[Slot].PlayingMemoryBlock, RecordMemorySlots[Slot].MemoryBlock, MainMemory.SizeInBytes);
            RecordMemorySlots[Slot].InputPlayIndex = 0;
        }
    }
    
    std::queue<SDL_Event> NextRecordInput(s32 Slot)
    {
        s32 Index = RecordMemorySlots[Slot].InputPlayIndex;
        
        // If we made it here, we need to restart the recording
        if(Index == RecordMemorySlots[Slot].InputSize)
        {
            Index = 0;
            RestartPlayback(Slot);
        }
        
        return RecordMemorySlots[Slot].InputRecord[Index];
    }
    
    u8* NextRecordInputContinuous(s32 Slot)
    {
        s32 Index = RecordMemorySlots[Slot].InputPlayIndex;
        
        // If we made it here, we need to restart the recording
        if(Index == RecordMemorySlots[Slot].InputSize)
        {
            Index = 0;
            RestartPlayback(Slot);
        }
        
        return RecordMemorySlots[Slot].InputRecordContinuous[Index].data();
    }
    
    void NextPlayIndex(s32 Slot)
    {
        RecordMemorySlots[Slot].InputPlayIndex++;
    }
    
    void* GetMemoryBlock(s32 Slot)
    {
        return RecordMemorySlots[Slot].PlayingMemoryBlock;
    }
    
    bool IsInitialized(s32 Slot)
    {
        return RecordMemorySlots[Slot].IsInitialized;
    }
    
    void RemoveRecord(s32 Slot)
    {
        if(RecordMemorySlots[Slot].IsInitialized)
        {
            RecordMemorySlots[Slot].IsInitialized = false;
            
            VirtualFree(RecordMemorySlots[Slot].MemoryBlock, 0, MEM_RELEASE);
            VirtualFree(RecordMemorySlots[Slot].PlayingMemoryBlock, 0, MEM_RELEASE);
            
            RecordMemorySlots[Slot].MemoryBlock = nullptr;
            RecordMemorySlots[Slot].InputRecord.clear();
            RecordMemorySlots[Slot].InputRecordContinuous.clear();
            RecordMemorySlots[Slot].InputPlayIndex = 0;
            RecordMemorySlots[Slot].InputSize = 0;
        }
    }
    
    void InputHandler(const SDL_Event& Event)
    {
        if(!ShouldPlay)
        {
            // Record/Stop using CTRL+#
            if(Event.key.keysym.mod & KMOD_CTRL &&
               Event.key.keysym.sym >= SDLK_1 && Event.key.keysym.sym <= SDLK_9)
            {
                s32 Slot = Event.key.keysym.sym - SDLK_1;
                
                if(IsRecording(Slot))
                {
                    std::cout << "Stopped recording slot #" << Slot << "\n";
                    EndRecord(Slot);
                }
                else
                {
                    std::cout << "Started recording slot #" << Slot << "\n";
                    BeginRecord(Slot);
                }
                
            }
        }
        
        // Delete using ALT+#
        if(Event.key.keysym.mod & KMOD_ALT &&
           Event.key.keysym.sym >= SDLK_1 && Event.key.keysym.sym <= SDLK_9)
        {
            s32 Slot = Event.key.keysym.sym - SDLK_1;
            if(Slot == CurrentlyPlaying)
            {
                std::cout << "Stopped playing slot #" << CurrentlyPlaying << "\n";
                ShouldPlay = false;
                CurrentlyPlaying = -1;
            }
            
            if(IsRecording(Slot))
            {
                std::cout << "Stopped recording slot #" << Slot << "\n";
                Recording::EndRecord(Slot);
            }
            
            if(IsInitialized(Slot))
            {
                std::cout << "Deleted slot #" << Slot << "\n";
                RemoveRecord(Slot);
            }
            
        }
        
        // Only playback if there is no recording in progress
        if(!IsThereRecordingInProgress())
        {
            // Playback using SHIFT+#
            if(Event.key.keysym.mod & KMOD_SHIFT &&
               Event.key.keysym.sym >= SDLK_1 && Event.key.keysym.sym <= SDLK_9)
            {
                
                s32 Slot = Event.key.keysym.sym - SDLK_1;
                if(!IsRecording(Slot))
                {
                    CurrentlyPlaying = Slot;
                    std::cout << "Started playing on slot " << CurrentlyPlaying << "\n";
                    RestartPlayback(CurrentlyPlaying);
                    ShouldPlay = true;
                }
            }
        }
        
        if(ShouldPlay)
        {
            // Stop ANY playback
            if(Event.key.keysym.sym == SDLK_p)
            {
                std::cout << "Stopped playing slot #" << CurrentlyPlaying << "\n";
                ShouldPlay = false;
                CurrentlyPlaying = -1;
            }
        }
    }
};
