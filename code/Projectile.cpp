#include "Projectile.h"

void UpdateAndRenderProjectile(r32 Delta, projectile& Projectile)
{
    TIMED_FUNCTION();
    // TODO(Naor): We need to remove the projectile if it is not alive
    if(Projectile.IsAlive)
    {
        r32 t = Projectile.Timestep;
        v3 OldPosition = Projectile.Position;
        Projectile.Position = Projectile.StartPosition + Projectile.StartVelocity * t;
        Projectile.Position.y -= 0.5f * Gravity * t * t;
        Projectile.Timestep += Delta;
        Projectile.Direction = Math::Normalize(OldPosition - Projectile.Position);
        
        DEBUG_POINT(Projectile.Position, Color::Red);
        
        // TODO(Naor): This needs to change when we introduce new projectiles
        const v3 Half = V3(0.5f);
        Projectile.BoundingBox = AABB(Projectile.Position - Half, Projectile.Position + Half);
        
        
        for(entity& E : GameState->Entities)
        {
            if(E.Type == EntityType_Golem)
            {
                if(CheckAABB(Projectile.BoundingBox, E.BoundingBox))
                {
                    Projectile.IsAlive = false;
                    E.Health -= Projectile.Power;
                }
            }
        }
    }
    else
    {
    }
}

