#pragma once 

static constexpr r32 Gravity = 9.8f*2.0f;

struct projectile
{
    aabb BoundingBox;
    v3 Position;
    v3 Direction;
    v3 Velocity;
    r32 Power;
    r32 Timestep = 0.0f;
    bool IsAlive = true;
    
    v3 StartPosition;
    v3 StartVelocity;
};


void UpdateAndRenderProjectile(r32 Delta, projectile& Projectile);