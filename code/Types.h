#pragma once

namespace Math
{
    class vec2;
    class vec3;
    class vec4;
    class mat2;
    class mat3;
    class mat4;
    class quat;
};

using int64 = int64_t;
using uint64 = uint64_t;
using int32 = int32_t;
using uint32 = uint32_t;
using int16 = int16_t;
using uint16 = uint16_t;
using int8 = int8_t;
using uint8 = uint8_t;

// TODO(Naor): Learn more about those two!
// casey seem to be using them quite a bit (umm and smm)
using uintptr = uintptr_t;
using sintptr = intptr_t;

using real64 = double;
using real32 = float;

using bool32 = int32;

using vec2 = Math::vec2;
using vec3 = Math::vec3;
using vec4 = Math::vec4;

using point2 = Math::vec2;
using point3 = Math::vec3;
using point4 = Math::vec4;

using mat2 = Math::mat2;
using mat3 = Math::mat3;
using mat4 = Math::mat4;

using quat = Math::quat;

struct gl_version
{
    int32 Major;
    int32 Minor;
};

// Shorts

using s64 = int64_t;
using u64 = uint64_t;
using s32 = int32_t;
using u32 = uint32_t;
using s16 = int16_t;
using u16 = uint16_t;
using s8 = int8_t;
using u8 = uint8_t;

using uptr = uintptr_t;
using sptr = intptr_t;
using ptrdiff = ptrdiff_t;

using r64 = double;
using r32 = float;

using b32 = int32;

using v2 = Math::vec2;
using v3 = Math::vec3;
using v4 = Math::vec4;

using p2 = Math::vec2;
using p3 = Math::vec3;
using p4 = Math::vec4;

using rgb = Math::vec3;

using m2 = Math::mat2;
using m3 = Math::mat3;
using m4 = Math::mat4;
