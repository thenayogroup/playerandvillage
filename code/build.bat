@echo off

REM TODO: 
REM Remove the -DNOMIXMAX and maybe the CRT_NO_WARNINGS

REM call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" x64

set CommonCompilerFlags=-MT -nologo -EHa- -WX -W4 -fp:fast -Od -Oi -FC -Z7 -wd4100 -wd4127 -wd4201 -wd4189 -wd4459 -wd4505
set CommonCompilerFlags=-D_HAS_EXCEPTIONS=0 -DNOMINMAX -D_CRT_SECURE_NO_WARNINGS %CommonCompilerFlags%
set CommonCompilerFlags=-DNAYO_DEBUG -DNAYO_INTRINSIC -DNAYO_WIN32 %CommonCompilerFlags%
set CommonLinkerFlags=-incremental:no  user32.lib gdi32.lib winmm.lib opengl32.lib SDL2.lib SDL2main.lib OpenAL32.lib
set CommonIncludeFlags=-I ..\deps\SDL\include -I ..\deps\gl3w\include -I ..\deps\OpenAL\include -I ..\deps\imgui

set AddLibsx64=-LIBPATH:..\deps\SDL\VisualC\x64\Release -LIBPATH:..\deps\OpenAL\libs\Win64
set AddLibsx32=-LIBPATH:..\deps\SDL\VisualC\Win32\Release -LIBPATH:..\deps\OpenAL\libs\Win32
set CustomPDB=/PDB:"Game_%date:~-4,4%%date:~-10,2%%date:~-7,2%_%time:~0,2%%time:~3,2%%time:~6,2%.pdb"

IF NOT EXIST ..\build mkdir ..\build
pushd ..\build

del *.pdb > NUL 2> NUL

REM 32-bit build
REM cl %CommonCompilerFlags% %CommonIncludeFlags% ..\code\main.cpp /link -subsystem:windows,5.1 %CustomPDB% %AddLibsx32% %%CommonLinkerFlags%

REM 64-bit build
echo WAITING FOR PDB > lock.tmp
cl %CommonCompilerFlags% %CommonIncludeFlags% ..\code\Game.cpp -LD /link -subsystem:console %AddLibsx64% %CustomPDB% %AddLibsx64% %CommonLinkerFlags%
cl %CommonCompilerFlags% %CommonIncludeFlags% ..\code\main.cpp /link -subsystem:console %AddLibsx64% %CommonLinkerFlags%
del lock.tmp
popd