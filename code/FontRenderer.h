#pragma once

constexpr s32 BITMAP_SIZE = 1024;
constexpr r32 FONT_MAX_SIZE = 128.0f;

#ifdef NAYO_DEBUG
#define DEBUG_DRAW_TEXT(font_info, x, y, color, size,text) ImmDrawText(GameState->FontRenderer, GameState->Ortho, ##font_info, ##x, ##y, ##color, ##size, ##text)
#else
#define DEBUG_DRAW_TEXT(font_info, x, y, color, size,text)
#endif

/*

TODO LIST:

      1. Work on functions like GetHeightInPixels & GetWidthInPixels
   2. Make the font_info render work on main.cpp
  3. What to do about the blending, because it's not sorted
   4. Take a look at what options Jon has
   
  5. Colorize each word like in fortnite 
  6. bold, italic ..
   7. Maybe put enum for the font_info type names
  9. !!Change the system to the shippable version of stb
  10. !Distance field like Valve
  11. !!When calling to DrawText try to send less parameters( lose the ortho etc..)
   12. Check if the DrawText of reference_frame is working
   13. Clean up
   
14. 3D Text
15. How to pack all the ttf of font family
16. There is a defect in the fonts in some sizes
*/

static const char* FontRender2DShaderVertSrc =R"(
#version 410 core

layout(location = 0) in vec2 Position;
layout(location = 1) in vec4 Color;
layout(location = 2) in vec2 TexCoords;

out VertexData
{
vec2 Position;
vec4 Color;
vec2 TexCoords;
} OutVert;

uniform mat4 Projection;
uniform mat3 Model;

void main()
{
gl_Position = Projection * vec4(Model*vec3(Position, 1.0f), 1.0f);
OutVert.Position = gl_Position.xy;
OutVert.Color = Color;
OutVert.TexCoords = TexCoords;
}


)";

static const char* FontRender2DShaderFragSrc =R"(
#version 410 core

in VertexData   
{
vec2 Position;
vec4 Color;
vec2 TexCoords;
}
InFrag;

uniform sampler2D Texture;

out vec4 OutColor;

void main()
{
vec4 TexColor = texture(Texture, InFrag.TexCoords);

OutColor = vec4(TexColor.r, TexColor.r, TexColor.r, TexColor.r);

OutColor *=  InFrag.Color;

//OutColor = vec4(InFrag.Color);

if(OutColor.a < 0.1)
discard;
}

)";


struct font_info
{
    stbtt_bakedchar Data[96];// ASCII 32..126 is 95 glyphs
    u32 Texture;
    r32 MaxHeight;
};

struct font_renderer
{
    std::vector<r32> Glyphs;
    mesh Mesh;
    shader_program Shader;
};

struct reference_frame
{
    v2 TopLeft;
    v2 Size;
};

