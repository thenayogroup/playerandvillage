#pragma once

// TODO(Naor): Change all the mallocs to our allocation system.
// make sure the allocation system wont just allocate memory for every string
// and such, but create a memory pool that we can pool blocks from.

// TODO(Naor): The shader live update is broken with the new system, fix it!

// TODO(Naor): Most of this code is shit, rewrite it.

// TODO(Naor): There is allocations in the code but we NEVER deallocate, this may be ok because
// we probably don't want to free any shaders in the runtime...
// TODO(Jonathan): Consider enum Type out of class shader_program
class shader_program
{
    public:
    
    enum type : GLenum
    {
        Vertex = GL_VERTEX_SHADER,
        Fragment = GL_FRAGMENT_SHADER,
        Geometry = GL_GEOMETRY_SHADER,
        Compute = GL_COMPUTE_SHADER,
        TessControl = GL_TESS_CONTROL_SHADER,
        TessEvaluation = GL_TESS_EVALUATION_SHADER,
        Multiple,
        
        ShaderType_None // TODO(Naor): We cant just call this "None" because this a global enum.. change this!
    };
    
    struct shader
    {
        b32 Exists;
        type Type;
        char* Filename;
        FILETIME LastWriteTime;
    };
    
    void AddShader(type ShaderType, const char* ShaderPath);
    void AddShaderBySource(type ShaderType, const char* ShaderSource);
    void Compile();
    void Use();
    void Delete();
    
    void UpdateIfModified();
    void Recompile();
    void SetSingleFileShader(const char* Filename);
    
    private:
    std::string GetShaderSource(const char* ShaderPath) const;
    enum type_index : s32
    {
        Fragment_Index,
        Vertex_Index,
        Geometry_Index,
        Compute_Index,
        TessControl_Index,
        TessEvaluation_Index,
        
        NUM_OF_SHADER_TYPE_COUNT
    };
    inline type_index GetTypeIndex(type ShaderType);
    
    public:
    u32 Id;
    s32 Uniforms[NUM_OF_SHADER_UNIFORMS];
    
    private:
    u32 Shaders[NUM_OF_SHADER_TYPE_COUNT];
    shader ShaderFiles[NUM_OF_SHADER_TYPE_COUNT];
};