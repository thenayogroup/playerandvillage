#pragma once

#include "ObjAssetLoader.h"

struct mesh
{
    // TODO(Jonathan): Does aabb should be in here?
    aabb BoundingBox;
    
    u32 Count;
    u32 Vao;
    u32 Ebo;
    u32 Vbo;
};


#if 0
struct mesh_aabb
{
    aabb BoundingBox;
    u32 Count;
    u32 Vao;
    u32 Ebo;
    u32 Vbo;
};
#endif

// Forward declaration
struct render_group;
static void RenderModel(render_group& RenderGroup, shader_program& Shader, mesh& Mesh, s32 Texture,
                        const m4& Projection, const m4& ModelMatrix, const v4& Color = V4(0.0f, 0.0f, 0.0f, 1.0f));
