#pragma once

static void InitCrosshairData(mesh& Mesh)
{
    r32 vertices[] =
    {
        
        0.0f, 0.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 1.0f,
        
        1.0f, 0.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 1.0f
    };
    
    u32 QuadBuffer;
    glGenVertexArrays(1, &Mesh.Vao);
    glGenBuffers(1, &QuadBuffer);
    
    glBindBuffer(GL_ARRAY_BUFFER, QuadBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    
    glBindVertexArray(Mesh.Vao);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0, 2, GL_FLOAT, false, 4 * sizeof(r32), nullptr);
    glVertexAttribPointer(1, 2, GL_FLOAT, false, 4 * sizeof(r32), reinterpret_cast<void *>(sizeof(r32) * 2));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    Mesh.Count = 6;
}


static void RenderSprite(shader_program& Shader, mesh& Mesh, u32 Texture, const v2& Position, const v2& Size,
                         r32 Angle = 0.0f, const v3& Color = v3(), bool Flip = false)
{
    m4 ModelMatrix = M4();
    ModelMatrix = Translate(ModelMatrix, V3(Position, 0.0f));
    ModelMatrix *= Math::YRotation(Angle);
    ModelMatrix = Scale(ModelMatrix, V3(Size, 1.0f));
    
    material Material = {};
    Material.Shader = &Shader;
    SetMatrix(Material, "Projection", GameState->Ortho, SHADER_UNIFORM_PROJECTION_INDEX);
    SetMatrix(Material, "Model", ModelMatrix, SHADER_UNIFORM_MODEL_INDEX);
    
    PushRender(GameState->RenderGroup, Material, &Mesh, Texture, RenderDrawType_Arrays);
}
