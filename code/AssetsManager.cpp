#include "AssetsManager.h"
#include "Assets.h"

static void InitializeAssets(assets& Assets)
{
    
    // Models
    LoadModel(Assets.Mesh_Dave, "models/Dave.obj");
    LoadModel(Assets.Mesh_Farmhouse, "models/House.obj");
    LoadModel(Assets.Mesh_Cottage, "models/cottage_mesh.obj");
    LoadModel(Assets.Mesh_Wall, "models/wall.obj");
    LoadModel(Assets.Mesh_Outpost, "models/Outpost2.obj");
    LoadModel(Assets.Mesh_Tree, "models/tree.obj");
    LoadModel(Assets.Mesh_Bush, "models/bush.obj");
    LoadModel(Assets.Mesh_Rock, "models/Rock.obj");
    LoadModel(Assets.Mesh_Golem, "models/Bone_Golem.obj");
    LoadModel(Assets.Mesh_Tree1, "models/Tree1.obj");
    LoadModel(Assets.Mesh_Tree2, "models/Tree2.obj");
    LoadModel(Assets.Mesh_Tree3, "models/Tree3.obj");
    LoadModel(Assets.Mesh_Trunk, "models/trunk2.obj");
    LoadModel(Assets.Mesh_Stone, "models/stone.obj");
    LoadModel(Assets.Mesh_Steel, "models/steel.obj");
    LoadModel(Assets.Mesh_FireCamp, "models/firecamp.obj");
    LoadQuadMesh(Assets.Mesh_Quad);
    
    // Textures
    Assets.Texture_Null = GL_TEXTURE_NULL_ID;
    LoadTexture(Assets.Texture_Dave, "textures/Character Texture.png");
    LoadTexture(Assets.Texture_Farmhouse, "textures/house_tex.png");
    LoadTexture(Assets.Texture_Cottage, "textures/cottage_diffuse.png");
    LoadTexture(Assets.Texture_Wall, "textures/Cottage Texture.jpg");
    LoadTexture(Assets.Texture_Golem, "textures/Bone_Golem_Diffuse.png");
    LoadTexture(Assets.Texture_Outpost, "textures/Outpost.png");
    LoadTexture(Assets.Texture_Crosshair, "textures/crosshair2.png");
    LoadTexture(Assets.Texture_Tree, "textures/TreeTexture.png");
    LoadTexture(Assets.Texture_Tree2, "textures/color_g.png");
    LoadTexture(Assets.Texture_Terrain, "textures/TerrainTexture.png");
    LoadTexture(Assets.Texture_Grass, "textures/grass.png");
    LoadTexture(Assets.Texture_InventoryItem, "textures/gui/items/inventory_item.png");
    LoadTexture(Assets.Texture_StoneSlot, "textures/gui/items/stone_slot.png");
    LoadTexture(Assets.Texture_WoodSlot, "textures/gui/items/wood_slot.png");
    LoadTexture(Assets.Texture_SteelSlot, "textures/gui/items/steel_slot.png");
    LoadTexture(Assets.Texture_FlintSlot, "textures/gui/items/flint_slot.png");
    LoadTexture(Assets.Texture_FireStoneSlot, "textures/gui/items/firestone_slot.png");
    LoadTexture(Assets.Texture_CampFireSlot, "textures/gui/items/campfire_slot.png");
    LoadTexture(Assets.Texture_CampFire, "textures/CampFire.png");
    
    // Shaders
    for(s32 ShaderIndex = 0; 
        ShaderIndex < NUM_OF_SHADERS;
        ++ShaderIndex)
    {
        ParseAndCompileShader(Assets.Shaders[ShaderIndex], AssetsLookup::Shaders[ShaderIndex]);
    }
    
    // Audio
    LoadWav("audio/Deep Bass.wav", Assets.Audio_DeepBass);
    LoadWav("audio/Bell.wav", Assets.Audio_Bell);
    LoadWav("audio/Wabble.wav", Assets.Audio_Wabble);
    LoadWav("audio/rlaunch.wav", Assets.Audio_RLaunch);
    
    LoadFont(Assets.TimesFont, "fonts/times.ttf");
    LoadFont(Assets.ArialFont, "fonts/arial.ttf");
    LoadFont(Assets.RobotoFont, "fonts/Roboto-Black.ttf");
    
}

// TODO(Naor): Change from using c lib to platform functions (reading files)
// + make sure the files are read correctly
static void ParseAndCompileShader(shader_program& ShaderProgram, const char* Filepath)
{
    FILE* FileHandle = fopen(Filepath, "r");
    Assert(FileHandle);
    
    fseek(FileHandle, 0, SEEK_END);
    const s32 FileSize = ftell(FileHandle);
    fseek(FileHandle, 0, SEEK_SET);
    
    // NOTE(Naor): We multiplied the allocation by 2 because we want
    // to use the additional space to store the decompressed source
    // in seperate strings.
    // (And the + 6 is for null-terminated string for each shader just in case)
    scoped_memory_block Memory((FileSize + 6) * 2);
    char* FileSource = (char*)Memory.Base();
    
    size_t ObjectsRead = fread(FileSource, FileSize, 1, FileHandle);
    FileSource[FileSize] = 0;
    Memory.Memory->Used += FileSize;
    
    fclose(FileHandle);
    
    // TODO(Naor): Check if we remove the last '\n' from the GLSL it still works.
    bool ReadFinish = false;
    while(!ReadFinish)
    {
        shader_program::type CurrentShaderType = shader_program::type::ShaderType_None;
        if(Utility::CStringEqualWithoutCase(FileSource, "#shader vertex"))
        {
            CurrentShaderType = shader_program::type::Vertex;
        }
        else if(Utility::CStringEqualWithoutCase(FileSource, "#shader fragment"))
        {
            CurrentShaderType = shader_program::type::Fragment;
        }
        else if(Utility::CStringEqualWithoutCase(FileSource, "#shader geometry"))
        {
            CurrentShaderType = shader_program::type::Geometry;
        }
        else if(Utility::CStringEqualWithoutCase(FileSource, "#shader compute"))
        {
            CurrentShaderType = shader_program::type::Compute;
        }
        else if(Utility::CStringEqualWithoutCase(FileSource, "#shader tesscontrol"))
        {
            CurrentShaderType = shader_program::type::TessControl;
        }
        else if(Utility::CStringEqualWithoutCase(FileSource, "#shader tesseval"))
        {
            CurrentShaderType = shader_program::type::TessEvaluation;
        }
        else
        {
            // NOTE(Naor): If we didn't find any shader information, we must be in
            // a comment or something, just skip this..
            while(*FileSource++ != '\n');
            continue;
        }
        // NOTE(Naor): In case we found a new shader, skip the "#shader ..." line
        while(*FileSource++ != '\n');
        
        char* CurrentShaderSource = ((char*)Memory.Base()) + Memory.Memory->Used;
        uptr CurrentlyUsedMemory = 0;
        while(!Utility::CStringEqualFirstContains(FileSource, "#shader"))
        {
            *CurrentShaderSource++ = *FileSource++;
            CurrentlyUsedMemory += sizeof(char);
        }
        *CurrentShaderSource = 0;
        Memory.Memory->Used += CurrentlyUsedMemory;
        CurrentShaderSource -= CurrentlyUsedMemory; // Back to the base so we can read the string
        
        ShaderProgram.AddShaderBySource(CurrentShaderType, CurrentShaderSource);
        
        if(Utility::CStringEqualFirstContains(FileSource, "#shader end"))
        {
            ReadFinish = true;
        }
    }
    ShaderProgram.SetSingleFileShader(Filepath);
    ShaderProgram.Compile();
}


static bool Test4Chars(const char* String, void* Data)
{
    // memcmp return 0 if all is equal so we want to negate it
    return (!memcmp(String, Data, 4));
}

static bool LoadWav(const char* Filepath, sound_buffer& Buffer)
{
    // The pack make sure there is no padding (at all!)
#pragma pack(push, 1)
    typedef struct
    {
        u16 FormatTag;
        u16 Channels;
        u32 SamplesPerSec;
        u32 AvgBytesPerSec;
        u16 BlockAlign;
        u16 BitsPerSample;
        u16 Size;
        u16 ValidBitsPerSample;
        u32 ChannelMask;
        u8 SubFormat[18];
    } WavFormat;
#pragma pack(pop)
    
    char* Data = nullptr;
    FILE* File = fopen(Filepath, "rb");
    if(File)
    {
        fseek(File, 0, SEEK_END);
        s32 SizeNum = ToInt32(ftell(File));
        fseek(File, 0, SEEK_SET);
        // TODO(Naor): Change malloc to something that we allocated
        Data = (char*)malloc(SizeNum);
        fread(Data, SizeNum, 1, File);
        fclose(File);
        
        char* DataOffset = Data;
        
        if(!Test4Chars("RIFF", DataOffset) || !Test4Chars("WAVE", DataOffset + 8))
        {
            std::cout << "[Error:Sound] The file " << Filepath << " has incorrect header, is this a WAV file?\n";
            return false;
        }
        
        DataOffset += 12;
        
        if(!Test4Chars("fmt ", DataOffset))
        {
            std::cout << "[Error:Sound] WavFormat chunk not found.\n";
            return false;
        }
        
        WavFormat Format;
        Format = *(WavFormat*)(DataOffset+8);
        
        if(Format.FormatTag != 1)
        {
            std::cout << "[Error:Sound] Only PCM WAV file are supported.\n";
            return false;
        }
        
        if(Format.Channels != 1 && Format.Channels != 2)
        {
            std::cout << "[Error:Sound] Only Mono or Stereo supported (too many channels detected)\n";
            return false;
        }
        
        if(Format.BitsPerSample != 16)
        {
            std::cout << "[Error:Sound] Only 16-bit per sample suported\n";
            return false;
        }
        
        if(Format.BlockAlign != Format.Channels * 2) //What does this mean??
        {
            std::cout << "[Error:Sound] Implementation Error\n";
            return false;
        }
        
        u32 Size = *(u32*)(DataOffset+4);
        Size = (Size+1) & ~1;
        DataOffset = DataOffset + 8 + Size;
        
        if(!Test4Chars("data", DataOffset))
        {
            std::cout << "[Error:Sound] Data chunk not found.\n";
            return false;
        }
        
        s32 SampleSize = *((u32*)(DataOffset+4));
        s32 SampleCount = SampleSize / (Format.Channels * sizeof(u16));
        Buffer.SampleCount = SampleCount;
        Buffer.ChannelCount = Format.Channels;
        
        // TODO(Naor): Should we allcate all the buffers once and use them?
        if(Buffer.ID == 0)
            alGenBuffers(1, &Buffer.ID);
        
        ALenum ALFormat;
        if (Format.Channels == 1)
        {
            if (Format.BitsPerSample == 8)
            {
                ALFormat = AL_FORMAT_MONO8;
            }
            else
            {
                ALFormat = AL_FORMAT_MONO16;
            }
        }
        else
        {
            if (Format.BitsPerSample == 8)
            {
                ALFormat = AL_FORMAT_STEREO8;
            }
            else
            {
                ALFormat = AL_FORMAT_STEREO16;
            }
        }
        
        alBufferData(Buffer.ID, ALFormat, DataOffset, SampleSize, Format.SamplesPerSec);
        
        free(Data);
        
        return true;
    }
    
    std::cout << "[Error:Sound] Couldn't open the file " << Filepath << "\n";
    return false;
}