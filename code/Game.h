#pragma once

// Engine
#include "gl3w.c"
#include <SDL.h>

#define STB_IMAGE_IMPLEMENTATION
#include "../deps/stb/stb_image.h"

#define STB_PERLIN_IMPLEMENTATION
#include "../deps/stb/stb_perlin.h"

#define STB_TRUETYPE_IMPLEMENTATION
#include "../deps/stb/stb_truetype.h"

#define STB_SPRINTF_IMPLEMENTATION 
#include "../deps/stb/stb_sprintf.h"

#ifdef NAYO_DEBUG
#include <imgui_draw.cpp>
#include <imgui.cpp>
#include <imgui_demo.cpp>
#endif

#include <iostream>
#include <queue>
#include <vector>

#include "Types.h"
#include "Macros.h"
#include "Math/Math.h"
#include "platform/Platform.h"
#include "Utility.h"
#include "Assets.h"
#include "ShaderProgram.h"

//
// Debug stuff
//
// TODO(Naor): When building for release, make sure these aren't included!
#include "debug/sdl_imgui_implementation.cpp"
#include "debug/ImGui.h"

#include "Memory.h"
#include "array.h"
#include "Audio.h"
#include "AABB.h"
#include "ShaderProgram.h"
#include "VertexFormats.h"
#include "Camera.cpp"
#include "Skybox.h"
#include "Colors.h"
#include "TriangleMesh.h"
#include "FontRenderer.h"
#include "AssetsManager.h"
#include "Renderer.h"
#include "RendererEvan.h"

// NOTE(Jonathan): Not good place for Inventory because it's not part of the 'generic' engine
#include "Inventory.h"
#include "MousePicker.h"
#include "Entity.h"

#include "debug/DebugPrimitives.h"

// Gameplay
#include "World.h"
#include "Projectile.h"

#define Nayo_Export extern "C" __declspec(dllexport)

// Function Declerations
static void ContinuousInput();
static void GameInputHandler(std::queue<SDL_Event>& GameInput);

// Enums
enum screen_state : int8
{
    InGame,
    MainMenu
};

static constexpr real32 TileDimension = 2.5f;
static constexpr real32 NumOfPlacingTiles = 5.0f;

static constexpr real32 PLANE_WIDTH = 250.0f;
static constexpr real32 PLANE_DEPTH = 250.0f;

#define MAX_ENTITIES 4096

struct tile_coordinate
{
    // TODO(Jonathan): Turn this to vec3i when we'll have one
    s32 X;
    s32 Z;
    v3 RelWorld;
};

// Variables in memory
struct game_state
{
    SDL_Window* Window;
    screen_state ScreenState;
    camera MainCamera;
    frustum Frustum;
    m4 MVP;
    m4 Ortho;
    assets Assets;
    audio_manager Audio;
    
    render_group RenderGroup;
    bool DrawCircle;
    v3 CirclePos;
    
    mesh CrosshairMesh;
    font_renderer FontRenderer;
    mouse_picker MousePicker;
    
    world World;
    
    u32 EntitiesCount;
    entity Entities[MAX_ENTITIES];
    
    entity* Player;
    entity* PlacingBuilding;
    u32 SoldierIndex;
    
    selected_entity SelectedEnv;
    selected_entity SelectedBuilding;
    
    inventory_item* ChosenInventoryItem;
    bool ShowInventory;
    
    // Building stuff
    // TODO(Jonathan): Consider moving that to some new struct building_state
    s32 BuildingType;
    v2 BuildingsTilesLocations[WORLD_SIZE];
    s32 BuildingsTilesLocationsCount  = 0;
    bool CanBuild;
    bool BuildMode;
    
    v3 CurrentTerrainPoint;
    tile_coordinate CurrentTileCoord;
    tile_coordinate PlacingTileCoord;
    
    // TODO(Naor): Make these change when the windows size is changed! (maybe the resolution too, check it out)
    s32 WindowWidth;
    s32 WindowHeight;
    s32 DrawableWidth;
    s32 DrawableHeight;
    
    bool IsCursorHidden;
    r32 CurrentTime;
    const u8 *PressedKeys;
    
    bool IsCameraOnPlayer = true;
    
#ifdef NAYO_DEBUG
    debug::primitives Primitives;
#endif
};

// TODO(Naor): There is a problem with this, no header defined above this
// can use this variable, but we wanted it to be global and accessable!
static game_state* GameState;
platform_api Platform;

#ifdef NAYO_DEBUG
debug_state* DebugState;
#endif