#pragma once

struct aabb
{
    v3 Min;
    v3 Max;
};

inline aabb AABB()
{
    aabb Result;
    Result.Min = {};
    Result.Max = {};
    return Result;
}

inline aabb AABB(const v3& tMin, const v3& tMax)
{
    aabb Result;
    Result.Min = tMin;
    Result.Max = tMax;
    return Result;
}

inline aabb operator+(const aabb& A, const v3& B)
{
    aabb Result = A;
    Result.Min += B;
    Result.Max += B;
    return Result;
}

inline void operator+=(aabb& A, const v3& B)
{
    A.Min += B;
    A.Max += B;
}

inline aabb operator*(r32 Scalar, const aabb& A)
{
    aabb Result = A;
    Result.Min *= Scalar;
    Result.Max *= Scalar;
    return Result;
}

inline aabb operator*(const aabb& A, r32 Scalar)
{
    aabb Result = Scalar * A;
    return Result;
}

inline aabb operator*(const aabb& A, const m3& B)
{
    aabb Result = A;
    Result.Min *= B;
    Result.Max *= B;
    return Result;
}

inline aabb operator*(const aabb& A, const m4& B)
{
    aabb Result = A;
    Result.Min = Mat4MulVec3(B, Result.Min);
    Result.Max = Mat4MulVec3(B, Result.Max);
    return Result;
}

inline void operator*=(aabb& A, const m3& B)
{
    A = A * B;
}

static void CalculateAABB(aabb &OutAABB, v3 Position, float Magnitude)
{
    OutAABB.Min = Position;
    OutAABB.Max = Position + Magnitude;
}

static void CalculateAABBHalf(aabb &OutAABB, v3 Position, float Magnitude)
{
    OutAABB.Min = Position - 0.5f * Magnitude;
    OutAABB.Max = Position + 0.5f * Magnitude;
}

static bool CheckAABB(aabb &Box, aabb &OtherBox)
{
    return (Box.Min.x <= OtherBox.Max.x && Box.Max.x >= OtherBox.Min.x &&
            Box.Min.y <= OtherBox.Max.y && Box.Max.y >= OtherBox.Min.y &&
            Box.Min.z <= OtherBox.Max.z && Box.Max.z >= OtherBox.Min.z);
}

static bool PointInAABB3D(const aabb& Box, const v3& Point)
{
    return(Point.x > Box.Min.x && Point.x < Box.Max.x &&
           Point.y > Box.Min.y && Point.y < Box.Max.y &&
           Point.z > Box.Min.z && Point.z < Box.Max.z);
}

static bool PointInAABB2D(const aabb& Box, const v2& Point)
{
    return(Point.x > Box.Min.x && Point.x < Box.Max.x &&
           Point.y > Box.Min.y && Point.y < Box.Max.y);
}


#include <limits>
static r32 SweptAABB(aabb &Box, aabb &OtherBox, v3 Velocity, v3 &Normal)
{
    v3 InvEntry, InvExit;
    
    constexpr r32 Threshold = 0.00001f;
    // find the distance between the objects on the near and far sides for both x and y
    if (Velocity.x > Threshold)
    {
        InvEntry.x = OtherBox.Min.x - Box.Max.x;
        InvExit.x = OtherBox.Max.x - Box.Min.x;
    }
    else
    {
        InvEntry.x = OtherBox.Max.x - Box.Min.x;
        InvExit.x = OtherBox.Min.x - Box.Max.x;
    }
    
    if (Velocity.y > Threshold)
    {
        InvEntry.y = OtherBox.Min.y - Box.Max.y;
        InvExit.y = OtherBox.Max.y - Box.Min.y;
    }
    else
    {
        InvEntry.y = OtherBox.Max.y - Box.Min.y;
        InvExit.y = OtherBox.Min.y - Box.Max.y;
    }
    
    if (Velocity.z > Threshold)
    {
        InvEntry.z = OtherBox.Min.z - Box.Max.z;
        InvExit.z = OtherBox.Max.z - Box.Min.z;
    }
    else
    {
        InvEntry.z = OtherBox.Max.z - Box.Min.z;
        InvExit.z = OtherBox.Min.z - Box.Max.z;
    }
    
    // find time of collision and time of leaving for each axis (if statement is to prevent divide by zero)
    v3 EntryTime, ExitTime;
    if (Velocity.x < Threshold && Velocity.x > -Threshold)
    {
        EntryTime.x = -std::numeric_limits<r32>::infinity();
        ExitTime.x = std::numeric_limits<r32>::infinity();
    }
    else
    {
        EntryTime.x = InvEntry.x / Velocity.x;
        ExitTime.x = InvExit.x / Velocity.x;
    }
    if (Velocity.y < Threshold && Velocity.y > -Threshold)
    {
        EntryTime.y = -std::numeric_limits<r32>::infinity();
        ExitTime.y = std::numeric_limits<r32>::infinity();
    }
    else
    {
        EntryTime.y = InvEntry.y / Velocity.y;
        ExitTime.y = InvExit.y / Velocity.y;
    }
    if (Velocity.z < Threshold && Velocity.z > -Threshold)
    {
        EntryTime.z = -std::numeric_limits<r32>::infinity();
        ExitTime.z = std::numeric_limits<r32>::infinity();
    }
    else
    {
        EntryTime.z = InvEntry.z / Velocity.z;
        ExitTime.z = InvExit.z / Velocity.z;
    }
    
    // find the earliest/latest times of collision
    float MaxEntryTime = std::max(std::max(EntryTime.x, EntryTime.y), EntryTime.z);
    float MinExitTime = std::min(std::min(ExitTime.x, ExitTime.y), ExitTime.z);
    
    if (MaxEntryTime > MinExitTime || EntryTime.x < 0.0f && EntryTime.y < 0.0f && EntryTime.z < 0.0f || EntryTime.x > 1.0f || EntryTime.y > 1.0f || EntryTime.z > 1.0f)
    {
        Normal.x = 0.0f;
        Normal.y = 0.0f;
        Normal.z = 0.0f;
        return 999.0f;
    }
    else // if there was a collision
    {
        // calculate normal of collided surface
        if (EntryTime.x > EntryTime.y)
        {
            if (EntryTime.x > EntryTime.z)
            {
                if (InvEntry.x < 0.0f)
                {
                    Normal.x = 1.0f;
                    Normal.y = 0.0f;
                    Normal.z = 0.0f;
                }
                else
                {
                    Normal.x = -1.0f;
                    Normal.y = 0.0f;
                    Normal.z = 0.0f;
                }
            }
            else
            {
                if (InvEntry.z < 0.0f)
                {
                    Normal.x = 0.0f;
                    Normal.y = 0.0f;
                    Normal.z = 1.0f;
                }
                else
                {
                    Normal.x = 0.0f;
                    Normal.y = 0.0f;
                    Normal.z = -1.0f;
                }
            }
        }
        else
        {
            if (EntryTime.y > EntryTime.z)
            {
                if (InvEntry.y < 0.0f)
                {
                    Normal.x = 0.0f;
                    Normal.y = 1.0f;
                    Normal.z = 1.0f;
                }
                else
                {
                    Normal.x = 0.0f;
                    Normal.y = -1.0f;
                    Normal.z = -1.0f;
                }
            }
            else
            {
                if (InvEntry.z < 0.0f)
                {
                    Normal.x = 0.0f;
                    Normal.y = 0.0f;
                    Normal.z = 1.0f;
                }
                else
                {
                    Normal.x = 0.0f;
                    Normal.y = 0.0f;
                    Normal.z = -1.0f;
                }
            }
        }
        // return the time of collision
        return MaxEntryTime;
    }
    
    // Usage:
    //
    //
    //  v3 Normal;
    // float collisiontime = SweptAABB(box, block, Velocity, Normal);
    // Position.x += Velocity.x * collisiontime;
    // Position.y += Velocity.y * collisiontime;
    // Position.z += Velocity.z * collisiontime;
    //
    //
}

