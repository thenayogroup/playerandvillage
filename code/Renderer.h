#pragma once

enum shader_uniform_type
{
    ShaderUniformType_Bool,
    ShaderUniformType_Int,
    ShaderUniformType_Float,
    ShaderUniformType_Vec2,
    ShaderUniformType_Vec3,
    ShaderUniformType_Vec4,
    ShaderUniformType_Mat2,
    ShaderUniformType_Mat3,
    ShaderUniformType_Mat4,
};

struct uniform_value
{
    shader_uniform_type Type;
    // TODO(Joey): now each element takes up the space of its largest 
    // element (mat4) which is 64 bytes; come up with a better solution!
    union
    {
        bool Bool;
        s32 Int;
        r32 Float;
        
        v2 Vec2;
        v3 Vec3;
        v4 Vec4;
        m2 Mat2;
        m3 Mat3;
        m4 Mat4;
    };
};

struct uniform
{
    char* Name;
    shader_uniform_index Index;
    uniform_value Value;
};

// TODO(Jonathan): Make global materials as pointers
#define UNIFORMS_SIZE 16
struct material
{
    // NOTE(Jonathan): Should this be a pointer? Do we want material to change the shader somehow?
    shader_program* Shader;
    
    uniform Uniforms[UNIFORMS_SIZE];
    u32 UniformsCount;
    
    
};

void SetMatrix(material& Material, char* Name, const m4& Value, shader_uniform_index Index);

enum render_draw_type
{
    RenderDrawType_None,
    RenderDrawType_Arrays,
    RenderDrawType_ArraysInstanced,
    RenderDrawType_Elements,
};

enum render_draw_mode
{
    RenderDrawMode_Points = GL_POINTS,
    RenderDrawMode_Lines = GL_LINES,
    RenderDrawMode_Line_Strip = GL_LINE_STRIP,
    RenderDrawMode_Triangles = GL_TRIANGLES,
    RenderDrawMode_Triangle_Strip = GL_TRIANGLE_STRIP,
    RenderDrawMode_Triangle_Fan = GL_TRIANGLE_FAN,
};

// Forward declaration
struct mesh;

struct render_instance
{
    material Material;
    render_draw_type DrawType;
    s32 Texture; // Has to be above -1
    mesh* Mesh;
    render_draw_mode DrawMode;
    
    // TODO(Jonathan): Consider moving that to struct mesh
    u32 Instances; 
};

struct render_group
{
    u32 Count;
    render_instance Renders[4096];
};


static void
PushRender(render_group& RenderGroup, const material& Material, mesh* Mesh, u32 Texture,
           render_draw_type DrawType, bool Element = false, u32 Instances = 0, render_draw_mode DrawMode = RenderDrawMode_Triangles);

#if 0

struct Instanced
{
    u32 MeshIndex;
    u32 Vbo;
    v3 Positions[4096];
};

if(Instanced.MeshIndex == MeshIndex)
{
    Instanced.Positions[Count] = Position;
}


struct InstancedModels
{
    Instanced Trees;
    Instanced Bushes;
    Instanced Rocks;
    Instanced Stones;
    Instanced Woods;
}

#endif
