#pragma once

// TODO(Naor): Read about fast inv-sqrt for Normalize: 
// https://en.wikipedia.org/wiki/Fast_inverse_square_root

// TODO(Naor): 
// Hadmard(?)
// Clamp

namespace Math
{
    class vec2
    {
        public:
        
        //
        // Variables
        //
        union
        {
            struct
            {
                r32 x, y;
            };
            r32 E[2];
        };
        //
        // Operations
        //
        inline const r32* Data() const
        {
            return &E[0];
        }
        
        r32 Dot(const vec2& Other) const
        {
            r32 Result = x * Other.x + y * Other.y;
            
            return Result;
        }
        
        void Cos()
        {
            x = Math::Cos(x);
            y = Math::Cos(y);
        }
        
        void Sin()
        {
            x = Math::Sin(x);
            y = Math::Sin(y);
        }
        
        r32 Length() const
        {
            r32 Result = SquareRoot(Dot(*this));
            
            return Result;
        }
        
        void Normalize()
        {
            *this *= (1.0f / Length());
        }
        
        r32 Distance(const vec2& Other) const
        {
            r32 Result = Math::Length(Other - (*this));
            
            return Result;
        }
        
        //
        // Operator Overloading
        //
        
        void operator+=(const vec2& Other)
        {
            x += Other.x;
            y += Other.y;
        }
        
        void operator-=(const vec2& Other)
        {
            x -= Other.x;
            y -= Other.y;
        }
        
        void operator*=(r32 Scalar)
        {
            x *= Scalar;
            y *= Scalar;
        }
        
        void operator*=(const m2& Other)
        {
            *this = (*this) * Other;
        }
    };
    
    class vec3
    {
        public:
        
        
        //
        // Variables
        //
        union
        {
            struct 
            {
                r32 x, y, z;
            };
            
            r32 E[3];
        };
        
        //
        // Operations
        //
        inline const r32* Data() const
        {
            return &E[0];
        }
        
        r32 Dot(const vec3& Other) const
        {
            r32 Result = x * Other.x + y * Other.y + z * Other.z;
            
            return Result;
        }
        
        void Cos()
        {
            x = Math::Cos(x);
            y = Math::Cos(y);
            z = Math::Cos(z);
        }
        
        void Sin()
        {
            x = Math::Sin(x);
            y = Math::Sin(y);
            z = Math::Sin(z);
        }
        
        r32 Length() const
        {
            r32 Result = SquareRoot(Dot(*this));
            
            return Result;
        }
        
        void Normalize()
        {
            *this *= (1.0f / Length());
        }
        
        r32 Distance(const vec3& Other) const
        {
            r32 Result = Math::Length(Other - (*this));
            
            return Result;
        }
        
        vec3 Cross(const vec3& Other) const
        {
            vec3 Result = {};
            Result.x = y * Other.z - Other.y * z;
            Result.y = z * Other.x - Other.z * x;
            Result.z = x * Other.y - Other.x * y;
            
            return Result;
        }
        
        //
        // Operator Overloading
        //
        
        void operator+=(const vec3& Other)
        {
            x += Other.x;
            y += Other.y;
            z += Other.z;
        }
        
        void operator-=(const vec3& Other)
        {
            x -= Other.x;
            y -= Other.y;
            z -= Other.z;
        }
        
        void operator*=(r32 Scalar)
        {
            x *= Scalar;
            y *= Scalar;
            z *= Scalar;
        }
        
        void operator*=(const m3& Other)
        {
            *this = (*this) * Other;
        }
        
    };
    
    class vec4
    {
        public:
        
        
        //
        // Variables
        //
        
        union
        {
            struct 
            {
                r32 x, y, z, w;
            };
            
            r32 E[3];
        };
        
        //
        // Operations
        //
        inline const r32* Data() const
        {
            return &E[0];
        }
        
        r32 Dot(const vec4& Other) const
        {
            r32 Result = x * Other.x + y * Other.y + z * Other.z + w * Other.w;
            
            return Result;
        }
        
        void Cos()
        {
            x = Math::Cos(x);
            y = Math::Cos(y);
            z = Math::Cos(z);
            w = Math::Cos(w);
        }
        
        void Sin() 
        {
            x = Math::Sin(x);
            y = Math::Sin(y);
            z = Math::Sin(z);
            w = Math::Sin(w);
        }
        
        r32 Length() const
        {
            r32 Result = SquareRoot(Dot(*this));
            
            return Result;
        }
        
        void Normalize()
        {
            *this *= (1.0f / Length());
        }
        
        r32 Distance(const vec4& Other) const
        {
            r32 Result = Math::Length(Other - (*this));
            
            return Result;
        }
        
        //
        // Operator Overloading
        //
        
        void operator+=(const vec4& Other)
        {
            x += Other.x;
            y += Other.y;
            z += Other.z;
            w += Other.w;
        }
        
        void operator-=(const vec4& Other)
        {
            x -= Other.x;
            y -= Other.y;
            z -= Other.z;
            w -= Other.w;
        }
        
        void operator*=(r32 Scalar)
        {
            x *= Scalar;
            y *= Scalar;
            z *= Scalar;
            w *= Scalar;
        }
        
        void operator*=(const m4& Other)
        {
            *this= (*this) * Other;
        }
    };
    
    //
    // Generation Functions
    //
    
    // Vec2
    inline v2 V2()
    {
        v2 Result = {};
        return Result;
    }
    
    inline v2 V2(r32 Value) 
    {
        v2 Result = {};
        Result.x = Value;
        Result.y = Value;
        return Result;
    }
    
    inline v2 V2(r32 X, r32 Y)
    {
        v2 Result = {};
        Result.x = X;
        Result.y = Y;
        return Result;
    }
    
    inline v2 V2(s32 X, s32 Y)
    {
        v2 Result = {};
        Result.x = (r32)X;
        Result.y = (r32)Y;
        return Result;
    }
    
    // Vec3
    inline v3 V3()
    {
        v3 Result = {};
        return Result;
    }
    
    
    
    inline v3 V3(r32 Value)
    {
        v3 Result = {};
        Result.x = Value;
        Result.y = Value;
        Result.z = Value;
        
        return Result;
    }
    
    inline v3 V3(r32 X, r32 Y, r32 Z)
    {
        v3 Result = {};
        Result.x = X;
        Result.y = Y;
        Result.z = Z;
        
        return Result;
    }
    
    inline v3 V3(r32 X, const v2& V)
    {
        v3 Result = {};
        Result.x = X;
        Result.y = V.x;
        Result.z = V.y;
        
        return Result;
    }
    
    inline v3 V3(const v2& V, r32 Z)
    {
        v3 Result = {};
        Result.x = V.x;
        Result.y = V.y;
        Result.z = Z;
        
        return Result;
    }
    
    // V4
    inline v4 V4()
    {
        v4 Result = {};
        return Result;
    }
    
    
    inline v4 V4(r32 Value)
    {
        v4 Result = {};
        Result.x = Value;
        Result.y = Value;
        Result.z = Value;
        Result.w = Value;
        
        return Result;
    }
    
    inline v4 V4(r32 X, r32 Y, r32 Z, r32 W)
    {
        v4 Result = {};
        Result.x = X;
        Result.y = Y;
        Result.z = Z;
        Result.w = W;
        
        return Result;
    }
    
    inline v4 V4(const v3& V, r32 W)
    {
        v4 Result = {};
        Result.x = V.x;
        Result.y = V.y;
        Result.z = V.z;
        Result.w = W;
        
        return Result;
    }
    
    inline v4 V4(r32 X, const v3& V)
    {
        v4 Result = {};
        Result.x = X;
        Result.y = V.x;
        Result.z = V.y;
        Result.w = V.z;
        
        return Result;
    }
    
    inline v4 V4(const v2& V, r32 Z, r32 W)
    {
        v4 Result = {};
        Result.x = V.x;
        Result.y = V.y;
        Result.z = Z;
        Result.w = W;
        
        return Result;
    }
    
    inline v4 V4(r32 X, r32 Y, const v2& V)
    {
        v4 Result = {};
        Result.x = X;
        Result.y = Y;
        Result.z = V.x;
        Result.w = V.y;
        
        return Result;
    }
    
    inline v4 V4(r32 X, const v2& V, r32 W)
    {
        v4 Result = {};
        Result.x = X;
        Result.y = V.x;
        Result.z = V.y;
        Result.w = W;
        
        return Result;
    }
    
    inline v4 V4(const v2& V1, const v2& V2)
    {
        v4 Result = {};
        Result.x = V1.x;
        Result.y = V1.y;
        Result.z = V2.x;
        Result.w = V2.y;
        
        return Result;
    }
    
    //
    // Vec2 Operator overloading
    //
    inline v2 operator*(const v2& A, const v2& B)
    {
        v2 Result = V2(A.x *B.x, A.y *B.y);
        
        return Result;
    }
    
    inline v2 operator*(const v2& A, r32 Scalar)
    {
        v2 Result = V2(A.x * Scalar, A.y * Scalar);
        
        return Result;
    }
    
    inline v2 operator*(r32 Scalar, const v2& A)
    {
        v2 Result(A * Scalar);
        
        return Result;
    }
    
    inline v2 operator+(const v2& A, const v2& B)
    {
        v2 Result = V2(A.x + B.x, A.y + B.y);
        
        return Result;
    }
    
    inline v2 operator+(const v2& A, r32 Scalar)
    {
        v2 Result = V2(A.x + Scalar, A.y + Scalar);
        
        return Result;
    }
    
    inline v2 operator+(r32 Scalar, const v2& A)
    {
        return A + Scalar;
    }
    
    inline v2 operator-(const v2& A, const v2& B)
    {
        v2 Result = V2(A.x - B.x, A.y - B.y);
        
        return Result;
    }
    
    inline v2 operator-(const v2& A, r32 Scalar)
    {
        v2 Result = V2(A.x - Scalar, A.y - Scalar);
        
        return Result;
    }
    
    inline v2 operator-(r32 Scalar, const v2& A)
    {
        return A - Scalar;
    }
    
    inline v2& operator-(v2& A)
    {
        A.x = -A.x;
        A.y = -A.y;
        
        return A;
    }
    
    inline bool operator==(const v2& A, const v2& B)
    {
        bool Result = (A.x == B.x) && (A.y == B.y);
        
        return Result;
    }
    
    inline bool operator==(const v2& A, r32 Scalar)
    {
        bool Result = (A.x == Scalar) && (A.y == Scalar);
        
        return Result;
    }
    
    inline bool operator==(r32 Scalar,const v2& A)
    {
        return A == Scalar;
    }
    
    inline bool operator!=(const v2& A, r32 Scalar)
    {
        return !(A == Scalar);
    }
    
    inline bool operator!=(r32 Scalar,const v2& A)
    {
        return !(A == Scalar);
    }
    
    inline bool operator!=(const v2& A, const v2& B)
    {
        bool Result = !(A == B);
        
        return Result;
    }
    
    inline std::ostream& operator<<(std::ostream& out, const v2& A)
    {
        out << "(" << A.x << ", " << A.y << ")";
        return out;
    }
    
    //
    // Vec3 Operator overloading
    //
    inline v3 operator*(const v3& A, const v3& B)
    {
        v3 Result = V3(A.x *B.x, A.y *B.y, A.z *B.z);
        
        return Result;
    }
    
    inline v3 operator*(const v3& A, r32 Scalar)
    {
        v3 Result = V3(A.x * Scalar, A.y * Scalar, A.z * Scalar);
        
        return Result;
    }
    
    inline v3 operator*(r32 Scalar, const v3& A)
    {
        v3 Result = A * Scalar;
        
        return Result;
    }
    
    inline v3 operator/(const v3& A, r32 Scalar)
    {
        v3 Result = V3(A.x / Scalar, A.y / Scalar, A.z / Scalar);
        
        return Result;
    }
    
    inline v3 operator+(const v3& A, const v3& B)
    {
        v3 Result = V3(A.x + B.x, A.y + B.y, A.z + B.z);
        
        return Result;
    }
    
    inline v3 operator+(const v3& A, r32 Scalar)
    {
        v3 Result = V3(A.x + Scalar, A.y + Scalar, A.z + Scalar);
        
        return Result;
    }
    
    inline v3 operator+(r32 Scalar, const v3& A)
    {
        return A + Scalar;
    }
    
    inline v3 operator-(const v3& A, const v3& B)
    {
        v3 Result = V3(A.x - B.x, A.y - B.y, A.z - B.z);
        
        return Result;
    }
    
    inline v3 operator-(const v3& A, r32 Scalar)
    {
        v3 Result = V3(A.x - Scalar, A.y - Scalar, A.z - Scalar);
        
        return Result;
    }
    
    inline v3 operator-(r32 Scalar, const v3& A)
    {
        return A - Scalar;
    }
    
    inline v3& operator-(v3& A)
    {
        A.x = -A.x;
        A.y = -A.y;
        A.z = -A.z;
        
        return A;
    }
    
    inline bool operator==(const v3& A, const v3& B)
    {
        bool Result = (A.x == B.x) && (A.y == B.y) && (A.z == B.z);
        
        return Result;
    }
    
    inline bool operator==(const v3& A, r32 Scalar)
    {
        bool Result = (A.x == Scalar) && (A.y == Scalar) && (A.z == Scalar);
        
        return Result;
    }
    
    inline bool operator==(r32 Scalar,const v3& A)
    {
        return A == Scalar;
    }
    
    inline bool operator!=(const v3& A, r32 Scalar)
    {
        return !(A == Scalar);
    }
    
    inline bool operator!=(r32 Scalar,const v3& A)
    {
        return !(A == Scalar);
    }
    
    inline bool operator!=(const v3& A, const v3& B)
    {
        bool Result = !(A == B);
        
        return Result;
    }
    
    inline std::ostream& operator<<(std::ostream& out, const v3& A)
    {
        out << "(" << A.x << ", " << A.y << ", " << A.z << ")";
        return out;
    }
    
    //
    // Vec4 Operator overloading
    //
    inline v4 operator*(const v4& A, const v4& B)
    {
        v4 Result = V4(A.x * B.x, A.y * B.y, A.z * B.z, A.w * B.w);
        
        return Result;
    }
    
    inline v4 operator*(const v4& A, r32 Scalar)
    {
        v4 Result = V4(A.x * Scalar, A.y * Scalar, A.z * Scalar, A.w * Scalar);
        
        return Result;
    }
    
    inline v4 operator*(r32 Scalar, const v4& A)
    {
        v4 Result(A * Scalar);
        
        return Result;
    }
    
    inline v4 operator+(const v4& A, const v4& B)
    {
        v4 Result = V4(A.x + B.x, A.y + B.y, A.z + B.z, A.w + B.w);
        
        return Result;
    }
    
    inline v4 operator+(const v4& A, r32 Scalar)
    {
        v4 Result = V4(A.x + Scalar, A.y + Scalar, A.z + Scalar, A.w + Scalar);
        
        return Result;
    }
    
    inline v4 operator+(r32 Scalar, const v4& A)
    {
        return A + Scalar;
    }
    
    inline v4 operator-(const v4& A, const v4& B)
    {
        v4 Result = V4(A.x - B.x, A.y - B.y, A.z - B.z, A.w - B.w);
        
        return Result;
    }
    
    inline v4 operator-(const v4& A, r32 Scalar)
    {
        v4 Result = V4(A.x - Scalar, A.y - Scalar, A.z - Scalar, A.w - Scalar);
        
        return Result;
    }
    
    inline v4 operator-(r32 Scalar, const v4& A)
    {
        return A - Scalar;
    }
    
    inline v4& operator-(v4& A)
    {
        A.x = -A.x;
        A.y = -A.y;
        A.z = -A.z;
        A.w = -A.w;
        
        return A;
    }
    
    inline bool operator==(const v4& A, const v4& B)
    {
        bool Result = (A.x == B.x) && (A.y == B.y) && (A.z == B.z) && (A.w == B.w);
        
        return Result;
    }
    
    inline bool operator==(const v4& A, r32 Scalar)
    {
        bool Result = (A.x == Scalar) && (A.y == Scalar) && (A.z == Scalar) && (A.w == Scalar);
        
        return Result;
    }
    
    inline bool operator==(r32 Scalar,const v4& A)
    {
        return A == Scalar;
    }
    
    inline bool operator!=(const v4& A, r32 Scalar)
    {
        return !(A == Scalar);
    }
    
    inline bool operator!=(r32 Scalar,const v4& A)
    {
        return !(A == Scalar);
    }
    
    inline bool operator!=(const v4& A, const v4& B)
    {
        bool Result = !(A == B);
        
        return Result;
    }
    
    inline std::ostream& operator<<(std::ostream& out, const v4& A)
    {
        out << "(" << A.x << ", " << A.y << ", " << A.z << ", " << A.w << ")";
        return out;
    }
    
    //
    // Operations on vectors
    //
    
    inline r32 Dot(const v2& A, const v2& B)
    {
        r32 Result = A.x * B.x + A.y * B.y;
        
        return Result;
    }
    
    inline r32 Dot(const v3& A, const v3& B)
    {
        r32 Result = A.x * B.x + A.y * B.y + A.z * B.z;
        
        return Result;
    }
    
    inline r32 Dot(const v4& A, const v4& B)
    {
        r32 Result = A.x * B.x + A.y * B.y + A.z * B.z + A.w * B.w;
        
        return Result;
    }
    
    inline v3 Cross(const v3& A, const v3& B)
    {
        v3 Result = V3(A.y * B.z - B.y * A.z,
                       A.z * B.x - B.z * A.x,
                       A.x * B.y - B.x * A.y);
        
        return Result;
    }
    
    inline r32 Length(const v2& A)
    {
        r32 Result = SquareRoot(Dot(A,A));
        
        return Result;
    }
    
    inline r32 Length(const v3& A)
    {
        r32 Result = SquareRoot(Dot(A,A));
        
        return Result;
    }
    
    inline r32 Length(const v4& A)
    {
        r32 Result = SquareRoot(Dot(A,A));
        
        return Result;
    }
    
    inline v2 Normalize(const v2& A)
    {
        v2 Result(A * (1.0f / Length(A)));
        
        return Result;
    }
    
    inline v3 Normalize(const v3& A)
    {
        v3 Result(A * (1.0f / Length(A)));
        
        return Result;
    }
    
    inline v4 Normalize(const v4& A)
    {
        v4 Result(A * (1.0f / Length(A)));
        
        return Result;
    }
    
    inline r32 Distance(const v2& A, const v2& B)
    {
        r32 Result = Length(B - A);
        
        return Result;
    }
    
    inline r32 Distance(const v3& A, const v3& B)
    {
        r32 Result = Length(B - A);
        
        return Result;
    }
    
    inline r32 Distance(const v4& A, const v4& B)
    {
        r32 Result = Length(B - A);
        
        return Result;
    }
    
    inline v2 Cos(const v2& A)
    {
        v2 Result = V2(Math::Cos(A.x), Math::Cos(A.y));
        
        return Result;
    }
    
    inline v3 Cos(const v3& A)
    {
        v3 Result = V3(Math::Cos(A.x), Math::Cos(A.y), Math::Cos(A.z));
        
        return Result;
    }
    
    inline v4 Cos(const v4& A)
    {
        v4 Result = V4(Math::Cos(A.x), Math::Cos(A.y), Math::Cos(A.z), Math::Cos(A.w));
        
        return Result;
    }
    
    inline v2 Sin(const v2& A)
    {
        v2 Result = V2(Math::Sin(A.x), Math::Sin(A.y));
        
        return Result;
    }
    
    inline v3 Sin(const v3& A)
    {
        v3 Result = V3(Math::Sin(A.x), Math::Sin(A.y), Math::Sin(A.z));
        
        return Result;
    }
    
    inline v4 Sin(const v4& A)
    {
        v4 Result = V4(Math::Sin(A.x), Math::Sin(A.y), Math::Sin(A.z), Math::Sin(A.w));
        
        return Result;
    }
    
    inline v2 Mix(const v2& A, const v2& B, r32 T)
    {
        v2 Result = A * (1 - T) + B * T;
        
        return Result;
    }
    
    inline v3 Mix(const v3& A, const v3& B, r32 T)
    {
        v3 Result = A * (1 - T) + B * T;
        
        return Result;
    }
    
    inline v4 Mix(const v4& A, const v4& B, r32 T)
    {
        v4 Result = A * (1 - T) + B * T;
        
        return Result;
    }
    
    inline v4 Fract(const v4& A)
    {
        v4 Result = V4(Fract(A.x), Fract(A.y), Fract(A.z), Fract(A.w));
        
        return Result;
    }
    
    inline v3 Fract(const v3& A)
    {
        v3 Result = V3(Fract(A.x), Fract(A.y), Fract(A.z));
        
        return Result;
    }
    
    inline v2 Fract(const v2& A)
    {
        v2 Result = V2(Fract(A.x), Fract(A.y));
        
        return Result;
    }
    
    inline v4 Floor(const v4& A)
    {
        v4 Result = V4(Floor(A.x), Floor(A.y), Floor(A.z), Floor(A.w));
        
        return Result;
    }
    
    inline v3 Floor(const v3& A)
    {
        v3 Result = V3(Floor(A.x), Floor(A.y), Floor(A.z));
        
        return Result;
    }
    
    inline v2 Floor(const v2& A)
    {
        v2 Result = V2(Floor(A.x), Floor(A.y));
        
        return Result;
    }
    
    inline v4 Ceil(const v4& A)
    {
        v4 Result = V4(Ceil(A.x), Ceil(A.y), Ceil(A.z), Ceil(A.w));
        
        return Result;
    }
    
    inline v3 Ceil(const v3& A)
    {
        v3 Result = V3(Ceil(A.x), Ceil(A.y), Ceil(A.z));
        
        return Result;
    }
    
    inline v2 Ceil(const v2& A)
    {
        v2 Result = V2(Ceil(A.x), Ceil(A.y));
        
        return Result;
    }
};