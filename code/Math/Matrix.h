#pragma once

// TODO(Naor):
// Inv-matrix

// TODO(Naor): Casey added another struct that contain the matrix itself and the inverse of it
// Shouls we add it?
namespace Math
{
    class mat2
    {
        public:
        //
        // Variables
        //
        r32 E[2][2];
        
        //
        // Operators
        //
        inline const r32* Data() const
        {
            return E[0];
        }
        
        void Transpose()
        {
            for(s32 i = 0; i < 2; ++i)
            {
                for(s32 j = 0; j < 2; ++j)
                {
                    E[i][j] = E[j][i];
                }
            }
        }
        
        //
        // Operator Overloading
        //
        void operator*=(const mat2& Other)
        {
            *this = (*this) * Other;
            
        }
    };
    
    class mat3
    {
        public:
        //
        // Variables
        //
        r32 E[3][3];
        
        //
        // Operators
        //
        inline const r32* Data() const
        {
            return E[0];
        }
        
        void Transpose()
        {
            for(s32 i = 0; i < 3; ++i)
            {
                for(s32 j = 0; j < 3; ++j)
                {
                    E[i][j] = E[j][i];
                }
            }
        }
        
        //
        // Operator Overloading
        //
        void operator*=(const mat3& Other)
        {
            *this = (*this) * Other;
            
        }
    };
    
    class mat4
    {
        public:
        //
        // Variables
        //
        r32 E[4][4];
        
        //
        // Operators
        //
        inline const r32* Data() const
        {
            return E[0];
        }
        
        void Transpose()
        {
            for(s32 i = 0; i < 4; ++i)
            {
                for(s32 j = 0; j < 4; ++j)
                {
                    E[i][j] = E[j][i];
                }
            }
        }
        
        //
        // Operator Overloading
        //
        void operator*=(const mat4& Other)
        {
            *this = (*this) * Other;
            
        }
        
    };
    
    //
    // Generations
    //
    
    // mat2
    inline m2 M2() 
    {
        m2 Result  = {};
        Result.E[0][0] = 1.0f;
        Result.E[1][1] = 1.0f;
        
        return Result;
    }
    inline m2 M2(r32 Scalar) 
    {
        m2 Result  = {};
        Result.E[0][0] = Scalar;
        Result.E[1][1] = Scalar;
        
        return Result;
    }
    inline m2 M2(r32 First, r32 Second) 
    {
        m2 Result  = {};
        Result.E[0][0] = First;
        Result.E[1][1] = Second;
        
        return Result;
    }
    inline m2 M2(const v2& V) 
    {
        m2 Result  = {};
        Result.E[0][0] = V.x;
        Result.E[1][1] = V.y;
        
        return Result;
    }
    inline m2 M2(r32 x1, r32 y1,
                 r32 x2, r32 y2) 
    {
        m2 Result  = {};
        Result.E[0][0] = x1;
        Result.E[0][1] = y1;
        
        Result.E[1][0] = x2;
        Result.E[1][1] = y2;
        
        return Result;
    }
    inline m2 M2(const v2& First, const v2& Second)
    {
        m2 Result = {};
        Result.E[0][0] = First.x;
        Result.E[0][1] = First.y;
        
        Result.E[1][0] = Second.x;
        Result.E[1][1] = Second.y;
        
        return Result;
    }
    
    
    // Mat3
    
    inline m3 M3()
    {
        m3 Result = {};
        Result.E[0][0] = 1.0f;
        Result.E[1][1] = 1.0f;
        Result.E[2][2] = 1.0f;
        
        return Result;
    }
    inline m3 M3(r32 Scalar) 
    {
        m3 Result = {};
        Result.E[0][0] = Scalar;
        Result.E[1][1] = Scalar;
        Result.E[2][2] = Scalar;
        
        return Result;
    }
    inline m3 M3(r32 First, r32 Second, r32 Third) 
    {
        m3 Result = {};
        Result.E[0][0] = First;
        Result.E[1][1] = Second;
        Result.E[2][2] = Third;
        
        return Result;
    }
    inline m3 M3(const v3& V) 
    {
        m3 Result = {};
        Result.E[0][0] = V.x;
        Result.E[1][1] = V.y;
        Result.E[2][2] = V.z;
        
        return Result;
    }
    inline m3 M3(const m3& Other)
    {
        m3 Result = {};
        for(s32 i = 0; i < 3; ++i)
        {
            for(s32 j = 0; j < 3; ++j)
            {
                Result.E[i][j] = Other.E[i][j];
            }
        }
        
        return Result;
    }
    inline m3 M3(r32 x1, r32 y1, r32 z1,
                 r32 x2, r32 y2, r32 z2,
                 r32 x3, r32 y3, r32 z3) 
    {
        m3 Result = {};
        Result.E[0][0] = x1;
        Result.E[0][1] = y1;
        Result.E[0][2] = z1;
        
        Result.E[1][0] = x2;
        Result.E[1][1] = y2;
        Result.E[1][2] = z2;
        
        Result.E[2][0] = x3;
        Result.E[2][1] = y3;
        Result.E[2][2] = z3;
        
        return Result;
    }
    inline m3 M3(const v3& First, const v3& Second, const v3& Third)
    {
        m3 Result = {};
        Result.E[0][0] = First.x;
        Result.E[0][1] = First.y;
        Result.E[0][2] = First.z;
        
        Result.E[1][0] = Second.x;
        Result.E[1][1] = Second.y;
        Result.E[1][2] = Second.z;
        
        Result.E[2][0] = Third.x;
        Result.E[2][1] = Third.y;
        Result.E[2][2] = Third.z;
        
        return Result;
    }
    
    // Mat4
    
    //
    // Constroctors
    //
    inline m4 M4() 
    {
        m4 Result = {};
        Result.E[0][0] = 1.0f;
        Result.E[1][1] = 1.0f;
        Result.E[2][2] = 1.0f;
        Result.E[3][3] = 1.0f;
        
        return Result;
    }
    inline m4 M4(r32 Scalar) 
    {
        m4 Result = {};
        Result.E[0][0] = Scalar;
        Result.E[1][1] = Scalar;
        Result.E[2][2] = Scalar;
        Result.E[3][3] = Scalar;
        
        return Result;
    }
    inline m4 M4(r32 First, r32 Second, r32 Third, r32 Fourth) 
    {
        m4 Result = {};
        Result.E[0][0] = First;
        Result.E[1][1] = Second;
        Result.E[2][2] = Third;
        Result.E[3][3] = First;
        
        return Result;
    }
    
    inline m4 M4(const v4& V) 
    {
        m4 Result = {};
        Result.E[0][0] = V.x;
        Result.E[1][1] = V.y;
        Result.E[2][2] = V.z;
        Result.E[3][3] = V.w;
        
        return Result;
    }
    inline m4 M4(const m3& Other)
    {
        m4 Result = {};
        Result.E[0][0] = Other.E[0][0];
        Result.E[0][1] = Other.E[0][1];
        Result.E[0][2] = Other.E[0][2];
        Result.E[0][3] = 0.0f;
        
        Result.E[1][0] = Other.E[1][0];
        Result.E[1][1] = Other.E[1][1];
        Result.E[1][2] = Other.E[1][2];
        Result.E[1][3] = 0.0f;
        
        Result.E[2][0] = Other.E[2][0];
        Result.E[2][1] = Other.E[2][1];
        Result.E[2][2] = Other.E[2][2];
        Result.E[2][3] = 0.0f;
        
        Result.E[3][0] = 0.0f;
        Result.E[3][1] = 0.0f;
        Result.E[3][2] = 0.0f;
        Result.E[3][3] = 1.0f;
        
        return Result;
    }
    inline m4 M4(const mat4& Other)
    {
        m4 Result = {};
        for(s32 i = 0; i < 4; ++i)
        {
            for(s32 j = 0; j < 4; ++j)
            {
                Result.E[i][j] = Other.E[i][j];
            }
        }
        
        return Result;
    }
    inline m4 M4(r32 x1, r32 y1, r32 z1, r32 w1,
                 r32 x2, r32 y2, r32 z2, r32 w2,
                 r32 x3, r32 y3, r32 z3, r32 w3,
                 r32 x4, r32 y4, r32 z4, r32 w4) 
    {
        m4 Result = {};
        Result.E[0][0] = x1;
        Result.E[0][1] = y1;
        Result.E[0][2] = z1;
        Result.E[0][3] = w1;
        
        Result.E[1][0] = x2;
        Result.E[1][1] = y2;
        Result.E[1][2] = z2;
        Result.E[1][3] = w2;
        
        Result.E[2][0] = x3;
        Result.E[2][1] = y3;
        Result.E[2][2] = z3;
        Result.E[2][3] = w3;
        
        Result.E[3][0] = x4;
        Result.E[3][1] = y4;
        Result.E[3][2] = z4;
        Result.E[3][3] = w4;
        
        return Result;
    }
    
    inline m4 M4(const v4& A, const v4& B, const v4& C, const v4& D)
    {
        m4 Result = {};
        Result.E[0][0] = A.x;
        Result.E[0][1] = A.y;
        Result.E[0][2] = A.z;
        Result.E[0][3] = A.w;
        
        Result.E[1][0] = B.x;
        Result.E[1][1] = B.y;
        Result.E[1][2] = B.z;
        Result.E[1][3] = B.w;
        
        Result.E[2][0] = C.x;
        Result.E[2][1] = C.y;
        Result.E[2][2] = C.z;
        Result.E[2][3] = C.w;
        
        Result.E[3][0] = D.x;
        Result.E[3][1] = D.y;
        Result.E[3][2] = D.z;
        Result.E[3][3] = D.w;
        
        return Result;
    }
    
    //
    // Operations
    //
    
    //
    // Matrix 2x2
    //
    inline m2 operator*(const m2& A, const m2& B)
    {
        m2 Result = {};
        
        for(s32 i = 0; i < 2; ++i)
        {
            for(s32 j = 0; j < 2; ++j)
            {
                r32 Current = 0.0f;
                for(s32 k = 0; k < 2; ++k)
                {
                    Current += A.E[k][j] * B.E[i][k];
                }
                Result.E[i][j] = Current;
            }
        }
        
        return Result;
    }
    
    inline m2 operator*(const m2& A, r32 Scalar)
    {
        m2 Result(A);
        
        for(s32 i = 0; i < 2; ++i)
        {
            for(s32 j = 0; j < 2; ++j)
            {
                Result.E[i][j] *= Scalar;
            }
        }
        
        return Result;
    }
    
    inline m2 operator*(r32 Scalar, const m2& A)
    {
        m2 Result = A * Scalar;
        
        return Result;
    }
    
    inline v2 operator*(const m2& A, const v2& B)
    {
        v2 Result = {};
        
        Result.x = A.E[0][0] * B.x + A.E[1][0] * B.y; 
        Result.y = A.E[0][1] * B.x + A.E[1][1] * B.y;
        
        return Result;
    }
    
    inline v2 operator*(const v2& A, const m2& B)
    {
        v2 Result = B * A;
        
        return Result;
    }
    
    inline m2 operator/(const m2& A, r32 Scalar)
    {
        r32 OneOver = 1.0f / Scalar;
        
        m2 Result = A * OneOver;
        
        return Result;
    }
    
    inline m2 Transpose(const m2& A)
    {
        m2 Result = {};
        
        for(s32 i = 0; i < 2; ++i)
        {
            for(s32 j = 0; j < 2; ++j)
            {
                Result.E[i][j] = A.E[j][i];
            }
        }
        
        return Result;
    }
    
    //
    // Matrix 3x3
    //
    inline m3 operator*(const m3& A, const m3& B)
    {
        m3 Result = {};
        
        for(s32 i = 0; i < 3; ++i)
        {
            for(s32 j = 0; j < 3; ++j)
            {
                r32 Current = 0.0f;
                for(s32 k = 0; k < 3; ++k)
                {
                    Current += A.E[k][j] * B.E[i][k];
                }
                Result.E[i][j] = Current;
            }
        }
        
        return Result;
    }
    
    inline m3 operator*(const m3& A, r32 Scalar)
    {
        m3 Result(A);
        
        for(s32 i = 0; i < 3; ++i)
        {
            for(s32 j = 0; j < 3; ++j)
            {
                Result.E[i][j] *= Scalar;
            }
        }
        
        return Result;
    }
    
    inline m3 operator*(r32 Scalar, const m3& A)
    {
        m3 Result = A * Scalar;
        
        return Result;
    }
    
    inline v3 operator*(const m3& A, const v3& B)
    {
        v3 Result = {};
        
        Result.x = A.E[0][0] * B.x + A.E[1][0] * B.y + A.E[2][0] * B.z; 
        Result.y = A.E[0][1] * B.x + A.E[1][1] * B.y + A.E[2][1] * B.z;
        Result.z = A.E[0][2] * B.x + A.E[1][2] * B.y + A.E[2][2] * B.z; 
        
        return Result;
    }
    
    inline v3 operator*(const v3& A, const m3& B)
    {
        v3 Result = B * A;
        
        return Result;
    }
    
    inline m3 operator/(const m3& A, r32 Scalar)
    {
        r32 OneOver = 1.0f / Scalar;
        
        m3 Result = A * OneOver;
        
        return Result;
    }
    
    inline m3 Transpose(const m3& A)
    {
        m3 Result = {};
        
        for(s32 i = 0; i < 3; ++i)
        {
            for(s32 j = 0; j < 3; ++j)
            {
                Result.E[i][j] = A.E[j][i];
            }
        }
        
        return Result;
    }
    
    //
    // Matrix 4x4
    //
    // TODO(Naor): This, including all other matrix multiplications, is not optimal,
    // fix in the future when needed.
    inline m4 operator*(const m4& A, const m4& B)
    {
        m4 Result = {};
        
        for(s32 i = 0; i < 4; ++i)
        {
            for(s32 j = 0; j < 4; ++j)
            {
                r32 Current = 0.0f;
                for(s32 k = 0; k < 4; ++k)
                {
                    Current += A.E[k][j] * B.E[i][k];
                }
                Result.E[i][j] = Current;
            }
        }
        
        return Result;
    }
    
    inline m4 operator*(const m4& A, r32 Scalar)
    {
        m4 Result(A);
        
        for(s32 i = 0; i < 4; ++i)
        {
            for(s32 j = 0; j < 4; ++j)
            {
                Result.E[i][j] *= Scalar;
            }
        }
        
        return Result;
    }
    
    inline m4 operator*(r32 Scalar, const m4& A)
    {
        m4 Result = A * Scalar;
        
        return Result;
    }
    
    inline v4 operator*(const m4& A, const v4& B)
    {
        v4 Result = {};
        
        Result.x = A.E[0][0] * B.x + A.E[1][0] * B.y + A.E[2][0] * B.z + A.E[3][0] * B.w; 
        Result.y = A.E[0][1] * B.x + A.E[1][1] * B.y + A.E[2][1] * B.z + A.E[3][1] * B.w;
        Result.z = A.E[0][2] * B.x + A.E[1][2] * B.y + A.E[2][2] * B.z + A.E[3][2] * B.w; 
        Result.w = A.E[0][3] * B.x + A.E[1][3] * B.y + A.E[2][3] * B.z + A.E[3][3] * B.w; 
        
        return Result;
    }
    
    // Note: This is temporay, only for boundingboxs for now
    inline v3 Mat4MulVec3(const m4& A, const v3& B)
    {
        v3 Result = {};
        
        Result.x = A.E[0][0] * B.x + A.E[1][0] * B.y + A.E[2][0] * B.z + A.E[3][0]; 
        Result.y = A.E[0][1] * B.x + A.E[1][1] * B.y + A.E[2][1] * B.z + A.E[3][1];
        Result.z = A.E[0][2] * B.x + A.E[1][2] * B.y + A.E[2][2] * B.z + A.E[3][2];
        
        return Result;
    }
    
    inline v4 operator*(const v4& A, const m4& B)
    {
        v4 Result = B * A;
        
        return Result;
    }
    
    inline m4 operator/(const m4& A, r32 Scalar)
    {
        r32 OneOver = 1.0f / Scalar;
        
        m4 Result = A * OneOver;
        
        return Result;
    }
    
    
    inline m4 Transpose(const m4& A)
    {
        m4 Result = M4();
        
        for(s32 i = 0; i < 4; ++i)
        {
            for(s32 j = 0; j < 4; ++j)
            {
                Result.E[i][j] = A.E[j][i];
            }
        }
        
        return Result;
    }
    
    // @Cleanup: The Math works fine, need some cleanup.
    inline m4 AffineInverse(const m4& Matrix)
    {
        m4 Result = Matrix;
        
        // Translate Portion
        m4 TranslatePortion = M4();
        TranslatePortion.E[3][0] = -(Result.E[3][0]); 
        TranslatePortion.E[3][1] = -(Result.E[3][1]); 
        TranslatePortion.E[3][2] = -(Result.E[3][2]); 
        
        // LinearPortion        
        m4 Cofactors = M4();
        r32 ZeroZero = (Result.E[1][1] * Result.E[2][2]) - (Result.E[1][2] * Result.E[2][1]);
        r32 ZeroOne = -((Result.E[1][0] * Result.E[2][2]) - (Result.E[1][2] * Result.E[2][0]));
        r32 ZeroTwo = (Result.E[1][0] * Result.E[2][1]) - (Result.E[1][1] * Result.E[2][0]);
        
        // NOTE: This  includes  transposition
        Cofactors.E[0][0] = ZeroZero;
        Cofactors.E[0][1] = -((Result.E[0][1] * Result.E[2][2]) - (Result.E[0][2] * Result.E[2][1]));
        Cofactors.E[0][2] = (Result.E[0][1] * Result.E[1][2]) - (Result.E[0][2] * Result.E[1][1]);
        
        Cofactors.E[1][0] = ZeroOne;
        Cofactors.E[1][1] = (Result.E[0][0] * Result.E[2][2]) - (Result.E[0][2] * Result.E[2][0]);
        Cofactors.E[1][2] = -((Result.E[0][0] * Result.E[1][2]) - (Result.E[0][2] * Result.E[1][0]));
        
        Cofactors.E[2][0] = ZeroTwo;
        Cofactors.E[2][1] = -((Result.E[0][0] * Result.E[2][1]) - (Result.E[0][1] * Result.E[2][0]));;
        Cofactors.E[2][2] = (Result.E[0][0] * Result.E[1][1]) - (Result.E[0][1] * Result.E[1][0]);
        
        r32 Det = (Result.E[0][0] * ZeroZero) + (Result.E[0][1] * ZeroOne) + (Result.E[0][2] * ZeroTwo);
        
        m4 Inverse = Cofactors / (Det);
        
        Result = Inverse * TranslatePortion;
        
        return Result;
    }
    
    inline m3 Mat4ToMat3(const m4& A)
    {
        m3 Result = M3();
        
        Result.E[0][0] = A.E[0][0];
        Result.E[0][1] = A.E[0][1];
        Result.E[0][2] = A.E[0][2];
        
        Result.E[1][0] = A.E[1][0];
        Result.E[1][1] = A.E[1][1];
        Result.E[1][2] = A.E[1][2];
        
        Result.E[2][0] = A.E[2][0];
        Result.E[2][1] = A.E[2][1];
        Result.E[2][2] = A.E[2][2];
        
        return Result;
    }
    
    inline m4 AffineMat4(const m3& A, const v3& T)
    {
        m4 Result = M4();
        
        Result.E[0][0] = A.E[0][0];
        Result.E[0][1] = A.E[0][1];
        Result.E[0][2] = A.E[0][2];
        Result.E[0][3] = 0;
        
        Result.E[1][0] = A.E[1][0];
        Result.E[1][1] = A.E[1][1];
        Result.E[1][2] = A.E[1][2];
        Result.E[1][3] = 0;
        
        Result.E[2][0] = A.E[2][0];
        Result.E[2][1] = A.E[2][1];
        Result.E[2][2] = A.E[2][2];
        Result.E[2][3] = 0;
        
        Result.E[3][0] = T.x; 
        Result.E[3][1] = T.y; 
        Result.E[3][2] = T.z;
        Result.E[3][3] = 1;
        
        return Result;
    }
    
    inline m4 AffineMul(const m4& A, m4& B)
    {
        m4 Result = M4();
        
        Result = AffineMat4(Mat4ToMat3(A) * Mat4ToMat3(B),
                            V3(A.E[3][0] + B.E[3][0],
                               A.E[3][1] + B.E[3][1],
                               A.E[3][2] + B.E[3][2]));
        
        return Result;
    }
};
