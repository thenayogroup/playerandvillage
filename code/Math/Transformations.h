#pragma once

namespace Math
{
    //
    // Mat3
    //
    
    inline m3 M3AffineScale(const v2& ScaleFactor)
    {
        m3 Result = M3();
        Result.E[0][0] *= ScaleFactor.x;
        Result.E[1][1] *= ScaleFactor.y;
        
        return Result;
    }
    
    inline m3 Mat3XRotation(r32 Angle)
    {
        const r32 c = Cos(Angle);
        const r32 s = Sin(Angle);
        
        m3 Result = M3();
        Result.E[1][1] = c;
        Result.E[1][2] = s;
        Result.E[2][1] = -s;
        Result.E[2][2] = c;
        
        return Result;
    }
    
    inline m3 Mat3YRotation(r32 Angle)
    {
        const r32 c = Cos(Angle);
        const r32 s = Sin(Angle);
        
        m3 Result = M3();
        Result.E[0][0] = c;
        Result.E[0][2] = -s;
        Result.E[2][0] = s;
        Result.E[2][2] = c;
        
        return Result;
    }
    
    inline m3 Mat3ZRotation(r32 Angle)
    {
        const r32 c = Cos(Angle);
        const r32 s = Sin(Angle);
        
        m3 Result = M3();
        Result.E[0][0] = c;
        Result.E[0][1] = s;
        Result.E[1][0] = -s;
        Result.E[1][1] = c;
        
        return Result;
    }
    
    inline m3 Translate(const v2& Translate)
    {
        m3 Result = M3();
        Result.E[2][0] = Translate.x;
        Result.E[2][1] = Translate.y;
        
        return Result;
    }
    
    inline m3 Translate(const m3& A, const v2& B)
    {
        m3 Result = A;
        
        Result.E[2][0] += B.x;
        Result.E[2][1] += B.y;
        
        return Result;
    }
    
    
    //
    // Mat4
    //
    
    
    inline m4 XRotation(r32 Angle)
    {
        const r32 c = Cos(Angle);
        const r32 s = Sin(Angle);
        
        m4 Result = M4();
        Result.E[1][1] = c;
        Result.E[1][2] = s;
        Result.E[2][1] = -s;
        Result.E[2][2] = c;
        
        return Result;
    }
    
    inline m4 YRotation(r32 Angle)
    {
        const r32 c = Cos(Angle);
        const r32 s = Sin(Angle);
        
        m4 Result = M4();
        Result.E[0][0] = c;
        Result.E[0][2] = -s;
        Result.E[2][0] = s;
        Result.E[2][2] = c;
        
        return Result;
    }
    
    inline m4 ZRotation(r32 Angle)
    {
        const r32 c = Cos(Angle);
        const r32 s = Sin(Angle);
        
        m4 Result = M4();
        Result.E[0][0] = c;
        Result.E[0][1] = s;
        Result.E[1][0] = -s;
        Result.E[1][1] = c;
        
        return Result;
    }
    
    inline m4 Rotation(r32 Angle, const v3& Axis)
    {
        const r32 c = Cos(Angle);
        const r32 s = Sin(Angle);
        
        v3 NewAxis = Normalize(Axis);
        v3 Temp = (1.0f - c) * NewAxis;
        
        m4 Result = M4();
        Result.E[0][0] = c + Temp.x * NewAxis.x;
        Result.E[0][1] = Temp.x * NewAxis.y + s * NewAxis.z;
        Result.E[0][2] = Temp.x * NewAxis.z - s * NewAxis.y;
        
        Result.E[1][0] = Temp.y * NewAxis.x - s * NewAxis.z;
        Result.E[1][1] = c + Temp.y * NewAxis.y;
        Result.E[1][2] = Temp.y * NewAxis.z + s * NewAxis.x;
        
        Result.E[2][0] = Temp.z * NewAxis.x + s * NewAxis.x;
        Result.E[2][1] = Temp.z * NewAxis.y - s * NewAxis.x;
        Result.E[2][2] = c + Temp.z * NewAxis.z;
        
        return Result;
    }
    
    inline m4 Scale(r32 ScaleFactor)
    {
        m4 Result = M4(ScaleFactor, ScaleFactor, ScaleFactor, 1.0f);
        
        return Result;
    }
    
    inline m4 Scale(const m4& Matrix, const v3& ScaleFactor)
    {
        m4 Result = Matrix;
        Result.E[0][0] *= ScaleFactor.x;
        Result.E[1][1] *= ScaleFactor.y;
        Result.E[2][2] *= ScaleFactor.z;
        return Result;
    }
    
    inline m4 Scale(const v3& ScaleFactor)
    {
        m4 Result = M4();
        Result.E[0][0] *= ScaleFactor.x;
        Result.E[1][1] *= ScaleFactor.y;
        Result.E[2][2] *= ScaleFactor.z;
        return Result;
    }
    
    inline m4 Translate(const v3& Translate)
    {
        m4 Result = M4();
        Result.E[3][0] = Translate.x;
        Result.E[3][1] = Translate.y;
        Result.E[3][2] = Translate.z;
        
        return Result;
    }
    
    inline m4 Translate(const m4& A, const v3& B)
    {
        m4 Result = A;
        
        Result.E[3][0] += B.x;
        Result.E[3][1] += B.y;
        Result.E[3][2] += B.z;
        
        return Result;
    }
    
    inline m4 ResetTranslate(const m4& A)
    {
        m4 Result = A;
        
        Result.E[3][0] = 0;
        Result.E[3][1] = 0;
        Result.E[3][2] = 0;
        
        return Result;
    }
    
    inline m4 Orthographic(r32 left, r32 right,
                           r32 bottom, r32 top,
                           r32 n, r32 f)
    {
        m4 Result = M4(1.0f);
        
        Result.E[0][0] = 2.0f / (right - left);
        Result.E[1][1] = 2.0f / (top - bottom);
        Result.E[3][0] = -(right + left) / (right - left);
        Result.E[3][1] = -(top + bottom) / (top - bottom);
        
        Result.E[2][2] = -2.0f/(f - n);
        Result.E[3][2] = -(f+n)/(f - n);
        
        
        return Result;
    }
    
    struct m4_regular_inverse
    {
        m4 Regular;
        m4 Inverse;
    };
    
    
    // NOTE(Naor): FocalLength = Field of View
    inline m4_regular_inverse Perspective(r32 AspectWidthOverHeight, r32 FocalLength,
                                          r32 zNear, r32 zFar)
    {
        const r32 fovy = FocalLength;
        const r32 aspect = AspectWidthOverHeight;
        
        const r32 tanHalfFovy = Tan(fovy * 0.5f);
        
        r32 t = 1.0f / tanHalfFovy;
        r32 u = (1.0f / aspect) * t;
        r32 fn = - (zFar + zNear) / (zFar - zNear);
        
        m4_regular_inverse Result =
        {
            {
                u, 0, 0, 0,
                0, t, 0, 0,
                0, 0, fn, -1,
                0, 0, 2.0f * fn, 0
            },
            {
                1/u, 0, 0, 0,
                0, 1/t, 0, 0,
                0, 0, 0, 1/(2.0f*fn),
                0, 0, -1, 0.5f
            }
        };
        
        return Result;
    }
    
    
    
    inline m4_regular_inverse PerspectiveProjection(r32 AspectWidthOverHeight, r32 FocalLength,
                                                    r32 NearClipPlane, r32 FarClipPlane)
    {
        r32 a = 1.0f;
        r32 b = AspectWidthOverHeight;
        r32 c = FocalLength;
        
        r32 n = NearClipPlane; // NOTE(casey): Near clip plane _distance_
        r32 f = FarClipPlane; // NOTE(casey): Far clip plane _distance_
        
        // NOTE(casey): These are the perspective correct terms, for when you divide by -z
        r32 d = (n+f) / (n-f);
        r32 e = (2*f*n) / (n-f);
        
        m4_regular_inverse Result =
        {
            // NOTE(casey): Forward
            {a*c,    0,  0,  0,
                0,  b*c,  0,  0,
                0,    0,  d,  -1,
                0,    0, e,  0},
            
            // NOTE(casey): Inverse
            {1/(a*c),       0,   0,   0,
                0, 1/(b*c),   0,   0,
                0,       0,   0,  1/e,
                0,       0, -1, d/e}
        };
        
        return(Result);
    }
    
    
    inline m4 LookAt(const v3& CameraPosition, const v3& PointToLookAt, const v3& CameraUp)
    {
        const v3 f = Normalize(PointToLookAt - CameraPosition);
        const v3 s = Normalize(Cross(f, CameraUp));
        const v3 u = Cross(s, f);
        
        m4 Result = M4();
        Result.E[0][0] = s.x;
        Result.E[0][1] = u.x;
        Result.E[0][2] = -f.x;
        Result.E[1][0] = s.y;
        Result.E[1][1] = u.y;
        Result.E[1][2] = -f.y;
        Result.E[2][0] = s.z;
        Result.E[2][1] = u.z;
        Result.E[2][2] = -f.z;
        Result.E[3][0] = -Dot(s, CameraPosition);
        Result.E[3][1] = -Dot(u, CameraPosition);
        Result.E[3][2] = Dot(f, CameraPosition);
        
        return Result;
    }
};