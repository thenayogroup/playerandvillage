#pragma once

namespace Math
{
    real32 LinearInterpolation(real32 A, real32 B, real32 T)
    {
        real32 Result = A * (1 - T) + B * T;
        
        return Result;
    }
    
    real32 CosineInterpolation(real32 A, real32 B, real32 T)
    {
        real32 T2 = (1.0f - Cos(T*Pi)) * 0.5f;
        real32 Result = LinearInterpolation(A, B, T2);
        
        return Result;
    }
};

