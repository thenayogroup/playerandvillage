
// TODO(Jonathan):  Move that to appropriate place
struct plane
{
    v3 Normal;
    r32 Distance;
};

void Normalize(plane& Plane)
{
    r32 Scale = 1 / Plane.Normal.Length();
    Plane.Normal.x *= Scale;
    Plane.Normal.y *= Scale;
    Plane.Normal.z *= Scale;
    Plane.Distance *= Scale;
}


