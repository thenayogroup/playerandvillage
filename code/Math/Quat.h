#pragma once

// TODO(Naor): 
// Interpolation
// Rotation
// Mat4/Mat3 to quaternions (See GLM: quaternion.hpp#283)

namespace Math
{
    class quat
    {
        public:
        
        //
        // Variables
        //
        r32 w, x, y, z;
        
        //
        // Operators
        //
        
        r32 Dot(const quat& Other)
        {
            r32 Result = (x * Other.x) + (y * Other.y) + (z * Other.z) + (w * Other.w);
            
            return Result;
        }
        
        
        r32 Length()
        {
            r32 Result = SquareRoot(Dot(*this));
            
            return Result;
        }
        
        void Normalize()
        {
            r32 QuatDot = Dot(*this);
            // NOTE(Naor): If the dot product of (A,A) is 1 we don't need
            // to normalize it (avoiding square root)
            if(QuatDot == 1.0f)
                return;
            
            r32 QuatLength = SquareRoot(QuatDot);
            
            // NOTE(Naor): We don't want to divide by zero..
            if(QuatLength <= 0.0f)
                return;
            
            r32 OneOverLength = 1.0f / QuatLength;
            
            w *= OneOverLength;
            x *= OneOverLength;
            y *= OneOverLength;
            z *= OneOverLength;
        }
        
        bool IsNormalized()
        {
            bool Result = Dot(*this) == 1.0f;
            
            return Result;
        }
        
        r32 Roll()
        {
            r32 Result = ATan2(2.0f * (x * y + w * z), w * w + x * x - y * y - z * z);
            
            return Result;
        }
        
        r32 Pitch()
        {
            r32 Result = ATan2(2.0f * (y * z + w * x), w * w - x * x - y * y + z * z);
            
            return Result;
        }
        
        r32 Yaw()
        {
            r32 Result = ArcSin(Clamp(-2.0f * (x * z - w * y), -1.0f, 1.0f));
            
            return Result;
        }
        
        v3 ToEulerAngles()
        {
            v3 Result = V3(Pitch(), Yaw(), Roll());
            
            return Result;
        }
        
        m4 ToMat4(bool CheckNormalize = false)
        {
            if(CheckNormalize)
            {
                if(IsNormalized())
                {
                    Normalize();
                }
            }
            
            const r32 xx = x * x;
            const r32 yy = y * y;
            const r32 zz = z * z;
            const r32 xz = x * z;
            const r32 xy = x * y;
            const r32 yz = y * z;
            const r32 wx = w * x;
            const r32 wy = w * y;
            const r32 wz = w * z;
            
            m4 Result;
            Result.E[0][0] = 1.0f - 2.0f * (yy + zz);
            Result.E[0][1] = 2.0f * (xy + wz);
            Result.E[0][2] = 2.0f * (xz - wy);
            Result.E[1][0] = 2.0f * (xy - wz);
            Result.E[1][1] = 1.0f - 2.0f * (xx + zz);
            Result.E[1][2] = 2.0f * (yz + wx);
            Result.E[2][0] = 2.0f * (xz + wy);
            Result.E[2][1] = 2.0f * (yz - wx);
            Result.E[2][2] = 1.0f - 2.0f * (xx + yy);
            Result.E[3][3] = 1.0f;
            
            return Result;
        }
        
        //
        // Operator Overloading
        //
        void operator-=(const quat& Other)
        {
            w -= Other.w;
            x -= Other.x;
            y -= Other.y;
            z -= Other.z;
        }
        
        void operator+=(const quat& Other)
        {
            w += Other.w;
            x += Other.x;
            y += Other.y;
            z += Other.z;
        }
        
        void operator*=(const quat& Other)
        {
            w = w * Other.w - x * Other.x - y * Other.y - z * Other.z;
            x = w * Other.x + x * Other.w + y * Other.z - z * Other.y;
            y = w * Other.y + y * Other.w + z * Other.x - x * Other.z;
            z = w * Other.z + z * Other.w + x * Other.y - y * Other.x;
        }
    };
    
    //
    // Generation
    //
    
    inline quat Q()
    {
        quat Result = {};
        Result.w = 1.0f;
        
        return Result;
    }
    inline quat Q(r32 W, r32 X, r32 Y, r32 Z)
    {
        quat Result = {};
        Result.w = W;
        Result.x = X;
        Result.y = Y;
        Result.z = Z;
        
        return Result;
    }
    inline quat Q(const v3& Euler)
    {
        quat Result = {};
        v3 c = Cos(Euler * 0.5f);
        v3 s = Sin(Euler * 0.5f);
        
        Result.w = c.x * c.y * c.z + s.x * s.y * s.z;
        Result.x = s.x * c.y * c.z - c.x * s.y * s.z;
        Result.y = c.x * s.y * c.z + s.x * c.y * s.z;
        Result.z = c.x * c.y * s.z - s.x * s.y * c.z;
        
        return Result;
    }
    inline quat Q(r32 Pitch, r32 Yaw, r32 Roll)
    {
        quat Result = Q(V3(Pitch, Yaw, Roll));
        
        return Result;
    }
    inline quat Q(const v3& Axis, r32 Angle)
    {
        quat Result = {};
        const r32 s = Sin(Angle * 0.5f);
        
        Result.w = Cos(Angle * 0.5f);
        Result.x = Axis.x * s;
        Result.y = Axis.y * s;
        Result.z = Axis.z * s;
        
        return Result;
    }
    
    
    inline quat EulerToQuaternion(const v3& A)
    {
        v3 c = Cos(A * 0.5f);
        v3 s = Sin(A * 0.5f);
        
        quat Result = Q(c.x * c.y * c.z + s.x * s.y * s.z,
                        s.x * c.y * c.z - c.x * s.y * s.z,
                        c.x * s.y * c.z + s.x * c.y * s.z,
                        c.x * c.y * s.z - s.x * s.y * c.z);
        
        return Result;
    }
    
    inline quat AngleAxisToQuaternion(const v3& Axis, r32 Angle)
    {
        const r32 s = Sin(Angle * 0.5f);
        
        quat Result = 
        {
            Cos(Angle * 0.5f),
            Axis.x * s,
            Axis.y * s,
            Axis.z * s
        };
        
        return Result;
    }
    
    //
    // Operations
    //
    
    inline r32 Dot(const quat& A, const quat& B)
    {
        r32 Result = (A.x * B.x) + (A.y * B.y) + (A.z * B.z) + (A.w * B.w);
        
        return Result;
    }
    
    inline r32 Length(const quat& A)
    {
        r32 Result = SquareRoot(Dot(A, A));
        
        return Result;
    }
    
    inline quat Normalize(const quat& A)
    {
        r32 QuatDot = Dot(A, A);
        // NOTE(Naor): If the dot product of (A,A) is 1 we don't need
        // to normalize it (avoiding square root)
        if(QuatDot == 1.0f)
            return A;
        
        r32 QuatLength = SquareRoot(QuatDot);
        
        // NOTE(Naor): We don't want to divide by zero..
        if(QuatLength <= 0.0f)
        {
            quat Result = {0.0f, 0.0f, 0.0f, 1.0f};
            
            return Result;
        }
        
        r32 OneOverLength = 1.0f / QuatLength;
        
        quat Result = {A.w * OneOverLength, A.x * OneOverLength, A.y * OneOverLength, A.z * OneOverLength};
        
        return Result;
    }
    
    inline bool IsNormalized(const quat& A)
    {
        bool Result = Dot(A, A) == 1.0f;
        
        return Result;
    }
    
    inline r32 Roll(const quat& A)
    {
        r32 Result = ATan2(2.0f * (A.x * A.y + A.w * A.z), A.w * A.w + A.x * A.x - A.y * A.y - A.z * A.x);
        
        return Result;
    }
    
    inline r32 Pitch(const quat& A)
    {
        r32 Result = ATan2(2.0f * (A.y * A.z + A.w * A.x), A.w * A.w - A.x * A.x - A.y * A.y + A.z * A.x);
        
        return Result;
    }
    
    inline r32 Yaw(const quat& A)
    {
        r32 Result = ArcSin(Clamp(-2.0f * (A.x * A.z - A.w * A.y), -1.0f, 1.0f));
        
        return Result;
    }
    
    inline v3 ToEulerAngles(const quat& A)
    {
        v3 Result = V3(Pitch(A), Yaw(A), Roll(A));
        
        return Result;
    }
    
    // NOTE(Naor): We don't want to pass it by reference becuase
    // if we want to check if it is normalized we want to change the 
    // quat itself (normalize it), so we will need to create a copy
    // of it anyway..
    inline m4 ToMat4(quat A, bool CheckNormalize = false)
    {
        if(CheckNormalize)
        {
            if(IsNormalized(A))
            {
                A = Normalize(A);
            }
        }
        
        const r32 xx = A.x * A.x;
        const r32 yy = A.y * A.y;
        const r32 zz = A.z * A.z;
        const r32 xz = A.x * A.z;
        const r32 xy = A.x * A.y;
        const r32 yz = A.y * A.z;
        const r32 wx = A.w * A.x;
        const r32 wy = A.w * A.y;
        const r32 wz = A.w * A.z;
        
        
        m4 Result;
        Result.E[0][0] = 1.0f - 2.0f * (yy + zz);
        Result.E[0][1] = 2.0f * (xy + wz);
        Result.E[0][2] = 2.0f * (xz - wy);
        Result.E[1][0] = 2.0f * (xy - wz);
        Result.E[1][1] = 1.0f - 2.0f * (xx + zz);
        Result.E[1][2] = 2.0f * (yz + wx);
        Result.E[2][0] = 2.0f * (xz + wy);
        Result.E[2][1] = 2.0f * (yz - wx);
        Result.E[2][2] = 1.0f - 2.0f * (xx + yy);
        Result.E[3][3] = 1.0f;
        
        return Result;
    }
    
    //
    // Operator Overloading
    //
    
    inline quat operator-(const quat& A)
    {
        quat Result = Q(-A.w, -A.x, -A.y, -A.z);
        
        return Result;
    }
    
    inline quat operator-(const quat& A, const quat& B)
    {
        quat Result = Q(A.w - B.w, A.x - B.x, A.y - B.y, A.z - B.z);
        
        return Result;
    }
    
    inline quat operator+(const quat& A, const quat& B)
    {
        quat Result = Q(A.w + B.w, A.x + B.x, A.y + B.y, A.z + B.z);
        
        return Result;
    }
    
    inline quat operator*(const quat& A, const quat& B)
    {
        quat Result = Q(A.w * B.w - A.x * B.x - A.y * B.y - A.z * B.z,
                        A.w * B.x + A.x * B.w + A.y * B.z - A.z * B.y,
                        A.w * B.y + A.y * B.w + A.z * B.x - A.x * B.z,
                        A.w * B.z + A.z * B.w + A.x * B.y - A.y * B.x);
        
        return Result;
    }
    
    inline v3 operator*(const quat& A, const v3& B)
    {
        const v3 QuatVector = V3(A.x, A.y, A.z);
        const v3 uv = Cross(QuatVector, B);
        const v3 uuv = Cross(QuatVector, uv);
        
        v3 Result = B + ((uv * A.w) + uuv) * 2.0f;
        
        return Result;
    }
    };