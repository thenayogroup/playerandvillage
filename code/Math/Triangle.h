#pragma once

namespace Math
{
    v3 CalculateNormal(const v3& P0, const v3& P1, const v3& P2)
    {
        v3 U = P1 - P0;
        v3 V = P2 - P0;
        
        v3 Result = U.Cross(V);
        
        return Normalize(Result);
    }
};
