#pragma once

#include <random>

struct NoiseSettings
{
    int32 Seed;
    int32 Octaves;
    real32 Amplitude;
    real32 Roughness;
};

// TODO(Naor): Complete the noise function from ThinMatrix and make the RandomEngine & NoiseGenerator
// static somehow WITH the Hot-Loading
static std::mt19937 RandomGen;
static std::uniform_real_distribution<real32> NoiseGenerator(-1.0f, 1.0f); 

static real32 GetNoise(int32 X, int32 Y)
{
    return NoiseGenerator(RandomGen);
}

static real32 SmoothNoise(int32 X, int32 Y)
{
    real32 Corners = (GetNoise(X - 1, Y - 1) + GetNoise(X + 1, Y - 1) + GetNoise(X - 1, Y + 1)
                      + GetNoise(X + 1, Y + 1)) / 16.0f;
    real32 Sides = (GetNoise(X - 1, Y) + GetNoise(X + 1, Y) + GetNoise(X, Y - 1) + GetNoise(X, Y + 1)) / 8.0f;
    real32 Center = GetNoise(X, Y) / 4.0f;
    return Corners + Sides + Center;
}

static real32 InterpolatedNoise(real32 X, real32 Y)
{
    int32 IntX = Math::FloorToInt(X);
    int32 IntY = Math::FloorToInt(Y);
    real32 FracX = Math::Fract(X);
    real32 FracY = Math::Fract(Y);
    
    real32 V1 = SmoothNoise(IntX, IntY);
    real32 V2 = SmoothNoise(IntX + 1, IntY);
    real32 V3 = SmoothNoise(IntX, IntY + 1);
    real32 V4 = SmoothNoise(IntX + 1, IntY + 1);
    real32 I1 = Math::CosineInterpolation(V1, V2, FracX);
    real32 I2 = Math::CosineInterpolation(V3, V4, FracX);
    return Math::CosineInterpolation(I1, I2, FracY);
}

static real32 PerlinNoise(const NoiseSettings& Settings, real32 X, real32 Y)
{
    RandomGen.seed(ToInt32(X) * 49632 + ToInt32(Y) * 325176 + Settings.Seed);
    
    real32 Total = 0.0f;
    real32 D = Math::Power(2, Settings.Octaves - 1.0f);
    
    for(int32 i = 0; i < Settings.Octaves; ++i)
    {
        real32 Freq = Math::Power(2.0f, ToReal32(i)) / D;
        real32 Amp = Math::Power(Settings.Roughness, ToReal32(i)) * Settings.Amplitude;
        Total += InterpolatedNoise(X * Freq, Y * Freq) * Amp;
    }
    
    return Total;
}

//
// Temp
//

real32 hash(Vec3 p)  // replace this by something better
{
    p  = 50.0f*Fract( p*0.3183099f + Vec3(0.71f,0.113f,0.419f));
    // TODO(Naor): Find out why we need Math::Fract here and above only Fract!!
    return -1.0f+2.0f*Math::Fract( p.x*p.y*p.z*(p.x+p.y+p.z) );
}

float hash1( float n )
{
    return Math::Fract( n*17.0f*Math::Fract( n*0.3183099f ) );
}

float hash1( Vec2 p )
{
    p  = 50.0f*Fract( p*0.3183099f );
    return Math::Fract( p.x*p.y*(p.x+p.y) );
}

// return value noise (in x) and its derivatives (in yzw)
Vec4 noised( Vec3 x )
{
    Vec3 p = Floor(x);
    Vec3 w = Fract(x);
    
#if 1
    // quintic interpolation
    Vec3 u = w*w*w*(w*(w*6.0f-15.0f)+10.0f);
    Vec3 du = 30.0f*w*w*(w*(w-2.0f)+1.0f);
#else
    // cubic interpolation
    Vec3 u = w*w*(3.0f-2.0f*w);
    Vec3 du = 6.0f*w*(1.0f-w);
#endif    
    
    
    real32 a = hash(p+Vec3(0.0f,0.0f,0.0f));
    real32 b = hash(p+Vec3(1.0f,0.0f,0.0f));
    real32 c = hash(p+Vec3(0.0f,1.0f,0.0f));
    real32 d = hash(p+Vec3(1.0f,1.0f,0.0f));
    real32 e = hash(p+Vec3(0.0f,0.0f,1.0f));
    real32 f = hash(p+Vec3(1.0f,0.0f,1.0f));
    real32 g = hash(p+Vec3(0.0f,1.0f,1.0f));
    real32 h = hash(p+Vec3(1.0f,1.0f,1.0f));
    
    real32 k0 =   a;
    real32 k1 =   b - a;
    real32 k2 =   c - a;
    real32 k3 =   e - a;
    real32 k4 =   a - b - c + d;
    real32 k5 =   a - c - e + g;
    real32 k6 =   a - b - e + f;
    real32 k7 = - a + b + c - d + e - f - g + h;
    
    return Vec4( k0 + k1*u.x + k2*u.y + k3*u.z + k4*u.x*u.y + k5*u.y*u.z + k6*u.z*u.x + k7*u.x*u.y*u.z, 
                du * Vec3( k1 + k4*u.y + k6*u.z + k7*u.y*u.z,
                          k2 + k5*u.z + k4*u.x + k7*u.z*u.x,
                          k3 + k6*u.x + k5*u.y + k7*u.x*u.y ) );
}

Vec3 noised( Vec2 x )
{
    Vec2 p = Floor(x);
    Vec2 w = Fract(x);
    
    Vec2 u = w*w*w*(w*(w*6.0f-15.0f)+10.0f);
    Vec2 du = 30.0f*w*w*(w*(w-2.0f)+1.0f);
    
    float a = hash1(p+Vec2(0.0f,0.0f));
    float b = hash1(p+Vec2(1.0f,0.0f));
    float c = hash1(p+Vec2(0.0f,1.0f));
    float d = hash1(p+Vec2(1.0f,1.0f));
    
    float k0 = a;
    float k1 = b - a;
    float k2 = c - a;
    float k4 = a - b - c + d;
    
    return Vec3( -1.0f+2.0f*(k0 + k1*u.x + k2*u.y + k4*u.x*u.y), 
                2.0f* du * Vec2( k1 + k4*u.y,
                                k2 + k4*u.x ) );
}

static const Mat3 m3( 0.00f,  0.80f,  0.60f,
                     -0.80f,  0.36f, -0.48f,
                     -0.60f, -0.48f,  0.64f );
static const Mat3 m3i( 0.00f, -0.80f, -0.60f,
                      0.80f,  0.36f, -0.48f,
                      0.60f, -0.48f,  0.64f );

static const Mat2 m2(  0.80f,  0.60f,
                     -0.60f,  0.80f );
static const Mat2 m2i( 0.80f, -0.60f,
                      0.60f,  0.80f );

Vec4 fbm(Vec3 x, int octaves ) 
{
    float f = 1.98f;  // could be 2.0
    float s = 0.49f;  // could be 0.5
    float a = 0.0f;
    float b = 0.5f;
    Vec3  d(0.0f);
    Mat3  m;
    for( int i=0; i < octaves; i++ )
    {
        Vec4 n = noised(x);
        a += b*n.x;          // accumulate values		
        d += b*m*Vec3(n.y, n.z, n.w);      // accumulate derivatives
        b *= s;
        x = f*m3*x;
        m = f*m3i*m;
    }
    return Vec4( a, d );
}

Vec3 fbmd_9( Vec2 x, int octaves )
{
    float f = 1.9f;
    float s = 0.55f;
    float a = 0.0f;
    float b = 0.5f;
    Vec2  d(0.0f);
    Mat2  m(1.0f,0.0f,0.0f,1.0f);
    for( int i=0; i<octaves; i++ )
    {
        Vec3 n = noised(x);
        a += b*n.x;          // accumulate values		
        d += b*m*Vec2(n.y, n.z);       // accumulate derivatives
        b *= s;
        x = f*m2*x;
        m = f*m2i*m;
    }
    return Vec3( a, d );
}