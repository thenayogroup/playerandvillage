#pragma once

namespace Math
{
    // TODO(Naor): Implement this!
    class Color
    {
        
    };
    
    inline v3 RGBTo01(const v3& RGB)
    {
        v3 Result = RGB * (1.0f/255.0f);
        return Result;
    }
    
    inline v3 RGBTo01(real32 Red, real32 Green, real32 Blue)
    {
        v3 Result = V3(Red, Green, Blue) * (1.0f/255.0f);
        return Result;
    }
    
    inline v3 MixRGB(const v3& A, const v3& B, real32 T)
    {
        v3 Result = A + (B-A)*T;
        
        return Result;
    }
    
    v3 RGBtoHSV(const v3& RGB)
    {
        v3 Result;
        float min, max, delta;
        
        const real32 r = RGB.x;
        const real32 g = RGB.y;
        const real32 b = RGB.z;
        
        min = Min(Min(r, g), b);
        max = Max(Max(r, g), b );
        Result.z = max;				// v
        
        delta = max - min;
        
        if( max != 0 )
            Result.y = delta / max;		// s
        else {
            // r = g = b = 0		// s = 0, v is undefined
            Result.y = 0;
            Result.x = -1;
            return Result;
        }
        
        if( r == max )
            Result.x = ( g - b ) / delta;		// between yellow & magenta
        else if( g == max )
            Result.x = 2 + ( b - r ) / delta;	// between cyan & yellow
        else
            Result.x = 4 + ( r - g ) / delta;	// between magenta & cyan
        
        Result.x *= 60;				// degrees
        if( Result.x < 0 )
            Result.x += 360;
        
        return Result;
    }
    
    v3 HSVtoRGB(const v3& HSV)
    {
        v3 Result;
        int i;
        float f, p, q, t;
        
        real32 h = HSV.x;
        real32 s = HSV.y;
        real32 v = HSV.z;
        
        if( s == 0 ) {
            // achromatic (grey)
            Result.x = Result.y = Result.z = v;
            return Result;
        }
        
        h /= 60;			// sector 0 to 5
        i = FloorI32(h);
        f = h - i;			// factorial part of h
        p = v * ( 1 - s );
        q = v * ( 1 - s * f );
        t = v * ( 1 - s * ( 1 - f ) );
        
        switch( i ) {
            case 0:
            Result.x = v;
            Result.y = t;
            Result.z = p;
            break;
            case 1:
            Result.x = q;
            Result.y = v;
            Result.z = p;
            break;
            case 2:
            Result.x = p;
            Result.y = v;
            Result.z = t;
            break;
            case 3:
            Result.x = p;
            Result.y = q;
            Result.z = v;
            break;
            case 4:
            Result.x = t;
            Result.y = p;
            Result.z = v;
            break;
            default:		// case 5:
            Result.x = v;
            Result.y = p;
            Result.z = q;
            break;
        }
        
        return Result;
    }
};