#pragma once

#include <random>

namespace Math
{
    static std::random_device RandoDevice;
    static std::mt19937 Generator(RandoDevice());
    static std::uniform_real_distribution<real32> RandomDistributor(-1.0f, 1.0f);
    
    // This will return a value between -1.0 and 1.0
    inline real32 Random()
    {
        real32 Result = RandomDistributor(Generator);
        
        return Result;
    }
    
    // We cant use real32 here because template doesn't support it
    template<int32 Min, int32 Max>
        inline real32 Random()
    {
        static std::uniform_real_distribution<real32> Random(Min, Max);
        
        real32 Result = Random(Generator);
        
        return Result;
    }
    
    template<int32 Min, int32 Max>
        inline int32 RandomInt32()
    {
        static std::uniform_int_distribution<int32> Random(Min, Max);
        
        int32 Result = Random(Generator);
        
        return Result;
    }
    
    template<int32 Min, int32 Max>
        inline v2 RandomVec2()
    {
        static std::uniform_real_distribution<real32> Random(Min, Max);
        
        v2 Result = V2(Random(Generator), Random(Generator));
        
        return Result;
    }
    
    template<int32 Min, int32 Max>
        inline v3 RandomVec3()
    {
        static std::uniform_real_distribution<real32> Random(Min, Max);
        
        v3 Result = v3(Random(Generator), Random(Generator), Random(Generator));
        
        return Result;
    }
    
    template<int32 Min, int32 Max>
        inline v4 RandomVec4()
    {
        static std::uniform_real_distribution<real32> Random(Min, Max);
        
        v4 Result = v4(Random(Generator), Random(Generator), Random(Generator), Random(Generator));
        
        return Result;
    }
};