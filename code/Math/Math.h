#pragma once

// TODO(Naor): 
// Euler angles
// Color Spaces
// Easing functions

namespace Math
{
    //
    // Constants
    //
    static constexpr real32 Pi = 3.14159265358979323846264338327950288f;
    static constexpr real32 TwoPi = 6.28318530717958647692528676655900576f;
    static constexpr real32 HalfPi = 1.57079632679489661923132169163975144f;
    static constexpr real32 RootPi = 1.772453850905516027f;
    static constexpr real32 E = 2.71828182845904523536f;
    static constexpr real32 Euler = 0.577215664901532860606f;
    static constexpr real32 GoldenRatio = 1.61803398874989484820458683436563811f;
    
    //
    // Operations
    //
    
#ifdef NAYO_INTRINSIC
    inline real32 SquareRoot(real32 Scalar)
    {
        real32 Result = _mm_cvtss_f32(_mm_sqrt_ss(_mm_set_ss(Scalar)));
        
        return(Result);
    }
#else
    inline real32 SquareRoot(real32 Scalar)
    {
        real32 Result = sqrtf(Scalar);
        
        return(Result);
    }
#endif
    
    inline real32 Square(real32 Scalar)
    {
        real32 Result = Scalar * Scalar;
        
        return Result;
    }
    
    inline real32 Power(real32 A, real32 B)
    {
        real32 Result = pow(A, B);
        
        return Result;
        
    }
    
    inline real32 Sin(real32 Angle)
    {
        real32 Result = sinf(Angle);
        
        return Result;
    }
    
    inline real32 ArcSin(real32 Angle)
    {
        real32 Result = asinf(Angle);
        
        return Result;
    }
    
    inline real32 Cos(real32 Angle)
    {
        real32 Result = cosf(Angle);
        
        return Result;
    }
    
    inline real32 ArcCos(real32 Angle)
    {
        real32 Result = acosf(Angle);
        
        return Result;
    }
    
    inline real32 Tan(real32 Angle)
    {
        real32 Result = tanf(Angle);
        
        return Result;
    }
    
    inline real32 ATan2(real32 Y, real32 X)
    {
        real32 Result = atan2f(Y, X);
        
        return Result;
    }
    
    inline real32 Lerp(real32 A, real32 B, real32 t)
    {
        real32 Result = (1.0f - t)*A + t*B;
        
        return Result;
    }
    
    inline real32 Clamp(real32 Value, real32 Min, real32 Max)
    {
        real32 Result = Value;
        
        if(Result < Min)
        {
            Result = Min;
        }
        else if(Result > Max)
        {
            Result = Max;
        }
        
        return Result;
    }
    
    inline real32 Floor(real32 A)
    {
        real32 Result = std::floorf(A);
        
        return Result;
    }
    
    inline int32 FloorI32(real32 A)
    {
        int32 Result = ToInt32(std::floorf(A));
        
        return Result;
    }
    
    inline real32 Ceil(real32 A)
    {
        real32 Result = std::ceilf(A);
        
        return Result;
    }
    
    inline int32 CeilI32(real32 A)
    {
        int32 Result = ToInt32(std::ceilf(A));
        
        return Result;
    }
    
    inline int32 RoundR32ToI32(real32 A)
    {
        int32 Result = ToInt32(A + 0.5);
        return Result;
    }
    
    inline real32 Abs(real32 A)
    {
        real32 Result = A;
        
        if(Result < 0.0f)
            Result = -Result;
        
        return Result;
    }
    
    inline int32 Abs(int32 A)
    {
        int32 Result = A;
        
        if(Result < 0)
            Result= -Result;
        
        return Result;
    }
    
    inline real32 Fract(real32 A)
    {
        real32 Result = A - Floor(A);
        
        return Result;
    }
    
    inline real32 ToRange(real32 Value, real32 OldMin, real32 OldMax,
                          real32 NewMin, real32 NewMax)
    {
        real32 Result = (((Value - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin;
        
        return Result;
    }
    
    inline real32 Min(real32 A, real32 B)
    {
        real32 Result = A < B ? A : B;
        
        return Result;
    }
    
    inline int32 Min(int32 A, int32 B)
    {
        int32 Result = A < B ? A : B;
        
        return Result;
    }
    
    inline real32 Max(real32 A, real32 B)
    {
        real32 Result = A > B ? A : B;
        
        return Result;
    }
    
    inline int32 Max(int32 A, int32 B)
    {
        int32 Result = A > B ? A : B;
        
        return Result;
    }
    
    inline real32 Radians(real32 Degree)
    {
        // 1 rad = 0.0174... deg
        real32 Result = Degree * 0.01745329251994329576923690768489f; 
        
        return Result;
    }
    
    inline real32 Degrees(real32 Radians)
    {
        // 1 deg = 57.2957... rad
        real32 Result = Radians * 57.295779513082320876798154814105f; 
        
        return Result;
    }
    
    // TODO: These need to be updated regularly when adding functions to the classes,
    // either fix this or try to be consistent with the prototypes.
    
    //
    // Prototypes
    //
    
    //
    // Generation 
    //
    inline v2 V2();
    inline v2 V2(r32 Value);
    inline v2 V2(r32 X, r32 Y);
    
    //
    // v2 Operator overloading
    //
    inline v2 operator*(const v2& A, const v2& B);
    inline v2 operator*(const v2& A, r32 Scalar);
    inline v2 operator*(r32 Scalar, const v2& A);
    inline v2 operator+(const v2& A, const v2& B);
    inline v2 operator-(const v2& A, const v2& B);
    inline v2& operator-(v2& A);
    inline bool operator==(const v2& A, const v2& B);
    inline bool operator!=(const v2& A, const v2& B);
    inline std::ostream& operator<<(std::ostream& out, const v2& A);
    //
    // v3 Operator overloading
    //
    inline vec3 operator*(const v3& A, const v3& B);
    inline vec3 operator*(const v3& A, r32 Scalar);
    inline vec3 operator*(r32 Scalar, const v3& A);
    inline vec3 operator/(const v3& A, r32 Scalar);
    inline vec3 operator+(const v3& A, const v3& B);
    inline vec3 operator-(const v3& A, const v3& B);
    inline vec3& operator-(v3& A);
    inline bool operator==(const v3& A, const v3& B);
    inline bool operator!=(const v3& A, const v3& B);
    inline std::ostream& operator<<(std::ostream& out, const v3& A);
    //
    // v4 Operator overloading
    //
    inline vec4 operator*(const v4& A, const v4& B);
    inline vec4 operator*(r32 Scalar, const v4& A);
    inline vec4 operator+(const v4& A, const v4& B);
    inline vec4 operator-(const v4& A, const v4& B);
    inline vec4& operator-(v4& A);
    inline bool operator==(const v4& A, const v4& B);
    inline bool operator!=(const v4& A, const v4& B);
    inline std::ostream& operator<<(std::ostream& out, const v4& A);
    //
    // Operations on vectors
    //
    inline r32 Dot(const v2& A, const v2& B);
    inline r32 Dot(const v3& A, const v3& B);
    inline r32 Dot(const v4& A, const v4& B);
    inline vec3 Cross(const v3& A, const v3& B);
    inline r32 Length(const v2& A);
    inline r32 Length(const v3& A);
    inline r32 Length(const v4& A);
    inline v2 Normalize(const v2& A);
    inline vec3 Normalize(const v3& A);
    inline vec4 Normalize(const v4& A);
    inline r32 Distance(const v2& A, const v2& B);
    inline r32 Distance(const v3& A, const v3& B);
    inline r32 Distance(const v4& A, const v4& B);
    inline v2 Cos(const v2& A);
    inline vec3 Cos(const v3& A);
    inline v4 Cos(const v4& A);
    inline v2 Sin(const v2& A);
    inline v3 Sin(const v3& A);
    inline v4 Sin(const v4& A);
    //
    // Matrix 2x2
    //
    inline m2 operator*(const m2& A, const m2& B);
    inline v2 operator*(const m2& A, const v2& B);
    inline v2 operator*(const v2& A, const m2& B);
    inline m2 operator*(const m2& A, r32 Scalar);
    inline m2 operator*(r32 Scalar, const m2& A);
    inline m2 Transpose(const m2& A);
    //
    // Matrix 3x3
    //
    inline m3 operator*(const m3& A, const m3& B);
    inline v3 operator*(const m3& A, const v3& B);
    inline v3 operator*(const v3& A, const m3& B);
    inline m3 operator*(const m3& A, r32 Scalar);
    inline m3 operator*(r32 Scalar, const m3& A);
    inline m3 operator/(const m3& A, r32 Scalar);
    inline m3 Transpose(const m3& A);
    //
    // Matrix 4x4
    //
    inline m4 operator*(const m4& A, const m4& B);
    inline v4 operator*(const m4& A, const v4& B);
    inline v4 operator*(const v4& A, const m4& B);
    inline m4 operator*(const m4& A, r32 Scalar);
    inline m4 operator*(r32 Scalar, const m4& A);
    inline m4 operator/(const m4& A, r32 Scalar);    
    inline m4 Transpose(const m4& A);
    inline m4 AffineInverse(const m4& Matrix);
    inline m4 Inverse(const m4& Matrix);
    
    
}
// NOTE(Naor): We put these header here because we want to be familiar with the above
#include "Vector.h"
#include "Matrix.h"
#include "Quat.h"
#include "Random.h"
#include "Transformations.h"
#include "Color.h"
#include "Interpolations.h"
#include "Triangle.h"
#include "Primitives.h"

using Math::V2;
using Math::V3;
using Math::V4;

using Math::M2;
using Math::M3;
using Math::M4;

using Math::Q;