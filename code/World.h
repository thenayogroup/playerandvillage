#pragma once

// NOTE(Naor): Currently, the terrain works with radius above 2!
static constexpr s32 WORLD_RADIUS = 50;
static constexpr r32 Y_OFFSET = 2.0f;
static constexpr r32 TERRAIN_SCALE = 5.0f;
static constexpr s32 WORLD_SIZE = int32(WORLD_RADIUS * WORLD_RADIUS * 2);

struct world
{
    mesh Mesh;
    r32 MaxHeight;
    r32 MinHeight;
};


// TODO(Naor): Change this height to something more smooth and get the position based on a grid.
static r32 GetWorldHeight(r32 X, r32 Z)
{
    static constexpr r32 Freq = 0.005f;
    r32 Result = stb_perlin_fbm_noise3(X*Freq, 0.0f, Z*Freq, 2.5f, 0.5f, 5, 0, 0, 0)*Y_OFFSET;
    return Result;
}
