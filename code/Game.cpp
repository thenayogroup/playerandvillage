#include "Game.h"
#include "ShaderProgram.cpp"
#include "Audio.cpp"
#include "Renderer.cpp"
#include "RendererEvan.cpp"
#include "TriangleMesh.cpp"
#include "FontRenderer.cpp"
#include "Inventory.cpp"
#include "MousePicker.cpp"
#include "Entity.cpp"
#include "World.cpp"
#include "Projectile.cpp"
#include "Crosshair.h"
#include "Buildings.cpp"
#include "AssetsManager.cpp"


// TODO(Naor): Find a better solution to how to access GameState, Globals and more, this is really bad..

// TODO(Naor): Remove every debug feature that is not meant to be on release build, there are a few that are
// out of the #ifdef NAYO_DEBUG scope..

static void FirstInitialize()
{
    stbi_set_flip_vertically_on_load(1);
    SDL_SetRelativeMouseMode(SDL_TRUE);
    GameState = new (GameState) game_state;
    
    //
    // Opengl Settings stuff
    //
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    glDepthFunc(GL_LEQUAL);
    glClearColor(0.15f, 0.4f, 0.7f, 1.0f);
    glProvokingVertex(GL_FIRST_VERTEX_CONVENTION);
    // Gamma correction
    //glEnable(GL_FRAMEBUFFER_SRGB);
    //
    // Global stuff
    //
    GameState->IsCursorHidden = true;
    SDL_GL_GetDrawableSize(GameState->Window, &GameState->DrawableWidth, &GameState->DrawableHeight);
    SDL_GetWindowSize(GameState->Window, &GameState->WindowWidth, &GameState->WindowHeight);
    GameState->ScreenState = screen_state::InGame;
    
    InitializeAudio(GameState->Audio);
    InitializeAssets(GameState->Assets);
    InitFontRenderer(GameState->FontRenderer);
    
    //
    // Game Stuff
    //
    
    GameState->EntitiesCount = 0;
    
    AddPlayer();
    u32 PlayerIndex = 0;
    GameState->Player = &GameState->Entities[PlayerIndex];
    
    // NOTE(Jonathan): We can only add items here and not in player,
    // because we need to init the inventory first. 
    
    InitInventory(GameState->Player->Inventory, V2(600.0f, 450.0f));
    GameState->ShowInventory = false;
    
    AddInventoryItem(GameState->Player->Inventory, InventoryItemType_Stone, InventoryItemState_Resource, 
                     100.0f);
    AddInventoryItem(GameState->Player->Inventory, InventoryItemType_Wood, InventoryItemState_Resource,
                     100.0f);
    AddInventoryItem(GameState->Player->Inventory, InventoryItemType_Steel, InventoryItemState_Resource, 
                     100.0f);
    
    AddSoldier();
    GameState->SoldierIndex = 1;
    
    AddPlacingBuilding();
    GameState->PlacingBuilding = GameState->Entities + (GameState->EntitiesCount - 1);
    
    Math::m4_regular_inverse Projection = Math::Perspective((r32)GameState->DrawableWidth / GameState->DrawableHeight, Math::Radians(45.0f), 0.1f, 500.0f);
    GameState->Ortho = Math::Orthographic(0.0f, (r32)GameState->DrawableWidth, 0.0f, (r32)GameState->DrawableHeight, -1.0f, 1.0f); 
    InitializeCamera(GameState->MainCamera, Projection.Regular, &GameState->Player->Position);
    GameState->MainCamera.ProjectionInverse = Projection.Inverse;
    
    GameState->MousePicker.Distance = 1000.0f;
    CreateWorld(GameState->World);
    InitEnv();
    
    InitCrosshairData(GameState->CrosshairMesh);
    
    GameState->BuildMode = false;
    GameState->BuildingType = BuildingType_None;
    
    ResetSelectedEntity(GameState->SelectedEnv);
    
    LoadRecipes(GameState->Player->Inventory);
    
    
    // TODO(Jonathan): Consider making the ifdef in DebugPrimitives
#ifdef NAYO_DEBUG
    debug::Initialize(GameState->Primitives);
#endif
}

static void OnHotLoad()
{
}

static void OnStartRecord()
{
    GameState->MainCamera.Attachment = &GameState->Player->Position;
}

Nayo_Export GAME_INIT(GameInit)
{
    if (gl3wInit())
    {
        LogCritical("GL3W couldn't initialize!");
    }
    
    Assert(sizeof(game_state) <= GameMemory.SizeInBytes);
    GameState = static_cast<game_state*>(GameMemory.MemoryBlock);
    Platform = GameMemory.PlatformAPI;
    GameState->Window = GameMemory.Window;
#ifdef NAYO_DEBUG
    DebugState = GameMemory.DebugStateAPI;
    ImGui::SetCurrentContext(DebugState->Context);
    ImGui::SetAllocatorFunctions(debug::ImGuiCustomAllocation, debug::ImGuiCustomDeAllocation, 0);
#endif
    if (GameState != nullptr)
    {
        if (!GameMemory.IsInitialized)
        {
            // TODO(Naor): Casey said maybe to move it to the main.cpp (Platform layer)
            GameMemory.IsInitialized = true;
            
            FirstInitialize();
        }
        else
        {
            OnHotLoad();
        }
    }
}

Nayo_Export GAME_UPDATE_AND_RENDER(GameUpdateAndRender)
{
    TIMED_FUNCTION();
    
    GameState = static_cast<game_state*>(GameMemory.MemoryBlock);
    GameState->PressedKeys = GameInputContinuous;
    GameState->CurrentTime += Delta;
    
#ifdef NAYO_DEBUG
    debug::NextFrame(GameState->Primitives);
    GameState->Assets.Shader_TexturedModel.UpdateIfModified();
    GameState->Assets.Shader_TexturedModelInstanced.UpdateIfModified();
    GameState->Assets.Shader_World.UpdateIfModified();
    GameState->Assets.Shader_Ortho.UpdateIfModified();
#endif
    
    GameState->RenderGroup.Count = 0;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
#ifdef NAYO_DEBUG
    if(!DebugState->IsCursorActive)
    {
#endif 
        ContinuousInput();
        GameInputHandler(GameInput);
        InputHandlerPlayer(*GameState->Player);
#ifdef NAYO_DEBUG
    }
#endif 
    switch (GameState->ScreenState)
    {
        case screen_state::InGame:
        {
            TIMED_BLOCK("InGame State");
            
            UpdateCamera(GameState->MainCamera, Delta);
            GameState->MVP = GameState->MainCamera.Projection * GameState->MainCamera.View;
            GameState->Frustum = Frustum(GameState->MVP);
            if(GameState->IsCameraOnPlayer)
            {
                // TODO(Naor): This may be expensive we may want to do it less often
                GameState->MousePicker.Ray = CalculateMouseRay(GameState->MainCamera,
                                                               0.5f * V2(GameState->DrawableWidth, GameState->DrawableHeight),
                                                               GameState->DrawableWidth,
                                                               GameState->DrawableHeight);
                if(IntersectionInRange(GameState->MainCamera, 0.0f, GameState->MousePicker.Distance, GameState->MousePicker.Ray))
                {
                    GameState->CurrentTerrainPoint = RayBinarySearch(GameState->MainCamera, 0, GameState->MousePicker.Distance, GameState->MousePicker.Ray);
                    if(GameState->CurrentTerrainPoint != V3())
                    {
                        // @Temporary: this cast is bad because we want the precision of CurrentTerrainPoint
                        GameState->CurrentTileCoord.X = ToInt32(GameState->CurrentTerrainPoint.x / (TileDimension));
                        GameState->CurrentTileCoord.Z = ToInt32(GameState->CurrentTerrainPoint.z / (TileDimension));
                        
                        GameState->CurrentTileCoord.RelWorld.x = GameState->CurrentTileCoord.X * TileDimension;
                        GameState->CurrentTileCoord.RelWorld.z = GameState->CurrentTileCoord.Z * TileDimension;
                        
                        GameState->PlacingTileCoord.X = ToInt32(GameState->CurrentTileCoord.X / NumOfPlacingTiles);
                        GameState->PlacingTileCoord.Z = ToInt32(GameState->CurrentTileCoord.Z / NumOfPlacingTiles);
                        
                        GameState->PlacingTileCoord.RelWorld.x = TileDimension * GameState->PlacingTileCoord.X;
                        GameState->PlacingTileCoord.RelWorld.z = TileDimension * GameState->PlacingTileCoord.Z;
                        
                    }
                }
            }
            
            UpdateAndRenderWorld(Delta, GameState->World);
            
            r32 T = GameState->CurrentTime*0.05f;
            DEBUG_SPHERE(V3(Math::Cos(T),
                            Math::Sin(T),
                            Math::Cos(T)) * 500.0f,
                         50.0f, 30, Color::Yellow);
            
            DEBUG_SPHERE(V3(-Math::Cos(T),
                            -Math::Sin(T),
                            -Math::Cos(T)) * 500.0f,
                         50.0f, 30, Color::Gray50);
            
            ResetSelectedEntity(GameState->SelectedEnv);
            ResetSelectedEntity(GameState->SelectedBuilding);
            
            u32 EntitiesDrawn = 0;
            
            for(u32 EntityIndex = 0; EntityIndex < GameState->EntitiesCount; ++EntityIndex)
            {
                entity& Entity = GameState->Entities[EntityIndex];
                
                switch(Entity.Type)
                {
                    case EntityType_Player:
                    {
                        UpdateAndRenderPlayer(Entity, GameState->MainCamera, Delta);
                    }
                    break;
                    case EntityType_Soldier:
                    {
                        UpdateAndRenderSoldier(Entity, Delta);
                    }
                    break;
                    case EntityType_Golem:
                    {
                        UpdateAndRenderGolem(Entity, Delta);
                    }
                    break;
                    case EntityType_Projectile:
                    {
                        UpdateAndRenderProjectile(Entity, Delta);
                    }
                    break;
                    case EntityType_FixedBuilding:
                    {
                        UpdateAndRenderBuildingType(Entity, Entity.BuildingType);
                    }
                    break;
                    case EntityType_PlacingBuilding:
                    {
                        UpdateAndRenderPlacingBuilding(*GameState->PlacingBuilding);
                    }
                    break;
                    case EntityType_Tree:
                    case EntityType_Bush:
                    case EntityType_Rock:
                    case EntityType_Stone:
                    case EntityType_Wood:
                    case EntityType_Steel:
                    case EntityType_Flint:
                    {
                        UpdateAndRenderEnv(Entity, Delta);
                    }
                    break;
                }
                
                // TODO(Jonathan): Add frustum check here, first I need to update the entity and then check it's
                // aabb against the frustum, because first we need to calculate the aabb through update
                
                if(!FrustumIntersection(GameState->Frustum, Entity.BoundingBox.Min, Entity.BoundingBox.Max) ||
                   (Entity.BoundingBox.Max == V3() && Entity.BoundingBox.Min == V3()))
                {
                    continue;
                }
                
                EntitiesDrawn++;
                
                
            }
            
            
            // NOTE(Jonathan): There's a bug when rendering twice 
            ImmDrawText(GameState->RenderGroup,
                        GameState->FontRenderer,
                        GameState->Assets.ArialFont,
                        GameState->Ortho,
                        150, 150, V4(1.0f, 1.0, 1.0f, 1.0f), 20, "Entities drawn: %d\nEntitiesCount: %d",
                        EntitiesDrawn, GameState->EntitiesCount);
            
            RenderSprite(GameState->Assets.Shader_Ortho, GameState->CrosshairMesh, GameState->Assets.Texture_Crosshair,
                         (0.5f * V2(GameState->DrawableWidth, GameState->DrawableHeight)) - 15, V2(30, 30));
            if(GameState->ShowInventory)
            {
                RenderInventory(GameState->Player->Inventory);
            }
            // Note(Jonathan): This should be last!
            GameState->MainCamera.HasMoved = false;
        }
        break;
        
        case screen_state::MainMenu:
        {
        }
        break;
    }
    
    // TODO(Naor): Include debug rendering in the new renderer
#ifdef NAYO_DEBUG
    
    
    if(GameState->DrawCircle)
    {
        //AddHackyCircle(GameState->Primitives, GameState->CirclePos, 10.0f, 100,  V3(1.0f));
        AddPlane(GameState->Primitives, V3(GameState->CirclePos.x - 15, 0, GameState->CirclePos.z-15),
                 V3(GameState->CirclePos.x + 15, 0, GameState->CirclePos.z+15), V3(0.0f));
    }
    
#if 0    
    if(GameState->BuildMode)
    {
        
        u32 Dim = u32(PLANE_WIDTH / TileDimension);
        for(u32 i = 0; i < Dim;++i)
        {
            for(u32 j = 0; j < Dim; ++j)
            {
                
                DEBUG_PLANE(V3(r32(i * TileDimension), 0.05f, r32(j * TileDimension)),
                            V3(r32(i * TileDimension + TileDimension), 0.05f,
                               r32(j * TileDimension + TileDimension)),
                            V3(0, 0, 0));
            }
        }
    }
#endif
#if 0
    for(u32 EntityIndex = 0; EntityIndex < GameState->EntitiesCount; ++EntityIndex)
    {
        entity& Entity = GameState->Entities[EntityIndex];
        for(int j = 0; j < Entity.TilesLocationsCount; ++j)
        {
            v2* Tiles = Entity.TilesLocations;
            DEBUG_PLANE(V3(r32(Tiles[j].x * TileDimension), 0.05f,
                           r32(Tiles[j].y * TileDimension)),
                        V3(r32(Tiles[j].x * TileDimension + TileDimension), 0.05f,
                           r32(Tiles[j].y * TileDimension + TileDimension)), V3(0.43f, 0.123f, 0.98f));
        }
        
        DEBUG_AABB(Entity.BoundingBox.Min, Entity.BoundingBox.Max, V3(0.32f, 1.0f, 0));
        
    }
#endif
    
    
    // Note: I dont want this to be in nayo_debug because i dont want to see the ray
    if(GameState->SelectedBuilding.Index != -1)
    {
        entity& SelectedBuilding = GameState->Entities[GameState->SelectedBuilding.Index];
        DEBUG_AABB(SelectedBuilding.BoundingBox.Min, SelectedBuilding.BoundingBox.Max, V3(1.0f, 0.5f, 0.3f));
        //NayoDebug::AddPoint(GameState->Primitives, Intersection, V3(0.1, 1, 0.7), 0.5);
    }
    
    
    // TODO: consider moving this
    if(GameState->SelectedEnv.Index != -1)
    {
        // Temp
        DEBUG_AABB(GameState->Entities[GameState->SelectedEnv.Index].BoundingBox.Min,
                   GameState->Entities[GameState->SelectedEnv.Index].BoundingBox.Max,
                   RGBTo01(V3(248.0f, 237.0f, 98.0f)));
    }
    
    // Note: simple hack very bad!!!!!!!!!!!!!!!!!!!!!!!
    static int FirstTime = 0;
    if(FirstTime < 10)
    {
        DEBUG_LINE(V3(1,2,3), V3(2,2,4), V3(1, 1, 1));
        FirstTime++;
    }
    
    debug::RenderFrame(GameState->RenderGroup, GameState->Primitives, GameState->MVP);
    // Note: When debugging the code (breakpoint)
    if (Delta > 1.0f)
        Delta = 1.0f / 60;
    
    // Here we will be updating the debugging data.
    DebugState->DrawableSize = V2(GameState->DrawableWidth, GameState->DrawableHeight);
    DebugState->WindowSize = V2(GameState->WindowWidth, GameState->WindowHeight);
#endif
    
    // TODO(Jonathan): Is this a good place?
    RenderScene(GameState->RenderGroup);
    if(GameState->ShowInventory)
    {
        DisplayInventoryItems(GameState->Player->Inventory);
    }
    //RenderAll(GameState->Renderer, GameState->Assets);
}

Nayo_Export GAME_DEBUG_RENDER(GameDebugRender)
{
    if(DebugState->DisplayProfiler)
        debug::RenderProfiler();
    if(DebugState->DisplayLogger)
        debug::RenderLogger();
}

static void MouseMotion(const SDL_MouseMotionEvent& Event)
{
    const int32 DX = Event.xrel;
    const int32 DY = Event.yrel;
    if (GameState->IsCursorHidden)
    {
        MoveCamera(GameState->MainCamera, DX, DY);
    }
    else
    {
        if(GameState->ChosenInventoryItem != nullptr)
        {
            GameState->ChosenInventoryItem->Position = V2((r32)Event.x, (r32)(GameState->WindowHeight - Event.y));
        }
    }
}

static void MouseButtonEvent(const SDL_MouseButtonEvent& Event)
{
    SDL_SetRelativeMouseMode(SDL_TRUE);
    GameState->IsCursorHidden = true;
    
    if (Event.type == SDL_MOUSEBUTTONDOWN)
    {
        if(Event.state == SDL_PRESSED && Event.button == SDL_BUTTON_LEFT)
        {
            if(GameState->BuildMode)
            {
                AddBuildingByPlayer(*GameState->Player,
                                    GameState->PlacingBuilding->BuildingType);
            }
            
            else
            {
                LogInfo("(%d, %d)", Event.x, GameState->WindowHeight - Event.y);
                //AddProjectile(GameState->Entities[GameState->PlayerIndex].Position + V3(0.0f, 3.0f, 0.0f),GameState->MousePicker.Ray, 5.0f, 300.0f);
                if(GameState->SelectedEnv.Index != -1)
                {
                    DestroyResource(GameState->Entities[GameState->SelectedEnv.Index]);
                }
                
                for(u32 Index = 0; Index < ArrayCount(GameState->Player->Inventory.Items); ++Index)
                {
                    inventory_item& Item = GameState->Player->Inventory.Items[Index];
                    if(PointInAABB2D(Item.BoundingBox, V2((r32)Event.x, (r32)(GameState->WindowHeight - Event.y))))
                    {
                        GameState->ChosenInventoryItem = &Item;
                    }
                }
            }
        }
        
        if(Event.state == SDL_PRESSED && Event.button == SDL_BUTTON_RIGHT)
        {
            RemoveAllEntities(EntityType_Projectile);
        }
    }
}

static void MouseWheel(const SDL_MouseWheelEvent& Event)
{
    
#ifdef NAYO_DEBUG
    if((Event.y < 0 &&GameState->MainCamera.Speed > 2) || (Event.y > 0 && GameState->MainCamera.Speed < 200))
        GameState->MainCamera.Speed += Event.y*2;
#endif
    ChooseBuildingWithMouseWheel(Event.y);
}


// TODO(Naor): Maybe the "GameInput" would be a problem if we want a function
// to access the these inputs. (When the time come, test and fix it)
static void GameInputHandler(std::queue<SDL_Event> &GameInput)
{
    TIMED_FUNCTION();
    
    while (!GameInput.empty())
    {
        auto Event = GameInput.front();
        switch (Event.type)
        {
            case SDL_KEYDOWN:
            case SDL_KEYUP:
            {
                // Single press
                SDL_Keycode Key = Event.key.keysym.sym;
                if (Event.key.state == SDL_PRESSED)
                {
                    // Exit
                    if (Key == SDLK_ESCAPE)
                    {
                        SDL_SetRelativeMouseMode(SDL_FALSE);
                        SDL_WarpMouseInWindow(GameState->Window, (s32)(GameState->DrawableWidth*0.5), (s32)(GameState->DrawableHeight*0.5));
                        GameState->IsCursorHidden = false;
#ifdef NAYO_DEBUG
                        DebugState->IsCursorActive = !GameState->IsCursorHidden;
#endif 
                    }
                    
                    // TODO(Naor): This will not work if SDL_SetRelativeMouseMode is false because
                    // of the DebugState->IsActiveWindow. This will probably be removed when we
                    // insert our own cursor in the game... (SDL_Relative that is)
                    // FullScreen
                    if(Event.key.keysym.sym == SDLK_RETURN && Event.key.keysym.mod & KMOD_ALT)
                    {
                        SDLToggleFullscreen(GameState->Window);
                    }
                    
#ifdef NAYO_DEBUG
                    if(Event.key.keysym.sym == SDLK_F1)
                    {
                        DebugState->DisplayProfiler = !DebugState->DisplayProfiler;
                    }
                    if(Event.key.keysym.sym == SDLK_F2)
                    {
                        DebugState->DisplayLogger = !DebugState->DisplayLogger;
                    }
#endif
                    
                    // Wireframe
                    static bool Wireframe = false;
                    if (Key == SDLK_f)
                    {
                        Wireframe = !Wireframe;
                        if (Wireframe)
                            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                        else
                            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
                    }
                    
                    if(Key == SDLK_i)
                    {
                        GameState->ShowInventory = !GameState->ShowInventory;
                    }
                    
                    
                    // Add Golem
                    if(Key == SDLK_KP_PLUS)
                    {
                        AddGolem(V3(0.0f, 0.0f, 0.0f), 50.0f, 50.0f);
                    }
                    
                    // Remove all Golems
                    if(Key == SDLK_KP_MINUS)
                    {
                        RemoveAllEntities(EntityType_Golem);
                    }
                    
                    // Detach camera from player
                    if(Key == SDLK_c)
                    {
                        if(GameState->BuildMode)
                        {
                            CheckCraftingOptionsForState(GameState->Player->Inventory, InventoryItemState_Resource);
                        }
                        else 
                        {
                            if(GameState->MainCamera.Attachment)
                            {
                                GameState->MainCamera.Attachment = nullptr;
                                GameState->IsCameraOnPlayer = false;
                            }
                            else
                            {
                                GameState->MainCamera.Attachment = &GameState->Player->Position;
                                GameState->IsCameraOnPlayer = true;
                            }
                        }
                    }
                    
                    if(Key == SDLK_SPACE) 
                    {
                        JumpEntity(*GameState->Player, PLAYER_JUMP_HEIGHT);
                    }
                    
                    // Build mode
                    if(Key == SDLK_b)
                    {
                        GameState->BuildMode = !GameState->BuildMode;
                        
                        if(!GameState->BuildMode)
                        {
                            GameState->BuildingType = BuildingType_None;
                        }
                        
                    }
                    
                    if(GameState->BuildMode)
                    {
                        // Rotate
                        if(Key == SDLK_r)
                        {
                            GameState->PlacingBuilding->Rotate = true;
                        }
                        
                    }
                    
                    if(Key == SDLK_e)
                    {
                        if(GameState->SelectedEnv.Index != -1)
                        {
                            CollectResource(GameState->Entities[GameState->SelectedEnv.Index]);
                        }
                    }
                    
                    
                    if(GameState->SelectedBuilding.Index != -1)
                    {
                        if(Key == SDLK_i)
                        {
                            // Inspect the building's states etc...
                        }
                        if(Key == SDLK_z)
                        {
                            // Demolish building
                            RemoveEntity(GameState->Entities[GameState->SelectedBuilding.Index]);
                            ResetSelectedEntity(GameState->SelectedBuilding);
                        }
                        if(Key == SDLK_e)
                        {
                            GameState->Entities[GameState->SoldierIndex].Direction = 
                                GameState->Entities[GameState->SelectedBuilding.Index].Position;
                        }
                    }
                    
                }
                else
                {
                }
            }
            break;
            
            case SDL_MOUSEMOTION:
            {
                MouseMotion(Event.motion);
            }
            break;
            
            case SDL_MOUSEWHEEL:
            {
                MouseWheel(Event.wheel);
            }
            break;
            
            case SDL_MOUSEBUTTONDOWN:
            case SDL_MOUSEBUTTONUP:
            {
                MouseButtonEvent(Event.button);
            }
            break;
            
            case SDL_WINDOWEVENT:
            {
                SDL_GL_GetDrawableSize(GameState->Window, &GameState->DrawableWidth, &GameState->DrawableHeight);
                SDL_GetWindowSize(GameState->Window, &GameState->WindowWidth, &GameState->WindowHeight);
            }break;
        }
        
        GameInput.pop();
    }
}

Nayo_Export GAME_INPUT_WHILE_PLAYBACK(GameInputWhilePlayback)
{
    if(First)
    {
        OnStartRecord();
    }
    
    GameState = static_cast<game_state *>(GameMemory.MemoryBlock);
    while (!GameInput.empty())
    {
        auto Event = GameInput.front();
        switch (Event.type)
        {
            case SDL_KEYDOWN:
            case SDL_KEYUP:
            {
                if (Event.key.keysym.sym == SDLK_ESCAPE)
                {
                    SDL_SetRelativeMouseMode(SDL_FALSE);
                    SDL_WarpMouseInWindow(GameState->Window, (s32)(GameState->DrawableWidth*0.5f), (s32)(GameState->DrawableHeight*0.5f));
                }
            }
            break;
            
            case SDL_MOUSEMOTION:
            {
                const int32 DX = Event.motion.xrel;
                const int32 DY = Event.motion.yrel;
            }
            break;
            
            case SDL_MOUSEWHEEL:
            {
            }
            break;
            
            case SDL_MOUSEBUTTONDOWN:
            case SDL_MOUSEBUTTONUP:
            {
                SDL_SetRelativeMouseMode(SDL_TRUE);
            }
            break;
        }
        
        GameInput.pop();
    }
}

static void ContinuousInput()
{
    if (GameState->PressedKeys[SDL_SCANCODE_W])
    {
        InputCamera(GameState->MainCamera, direction::Forward);
    }
    
    if (GameState->PressedKeys[SDL_SCANCODE_S])
    {
        InputCamera(GameState->MainCamera, direction::Backward);
    }
    
    if (GameState->PressedKeys[SDL_SCANCODE_A])
    {
        InputCamera(GameState->MainCamera, direction::Left);
    }
    
    if (GameState->PressedKeys[SDL_SCANCODE_D])
    {
        InputCamera(GameState->MainCamera, direction::Right);
    }
    
    if (GameState->PressedKeys[SDL_SCANCODE_SPACE])
    {
        InputCamera(GameState->MainCamera, direction::Up);
    }
    
    if (GameState->PressedKeys[SDL_SCANCODE_LSHIFT])
    {
        InputCamera(GameState->MainCamera, direction::Down);
    }
}
