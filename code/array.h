#pragma once

#include "memory.h"

// TODO(Naor): Create fixed_array (fixed size)
// TODO(Naor): This is really bad array... there is no delete, the initial size 
// is minimum 2MB (can be configured but still).
// Basically CANT use it extensively.
// + The ElementSize is not initialized correctly if we are not calling Reserve.
template<typename type>
struct dynamic_array
{
    memory_arena Arena = {};
    size_t Count = 0;
    size_t ElementSize = 0;
    
    inline void PushBack(const type* Data, s32 ElementsCount)
    {
        Assert(ElementSize != 0);
        
        type* Memory = PushStructMultiple(&Arena, type, ElementsCount);
        memcpy((void*)Memory, Data, sizeof(type) * ElementsCount);
        Count++;
    }
    
    inline void PushBack(const type& Data)
    {
        Assert(ElementSize != 0);
        
        PushBack(&Data, 1);
    }
    
    inline type& Get(s32 Index)
    {
        platform_memory_block* Block = Arena.FirstBlock;
        size_t SizeOffset = 0;
        size_t FixedIndex = Index * ElementSize;
        while(Block->Size + SizeOffset <= FixedIndex)
        {
            SizeOffset += Block->Size;
            Block = Block->ArenaNext;
        }
        
        type& Result = *(type*)(Block->Base + (FixedIndex - SizeOffset));
        return Result;
    }
    
    inline const type& Get(s32 Index) const 
    {
        platform_memory_block* Block = Arena.FirstBlock;
        size_t SizeOffset = 0;
        size_t FixedIndex = Index * ElementSize;
        while(Block->Size + SizeOffset <= FixedIndex)
        {
            SizeOffset += Block->Size;
            Block = Block->ArenaNext;
        }
        
        const type& Result = *(type*)(Block->Base + (FixedIndex - SizeOffset));
        
        return Result;
    }
    
    inline type& operator[](s32 Index)
    {
        type& Result = Get(Index);
        return Result;
    }
    
    inline const type& operator[](s32 Index) const
    {
        const type& Result = Get(Index);
        return Result;
    }
    
    // NOTE(Naor): This is only useful if we use 
    inline type* Data()
    {
        type* Result = (type*)(Arena.FirstBlock->Base);
        return Result;
    }
    
    inline void Clear()
    {
        Count = 0;
        while(Arena.CurrentBlock->ArenaPrev)
        {
            FreeLastBlock(&Arena);
        }
        Arena.CurrentBlock->Used = 0;
    }
};


// NOTE(Naor): This will clear the array and deallocate the arena!
template<typename type>
inline void Reserve(dynamic_array<type>* Array, size_t Count)
{
    FreeArena(&Array->Arena);
    InitializeArena(&Array->Arena, type, Count);
    Array->ElementSize = sizeof(type) + GetAlignmentOffset(&Array->Arena, sizeof(type));
    Array->Count = 0;
}

template<typename type>
inline void InitializeDynamicArray(dynamic_array<type>* Array)
{
    // This will allocate memory with 1MB (minimum block)
    Reserve(Array, 0);
}

template<typename type>
inline void FreeDynamicArray(dynamic_array<type>* Array)
{
    FreeArena(&Array->Arena);
}
