#pragma once

/*

TODO:

1. Seperate buildings and items when crafting
2. Handle what happens when we add some item and it needs to have updateandrender, handle the entity side of it
3. Position the inventory according to some ratio so that the position will be fit in all the resolutions
5. Functions for debugging and console (Give, Clear, etc...)(Look at http://subnautica.wikia.com/wiki/Debug_Console_Commands)
6. Add gui scaling for very large or small resolutions
7. Consider using metaprogramming for getting the names from the types
*/

#include "RecipeParser.cpp"

static constexpr s32 SLOT_ROW = 4; 
static constexpr s32 SLOT_COLUMN = 4;

static constexpr r32 SLOT_HEIGHT = 64.0f;
static constexpr r32 SLOT_WIDTH = 64.0f;
static constexpr r32 SLOT_DIM = 64.0f;


static constexpr s32 MAX_INVENTORY_ITEMS = 16;
// TODO(Jonathan): Think of better size for the max ingredients
static constexpr s32 MAX_INVENTORY_INGREDIENTS = 16;
static constexpr s32 MAX_INVENTORY_RECIPES = 32;


enum inventory_item_type : s32
{
    InventoryItemType_None = 0,
    
    // Resources
    InventoryItemType_Wood,
    InventoryItemType_Stone,
    InventoryItemType_Steel,
    InventoryItemType_FireStone,
    InventoryItemType_Flint,
    
    // Buildings
    InventoryItemType_Farmhouse,
    InventoryItemType_Cottage,
    InventoryItemType_Wall,
    InventoryItemType_Outpost,
    InventoryItemType_CampFire,
    
    InventoryItemType_NumItems,
};

enum inventory_item_state : s32
{
    InventoryItemState_Resource,
    InventoryItemState_Building,
    
    InventoryItemState_NumStates
};

struct inventory_item
{
    // GUI
    v2 Position;
    aabb BoundingBox;
    char* Name;
    char* Description;
    u32 Texture;
    
    // Logic
    r32 Amount;
    
    inventory_item_type Type;
    inventory_item_state State;
};


enum recipe_index : s32
{
    RecipeIndex_FireStone = 0,
    RecipeIndex_CampFire,
    RecipeIndex_Farmhouse,
    RecipeIndex_Cottage,
    RecipeIndex_Wall,
    RecipeIndex_Outpost,
    
    RecipeIndex_NumOfRecipes
};


struct recipe
{
    inventory_item Ingredients[MAX_INVENTORY_INGREDIENTS];
    u32 IngredientsCount;
    
    inventory_item CraftedItem;
};

struct inventory
{
    inventory_item Items[MAX_INVENTORY_ITEMS];
    u32 NumExsitingItems;
    
    // GUI
    v2 Position;
    
    // TODO(Jonathan): Change the size to more suitable size
    recipe Recipes[MAX_INVENTORY_RECIPES];
    u32 ExistingRecipesCount;
};

// Prototypes
static void ModifyInventoryItem(inventory& Inventory, inventory_item_type Type, r32 Amount);
static r32 AddInventoryItem(inventory& Inventory, inventory_item_type ItemType, inventory_item_state ItemState, r32 Amount);
inline r32 AddInventoryItem(inventory& Inventory, const inventory_item& Item);
static inventory_item_type GetInventoryItemTypeByName(const char* Name);
static inventory_item* GetInventoryItemByType(inventory& Inventory, inventory_item_type Type);
static inventory_item InitInventoryItem(const v2& Position);
static s32 GetInventoryItemTextureByType(inventory_item_type Type);
static char* GetInventoryItemNameByType(inventory_item_type Type);
static recipe_index GetRecipeIndexByInventoryItemType(inventory_item_type Type);
static inventory_item_state GetInventoryItemStateByName(char* Name);