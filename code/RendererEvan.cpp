#pragma once

#include "Renderer.h"

static void RenderAll(renderer& Renderer, assets& Assets)
{
    while(Renderer.ReadCommandCount < Renderer.CommandCount)
    {
        render_op Op = *(Renderer.Read<render_op>());
        switch(Op)
        {
            case render_op::Shader:
            {
                s32 ShaderIndex = *(Renderer.Read<shader_index>());
                
                //if(Renderer.CurrentShaderIndex != ShaderIndex)
                {
                    Renderer.CurrentShaderIndex = ShaderIndex;
                    
                    Assets.Shaders[ShaderIndex].Use();
                }
            }break;
            
            case render_op::Uniform:
            {
                s32 UniformIndex = *(Renderer.Read<shader_uniform_index>());
                Assert(UniformIndex != -1);
                
                s32 UniformLocation = Assets.Shaders[Renderer.CurrentShaderIndex].Uniforms[UniformIndex];
                Assert(UniformLocation != -1);
                
                uniform_data_type DataType = *(Renderer.Read<uniform_data_type>());
                switch(DataType)
                {
                    case uniform_data_type::Float:
                    {
                        r32 Value = *(Renderer.Read<r32>());
                        
                        glUniform1f(UniformLocation, Value);
                    }break;
                    case uniform_data_type::Vec2:
                    {
                        v2 Value = *(Renderer.Read<v2>());
                        
                        glUniform2fv(UniformLocation, 1, Value.Data());
                    }break;
                    case uniform_data_type::Vec3:
                    {
                        v3 Value = *(Renderer.Read<v3>());
                        
                        glUniform3fv(UniformLocation, 1, Value.Data());
                    }break;
                    case uniform_data_type::Vec4:
                    {
                        v4 Value = *(Renderer.Read<v4>());
                        
                        glUniform4fv(UniformLocation, 1, Value.Data());
                    }break;
                    case uniform_data_type::Mat2:
                    {
                        m2 Value = *(Renderer.Read<m2>());
                        
                        glUniformMatrix2fv(UniformLocation, 1, false, Value.Data());
                    }break;
                    case uniform_data_type::Mat3:
                    {
                        m3 Value = *(Renderer.Read<m3>());
                        
                        glUniformMatrix3fv(UniformLocation, 1, false, Value.Data());
                    }break;
                    case uniform_data_type::Mat4:
                    {
                        m4 Value = *(Renderer.Read<m4>());
                        
                        glUniformMatrix4fv(UniformLocation, 1, false, Value.Data());
                    }break;
                }
            }break;
            
            case render_op::Texture:
            {
                texture_type TextureType = *(Renderer.Read<texture_type>());
                u32 TextureIndex = Assets.Textures[*(Renderer.Read<texture_index>())];
                switch(TextureType)
                {
                    case texture_type::Texture_2D:
                    {
                        glBindTexture(GL_TEXTURE_2D, TextureIndex);
                    }break;
                }
            }break;
            
            // TODO(Naor): Should we keep track of what vao is currently bind? (like the shader)
            case render_op::Vao:
            {
                u32 VaoIndex = *(Renderer.Read<u32>());
                glBindVertexArray(VaoIndex);
            }break;
            
            case render_op::Unbind_Vao:
            {
                glBindVertexArray(0);
            }break;
            
            case render_op::Draw:
            {
                draw_type DrawType = *(Renderer.Read<draw_type>());
                draw_primitive_type PrimitiveType = *(Renderer.Read<draw_primitive_type>());
                u32 GLPrimitive = 0xFFFF; // primitives are between 0x0->0x6 so we just plug wrong value in order
                // to know we got the correct primitive and assert.
                
                switch(PrimitiveType)
                {
                    case draw_primitive_type::Triangles:
                    {
                        GLPrimitive = GL_TRIANGLES;
                    }break;
                }
                
                Assert(GLPrimitive != 0xFFFF);
                
                switch(DrawType)
                {
                    case draw_type::Elements:
                    {
                        u32 ElementsCount = *(Renderer.Read<u32>());
                        // TODO(Naor): Get this out like PrimitiveType, we need more draw calls to
                        // know where this will go.. (draw_data_type & switch)
                        draw_data_type IndexType = *(Renderer.Read<draw_data_type>());
                        u32 GLDataType = 0x0; // Again, just some value to assert
                        switch(IndexType)
                        {
                            case draw_data_type::UInt32:
                            {
                                GLDataType = GL_UNSIGNED_INT;
                            }break;
                        };
                        
                        Assert(GLDataType != 0x0);
                        
                        void* IndiciesOffset = *(Renderer.Read<void*>());
                        
                        glDrawElements(GLPrimitive, ElementsCount, GLDataType, IndiciesOffset);
                    }break;
                    
                    case draw_type::Arrays:
                    {
                        s32 FirstIndex = *(Renderer.Read<s32>());
                        u32 DrawCount = *(Renderer.Read<u32>());
                        glDrawArrays(GLPrimitive, FirstIndex, DrawCount);
                    }break;
                }break;
                
                
            }break;
            
            default:
            {
                printf("Error: Couldn't figure out what render_operation you wanted to perform.\n");
                Assert(false);
            }break;
        }
    }
}
