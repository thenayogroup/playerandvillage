#include "MousePicker.h"

static v3 CalculateMouseRay(const camera& Camera, const v2& Mouse, s32 Width, s32 Height)
{
    v2 NormlizedCoords = {};
    NormlizedCoords.x = ((2.0f * Mouse.x) / Width) - 1.0f;
    NormlizedCoords.y = 1.0f - ((2.0f * Mouse.y) / Height);
    
    v4 ClipCoords = V4(NormlizedCoords.x, NormlizedCoords.y, -1.0f, 1.0f);
    v4 EyeCoords = Camera.ProjectionInverse * ClipCoords;
    
    EyeCoords.z = -1.0f;
    EyeCoords.w = 0.0f;
    
    v4 RayWorldSpace = Math::AffineInverse(Camera.View) * EyeCoords;
    v3 Result = Normalize(V3(RayWorldSpace.x, RayWorldSpace.y, RayWorldSpace.z));
    
    return Result;
}

static bool IsUnderGround(v3 TestPoint) 
{
    return(TestPoint.y < GetWorldHeight(TestPoint.x, TestPoint.z));
}

static bool IntersectionInRange(const camera& Camera, r32 Start, r32 Finish, v3 Ray)
{
    v3 StartPoint = Camera.Position;
    v3 EndPoint = Camera.Position + Finish * Ray;
    return (!IsUnderGround(StartPoint) && IsUnderGround(EndPoint));
}

static v3 RayBinarySearch(const camera& Camera, r32 Start, r32 Finish, v3 Ray)
{
    TIMED_FUNCTION();
    
    v3 Result = {};
    r32 Half;
    
    while (Start <= Finish)
    {
        Half = Start + ((Finish - Start) / 2.0f);
        
        if (IntersectionInRange(Camera, Start, Half, Ray) )
        {
            Finish = Half;
        }
        else if (IntersectionInRange(Camera, Half, Finish, Ray))
        {
            Start = Half;
        }
        
        // TODO: fix bug that make the game wait until it is true and pauses the game
        // NOTE: 1 number after the point because 2 and above causes the game to pause
        if(Math::Abs(Start - Finish) < 0.1f)// && ToInt32(Start * 10) % 10 == ToInt32(Finish * 10) % 10)
        {
            Result = Camera.Position + Half * Ray;
            break;
        }
    }
    return Result;
}


static bool LineIntersectionWithAxis(s32 Dimension, const v3& Start, const v3& End, const aabb& Box, r32& FractionMin, r32& FractionMax)
{
    r32 FractionDimensionMin = (Box.Min.E[Dimension] - Start.E[Dimension]) / (End.E[Dimension] - Start.E[Dimension]);
    r32 FractionDimensionMax = (Box.Max.E[Dimension] - Start.E[Dimension]) / (End.E[Dimension] - Start.E[Dimension]);
    
    if (FractionDimensionMax < FractionDimensionMin)
    {
        r32 Temp = FractionDimensionMax;
        FractionDimensionMax = FractionDimensionMin;
        FractionDimensionMin = Temp;
    }
    
    if (FractionDimensionMax < FractionMin)
        return false;
    
    if (FractionDimensionMin > FractionMax)
        return false;
    
    FractionMin = Math::Max(FractionDimensionMin, FractionMin); // Find the closest object that is in-front of you
    FractionMax = Math::Min(FractionDimensionMax, FractionMax);
    
    if (FractionMax < FractionMin)
        return false;
    
    return true;
}

static bool LineIntersectionWithBoundingBox(const v3& Start, const v3& End, const aabb& Box, v3& Intersection)
{
    r32 FractionMin = 0.0f, FractionMax = 1.0f;
    
    v3 Line = End - Start;
    
    if (!LineIntersectionWithAxis(0, Start, End, Box, FractionMin, FractionMax))
        return false;
    
    if (!LineIntersectionWithAxis(1, Start, End, Box, FractionMin, FractionMax))
        return false;
    
    if (!LineIntersectionWithAxis(2, Start, End, Box, FractionMin, FractionMax))
        return false;
    
    Intersection = Start + Line * FractionMin;
    
    return true;
}

inline void ResetSelectedEntity(selected_entity& Entity)
{
    Entity.Index = -1;
    Entity.Intersection = V3(10000.0f, 10000.0f, 10000.0f);
}

static void CheckRayCollisionWithEntity(selected_entity& SelectedEntity,
                                        const camera& Camera, const mouse_picker& Picker,
                                        entity* Entities, const entity& Entity, r32 Range)
{
    TIMED_FUNCTION();
    
    v3 StartRay = Camera.Position;
    v3 EndRay = Camera.Position + Picker.Distance * Picker.Ray;
    
    s32& Index = SelectedEntity.Index;
    v3& Intersection = SelectedEntity.Intersection;
    
    if(Distance(Entity.Position, GameState->Player->Position) <= Range)
    {
        v3 NewIntersection = {};
        if(LineIntersectionWithBoundingBox(StartRay, EndRay, Entity.BoundingBox, NewIntersection))
        {
            // TODO(Jonathan): Save the closest distance, so we dont check for again.
            if(Distance(Intersection, StartRay) > Distance(NewIntersection, StartRay))
            {
                Index = s32(&Entity - Entities);
                Intersection = NewIntersection;
                DEBUG_POINT(Intersection, V3(0.4f));
            }
        }
        
    }
}