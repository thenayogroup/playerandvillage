

enum token_type : s32
{
    Token_Unknown,
    
    Token_OpenBraces,
    Token_CloseBraces,
    
    Token_String,
    Token_Number,
    Token_Hyphen,
    Token_GreaterThan,
    
    Token_EndOfStream,
};

struct token
{
    token_type Type;
    
    size_t TextLength;
    char *Text;
};

struct tokenizer
{
    char *At;
};
