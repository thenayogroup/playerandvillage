#pragma once

// TODO(Naor): Add the rest of the platform depanded functions here!

// TODO(Naor): For now we are not using the next and prev in this struct,
// learn what casey did with them and then consider if we need it
struct win32_memory_block
{
    platform_memory_block Block;
    win32_memory_block* Prev;
    win32_memory_block* Next;
    u32 Pad[2];
};

// TODO(Naor): Add underflow and overflow checking like casey!
PLATFORM_ALLOCATE_MEMORY(Win32AllocateMemory) // (size_t)Size
{
    Assert(sizeof(win32_memory_block) == 64);
    
    uptr TotalSize = Size + sizeof(win32_memory_block);
    uptr BaseOffset = sizeof(win32_memory_block); // the win32_memory_block part is at the start
    
    win32_memory_block* Block = (win32_memory_block*)VirtualAlloc(0, TotalSize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
    Assert(Block); // Make sure the block allocated
    
    Block->Block.Base = (u8*)Block + BaseOffset;
    // Just to make sure everything is zero
    Assert(Block->Block.Used == 0);
    Assert(Block->Block.ArenaPrev == 0);
    
    Block->Block.Size = Size;
    
    platform_memory_block* PlatformBlock = &Block->Block;
    
    return PlatformBlock;
}

PLATFORM_DEALLOCATE_MEMORY(Win32DeallocMemory)
{
    if(Block)
    {
        win32_memory_block* Win32Block = ((win32_memory_block*)Block);
        VirtualFree(Win32Block, 0, MEM_RELEASE);
    }
}