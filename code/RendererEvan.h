#pragma once

// Forward declaration
struct assets;

enum class render_op : s32
{
    Shader,
    Uniform,
    Texture,
    Vao,
    Unbind_Vao,
    Draw,
};

enum class uniform_data_type : s32
{
    Float,
    Vec2,
    Vec3,
    Vec4,
    Mat2,
    Mat3,
    Mat4,
};

enum class texture_type : s32
{
    Texture_2D,
};

enum class draw_type : s32 
{
    Elements,
    Arrays,
};

enum class draw_primitive_type : s32
{
    Triangles,
};

enum class draw_data_type : s32
{
    UInt32,
};

// TODO(Naor): When implementing this renderer, take into consideration that
// we may want to sort the rendering commands based on shader uses, textures and more to
// avoid multiple uses of glBindTexture or glUseProgram etc..

// TODO(Naor): Consider to make this an extern so we wont need to be making calls to it like:
// GameState->Renderer.Write, instead we just need to call it like this Renderer.Write..
struct renderer
{
    // NOTE(Naor): We use casey idea of Zero Is Initialization so we don't need to allcoate the memory ourselfs
    memory_arena Arena = {};
    size_t ReadMarker = 0;
    size_t CommandCount = 0;
    size_t ReadCommandCount = 0;
    
    // NOTE(Naor): These are the opengl context properties, they may be changed,
    // outside of the renderer which can cause a problem!
    // So if we want to use OpenGL directly, remember to reverse the process of binding
    // and others, for example, using a shader can cause the renderer to not know what 
    // is the current shader.
    s32 CurrentShaderIndex = -1; // initialy, not set to anything.
    
    template<typename T>
        void Write(const T* Data, s32 Count)
    {
        T* Memory = PushStructMultiple(&Arena, T, Count);
        memcpy((void*)Memory, Data, sizeof(T) * Count);
        CommandCount++;
    }
    
    template<typename T>
        void Write(const T& Data)
    {
        T* Memory = PushStruct(&Arena, T);
        memcpy((void*)Memory, &Data, sizeof(T));
        CommandCount++;
    }
    
    template<typename T>
        const T* Read(s32 Count = 1)
    {
        T* Memory = (T*)(Arena.CurrentBlock->Base + ReadMarker);
        ReadMarker += sizeof(T) * Count + GetAlignmentOffsetForAddress(Memory, DEFAULT_ALIGNMENT);
        ReadCommandCount++;
        return Memory;
    }
    
    void Clear()
    {
        FreeArena(&Arena);
        ReadMarker = 0;
        CommandCount = 0;
        ReadCommandCount = 0;
        
        // NOTE(Naor): We DO NOT want to clear GL properties!
    }
};

static void RenderAll(renderer& Renderer, assets& Assets);

