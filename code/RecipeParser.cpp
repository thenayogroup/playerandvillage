#include "RecipeParser.h"

static char* ReadFileWithNullTerminated(const char *Path)
{
    char *Content = nullptr;
    long Length = 0;
    FILE *FilePtr = nullptr;
    fopen_s(&FilePtr, Path, "rb");
    
    if (FilePtr)
    {
        fseek(FilePtr, 0, SEEK_END);
        Length = ftell(FilePtr);
        fseek(FilePtr, 0, SEEK_SET);
        // TODO(Jonathan): Get rid of the malloc
        Content = (char *)malloc(Length + 1);
        if (Content)
        {
            fread(Content, 1, Length, FilePtr);
            Content[Length] = 0;
        }
        fclose(FilePtr);
    }
    else
    {
        Assert(!"Cannot find the file!");
    }
    
    return Content;
}

inline bool IsEndOfLine(char C)
{
    return  ((C == '\n') ||
             (C == '\r'));
}

inline bool IsWhiteSpace(char C)
{
    return ((C == ' ') ||
            (C == '\t') ||
            IsEndOfLine(C));
}

inline bool IsNum(char C)
{
    bool Result = ((C >= '0') && (C <= '9'));
    return Result;
}


static void EatAllWhitespace(tokenizer *Tokenizer)
{
    for (;;)
    {
        if (IsWhiteSpace(Tokenizer->At[0]))
        {
            ++Tokenizer->At;
        }
        else if(Tokenizer->At[0]=='/' && Tokenizer->At[1]=='/')
        {
            Tokenizer->At += 2;
            while(!IsEndOfLine(Tokenizer->At[0]) && Tokenizer->At[0])
            {
                ++Tokenizer->At;
            }
        }
        else if(Tokenizer->At[0]=='/' && Tokenizer->At[1]=='*')
        {
            Tokenizer->At += 2;
            while(Tokenizer->At[0] && Tokenizer->At[0]=='*' && Tokenizer->At[1]=='/')
            {
                ++Tokenizer->At;
            }
            if(Tokenizer->At[0]=='*')
            {
                Tokenizer->At += 2;            
            }
        }
        else
        {
            break;
        }
    }
}


static token GetToken(tokenizer *Tokenizer)
{
    EatAllWhitespace(Tokenizer);
    
    token Token = {};
    Token.TextLength = 1;
    Token.Text = Tokenizer->At;
    
    char C = Tokenizer->At[0]; 
    ++Tokenizer->At;
    
    switch(C)
    {
        case '\0':{Token.Type = Token_EndOfStream;} break;
        
        case '{':{Token.Type = Token_OpenBraces;} break;
        case '}':{Token.Type = Token_CloseBraces;} break;
        case '-':{Token.Type = Token_Hyphen;} break;
        case '>':{Token.Type = Token_GreaterThan;} break;
        
        
        case '"':
        {
            Token.Text = Tokenizer->At;
            while(Tokenizer->At[0] && Tokenizer->At[0] != '"')
            {
                if(Tokenizer->At[0]=='\\' && Tokenizer->At[1])
                {
                    ++Tokenizer->At;
                }
                ++Tokenizer->At;
            }
            Token.Type = Token_String;
            Token.TextLength = Tokenizer->At - Token.Text;
            if(Tokenizer->At[0] == '"')
            {
                ++Tokenizer->At;
            }
            
        }
        break;
        
        default:
        {
            if(IsNum(C))
            {
                Token.Type = Token_Number;
                while(IsNum(Tokenizer->At[0]))
                {
                    Tokenizer->At++;
                }
                Token.TextLength = Tokenizer->At - Token.Text;
            }
            else
            {
                Token.Type = Token_Unknown;
            }
        }
        break;
        
    }
    
    return Token;
}

static void ParseRecipes(token* Tokens, u32& TokensCount, u32 TokensSize, char* RecipesPath)
{
    char* FileContents = ReadFileWithNullTerminated(RecipesPath);
    tokenizer Tokenizer = {};
    Tokenizer.At = FileContents;
    
    
    bool Parsing = true;
    while (Parsing)
    {
        token Token = GetToken(&Tokenizer);
        
        switch (Token.Type)
        {
            case Token_EndOfStream:
            {
                Parsing = false;
            }
            break;
            case Token_OpenBraces:
            case Token_CloseBraces:
            case Token_Number:
            case Token_String:
            case Token_Hyphen:
            case Token_GreaterThan:
            {
                Assert(TokensCount < TokensSize);
                token& NewToken = Tokens[TokensCount++];
                NewToken = Token;
            }
        }
    }
    
    
    // TODO(Jonathan): Get rid of the free
    free(FileContents);
}
