#pragma once

struct win32_game_code
{
    HMODULE GameCodeDLL;
    FILETIME DLLLastWriteTime;
    game_update_and_render* UpdateAndRender;
    game_init* GameInit;
    game_input_while_playback* GameInputWhilePlayback;
    
#ifdef NAYO_DEBUG
    game_debug_render* GameDebugRender;
#endif
    
    bool IsValid;
};


static win32_game_code LoadGameCode(const char* Filename)
{
    win32_game_code Result = {};
    
    WIN32_FILE_ATTRIBUTE_DATA Ignored;
    std::string LockFilename = Utility::AppendToPath("lock.tmp");
    if(!GetFileAttributesEx(LockFilename.c_str(), GetFileExInfoStandard, &Ignored))
    {
        LogInfo("Reloading %s", Filename);
        
        std::string TempDLLName = Utility::AppendToPath("Game_temp.dll");
        Result.DLLLastWriteTime = Utility::GetLastWriteTime(Filename);
        CopyFile(Filename, TempDLLName.c_str(), false);
        Result.GameCodeDLL = LoadLibraryA(TempDLLName.c_str());
        
        if(Result.GameCodeDLL)
        {
            Result.UpdateAndRender = (game_update_and_render*)GetProcAddress(Result.GameCodeDLL, "GameUpdateAndRender");
            Result.GameInit = (game_init*)GetProcAddress(Result.GameCodeDLL, "GameInit");
            Result.GameInputWhilePlayback = (game_input_while_playback*)GetProcAddress(Result.GameCodeDLL, "GameInputWhilePlayback");
            
#ifdef NAYO_DEBUG
            Result.GameDebugRender = (game_debug_render*)GetProcAddress(Result.GameCodeDLL, "GameDebugRender");
#endif
            
            Result.IsValid = Result.UpdateAndRender;
            
            // Initialize the game!
            Result.GameInit(OpenGLVersion, MainMemory);
        }
    }
    if(!Result.IsValid)
    {
        Result.UpdateAndRender = 0;
        Result.GameInit = 0;
        Result.GameInputWhilePlayback = 0;
    }
    
    return Result;
}

static void UnloadGameCode(win32_game_code* GameCode)
{
    
    if(GameCode->GameCodeDLL)
    {
        FreeLibrary(GameCode->GameCodeDLL);
        GameCode->GameCodeDLL = 0;
    }
    
    GameCode->IsValid = false;
    GameCode->UpdateAndRender = 0;
    GameCode->GameInit = 0;
    GameCode->GameInputWhilePlayback = 0;
    GameCode->GameDebugRender = 0;
}