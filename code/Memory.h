#pragma once

// TODO(Naor): We want to change this after we learn more about alignments in memory.
constexpr size_t DEFAULT_ALIGNMENT = 4;

struct memory_arena
{
    platform_memory_block* CurrentBlock;
    platform_memory_block* FirstBlock; // This is used so we can easily index into the arena from the start
    uptr MinimumBlockSize;
    
    // u64 AllocationFlags; // TODO(Naor): Learn about this from casey!
};

inline void FreeLastBlock(memory_arena* Arena)
{
    platform_memory_block* Free = Arena->CurrentBlock;
    Arena->CurrentBlock = Free->ArenaPrev;
    Platform.DeallocateMemory(Free);
}

inline void FreeArena(memory_arena* Arena)
{
    while(Arena->CurrentBlock)
    {
        FreeLastBlock(Arena);
    }
    Arena->FirstBlock = 0;
    // TODO(Naor): Maybe zero the MinimumBlockSize?
}

inline size_t GetAlignmentOffsetForAddress(void* Memory, size_t Alignment)
{
    size_t AlignmentOffset = 0;
    
    size_t ResultPointer = (size_t)Memory;
    size_t AlignmentMask = Alignment-1;
    if(ResultPointer & AlignmentMask)
    {
        AlignmentOffset = Alignment - (ResultPointer & AlignmentMask);
    }
    
    return AlignmentOffset;
}

inline size_t GetAlignmentOffset(memory_arena* Arena, size_t Alignment)
{
    size_t Result = GetAlignmentOffsetForAddress(Arena->CurrentBlock->Base + Arena->CurrentBlock->Used, Alignment);
    
    return Result;
}

// TODO(Naor): Add push_params like casey?
inline size_t GetEffectiveSizeFor(memory_arena* Arena, size_t SizeInit)
{
    size_t Size = SizeInit;
    size_t AlignmentOffset = GetAlignmentOffset(Arena, DEFAULT_ALIGNMENT);
    
    Size += AlignmentOffset;
    return Size;
}


#define InitializeArena(Arena, type, Count) _InitializeArena(Arena, sizeof(type) * Count)
inline size_t _InitializeArena(memory_arena* Arena, size_t SizeInit)
{
    size_t Size = 0;
    if(Arena->CurrentBlock)
    {
        Size = GetEffectiveSizeFor(Arena, SizeInit);
    }
    
    if(!Arena->CurrentBlock || 
       (Arena->CurrentBlock->Used + Size) > Arena->CurrentBlock->Size)
    {
        // TODO(Naor): This is not aligned and casey said it is, fix it.
        Size = SizeInit;
        
        if(!Arena->MinimumBlockSize)
        {
            // TODO(Naor): This should be tuned according to casey
            Arena->MinimumBlockSize = Megabyte(1);
        }
        size_t BlockSize = Maximum(Size, Arena->MinimumBlockSize);
        
        platform_memory_block* NewBlock = Platform.AllocateMemory(BlockSize);
        NewBlock->ArenaPrev = Arena->CurrentBlock;
        
        if(!Arena->CurrentBlock)
        {
            Arena->FirstBlock = NewBlock;
        }
        else
        {
            Arena->CurrentBlock->ArenaNext = NewBlock;
        }
        
        Arena->CurrentBlock = NewBlock;
    }
    
    // Check that we are not overflowing
    Assert((Arena->CurrentBlock->Used + Size) <= Arena->CurrentBlock->Size);
    Assert(Size >= SizeInit);
    
    return Size;
}

#define PushStruct(Arena, type) (type*)_PushStruct(Arena, sizeof(type))
#define PushStructMultiple(Arena, type, Count) (type*)_PushStruct(Arena, sizeof(type) * Count)
inline void* _PushStruct(memory_arena* Arena, size_t SizeInit)
{
    size_t Size = _InitializeArena(Arena, SizeInit);
    size_t AlignmentOffset = GetAlignmentOffset(Arena, DEFAULT_ALIGNMENT);
    void* Result = Arena->CurrentBlock->Base + Arena->CurrentBlock->Used + AlignmentOffset;
    Arena->CurrentBlock->Used += Size;
    
    // TODO(Naor): Make this zeroing optional
    u8* TempBase = (u8*)Result;
    size_t TempSize = Size;
    while(TempSize--)
    {
        *TempBase++ = 0;
    }
    
    return Result;
}