#pragma once

struct vertex_pn
{
    v3 Position;
    v3 Normal;
    
    vertex_pn() = default;
    vertex_pn(v3 Position, v3 Normal = v3()) : Position(Position), Normal(Normal)
    {
    }
};

struct vertex_pnt
{
    v3 Position;
    v3 Normal;
    v2 TextureCoordinate;
    
    vertex_pnt() = default;
    vertex_pnt(v3 Position, v3 Normal = v3(), v2 TextureCoordinate = v2()) : Position(Position), Normal(Normal), TextureCoordinate(TextureCoordinate)
    {
    }
};

struct vertex_pc
{
    v3 Position;
    v3 Color;
    
    vertex_pc() = default;
    vertex_pc(v3 Position, v3 Color = v3()) : Position(Position), Color(Color)
    {
    }
};

// Position, Color, Normal
struct vertex_pnc
{
    v3 Position;
    v3 Normal;
    v3 Color;
    
    vertex_pnc() = default;
    vertex_pnc(v3 Position, v3 Normal, v3 Color) : Position(Position), Normal(Normal), Color(Color)
    {
    }
};