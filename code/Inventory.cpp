#include "Inventory.h"

//
// Recipes 
//
static void AddIngredient(recipe& Recipe, inventory_item_type IngredientType, r32 IngredientAmount)
{
    Assert(Recipe.IngredientsCount < ArrayCount(Recipe.Ingredients));
    inventory_item& Ingredient = Recipe.Ingredients[Recipe.IngredientsCount++];
    Ingredient.Type = IngredientType;
    // TODO(Jonathan): Currently, this will always be a resouce, but if we will have recipes that include other stuff
    // this will be changed
    Ingredient.State = InventoryItemState_Resource;
    Ingredient.Name = GetInventoryItemNameByType(IngredientType);
    Ingredient.Amount = IngredientAmount;
}

static void AddIngredientByRecipeIndex(inventory& Inventory, recipe_index RecipeIndex, 
                                       inventory_item_type IngredientType,
                                       r32 IngredientAmount)
{
    recipe* Recipe = &Inventory.Recipes[RecipeIndex];
    if(Recipe != nullptr)
    {
        AddIngredient(*Recipe, IngredientType, IngredientAmount);
    }
    else 
    {
        Assert("Couldn't find recipe!");
    }
}

static void CraftRecipe(inventory& Inventory, const recipe& Recipe)
{
    for(u32 Index = 0; Index < Recipe.IngredientsCount; ++Index)
    {
        ModifyInventoryItem(Inventory, Recipe.Ingredients[Index].Type, -Recipe.Ingredients[Index].Amount);
    }
    
    AddInventoryItem(Inventory, Recipe.CraftedItem);
}

static bool IsRecipeCraftable(inventory& Inventory, const recipe& Recipe)
{
    bool Result = true;
    for(u32 Index = 0; Index < Recipe.IngredientsCount; ++Index)
    {
        inventory_item* Ingerdient = GetInventoryItemByType(Inventory, Recipe.Ingredients[Index].Type);
        if (Ingerdient == nullptr)
        {
            Result = false;
            LogInfo("%s isn't in the inventory.", Recipe.Ingredients[Index].Name);
        }
        else
        {
            if (Ingerdient->Amount < Recipe.Ingredients[Index].Amount)
            {
                Result = false;
                r32 Diff = Recipe.Ingredients[Index].Amount - Ingerdient->Amount;
                if(!Result && Diff > 0)
                {
                    LogInfo("Not enough %s you need %f more.", Ingerdient->Name, Diff);
                }
            }
        }
        
    }
    
    return Result;
}

static void CheckCraftingOptions(inventory& Inventory)
{
    for(u32 Index = 0; Index < Inventory.ExistingRecipesCount; ++Index)
    {
        if(IsRecipeCraftable(Inventory, Inventory.Recipes[Index]))
        {
            // TODO(Jonathan): Add to some carftable array
            LogInfo("Craft recipe %s!", Inventory.Recipes[Index].CraftedItem.Name);
            CraftRecipe(Inventory, Inventory.Recipes[Index]);
        }
        else 
        {
            LogInfo("Can't craft recipe %s not enough ingredients", Inventory.Recipes[Index].CraftedItem.Name);
        }
    }
    
}

static void CheckCraftingOptionsForState(inventory& Inventory, inventory_item_state State)
{
    for(u32 Index = 0; Index < Inventory.ExistingRecipesCount; ++Index)
    {
        recipe& Recipe = Inventory.Recipes[Index];
        if(Recipe.CraftedItem.State == State)
        {
            if(IsRecipeCraftable(Inventory, Recipe))
            {
                // TODO(Jonathan): Add to some carftable array
                LogInfo("Craft recipe %s!", Recipe.CraftedItem.Name);
                CraftRecipe(Inventory, Recipe);
            }
            else 
            {
                LogInfo("Can't craft recipe %s not enough ingredients", Recipe.CraftedItem.Name);
            }
        }
    }
}


static recipe& CreateRecipe(inventory& Inventory, recipe_index RecipeIndex,
                            inventory_item_type CraftedItemType, inventory_item_state CraftedItemState)
{
    Assert(Inventory.ExistingRecipesCount < ArrayCount(Inventory.Recipes));
    recipe& Recipe = Inventory.Recipes[RecipeIndex];
    Inventory.ExistingRecipesCount++;
    Recipe.IngredientsCount = 0;
    Recipe.CraftedItem.Type = CraftedItemType;
    Recipe.CraftedItem.Name = GetInventoryItemNameByType(CraftedItemType);
    Recipe.CraftedItem.Amount = 1;
    Recipe.CraftedItem.State = CraftedItemState;
    return Recipe;
}


static void LoadRecipes(inventory& Inventory)
{
    // NOTE(Jonathan): Watch out fixed size!!
    token Tokens[100] = {};
    u32 TokensCount = 0;
    
    ParseRecipes(Tokens, TokensCount, ArrayCount(Tokens), "Recipes.txt");
    
    u32 Index = 0;
    while(Index < TokensCount)
    {
        token Token = Tokens[Index];
        if(Token.Type == Token_String && Tokens[Index + 1].Type == Token_OpenBraces)
        {
            // NOTE(Jonathan): Watch out fixed size!!
            char RecipeName[128] = {};
            sprintf(RecipeName, "%.*s", (s32)Token.TextLength, Token.Text);
            inventory_item_type RecipeType = GetInventoryItemTypeByName(RecipeName);
            
            recipe &Recipe = CreateRecipe(Inventory, GetRecipeIndexByInventoryItemType(RecipeType), RecipeType,
                                          GetInventoryItemStateByName(RecipeName));
            
            
            Index += 2;
            
            token IngredientToken = Tokens[Index];
            while(IngredientToken.Type != Token_CloseBraces && 
                  Index < TokensCount)
            {
                token NextToken = Tokens[Index + 1];
                if(Token.Type == Token_String && NextToken.Type == Token_Number)
                {
                    // NOTE(Jonathan): Watch out fixed size!!
                    char IngredientName[128] = {};
                    sprintf(IngredientName, "%.*s", (s32)IngredientToken.TextLength, IngredientToken.Text);
                    
                    // NOTE(Jonathan): Watch out fixed size!!
                    char AmountText[128] = {};
                    sprintf(AmountText, "%.*s", (s32)NextToken.TextLength, NextToken.Text);
                    
                    //setlocale(LC_ALL|~LC_NUMERIC, "");
                    // NOTE(Jonathan): Atof can not work sometimes! 
                    r32 Amount = (r32)atof(AmountText);
                    
                    inventory_item_type IngredientType = GetInventoryItemTypeByName(IngredientName);
                    AddIngredient(Recipe, IngredientType, Amount);
                    Index += 2;
                }
                else
                {
                    ++Index;
                }
                IngredientToken = Tokens[Index];
            }
        }
        else
        {
            ++Index;
        }
    }
}

//
// InventoryItem
//

static void ModifyInventoryItem(inventory& Inventory, inventory_item_type Type, r32 Amount)
{
    inventory_item* Item = GetInventoryItemByType(Inventory, Type);
    Item->Amount += Amount;
    
    Assert(Item->Amount >= 0);
    if(Item->Amount == 0)
    {
        *Item = InitInventoryItem(Item->Position);
        if(Inventory.NumExsitingItems > 0)
        {
            inventory_item& Dst = Inventory.Items[Inventory.NumExsitingItems - 1];
            Utility::Swap(*Item, Dst);
            Utility::Swap(Item->Position, Dst.Position);
            --Inventory.NumExsitingItems;
        }
    }
}

static void CreateInventoryItem(inventory& Inventory, inventory_item_type ItemType,
                                inventory_item_state ItemState, r32 Amount)
{
    Assert(Inventory.NumExsitingItems < ArrayCount(Inventory.Items));
    
    inventory_item& Item = Inventory.Items[Inventory.NumExsitingItems++];
    Item.Type = ItemType;
    Item.State = ItemState;
    Item.Amount = Amount;
    Item.Name = GetInventoryItemNameByType(Item.Type);
    Item.Texture = GameState->Assets.Textures[GetInventoryItemTextureByType(Item.Type)];
    
}

static r32 AddInventoryItem(inventory& Inventory, inventory_item_type ItemType, inventory_item_state ItemState,
                            r32 Amount)
{
    for(u32 ItemIndex = 0; ItemIndex < Inventory.NumExsitingItems; ItemIndex++)
    {
        if(Inventory.Items[ItemIndex].Type == ItemType)
        {
            Inventory.Items[ItemIndex].Amount += Amount;
            
            return Amount;
        }
    }
    
    // Add new item
    CreateInventoryItem(Inventory, ItemType, ItemState, Amount);
    
    return Amount;
}

// TODO(Jonathan): Check if this is in use?
inline r32 AddInventoryItem(inventory& Inventory, const inventory_item& Item)
{
    return AddInventoryItem(Inventory, Item.Type, Item.State, Item.Amount);
}

static inventory_item* GetInventoryItemByType(inventory& Inventory, inventory_item_type Type)
{
    for(u32 Index = 0; Index < Inventory.NumExsitingItems; ++Index)
    {
        if(Inventory.Items[Index].Type == Type)
        {
            return &Inventory.Items[Index];
        }
    }
    
    return nullptr;
}


static inventory_item InitInventoryItem(const v2& Position)
{
    inventory_item Result = {};
    Result.Type = InventoryItemType_None;
    Result.Position = Position;
    Result.BoundingBox = (GameState->Assets.Meshes[MESH_QUAD_INDEX].BoundingBox * SLOT_DIM) + V3(Result.Position, 0.0f);
    Result.Texture = GameState->Assets.Textures[TEXTURE_INVENTORYITEM_INDEX];
    Result.Name = "";
    Result.Description = "";
    Result.Amount = 0.0f;
    return Result;
}

inline v2 CalcInventoryItemPosition(const v2& StartPosition, s32 X, s32 Y)
{
    return (StartPosition + V2(SLOT_WIDTH * (X), (SLOT_COLUMN - 1 - Y) * SLOT_HEIGHT));
}

static void RenderInventoryItem(inventory_item& InventoryItem)
{
    mat4 ModelMatrix = M4();
    ModelMatrix = Translate(ModelMatrix, V3(InventoryItem.Position, 0.0f));
    ModelMatrix = Scale(ModelMatrix, V3(SLOT_WIDTH, SLOT_HEIGHT, 1.0));
    
    material Material = {};
    Material.Shader = &GameState->Assets.Shaders[SHADER_ORTHO_INDEX];
    SetMatrix(Material, "Projection", GameState->Ortho, SHADER_UNIFORM_PROJECTION_INDEX);
    SetMatrix(Material, "Model", ModelMatrix, SHADER_UNIFORM_MODEL_INDEX);
    
    PushRender(GameState->RenderGroup, Material, &GameState->Assets.Meshes[MESH_QUAD_INDEX],
               InventoryItem.Texture, RenderDrawType_Arrays);
}

//
// Inventory
//

static void InitInventory(inventory& Inventory, const v2& Position)
{
    Inventory.Position = Position;
    for(s32 ColumnIndex = SLOT_COLUMN - 1; ColumnIndex >= 0 ; --ColumnIndex)
    {
        for(s32 RowIndex = SLOT_ROW - 1; RowIndex >= 0 ; --RowIndex)
        {
            u32 Index = ColumnIndex * SLOT_COLUMN + RowIndex;
            inventory_item& Item = Inventory.Items[Index];
            Item = InitInventoryItem(CalcInventoryItemPosition(Inventory.Position, RowIndex, ColumnIndex));
        }
    }
}

static void MoveInventory(inventory& Inventory, const v2& Position)
{
    Inventory.Position = Position;
    for(s32 ColumnIndex = SLOT_COLUMN - 1; ColumnIndex >= 0 ; --ColumnIndex)
    {
        for(s32 RowIndex = SLOT_ROW - 1; RowIndex >= 0 ; --RowIndex)
        {
            u32 Index = ColumnIndex * SLOT_COLUMN + RowIndex;
            inventory_item& Item = Inventory.Items[Index];
            Item.Position = CalcInventoryItemPosition(Inventory.Position, RowIndex, ColumnIndex);
        }
    }
}

static void RenderInventory(inventory& Inventory)
{
    for(u32 Index = 0; Index < ArrayCount(Inventory.Items); ++Index)
    {
        inventory_item& Item = Inventory.Items[Index];
        RenderInventoryItem(Item);
    }
}

// TODO NOTE(Jonathan): This is a hacky way for us to see the texts of the items.
// because the renderer is buggy this is the only way to do that,
// delete this when the renderer would work properly!
static void DisplayInventoryItems(inventory& Inventory)
{
    for(u32 Index = 0; Index < Inventory.NumExsitingItems; ++Index)
    {
        inventory_item& Item = Inventory.Items[Index];
        ImmDrawText(GameState->RenderGroup,
                    GameState->FontRenderer,
                    GameState->Assets.ArialFont,
                    GameState->Ortho,
                    Item.Position.x, Item.Position.y, V4(1.0f, 1.0, 1.0f, 1.0f), 18,"%.2f", Item.Amount);
    }
    ImmDrawText(GameState->RenderGroup,
                GameState->FontRenderer,
                GameState->Assets.ArialFont,
                GameState->Ortho,
                Inventory.Position.x, Inventory.Position.y, V4(1.0f, 1.0, 1.0f, 1.0f), 28,"Inventory");
}

static void ClearInventory(inventory& Inventory)
{
    Inventory.NumExsitingItems = 0;
}

static inventory_item_type GetInventoryItemTypeByName(const char* Name)
{
    if(Utility::CStringEqual("Wood", Name)){return InventoryItemType_Wood;}
    else if(Utility::CStringEqual("Stone", Name)){return InventoryItemType_Stone;}
    else if(Utility::CStringEqual("Steel", Name)){return InventoryItemType_Steel;}
    else if(Utility::CStringEqual("Flint", Name)){return InventoryItemType_Flint;}
    else if(Utility::CStringEqual("Fire Stone", Name)){return InventoryItemType_FireStone;}
    else if(Utility::CStringEqual("Farmhouse", Name)){return InventoryItemType_Farmhouse;}
    else if(Utility::CStringEqual("Cottage", Name)){return InventoryItemType_Cottage;}
    else if(Utility::CStringEqual("Outpost", Name)){return InventoryItemType_Outpost;}
    else if(Utility::CStringEqual("Camp Fire", Name)){return InventoryItemType_CampFire;}
    else if(Utility::CStringEqual("Wall", Name)){return InventoryItemType_Wall;}
    
    Assert(!"Not found");
    return InventoryItemType_None;
}

static inventory_item_state GetInventoryItemStateByName(char* Name)
{
    inventory_item_type Type = GetInventoryItemTypeByName(Name);
    switch(Type)
    {
        case InventoryItemType_Wood:
        case InventoryItemType_Stone:
        case InventoryItemType_Steel:
        case InventoryItemType_FireStone:
        case InventoryItemType_Flint:
        {
            return InventoryItemState_Resource;
        }
        case InventoryItemType_Farmhouse:
        case InventoryItemType_Cottage:
        case InventoryItemType_Wall:
        case InventoryItemType_Outpost:
        case InventoryItemType_CampFire:
        {
            return InventoryItemState_Building;
        }
        
        
    }
    Assert(!"Not Found");
    return inventory_item_state(-1);
}


// TODO(Jonathan): Check if is is working properly
static void InventoryGive(inventory& Inventory, const char* ItemName, inventory_item_state ItemState,
                          r32 ItemAmount)
{
    inventory_item* Item = GetInventoryItemByType(Inventory, GetInventoryItemTypeByName(ItemName));
    if(Item)
    {
        Item->Amount += ItemAmount;
    }
    else
    {
        CreateInventoryItem(Inventory, GetInventoryItemTypeByName(ItemName), ItemState, ItemAmount);
    }
}

inline void InventoryGiveResource(inventory& Inventory, const char* ItemName, r32 ItemAmount)
{
    InventoryGive(Inventory, ItemName, InventoryItemState_Resource, ItemAmount);
}

inline void InventoryGiveBuilding(inventory& Inventory, const char* ItemName, r32 ItemAmount)
{
    InventoryGive(Inventory, ItemName, InventoryItemState_Building, ItemAmount);
}


static s32 GetInventoryItemTextureByType(inventory_item_type Type)
{
    switch(Type)
    {
        case InventoryItemType_Wood:{return TEXTURE_WOOD_SLOT_INDEX;}
        case InventoryItemType_Stone:{return TEXTURE_STONE_SLOT_INDEX;}
        case InventoryItemType_Steel:{return TEXTURE_STEEL_SLOT_INDEX;}
        case InventoryItemType_Flint:{return TEXTURE_FLINT_SLOT_INDEX;}
        case InventoryItemType_FireStone:{return TEXTURE_FIRESTONE_SLOT_INDEX;}
        case InventoryItemType_CampFire:{return TEXTURE_CAMPFIRE_SLOT_INDEX;}
        case InventoryItemType_Cottage:
        case InventoryItemType_Farmhouse:
        case InventoryItemType_Wall:
        case InventoryItemType_Outpost:
        case InventoryItemType_None:{return TEXTURE_INVENTORYITEM_INDEX;}
        default:{Assert(!"Not found!");}
    }
    return TEXTURE_NULL_INDEX;
}

static char* GetInventoryItemNameByType(inventory_item_type Type)
{
    switch(Type)
    {
        case InventoryItemType_Wood:{return "Wood";}
        case InventoryItemType_Stone:{return "Stone";}
        case InventoryItemType_Steel:{return "Steel";}
        case InventoryItemType_Flint:{return "Flint";}
        case InventoryItemType_FireStone:{return "Fire Stone";}
        case InventoryItemType_Cottage:{return "Cottage";}
        case InventoryItemType_Farmhouse:{return "Farmhouse";}
        case InventoryItemType_Wall:{return "Wall";}
        case InventoryItemType_Outpost:{return "Outpost";}
        case InventoryItemType_CampFire:{return "Camp Fire";}
        case InventoryItemType_None:{return "";};
        default:{Assert(!"Not found!");}
    }
    return "";
}

static inventory_item_type GetInventoryTypeByBuildingType(building_type Type)
{
    switch(Type)
    {
        case BuildingType_Farmhouse:{return InventoryItemType_Farmhouse;}
        case BuildingType_Cottage:{return InventoryItemType_Cottage;}
        case BuildingType_Wall:{return InventoryItemType_Wall;}
        case BuildingType_Outpost:{return InventoryItemType_Outpost;}
        case BuildingType_CampFire:{return InventoryItemType_CampFire;}
        default:{Assert(!"Not found!");}
    }
    return InventoryItemType_None;
}

static recipe_index GetRecipeIndexByInventoryItemType(inventory_item_type Type)
{
    switch(Type)
    {
        case InventoryItemType_Farmhouse:{return RecipeIndex_Farmhouse;}
        case InventoryItemType_Cottage:{return RecipeIndex_Cottage;}
        case InventoryItemType_Wall:{return RecipeIndex_Wall;}
        case InventoryItemType_Outpost:{return RecipeIndex_Outpost;}
        case InventoryItemType_CampFire:{return RecipeIndex_CampFire;}
        case InventoryItemType_FireStone:{return RecipeIndex_FireStone;};
        default:{Assert(!"Not found!");}
    }
    return recipe_index(0);
}