#pragma once

namespace Utility
{
#define POINT_COMPRESSION(p1, p2, p3) V3(##p1.x, ##p2.y, ##p3.z)
    static std::string AppendToPath(const char *DLLFilename)
    {
        char FullDLLFilenamePath[MAX_PATH];
        DWORD SizeOfFilename = GetModuleFileName(0, FullDLLFilenamePath, sizeof(FullDLLFilenamePath));
        char *OnePastLastSlash = FullDLLFilenamePath;
        for (char *Scan = FullDLLFilenamePath; *Scan; ++Scan)
        {
            if (*Scan == '\\')
            {
                OnePastLastSlash = Scan + 1;
            }
        }
        
        const char *DLLSourceFile = DLLFilename;
        int32_t SizeOfDLLSourceFile = static_cast<int32_t>(strlen(DLLSourceFile));
        for (int Index = 0; Index < SizeOfDLLSourceFile; ++Index)
        {
            *OnePastLastSlash++ = *DLLSourceFile++;
        }
        *OnePastLastSlash++ = 0;
        
        return FullDLLFilenamePath;
    }
    
    inline FILETIME GetLastWriteTime(const char *Filename)
    {
        FILETIME LastWriteTime = {};
        WIN32_FILE_ATTRIBUTE_DATA Data;
        if (GetFileAttributesEx(Filename, GetFileExInfoStandard, &Data))
        {
            LastWriteTime = Data.ftLastWriteTime;
        }
        return LastWriteTime;
    }
    
    static std::string GetFilenameFromPath(const std::string &FontFilename, bool WithExtension = false, bool WithSpaces = false)
    {
        std::size_t StartOfLastSlash = FontFilename.find_last_of("\\");
        
        // If '\' not found, search for '/'
        if (StartOfLastSlash == std::string::npos)
        {
            StartOfLastSlash = FontFilename.find_last_of("/");
        }
        
        std::string FontWithExtension = FontFilename.substr(StartOfLastSlash + 1);
        
        std::string Result;
        
        if (!WithExtension)
        {
            std::size_t StartOfLastDot = FontWithExtension.find_last_of(".");
            
            Result = FontWithExtension.substr(0, StartOfLastDot);
        }
        else
        {
            Result = FontWithExtension;
        }
        
        if (!WithSpaces)
        {
            auto Iterator = std::remove_if(Result.begin(), Result.end(), isspace);
            Result.erase(Iterator, Result.end());
        }
        
        return Result;
    }
    
    static s32 CStringFindLast(const char* Source, const char* Target)
    {
        const char* Temp = Target;
        s32 FoundIndex = 0;
        while(Source)
        {
            FoundIndex++;
            if(*Source++ == *Temp)
            {
                Temp++;
                if(!*Temp)
                {
                    return FoundIndex;
                }
            }
            else
            {
                Temp = Target;
            }
        }
        
        return 0;
    }
    
    static s32 CStringLength(const char* Str)
    {
        s32 Result = 0;
        
        while(*Str++) Result++;
        
        return Result;
    }
    
    // NOTE(Naor): This assumes that Target has enough memory
    static void CCopyString(char* Target, const char* Source)
    {
        while(*Source) *Target++ = *Source++;
        *Target = 0;
    }
    
    
    static bool CStringEqual(const char* Source, const char* Compare)
    {
        while(*Compare && *Source)
        {
            if(*Compare++ != *Source++)
                return false;
        }
        
        return !(*Compare || *Source);
    }
    
    static bool CStringEqualFirstContains(const char* Source, const char* Compare)
    {
        while(*Compare)
        {
            if(*Compare++ != *Source++)
                return false;
        }
        return true;
    }
    
    static bool CStringEqualWithoutCase(const char* Source, const char* Compare)
    {
        char S, C;
        while(*Compare)
        {
            C = *Compare;
            S = *Source;
            if(C >= 'A' && C <= 'Z')
            {
                C+= 'a' - 'A';
            }
            if(S >= 'A' && S <= 'Z')
            {
                S += 'a' - 'A';
            }
            
            if(C != S)
                return false;
            
            *Compare++;
            *Source++;
        }
        return true;
    }
    
    template <typename T>
        void Swap(T &One, T &Other)
    {
        T Temp = One;
        One = Other;
        Other = Temp;
    }
    
}