#pragma once

// TODO(Naor): There is a bug when using std containers that allocate memory on their own,
// we either need to create out own allocator or create our own container, both of which
// need to use the memory allocated for the game.
namespace Recording
{
    
    struct record_memory
    {
        std::vector<std::queue<SDL_Event>> InputRecord;
        std::vector<std::array<u8, SDL_NUM_SCANCODES>> InputRecordContinuous;
        s32 InputPlayIndex;
        s32 InputSize;
        
        bool IsRecording;
        bool IsPlaying;
        void* MemoryBlock;
        void* PlayingMemoryBlock;
        bool IsInitialized;
    };
    
    static constexpr s32 NUM_OF_RECORDING_SLOTS = 9;
    static record_memory RecordMemorySlots[NUM_OF_RECORDING_SLOTS];
    static bool ShouldPlay = false;
    static s32 CurrentlyPlaying = -1;
    static bool JustStartedPlayback = true;
    
    bool IsRecording(s32 Slot);
    bool IsThereRecordingInProgress();
    void RecordInput(std::queue<SDL_Event> GameInput);
    void EndRecord(s32 Slot);
    void RestartPlayback(s32 Slot);
    void BeginPlayback(s32 Slot);
    std::queue<SDL_Event> NextRecordInput(s32 Slot);
    u8* NextRecordInputContinuous(s32 Slot);
    void NextPlayIndex(s32 Slot);
    void* GetMemoryBlock(s32 Slot);
    bool IsInitialized(s32 Slot);
    void RemoveRecord(s32 Slot);
    void InputHandler(const SDL_Event& Event);
};

