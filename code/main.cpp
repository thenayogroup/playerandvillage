#include <Windows.h>
#include <iostream>
#include <queue>
#include <array>

// NOTE(Naor): REMEMBER WHEN UPDATING LIBS TO UPDATE DLL!@!!
#include "gl3w.c"
#include <SDL.h>
#define STB_TRUETYPE_IMPLEMENTATION
#include "../deps/stb/stb_truetype.h"

#define STB_SPRINTF_IMPLEMENTATION 
#include "../deps/stb/stb_sprintf.h"

#ifdef NAYO_DEBUG
#include <imgui_draw.cpp>
#include <imgui.cpp>
#include <imgui_demo.cpp>
#endif

#include "Types.h"
#include "Macros.h"
#include "Math/Math.h"

#include "platform/Platform.h"
#if NAYO_WIN32
#include "Win32.h"
#else
// TODO(Naor): Implement other platforms here!
#endif

#include "Utility.h"
#include "memory.h"
#include "array.h"

//
// Debug includes
//
#include "debug/sdl_imgui_implementation.cpp"
#include "debug/ImGui.h"

static constexpr r32 SCREEN_WIDTH = 1280.0f;//1920.0f;
static constexpr r32 SCREEN_HEIGHT = 720.0f;//1080.0f;

// TODO:
// - Replace all std::cout with a log system
// - Add fullscreen feature
// - Make sure the fullscreen will let the users decide which monitor to display on
// - Change resolution
// - Config file for the settings (resolution, cursor speed etc..)
// - Add all the functions to the window through the game to dedicated function...
//   (So we won't need to pass the SDL_Window to the game)

static const v2 WindowSize = V2(SCREEN_WIDTH, SCREEN_HEIGHT);
// TODO(Naor): Remove the DrawableSize!! this is not updating at all! (or fix it.. you know)
static v2 DrawableSize = {};
static const gl_version OpenGLVersion = {4,1};
static bool ShouldClose = false;
static SDL_Window* Window = nullptr;
static SDL_GLContext GlContext;

game_memory MainMemory = {};
platform_api Platform;

#ifdef NAYO_DEBUG
debug_state* DebugState;
#endif

// NOTE: This needs to be here for now because it is using `MainMemory`
#include "Recording.cpp"
#include "Hotloading.h"

static void InitializeSDL()
{
    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_EVENTS) != 0) 
    {
        std::cout << "Failed to initialize SDL: " << SDL_GetError() << "\n";
    }
    
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
    
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, OpenGLVersion.Major);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, OpenGLVersion.Minor);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    
    Window = SDL_CreateWindow("title", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, ToInt32(WindowSize.x),
                              ToInt32(WindowSize.y), SDL_WINDOW_OPENGL);
    
    GlContext = SDL_GL_CreateContext(Window);
    
    if (gl3wInit())
    {
        std::cout << "GL3W couldn't initialize!\n";
    }
    if (!gl3wIsSupported(OpenGLVersion.Major, OpenGLVersion.Minor))
    {
        std::cout << "OpenGL " << OpenGLVersion.Major << "." << OpenGLVersion.Minor << " is not supported!\n";
    }
    
    SDL_GL_MakeCurrent(Window, GlContext);
    
    s32 DrawableWidth;
    s32 DrawableHeight;
    SDL_GL_GetDrawableSize(Window, &DrawableWidth, &DrawableHeight);
    DrawableSize = V2(ToReal32(DrawableWidth), ToReal32(DrawableHeight));
    
    glViewport(0, 0, DrawableWidth, DrawableHeight);
    std::cout << "Setting up the viewport with width of " << DrawableSize.x << " and height of " << DrawableSize.y << "\n";
    
    // IMPORTANT: If you want to disable VSync remember that
    // high FPS might conflict with the playback!! AND MAY RESULT IN A **SYSTEM** CRASH
    SDL_GL_SetSwapInterval(1);
}

int main(int argc, char* argv[])
{
    // Loading game code
    std::string FullDLLFilenamePath = Utility::AppendToPath("Game.dll");
    win32_game_code GameCode;
    
    InitializeSDL();
    
#if NAYO_WIN32
    // Setting up the platform functions
    MainMemory.PlatformAPI.AllocateMemory = Win32AllocateMemory;
    MainMemory.PlatformAPI.DeallocateMemory = Win32DeallocMemory;
    MainMemory.PlatformAPI.MemoryBaseOffset = sizeof(win32_memory_block);
#else
    Assert(false);
#endif
    
    Platform = MainMemory.PlatformAPI;
    
#ifdef NAYO_DEBUG
    // NOTE(Naor): This block will allways be used so we don't need to deallocate it.
    platform_memory_block* DebugStateMemoryBlock = Platform.AllocateMemory(sizeof(debug_state));
    MainMemory.DebugStateAPI = new (DebugStateMemoryBlock->Base) debug_state;
    DebugState = MainMemory.DebugStateAPI;
    
    debug::UpdateQuoteOfTheDay();
    InitializeDynamicArray(&DebugState->Logger.Log);
    
    DebugState->Context = debug::InitializeImGui(DebugState->Context, Window);
#endif
    
    // Game Memory
    MainMemory.SizeInBytes = Megabyte(512);
    // We use VirtualAlloc because we cant use `new` so easily..
    MainMemory.MemoryBlock = Platform.AllocateMemory(MainMemory.SizeInBytes);
    MainMemory.Window = Window;
    
    GameCode = LoadGameCode(FullDLLFilenamePath.c_str());
    
    void* MainMemoryBlockBackup = MainMemory.MemoryBlock;
    std::queue<SDL_Event> GameInput;
    u64 CurrentTime = SDL_GetPerformanceCounter();
    u64 LastTime = 0;
    r32 DeltaTime = 0.0f;
    
    while(!ShouldClose) {
        LastTime = CurrentTime;
        CurrentTime = SDL_GetPerformanceCounter();
        DeltaTime = (CurrentTime - LastTime) / static_cast<r32>(SDL_GetPerformanceFrequency());
        //std::cout << 1.0f / DeltaTime << std::endl; // FPS
        {
            std::queue<SDL_Event> EmptyInput;
            std::swap(GameInput, EmptyInput);
        }
        
        // TODO(Naor): We can implement a system that will swap between DLL's 
        // first will be Game_temp_1.dll that will be loaded, and when we hot load,
        // we will copy the DLL to a new file called Game_temp_2.dll and try to load
        // the functions from there, while it is not loaded, we use Game_temp_1.dll we 
        // loaded earlier.
        // when we finished loading Game_temp_2.dll we can unload Game_temp_1.dll and
        // on the next hot-loading we will do the same but with Game_temp_1.dll.
        FILETIME NewDLLWriteTime = Utility::GetLastWriteTime(FullDLLFilenamePath.c_str());
        if(CompareFileTime(&GameCode.DLLLastWriteTime, &NewDLLWriteTime) != 0)
        {
            UnloadGameCode(&GameCode);
            for(u32 LoadTryIndex = 0;
                !GameCode.IsValid && (LoadTryIndex < 100);
                ++LoadTryIndex)
            {
                GameCode = LoadGameCode(FullDLLFilenamePath.c_str());
                Sleep(100);
            }
            if(!GameCode.IsValid)
            {
                LogCritical("Failed to load DLL");
            }
            
            if(Recording::ShouldPlay)
            {
                Recording::RestartPlayback(Recording::CurrentlyPlaying);
            }
        } 
        
        SDL_Event Event;
        while(SDL_PollEvent(&Event))
        {
#ifdef NAYO_DEBUG
            ImGui_SDL_ProcessEvent(&Event);
#endif
            GameInput.push(Event);
            switch(Event.type)
            {
                case SDL_QUIT:
                {
                    ShouldClose = true;
                }; break;
                
                case SDL_WINDOWEVENT:
                {
                }break;
                
                case SDL_KEYDOWN:
                {
#ifdef NAYO_DEBUG
                    if(!DebugState->IsCursorActive)
                        Recording::InputHandler(Event);
#endif
                }break;
            }
        }
        
#if NAYO_DEBUG
        ImGui_SDL_NewFrame(Window);
#endif
        
        Recording::RecordInput(GameInput);
        
        // NOTE: We want to check if the record indeed was initialized because
        // if it isn't, we are trying to access nullptr!
        if(Recording::ShouldPlay && Recording::IsInitialized(Recording::CurrentlyPlaying))
        {
            MainMemory.MemoryBlock = Recording::GetMemoryBlock(Recording::CurrentlyPlaying);
            GameCode.UpdateAndRender(DeltaTime, MainMemory,
                                     Recording::NextRecordInput(Recording::CurrentlyPlaying),
                                     Recording::NextRecordInputContinuous(Recording::CurrentlyPlaying));
            GameCode.GameInputWhilePlayback(MainMemory, GameInput, SDL_GetKeyboardState(nullptr), Recording::JustStartedPlayback);
            Recording::NextPlayIndex(Recording::CurrentlyPlaying);
            
            Recording::JustStartedPlayback = false;
        }
        else
        {
            // We want to get the backup memory back
            if(MainMemory.MemoryBlock != MainMemoryBlockBackup)
            {
                MainMemory.MemoryBlock = MainMemoryBlockBackup;
            }
            
            GameCode.UpdateAndRender(DeltaTime, MainMemory, GameInput, SDL_GetKeyboardState(nullptr));
            
            Recording::JustStartedPlayback = true;
        }
        
#if NAYO_DEBUG
        GameCode.GameDebugRender(DeltaTime);
        if(DebugState->IsAnyWindowDisplayed)
        {
            ImGui::Render();
            ImGui_SDL_RenderDrawData(ImGui::GetDrawData());
        }
        else
        {
            ImGui::EndFrame();
        }
#endif
        SDL_GL_SwapWindow(Window);
    }
    
    exit(0);
}
