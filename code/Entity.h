#pragma once

static constexpr s32 MAX_TILES = 256;

// Golem
static constexpr r32 DEFAULT_GOLEM_HEALTH = 10.0f;
static constexpr r32 DEFAULT_GOLEM_POWER = 5.0f;

// Player
static constexpr r32 PLAYER_SPEED = 50.0f; 

static constexpr r32 PLAYER_JUMP_HEIGHT = 3.0f;
static constexpr r32 PLAYER_GRAVITY_AMOUNT = -50.0f;
static constexpr s32  PLAYER_MAX_TILES = 9;

// Env
static constexpr r32 WOOD_VALUE = 20.0f;
static constexpr r32 STONE_VALUE = 10.0f;
static constexpr r32 STEEL_VALUE = 5.0f;

// Buildingg
static constexpr r32 SelectionMaxDistance = 25.0f;
static constexpr r32 GlobalBuildingTime = 25.0f;

enum building_type
{
    BuildingType_None = 0,
    BuildingType_Farmhouse,
    BuildingType_Cottage,
    BuildingType_Wall,
    BuildingType_Outpost,
    BuildingType_CampFire,
    BuildingType_NumOfBuildings
};

struct move_info
{
    v3 Acceleration;
    r32 Speed;
    r32 Friction;
    r32 Gravity;
};

enum entity_type
{
    EntityType_None,
    EntityType_Player,
    EntityType_Soldier,
    EntityType_Golem,
    EntityType_Projectile,
    EntityType_FixedBuilding,
    EntityType_PlacingBuilding,
    EntityType_Tree,
    EntityType_Bush,
    EntityType_Rock,
    EntityType_Stone,
    EntityType_Wood,
    EntityType_Steel,
    EntityType_Flint,
};

struct entity
{
    entity_type Type;
    
    aabb BoundingBox;
    v2 TilesLocations[MAX_TILES];
    s32 TilesLocationsCount;
    
    v3 Velocity;
    v3 Position;
    v3 Direction;
    move_info MoveInfo;
    
    r32 JumpVel;
    r32 NextJump;
    bool OnGround;
    
    r32 Health;
    r32 Power;
    r32 AttackRange;
    
    inventory Inventory;
#if 0    
    r32 WoodAmount;
    r32 StoneAmount;
#endif
    
    v4 Color;
    r32 WoodCost;
    r32 StoneCost;
    bool Rotate;
    
    building_type BuildingType;
    bool IsVisible;
    bool IsFirstPlaced;
    r32 Angle;
};

static char* GetEntityTypeName(entity_type Type)
{
    switch(Type)
    {
        case(EntityType_None):
        {
            return "None";
        }break;
        case(EntityType_Player):
        {
            return "Player";
        }break;
        case(EntityType_Soldier):
        {
            return "Soldier";
        }break;
        case(EntityType_Golem):
        {
            return "Golem";
        }break;
        case(EntityType_Projectile):
        {
            return "Projectile";
        }break;
        case(EntityType_FixedBuilding):
        {
            return "Fixed Building";
        }break;
        case(EntityType_PlacingBuilding):
        {
            return "PlacingBuilding";
        }break;
        case(EntityType_Tree):
        {
            return "Tree";
        }break;
        case(EntityType_Bush):
        {
            return "Bush";
        }break;
        case(EntityType_Rock):
        {
            return "Rock";
        }break;
        case(EntityType_Stone):
        {
            return "Stone";
        }break;
        case(EntityType_Wood):
        {
            return "Wood";
        }break;
    }
    
    return "Not Found";
}
