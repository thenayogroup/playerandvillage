#pragma once

struct mouse_picker
{
    v3 Ray = {};
    r32 Distance;
};


struct selected_entity
{
    v3 Intersection;
    s32 Index;
};
