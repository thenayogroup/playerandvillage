#include "TriangleMesh.h"

static void LoadModel(mesh& Mesh, const char* FilePath)
{
    std::vector<vertex_pnt> Vertices;
    std::vector<u32> Indicies;
    
    Mesh.BoundingBox = LoadObj(Vertices, FilePath);
    
    glGenVertexArrays(1, &Mesh.Vao);
    glBindVertexArray(Mesh.Vao);
    
    glGenBuffers(1, &Mesh.Vbo);
    glBindBuffer(GL_ARRAY_BUFFER, Mesh.Vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_pnt) * Vertices.size(), &Vertices[0], GL_STATIC_DRAW);
    
    u32 Stride = sizeof(vertex_pnt);
    
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, Stride, nullptr);
    glEnableVertexAttribArray(0);
    
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, Stride, reinterpret_cast<void *>(sizeof(v3)));
    glEnableVertexAttribArray(1);
    
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, Stride, reinterpret_cast<void *>(2 * sizeof(v3)));
    glEnableVertexAttribArray(2);
    
    Mesh.Count = static_cast<u32>(Vertices.size());
}

static void LoadTexture(u32& Texture, const char* TexturePath)
{
    glGenTextures(1, &Texture);
    glBindTexture(GL_TEXTURE_2D, Texture);
    
    s32 TexWidth, TexHeight, TexChannel;
    
    unsigned char* TexData = stbi_load(TexturePath, &TexWidth, &TexHeight, &TexChannel, 0);
    
    if (TexData == nullptr)
        LogError("loading texture: %s", TexturePath);
    
    if (TexChannel == 4)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, TexWidth, TexHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, TexData);
    else
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, TexWidth, TexHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, TexData);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
    glGenerateMipmap(GL_TEXTURE_2D);
    
    stbi_image_free(TexData);
}

static void RenderModel(render_group& RenderGroup, shader_program& Shader, mesh& Mesh, s32 Texture, const m4& Projection, const m4& ModelMatrix, const v4& Color)
{
    material Material = {};
    Material.Shader = &Shader;
    SetMatrix(Material, "ProjectionView", Projection, SHADER_UNIFORM_VP_INDEX);
    SetFloat(Material, "Time", GameState->CurrentTime, SHADER_UNIFORM_TIME_INDEX);
    SetMatrix(Material, "Model", ModelMatrix, SHADER_UNIFORM_MODEL_INDEX);
    SetVector(Material, "Color", Color, SHADER_UNIFORM_COLOR_INDEX);
    
    u32 TextureID = TEXTURE_NULL_INDEX;
    if(Texture != TEXTURE_NULL_INDEX)
    {
        TextureID = GameState->Assets.Textures[Texture];
    }
    PushRender(RenderGroup, Material, &Mesh, TextureID, RenderDrawType_Arrays);
}

// TODO(Jonathan): Change place
static void AddVertex(r32* Verticies, u32& VerticiesCount, r32 Value)
{
    r32& Vert = Verticies[VerticiesCount++];
    Vert = Value;
}

static void LoadQuadMesh(mesh& Mesh)
{
    r32 Verts[24] = {};
    u32 VertsCount = 0;
    r32 Row = 0.0f, Column = 0.0f;
    
    AddVertex(Verts, VertsCount, Row);
    AddVertex(Verts, VertsCount, Column);
    AddVertex(Verts, VertsCount, 0.0f);
    AddVertex(Verts, VertsCount, 0.0f);
    
    AddVertex(Verts, VertsCount, Row + 1.0f);
    AddVertex(Verts, VertsCount, Column);
    AddVertex(Verts, VertsCount, 1.0f);
    AddVertex(Verts, VertsCount, 0.0f);
    
    AddVertex(Verts, VertsCount, Row);
    AddVertex(Verts, VertsCount, Column + 1.0f);
    AddVertex(Verts, VertsCount, 0.0f);
    AddVertex(Verts, VertsCount, 1.0f);
    
    AddVertex(Verts, VertsCount, Row + 1.0f);
    AddVertex(Verts, VertsCount, Column);
    AddVertex(Verts, VertsCount, 1.0f);
    AddVertex(Verts, VertsCount, 0.0f);
    
    AddVertex(Verts, VertsCount, Row + 1.0f);
    AddVertex(Verts, VertsCount, Column + 1.0f);
    AddVertex(Verts, VertsCount, 1.0f);
    AddVertex(Verts, VertsCount, 1.0f);
    
    AddVertex(Verts, VertsCount, Row);
    AddVertex(Verts, VertsCount, Column + 1.0f);
    AddVertex(Verts, VertsCount, 0.0f);
    AddVertex(Verts, VertsCount, 1.0f);
    
    u32 QuadBuffer;
    glGenVertexArrays(1, &Mesh.Vao);
    glGenBuffers(1, &QuadBuffer);
    
    glBindBuffer(GL_ARRAY_BUFFER, QuadBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Verts), Verts, GL_STATIC_DRAW);
    
    glBindVertexArray(Mesh.Vao);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0, 2, GL_FLOAT, false, 4 * sizeof(r32), nullptr);
    glVertexAttribPointer(1, 2, GL_FLOAT, false, 4 * sizeof(r32), reinterpret_cast<void *>(sizeof(r32) * 2));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    Mesh.Count = ArrayCount(Verts) / 4;
    Mesh.BoundingBox = AABB(V3(0.0f, 0.0f, 0.0f), V3(1.0f, 1.0f, 0.0f));
}
