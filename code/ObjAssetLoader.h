#pragma once

#include "../deps/tinyobjloader/tiny_obj_loader.cc"

// @unused
static v3 LoadObj(std::vector<vertex_pn>& Data, const char* FileName)
{
    std::string basePath = "models/";
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    
    v3 Max = {};
    
    std::string err;
    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, FileName, basePath.c_str());
    
    if (!err.empty() || !ret)
    {
        LogError("%s", err.c_str());
        return {};
    }
    
    // Loop over shapes
    for (size_t s = 0; s < shapes.size(); s++)
    {
        // Loop over faces(polygon)
        size_t index_offset = 0;
        for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++)
        {
            int fv = shapes[s].mesh.num_face_vertices[f];
            
            // Loop over vertices in the face.
            for (size_t v = 0; v < fv; v++)
            {
                // access to vertex
                tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
                r32 vx = attrib.vertices[3 * idx.vertex_index + 0];
                r32 vy = attrib.vertices[3 * idx.vertex_index + 1];
                r32 vz = attrib.vertices[3 * idx.vertex_index + 2];
                r32 nx = attrib.normals[3 * idx.normal_index + 0];
                r32 ny = attrib.normals[3 * idx.normal_index + 1];
                r32 nz = attrib.normals[3 * idx.normal_index + 2];
                //r32 tx = attrib.texcoords[2 * idx.texcoord_index + 0];
                //r32 ty = attrib.texcoords[2 * idx.texcoord_index + 1];
                
                Data.emplace_back(vertex_pn{V3(vx, vy, vz), V3(nx, ny, nz)});
                
                if (vx > Max.x)
                    Max.x = vx;
                if (vy > Max.y)
                    Max.x = vx;
                if (vz > Max.z)
                    Max.z = vz;
            }
            index_offset += fv;
            
            // per-face material
            shapes[s].mesh.material_ids[f];
        }
    }
    return Max;
}

static aabb LoadObj(std::vector<vertex_pnt>& Vertices, const char* FileName)
{
    std::string basePath = "models/";
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    
    aabb Box = {};
    Box.Min = V3(100.0f);
    
    std::string err;
    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, FileName, basePath.c_str());
    
    if (!err.empty() || !ret)
    {
        LogError("%s", err.c_str());
        return {};
    }
    
    // Loop over shapes
    for (size_t s = 0; s < shapes.size(); s++)
    {
        // Loop over faces(polygon)
        size_t index_offset = 0;
        for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++)
        {
            int fv = shapes[s].mesh.num_face_vertices[f];
            
            // Loop over vertices in the face.
            for (size_t v = 0; v < fv; v++)
            {
                // access to vertex
                tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
                r32 vx = attrib.vertices[3 * idx.vertex_index + 0];
                r32 vy = attrib.vertices[3 * idx.vertex_index + 1];
                r32 vz = attrib.vertices[3 * idx.vertex_index + 2];
                r32 nx = attrib.normals[3 * idx.normal_index + 0];
                r32 ny = attrib.normals[3 * idx.normal_index + 1];
                r32 nz = attrib.normals[3 * idx.normal_index + 2];
                
                // TODO(Jonathan): Temporary, because there is for now some objs that don't have textures
                r32 tx = 0, ty = 0;
                if(idx.texcoord_index != -1)
                {
                    tx = attrib.texcoords[2 * idx.texcoord_index + 0];
                    ty = attrib.texcoords[2 * idx.texcoord_index + 1];
                }
                
                Vertices.emplace_back(vertex_pnt{V3(vx, vy, vz), V3(nx, ny, nz), V2(tx, ty)});
                
                if (vx > Box.Max.x)
                    Box.Max.x = vx;
                if (vy > Box.Max.y)
                    Box.Max.y = vy;
                if (vz > Box.Max.z)
                    Box.Max.z = vz;
                
                if (vx < Box.Min.x)
                    Box.Min.x = vx;
                if (vy < Box.Min.y)
                    Box.Min.y = vy;
                if (vz < Box.Min.z)
                    Box.Min.z = vz;
            }
            index_offset += fv;
            
            // per-face material
            shapes[s].mesh.material_ids[f];
        }
    }
    return Box;
}
