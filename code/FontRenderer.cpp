#include "FontRenderer.h"


// TODO: Change the malloc with our own allocation.
void LoadFont(font_info& Font, char* FontPath)
{
    u8* TTFBuffer = (u8*)malloc(1<<20);
    u8* TempBitmap = (u8*)malloc(BITMAP_SIZE*BITMAP_SIZE);
    fread(TTFBuffer, 1, 1<<20, fopen(FontPath, "rb"));
    
    stbtt_fontinfo FontInfo;
    
    // The 0 in GetFontOffsetForIndex is for the font inside the font file (bold, italic, etc...)
    stbtt_InitFont(&FontInfo, TTFBuffer, stbtt_GetFontOffsetForIndex(TTFBuffer, 0));
    s32 ascent;
    s32 descent;
    s32 lineGap;
    stbtt_GetFontVMetrics(&FontInfo, &ascent, &descent, &lineGap);
    
    r32 scale = stbtt_ScaleForPixelHeight(&FontInfo, FONT_MAX_SIZE);
    ascent = s32(ascent * scale);
    descent = s32(descent * scale);
    lineGap = s32(lineGap * scale);
    Font.MaxHeight = r32(ascent - descent + lineGap);
    
    stbtt_BakeFontBitmap(TTFBuffer, 0, FONT_MAX_SIZE, TempBitmap, BITMAP_SIZE, BITMAP_SIZE, 32, 96, Font.Data); // no guarantee this fits!
    
    // can free ttf_buffer at this point
    free(TTFBuffer);
    glGenTextures(1, &Font.Texture);
    glBindTexture(GL_TEXTURE_2D, Font.Texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, BITMAP_SIZE, BITMAP_SIZE, 0, GL_RED, GL_UNSIGNED_BYTE, TempBitmap);
    
    // can free temp_bitmap at this point
    free(TempBitmap);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);
}


static void InitFontRenderer(font_renderer& FontRenderer)
{
    glGenVertexArrays(1, &FontRenderer.Mesh.Vao);
    glGenBuffers(1, &FontRenderer.Mesh.Vbo);
    glBindBuffer(GL_ARRAY_BUFFER, FontRenderer.Mesh.Vbo);
    
    glBindVertexArray(FontRenderer.Mesh.Vao);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(0, 2, GL_FLOAT, false, 8 * sizeof(r32), nullptr);
    glVertexAttribPointer(1, 4, GL_FLOAT, false, 8 * sizeof(r32), reinterpret_cast<void *>(sizeof(r32) * 2));
    glVertexAttribPointer(2, 2, GL_FLOAT, false, 8 * sizeof(r32), reinterpret_cast<void *>(sizeof(r32) * 6)); 
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    
    FontRenderer.Shader.AddShaderBySource(shader_program::type::Vertex, FontRender2DShaderVertSrc);
    FontRenderer.Shader.AddShaderBySource(shader_program::type::Fragment, FontRender2DShaderFragSrc);
    FontRenderer.Shader.Compile();
}


static std::vector<r32> PrepareText(font_renderer& FontRenderer, font_info& Font, char* Text, const v4& Color)
{
    std::vector<r32> Glyphs;
    
    r32 X = -(Font.Data + *Text - 32)->xoff;
    r32 Y = 0;
    
    r32 OriginalX = X;
    while (*Text) 
    {
        if(*Text == '\n')
        {
            X = OriginalX;
            Y -= Font.MaxHeight; // + YOffset;
        }
        
        if (*Text >= 32 && *Text < 128) 
        {
            stbtt_aligned_quad Quad;
            stbtt_GetBakedQuad(Font.Data, BITMAP_SIZE, BITMAP_SIZE, *Text - 32, &X, &Y, &Quad, 1);
            
            // 1
            
            Glyphs.emplace_back(Quad.x0);
            Glyphs.emplace_back(Quad.y0);
            Glyphs.emplace_back(Color.x);
            Glyphs.emplace_back(Color.y);
            Glyphs.emplace_back(Color.z);
            Glyphs.emplace_back(Color.w);
            Glyphs.emplace_back(Quad.s0);
            Glyphs.emplace_back(Quad.t1);
            
            Glyphs.emplace_back(Quad.x1);
            Glyphs.emplace_back(Quad.y0);
            Glyphs.emplace_back(Color.x);
            Glyphs.emplace_back(Color.y);
            Glyphs.emplace_back(Color.z);
            Glyphs.emplace_back(Color.w);
            Glyphs.emplace_back(Quad.s1);
            Glyphs.emplace_back(Quad.t1);
            
            Glyphs.emplace_back(Quad.x0);
            Glyphs.emplace_back(Quad.y1);
            Glyphs.emplace_back(Color.x);
            Glyphs.emplace_back(Color.y);
            Glyphs.emplace_back(Color.z);
            Glyphs.emplace_back(Color.w);
            Glyphs.emplace_back(Quad.s0);
            Glyphs.emplace_back(Quad.t0);
            
            
            // 2
            
            Glyphs.emplace_back(Quad.x1);
            Glyphs.emplace_back(Quad.y0);
            Glyphs.emplace_back(Color.x);
            Glyphs.emplace_back(Color.y);
            Glyphs.emplace_back(Color.z);
            Glyphs.emplace_back(Color.w);
            Glyphs.emplace_back(Quad.s1);
            Glyphs.emplace_back(Quad.t1);
            
            Glyphs.emplace_back(Quad.x1);
            Glyphs.emplace_back(Quad.y1);
            Glyphs.emplace_back(Color.x);
            Glyphs.emplace_back(Color.y);
            Glyphs.emplace_back(Color.z);
            Glyphs.emplace_back(Color.w);
            Glyphs.emplace_back(Quad.s1);
            Glyphs.emplace_back(Quad.t0);
            
            Glyphs.emplace_back(Quad.x0);
            Glyphs.emplace_back(Quad.y1);
            Glyphs.emplace_back(Color.x);
            Glyphs.emplace_back(Color.y);
            Glyphs.emplace_back(Color.z);
            Glyphs.emplace_back(Color.w);
            Glyphs.emplace_back(Quad.s0);
            Glyphs.emplace_back(Quad.t0);
            
        }
        ++Text;
    }
    return Glyphs;
}


inline void AddText(font_renderer& FontRenderer, font_info& Font, char* Text, r32 X, r32 Y, s32 Size, const v4& Color)
{ 
    PrepareText(FontRenderer, Font, Text, Color);
}

static void UpdateAndRenderFont(render_group& PieceGroup, font_renderer& FontRenderer,
                                const m4& Projection, u32 Texture, r32 X, r32 Y, r32 Size)
{
    
    // Flush/Update the new texts(Return this to game.cpp)
    glBindBuffer(GL_ARRAY_BUFFER, FontRenderer.Mesh.Vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(r32) * FontRenderer.Glyphs.size(), &FontRenderer.Glyphs[0], GL_DYNAMIC_DRAW);
    
    // TODO(Jonathan): When the renderer will work properly, delete the part after the #if 0
    // this is only for the fonts to be rendered first so we can see them in places like the inventory.
#if 0    
    m3 ModelMatrix = Math::M3AffineScale(V2(Size));
    ModelMatrix  = Math::Translate(ModelMatrix, V2(X, Y));
    
    material Material = {};
    Material.Shader = &FontRenderer.Shader;
    SetMatrix(Material, "Projection", Projection, SHADER_UNIFORM_PROJECTION_INDEX);
    SetMatrix(Material,"Model", ModelMatrix, SHADER_UNIFORM_MODEL_INDEX);
    
    PushRender(PieceGroup, Material, &FontRenderer.Mesh, Texture, RenderDrawType_Arrays);
#endif
    
    FontRenderer.Shader.Use();
    
    m3 ModelMatrix = Math::M3AffineScale(V2(Size));
    ModelMatrix  = Math::Translate(ModelMatrix, V2(X, Y));
    glUniformMatrix4fv(FontRenderer.Shader.Uniforms[SHADER_UNIFORM_PROJECTION_INDEX], 1, false, Projection.Data());
    glUniformMatrix3fv(FontRenderer.Shader.Uniforms[SHADER_UNIFORM_MODEL_INDEX], 1, false, ModelMatrix.Data());
    
    glBindTexture(GL_TEXTURE_2D, Texture);
    glBindVertexArray(FontRenderer.Mesh.Vao);
    glDrawArrays(GL_TRIANGLES, 0, static_cast<GLsizei>(FontRenderer.Glyphs.size() / 8));
    
    // Clear all the texts (Return this to game.cpp)
    FontRenderer.Glyphs.clear();
}

#define FORMAT_TEXT(Result, Text) \
va_list ArgPtr; \
va_start(ArgPtr, Text);\
stbsp_vsprintf(Result, Text, ArgPtr);\
va_end(ArgPtr);


// NOTE(Johnatan): This function only accepts strings up to 512 characters.
static void ImmDrawText(render_group& RenderGroup, font_renderer& FontRenderer, font_info& Font,
                        const m4& Projection, r32 X, r32 Y, v4 Color, s32 Size, char* Text, ...)
{
    char Result[512];
    FORMAT_TEXT(Result, Text);
    FontRenderer.Glyphs = PrepareText(FontRenderer, Font, Result, Color);
    FontRenderer.Mesh.Count = static_cast<GLsizei>(FontRenderer.Glyphs.size() / 8);
    UpdateAndRenderFont(RenderGroup, FontRenderer, Projection, Font.Texture, X, Y, Size / FONT_MAX_SIZE);
}

// TODO(Johnatan): Check if this is working
static void DrawText(render_group& RenderGroup, font_renderer& FontRenderer, font_info& Font,
                     const m4& Projection, r32 X, r32 Y, v4 Color, s32 Size, reference_frame Frame, char* Text, ...)
{
    r32 XFinal = Frame.TopLeft.x;
    r32 YFinal = Frame.TopLeft.y;
    
    XFinal += X * Frame.Size.x;
    YFinal += Y * Frame.Size.y;
    
    ImmDrawText(RenderGroup, FontRenderer, Font, Projection, XFinal, YFinal, Color, Size, Text);
}


