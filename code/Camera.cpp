#include "Camera.h"

inline void InitializeCamera(camera &Camera, const m4 &Projection, v3* Attachment)
{
    Camera.Projection = Projection;
    Camera.Orientation.w = 1.0f;
    Camera.Right = V3(1.0f, 0.0f, 0.0f);
    Camera.Forward = V3(0.0f, 0.0f, -1.0f);
    Camera.Up = V3(0.0f, 1.0f, 0.0f);
    Camera.Distance = 10.0f;
    Camera.Speed = DEFAULT_SPEED;
    
    //Camera.View = Math::LookAt(Camera.Position, Camera.Position + Camera.Forward, Camera.Up);
    Camera.View = Math::LookAt(Camera.Position - (Camera.Forward * Camera.Distance), Camera.Position, Camera.Up);
    
    Camera.Distance = 10.0f;
    
    Camera.Attachment = Attachment;
}

static void UpdateCamera(camera &Camera, real32 Delta)
{
    Camera.Velocity *= Delta;
    Camera.Position += Camera.Velocity;
    
    Camera.Velocity = {};
    
    if(Camera.Attachment)
    {
        const v3 CameraOffset = V3(0.0f, 4.5f, 0.0f) + Camera.Right*1.2f;
        Camera.Position = *Camera.Attachment + CameraOffset;
        
        r32 NewDistance = Camera.Distance;
        v3 NewPosition = Camera.Position - (Camera.Forward * NewDistance);
        
#if 0
        // TODO(Naor): This is another solution for the camera, this is not really "smooth"
        // but we CAN use it to create some cool effects like Fortnite
        while(NewPosition.y < GetWorldHeight(NewPosition.x, NewPosition.z) + 0.3f)
        {
            NewDistance -= 0.1f;
            NewPosition = Camera.Position - (Camera.Forward * NewDistance);
        }
#endif
        
        if(NewPosition.y < GetWorldHeight(NewPosition.x, NewPosition.z) + 0.3f)
        {
            NewPosition.y = GetWorldHeight(NewPosition.x, NewPosition.z) + 0.3f;
        }
        
        
        Camera.View = Math::LookAt(NewPosition, Camera.Position, Camera.Up);
    }
    else
    {
        Camera.View = Math::LookAt(Camera.Position, Camera.Position + Camera.Forward, Camera.Up);
    }
}

static void InputCamera(camera &Camera, direction Dir)
{
    Camera.HasMoved = true;
    switch (Dir)
    {
        case direction::Left:
        {
            Camera.Velocity -= Camera.Right * Camera.Speed;
        }
        break;
        case direction::Right:
        {
            Camera.Velocity += Camera.Right * Camera.Speed;
        }
        break;
        case direction::Forward:
        {
            Camera.Velocity += Camera.Forward * Camera.Speed;
        }
        break;
        case direction::Backward:
        {
            Camera.Velocity -= Camera.Forward * Camera.Speed;}
        break;
        case direction::Up:
        {
            Camera.Velocity += V3(0.0f, 1.0f, 0.0f) * Camera.Speed;
        }
        break;
        case direction::Down:
        {
            Camera.Velocity += V3(0.0f, -1.0f, 0.0f) * Camera.Speed;
        }
        break;
    }
    
}

// TODO(Naor): This has a problem if DX or DY is too fast,
// the camera seems to flip or tilt
static void MoveCamera(camera &Camera, int32 DX, int32 DY)
{
    Camera.HasMoved = true;
    quat Yaw = EulerToQuaternion(V3(-DY * DEFAULT_SENSITIVITY, 0.0f, 0.0f));
    quat Pitch = EulerToQuaternion(V3(0.0f, -DX * DEFAULT_SENSITIVITY, 0.0f));
    
    quat NewOrientation = Math::Normalize(Camera.Orientation * Yaw);
    v3 NewUp = NewOrientation * V3(0.0f, 1.0f, 0.0f);
    
    if (NewUp.y <= DEFAULT_DEADZONE)
    {
        Camera.Orientation = Math::Normalize(Pitch * Camera.Orientation);
    }
    else
    {
        Camera.Orientation = Math::Normalize(Pitch * NewOrientation);
    }
    
    Camera.Forward = Camera.Orientation * V3(0.0f, 0.0f, -1.0f);
    Camera.Right = Math::Normalize(Math::Cross(Camera.Forward, V3(0.0f, 1.0f, 0.0f)));
    Camera.Up = NewUp;
}