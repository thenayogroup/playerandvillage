#pragma once 
// NOTE(Naor): Altough we use OpenAL Soft, we STILL need to install OpenAL from openal.org to the users!
#include "AL/al.h"
#include "AL/alc.h"

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!THIS IS IMPORTANT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// The number of max source is not THAT high (255 and sometimes less than 30!), so we want to create an array
// of sources and use them depending on who is no used.

// TODO(Naor): We want to create multiple buffers and sources and use them, 
// for example instead of creating one buffer every time we want a new sound, 
// or a one new source every time we add source, we can create 16 sources, 
// use them until we run out, and then allocate another 16 or so and use them,
// + we can deallocate the unused ones or reuse them


// TODO(Naor): What happens when the use switch output device mid game??

// TODO(Naor): There is a bug when Hot-Loading, some sounds keep going (loop ones) and new sounds
// wont play, maybe it is because of the initialize function, find a way to re-initialize without losing the sources
// or buffers.
struct sound_buffer
{
    u32 ID;
    s32 SampleCount;
    s32 ChannelCount;
};

struct sound_source
{
    u32 ID;
    v3 Position;
    v3 Velocity;
    v3 Direction; // If = v3(0.0f) then the sound is NOT directional
    r32 Gain; // Only positive
    r32 Pitch; // Only positive
    b32 Loop;
    bool IsPlaying;
    bool IsDeleted;
    
    inline void SetPosition(const v3& Position);
    inline void SetVelocity(const v3& Velocity);
    inline void SetDirection(const v3& Direction);
    inline void SetGain(r32 Gain);
    inline void SetPitch(r32 Pitch);
    inline void SetLoop(b32 Loop);
    inline void Play();
    inline void Pause();
    inline void Stop();
};

struct listener
{
    v3 Position;
    v3 Velocity;
    v3 Forward;
    v3 Up;
};

struct audio_manager
{
    ALCdevice* Device;
    ALCcontext* Context;
    listener Listener;
};