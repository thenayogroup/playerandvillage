#pragma once

// TODO(Jonathan): Consider making each type of asset(Mesh, Texture, Shader...) hash table and map so that we
// can access them w/ some string like on Cell

struct assets
{
    
    // Meshes
    union
    {
        struct
        {
            mesh Mesh_Dave;
            mesh Mesh_Farmhouse;
            mesh Mesh_Cottage;
            mesh Mesh_Wall;
            mesh Mesh_Outpost;
            mesh Mesh_Tree;
            mesh Mesh_Bush;
            mesh Mesh_Rock;
            mesh Mesh_Golem;
            mesh Mesh_Tree1;
            mesh Mesh_Tree2;
            mesh Mesh_Tree3;
            mesh Mesh_Trunk;
            mesh Mesh_Stone;
            mesh Mesh_Steel;
            mesh Mesh_FireCamp;
            mesh Mesh_Quad;
        };
        mesh Meshes[NUM_OF_MESHS];
    };
    
    
    // Textures
    union
    {
        struct
        {
            u32 Texture_Null;
            u32 Texture_Dave;
            u32 Texture_Farmhouse;
            u32 Texture_Cottage;
            u32 Texture_Wall;
            u32 Texture_Golem;
            u32 Texture_Outpost;
            u32 Texture_Crosshair;
            u32 Texture_Tree;
            u32 Texture_Tree2;
            u32 Texture_Terrain;
            u32 Texture_Grass;
            u32 Texture_InventoryItem;
            u32 Texture_CampFire;
            u32 Texture_StoneSlot;
            u32 Texture_WoodSlot;
            u32 Texture_SteelSlot;
            u32 Texture_FlintSlot;
            u32 Texture_FireStoneSlot;
            u32 Texture_CampFireSlot;
        };
        u32 Textures[NUM_OF_TEXTURES];
    };
    
    
    // Shaders
    union
    {
        struct
        {
            shader_program Shader_World;
            shader_program Shader_TexturedModel;
            shader_program Shader_TexturedModelInstanced;
            shader_program Shader_Ortho;
        };
        shader_program Shaders[NUM_OF_SHADERS];
    };
    
    
    // Audio
    sound_buffer Audio_DeepBass;
    sound_buffer Audio_Bell;
    sound_buffer Audio_Wabble;
    sound_buffer Audio_RLaunch;
    
    // Font
    font_info TimesFont;
    font_info ArialFont;
    font_info RobotoFont;
};

static void InitializeAssets(assets& Assets);
static bool LoadWav(const char* Filepath, sound_buffer& Buffer);
static void ParseAndCompileShader(shader_program& ShaderProgram, const char* Filepath);