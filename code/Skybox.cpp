#include "skybox.h"

static skybox CreateSkybox(const std::vector<std::string> Faces)
{
    if(Faces.size() != 6)
    {
        std::cout << "The vector must contain 6 textures!\n";
        return {0};
    }
    
    // We don't want to vertically flip the skybox apperantly.
    stbi_set_flip_vertically_on_load(0);
    skybox Result = {0};
    
    glGenVertexArrays(1, &Result.Vao);
    glBindVertexArray(Result.Vao);
    
    glGenBuffers(1, &Result.Vbo);
    glBindBuffer(GL_ARRAY_BUFFER, Result.Vbo);
    
    real32 SkyboxVertices[] = {
        // positions          
        -1.0f,  1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        
        -1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,
        
        1.0f, -1.0f, -1.0f,
        1.0f, -1.0f,  1.0f,
        1.0f,  1.0f,  1.0f,
        1.0f,  1.0f,  1.0f,
        1.0f,  1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        
        -1.0f, -1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
        1.0f,  1.0f,  1.0f,
        1.0f,  1.0f,  1.0f,
        1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,
        
        -1.0f,  1.0f, -1.0f,
        1.0f,  1.0f, -1.0f,
        1.0f,  1.0f,  1.0f,
        1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f, -1.0f,
        
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
        1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
        1.0f, -1.0f,  1.0f
    };
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(SkyboxVertices), &SkyboxVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(real32), nullptr);
    glEnableVertexAttribArray(0);
    
    glGenTextures(1, &Result.Texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, Result.Texture);
    
    int32 Width, Height, Channels;
    uint8* Data;
    
    for(int32 Face = 0; Face < 6; ++Face)
    {
        std::string FaceFilename = Faces[Face];
        
        Data = stbi_load(FaceFilename.c_str(), &Width, &Height, &Channels, 0);
        
        if(Data != nullptr)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + Face, 0, GL_RGB, Width, Height, 0, GL_RGB, GL_UNSIGNED_BYTE, Data);
        }
        else
        {
            std::cout << "Could not load Face '" << Faces[Face] << "'!\n";
        }
        
        stbi_image_free(Data);
    }
    
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    
    // Return the vertical flip when done
    stbi_set_flip_vertically_on_load(1);
    return Result;
}

static void RenderSkybox(const skybox& Skybox)
{
    // Change depth function so depth test passes when values are equal to depth buffer's content
    glDepthFunc(GL_LEQUAL);
    
    glBindVertexArray(Skybox.Vao);
    glBindTexture(GL_TEXTURE_CUBE_MAP, Skybox.Texture);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    
    // Back to default
    glDepthFunc(GL_LESS);
}