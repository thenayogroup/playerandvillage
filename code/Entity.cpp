#include "Entity.h"

static void CalculateTilesLocations(const aabb& BoundingBox, 
                                    v2* Locations, s32& TilesLocationsCount,
                                    r32 TileDimension)
{
    s32 StartX = (s32)(BoundingBox.Min.x / TileDimension);
    s32 StartZ = (s32)(BoundingBox.Min.z / TileDimension);
    
    s32 EndX = (s32)(BoundingBox.Max.x / TileDimension);
    s32 EndZ = (s32)(BoundingBox.Max.z / TileDimension);
    
    if(StartX > EndX)
    {
        Utility::Swap(StartX, EndX);
    }
    
    if(StartZ > EndZ)
    {
        Utility::Swap(StartZ, EndZ);
    }
    
    s32 Index = TilesLocationsCount;
    for(s32 i = StartX; i <= EndX;++i)
    {
        for(s32 j = StartZ; j <= EndZ; ++j)
        {
            if(Index >= 0 && Index <= WORLD_SIZE)
            {
                Locations[Index++] = V2(r32(i),r32(j));
            }
            else
            {
                Assert(false);
                // TODO: Add error message
                break;
            }
        }
    }
    TilesLocationsCount = Index;
}


inline void JumpEntity(entity& Entity, r32 JumpAmount)
{
    if (Entity.OnGround)
    {
        Entity.Velocity.y = JumpAmount;
        Entity.OnGround = false;
    }
}

static bool TestCollision(r32 Wall, r32 PositionDeltaX, r32 PositionDeltaY,
                          r32 DestX, r32 DestY, r32& tMin,
                          r32 MinY, r32 MaxY)
{
    bool Result = false;
    real32 tEpsilon = 0.001f;
    
    if(PositionDeltaX != 0.0f)
    {
        r32 tResult = (Wall - DestX) / PositionDeltaX;
        r32 Y = DestY + tResult*PositionDeltaY;
        
        if((tResult >= 0.0f) && (tMin > tResult))
        {
            if((Y >= MinY) && (Y <= MaxY))
            {
                tMin = Maximum(0.0f, tResult - tEpsilon);
                Result = true;
            }
        }
    }
    
    return Result;
}

static void MoveEntity(entity& Entity, move_info& MoveInfo, r32 Delta)
{
    // For the diagonal movement
    if(MoveInfo.Acceleration.Length() > 1.0f)
    {
        MoveInfo.Acceleration.Normalize();
    }
    
    MoveInfo.Acceleration *= MoveInfo.Speed;
    MoveInfo.Acceleration.y = 0.0f;
    
    // Friction and gravity
    MoveInfo.Acceleration += -MoveInfo.Friction * Entity.Velocity;
    MoveInfo.Acceleration.y = -MoveInfo.Gravity;
    
    v3 OldPosition = Entity.Position;
    
    v3 PositionDelta = (0.5f * MoveInfo.Acceleration * Delta * Delta) + (Entity.Velocity * Delta);
    
    Entity.Velocity += MoveInfo.Acceleration * Delta;
    
#if 0    
    //v3 NewPosition = OldPosition + PositionDelta;
    
    for(u32 i = 0; i < 4; ++i)
    {
        r32 tMin = 1.0f;
        
        v3 DesiredPosition = Entity.Position + PositionDelta;
        
        for(u32 EntityIndex = 0; EntityIndex < GameState->EntitiesCount; ++EntityIndex)
        {
            entity& TestEntity = GameState->Entities[EntityIndex];
            if(&TestEntity != &Entity)
            {
                if(CheckAABB(TestEntity.BoundingBox, Entity.BoundingBox))
                {
                    Entity.Position - TestEntity.Position
                        r32 tResult = ;
                }
            }
        }
        
        Entity.Position += tMin*PositionDelta;
    }
#endif
    
    Entity.Position += PositionDelta;
}

void InputHandlerPlayer(entity& Player)
{
    if(GameState->IsCameraOnPlayer)
    {
        if(GameState->PressedKeys[SDL_SCANCODE_W])
        {
            Player.MoveInfo.Acceleration.x = 1.0f;
        }
        if(GameState->PressedKeys[SDL_SCANCODE_S])
        {
            Player.MoveInfo.Acceleration.x = -1.0f;
        }
        if(GameState->PressedKeys[SDL_SCANCODE_A])
        {
            Player.MoveInfo.Acceleration.z = -1.0f;
        }
        if(GameState->PressedKeys[SDL_SCANCODE_D])
        {
            Player.MoveInfo.Acceleration.z = 1.0f;
        }
    }
}

void UpdateAndRenderPlayer(entity& Player, const camera& Camera, r32 Delta)
{
    r32 Angle = 0;
    
    if(GameState->IsCameraOnPlayer)
    {
        r32 PlayerSpeed = PLAYER_SPEED;
        if (GameState->PressedKeys[SDL_SCANCODE_LSHIFT])
        {
            PlayerSpeed *= 5.0f;
        }
        
        Player.MoveInfo.Acceleration = Player.MoveInfo.Acceleration.x * Camera.Forward + Player.MoveInfo.Acceleration.z * Camera.Right;
        
        Player.MoveInfo.Speed = PlayerSpeed;
        Player.MoveInfo.Friction = 3.0f;
        Player.MoveInfo.Gravity = 9.8;
        MoveEntity(Player, Player.MoveInfo,  Delta);
        
        r32 WorldHeight = GetWorldHeight(Player.Position.x, Player.Position.z);
        
        if (Player.Position.y <= WorldHeight)
        {
            Player.Position.y = WorldHeight;
            Player.Velocity.y = 0.0f;
            Player.OnGround = true;
        }
        
        v3 NewForward = V3(Camera.Forward.x, 0.0f, Camera.Forward.z);
        NewForward.Normalize();
        Angle = Math::ArcCos(Dot(V3(0.0f, 0.0f, 1.0f), NewForward));
        if(Camera.Forward.x < 0.0f)
        {
            Angle = -Angle;
        }
    }
    
    Player.MoveInfo.Acceleration = {};
    m3 Rot = Math::Mat3YRotation(Angle);
    
    Player.BoundingBox = ((GameState->Assets.Mesh_Dave.BoundingBox) * Rot) + Player.Position;
    
    Player.TilesLocationsCount = 0;
    CalculateTilesLocations(Player.BoundingBox, Player.TilesLocations, Player.TilesLocationsCount, TileDimension);
    Assert(Player.TilesLocationsCount < PLAYER_MAX_TILES);
    
    m4 ModelMatrix = Translate(Player.Position);
    
    ModelMatrix *= M4(Rot);
    
    RenderModel(GameState->RenderGroup, GameState->Assets.Shader_TexturedModel, GameState->Assets.Mesh_Dave,
                TEXTURE_DAVE_INDEX, GameState->MVP, ModelMatrix);
    
    // Setting up the listener for the audio
    // TODO(Naor): Move this somewhere more appropriate 
    
    GameState->Audio.Listener.Position = Player.Position;
    GameState->Audio.Listener.Position = GameState->MainCamera.Position;
    GameState->Audio.Listener.Velocity = Player.Velocity;
    GameState->Audio.Listener.Forward = GameState->MainCamera.Forward;
    GameState->Audio.Listener.Up = GameState->MainCamera.Up;
    UpdateListener(GameState->Audio);
}

void UpdateAndRenderSoldier(entity& Entity, r32 Delta)
{
    v3 Acceleration = V3();
    
    if(Entity.Direction != V3())
    {
        Acceleration = Math::Normalize(Entity.Direction - Entity.Position);
    }
    
    
    move_info MoveInfo = {};
    MoveInfo.Speed = 30.0f;
    MoveInfo.Acceleration = Acceleration;
    MoveInfo.Friction = 2.5f;
    MoveInfo.Gravity = 9.8;
    MoveEntity(Entity, MoveInfo, Delta);
    
    r32 WorldHeight = GetWorldHeight(Entity.Position.x, Entity.Position.z);
    
    if (Entity.Position.y <= WorldHeight)
    {
        Entity.Position.y = WorldHeight;
        Entity.Velocity.y = 0.0f;
    }
    
    
    if(GameState->SelectedBuilding.Index != -1)
    {
        Entity.Position.y = 70;
    }
    
    
    
    Entity.BoundingBox = GameState->Assets.Mesh_Dave.BoundingBox + Entity.Position;
    
    Entity.TilesLocationsCount = 0;
    CalculateTilesLocations(Entity.BoundingBox, Entity.TilesLocations, Entity.TilesLocationsCount, TileDimension);
    Assert(Entity.TilesLocationsCount < MAX_TILES);
    
    
    m4 ModelMatrix = Translate(Entity.Position);
    
    RenderModel(GameState->RenderGroup, GameState->Assets.Shader_TexturedModel, GameState->Assets.Mesh_Dave,
                TEXTURE_DAVE_INDEX, GameState->MVP, ModelMatrix);
}

void UpdateAndRenderGolem(entity& Entity, r32 Delta)
{
    m4 ModelMatrix = M4();
    
    v3 Direction = Entity.Direction - Entity.Position;
    Direction.Normalize();
    v3 Acceleration = Direction;
    
    
    move_info MoveInfo = {};
    MoveInfo.Speed = 30.0f;
    MoveInfo.Acceleration = Acceleration;
    MoveInfo.Friction = 1.5f;
    MoveInfo.Gravity = 9.8;
    MoveEntity(Entity, MoveInfo, Delta);
    
    r32 WorldHeight = GetWorldHeight(Entity.Position.x, Entity.Position.z);
    
    // TODO(Jonathan): Make that work with JumpEntity
    if(Entity.OnGround)
    {
        if(Entity.Position.y <= WorldHeight)
        {
            Entity.Position.y = WorldHeight;
        }
        
        Entity.NextJump -= Delta;
        if(Entity.OnGround && Entity.NextJump <= 0.0f)
        {
            Entity.OnGround = false;
            Entity.JumpVel = 3.0f;
        }
    }
    else
    {
        Entity.Velocity.y += Entity.JumpVel;
        Entity.JumpVel -= Gravity * Delta;
        
        if(Entity.Position.y <= WorldHeight)
        {
            Entity.Position.y = WorldHeight;
            Entity.JumpVel = 0.0f;
            Entity.OnGround = true;
            Entity.NextJump = Math::Random<0, 5>();
        }
        
    }
    
    
    Entity.BoundingBox = GameState->Assets.Mesh_Golem.BoundingBox + Entity.Position;
    
    Entity.TilesLocationsCount = 0;
    CalculateTilesLocations(Entity.BoundingBox, Entity.TilesLocations, Entity.TilesLocationsCount, TileDimension);
    Assert(Entity.TilesLocationsCount < MAX_TILES);
    
    
    ModelMatrix = Math::Translate(Entity.Position);
    
    RenderModel(GameState->RenderGroup, GameState->Assets.Shader_TexturedModel,
                GameState->Assets.Mesh_Golem, TEXTURE_GOLEM_INDEX, GameState->MVP, ModelMatrix);
}

static void UpdateAndRenderProjectile(entity& Entity, r32 Delta)
{
    // TODO(Jonathan): Make this more parabolic
    move_info MoveInfo = {};
    MoveInfo.Speed = 300.0f;
    MoveInfo.Acceleration = Entity.Direction;
    MoveInfo.Friction = 1.5f;
    MoveInfo.Gravity = 0.5*9.8;
    MoveEntity(Entity, MoveInfo, Delta);
    
    const v3 Half = V3(0.5f);
    Entity.BoundingBox = AABB(Entity.Position - Half, Entity.Position + Half);
    
    DEBUG_POINT(Entity.Position, Color::Red);
}

static void UpdateAndRenderEnv(entity& Entity,  r32 Delta)
{
    mat4 ModelMatrix = Math::Translate(Entity.Position);
    
    texture_index TextureIndex = TEXTURE_NULL_INDEX;
    u32 MeshIndex = 0;
    
    switch(Entity.Type)
    {
        case EntityType_Tree:
        {
            MeshIndex = MESH_TREE_INDEX;
            TextureIndex = TEXTURE_TREE_2_INDEX;
        }
        break;
        case EntityType_Bush:
        {
            MeshIndex = MESH_BUSH_INDEX;
            TextureIndex = TEXTURE_DAVE_INDEX;
        }
        break;
        case EntityType_Rock:
        {
            MeshIndex = MESH_ROCK_INDEX;
            TextureIndex = TEXTURE_TREE_2_INDEX;
        }
        break;
        case EntityType_Wood:
        {
            MeshIndex = MESH_TRUNK_INDEX;
            TextureIndex = TEXTURE_TREE_2_INDEX;
        }
        break;
        case EntityType_Stone:
        {
            MeshIndex = MESH_STONE_INDEX;
            TextureIndex = TEXTURE_TREE_2_INDEX;
        }
        break;
        case EntityType_Steel:
        {
            MeshIndex = MESH_STEEL_INDEX;
            TextureIndex = TEXTURE_DAVE_INDEX;
        }
        break;
        case EntityType_Flint:
        {
            MeshIndex = MESH_ROCK_INDEX;
            TextureIndex = TEXTURE_DAVE_INDEX;
        }
        break;
    }
    
    Entity.BoundingBox = GameState->Assets.Meshes[MeshIndex].BoundingBox + Entity.Position;
    
    Entity.TilesLocationsCount = 0;
    CalculateTilesLocations(Entity.BoundingBox, Entity.TilesLocations, Entity.TilesLocationsCount, TileDimension);
    Assert(Entity.TilesLocationsCount < MAX_TILES);
    
    CheckRayCollisionWithEntity(GameState->SelectedEnv, GameState->MainCamera, GameState->MousePicker,GameState->Entities, Entity, 20.0f);
    
    RenderModel(GameState->RenderGroup, GameState->Assets.Shader_TexturedModel,
                GameState->Assets.Meshes[MeshIndex], GameState->Assets.Textures[TextureIndex], GameState->MVP, 
                ModelMatrix);
}

inline s32 AddEntity(entity& NewEntity)
{
    if(GameState->EntitiesCount + 1 < MAX_ENTITIES)
    {
        entity& Entity = GameState->Entities[GameState->EntitiesCount];
        Entity = NewEntity;
        return GameState->EntitiesCount++;
    }
    else
    {
        Assert(!"Too much enitities!");
    }
    
    return -1;
}

inline s32 AddPlacingBuilding()
{
    entity Entity = {};
    Entity.Type = EntityType_PlacingBuilding;
    s32 Result = AddEntity(Entity);
    return Result;
}


inline s32 AddBuilding(building_type BuildingType, v3 Position, r32 Angle)
{
    entity Entity = {};
    Entity.Type = EntityType_FixedBuilding;
    
    Entity.BuildingType = BuildingType;
    Entity.Position = Position;
    Entity.Angle = Angle;
    Entity.IsVisible = true;
    Entity.IsFirstPlaced = true;
    
    s32 Result = AddEntity(Entity);
    return Result;
}


inline s32 AddGolem(const v3& Position, r32 Health, r32 Power)
{
    entity Entity = {};
    Entity.Type = EntityType_Golem;
    
    Entity.Position = Position;
    Entity.Health = Health;
    Entity.Power = Power;
    Entity.OnGround = true;
    Entity.Direction = V3(PLANE_WIDTH * 0.5f, 0.0f, PLANE_DEPTH * 0.5f);
    
    s32 Result = AddEntity(Entity);
    return Result;
}

inline s32 AddPlayer()
{
    entity Entity = {};
    Entity.Type = EntityType_Player;
    
    Entity.Health = 100.0f;
    Entity.Power = 15.0f;
    Entity.AttackRange = 20.0f;
    Entity.OnGround = true;
    
    s32 Result = AddEntity(Entity);
    return Result;
}

inline s32 AddSoldier()
{
    entity Entity = {};
    Entity.Type = EntityType_Soldier;
    
    Entity.Health = 100.0f;
    Entity.Power = 15.0f;
    Entity.AttackRange = 20.0f;
    Entity.OnGround = true;
    
    s32 Result = AddEntity(Entity);
    return Result;
}

inline s32 AddProjectile(const v3& Position, const v3& Direction, r32 Power, r32 Force)
{
    entity Entity = {};
    Entity.Type = EntityType_Projectile;
    
    Entity.Position = Position;
    Entity.Direction = Direction;
    Entity.Power = Power;
    
    s32 Result = AddEntity(Entity);
    return Result;
}

inline s32 AddEnvEntity(entity_type EnvEntityType, const v3& Position)
{
    entity Entity = {};
    Entity.Type = EnvEntityType;
    
    Entity.IsVisible = true;
    Entity.Position = Position;
    Entity.Health = 100.0f;
    
    s32 Result = AddEntity(Entity);
    return Result;
}

inline void RemoveEntity(entity& Entity)
{
    if(GameState->EntitiesCount > 0)
    {
        Entity.Type = EntityType_None;
        // NOTE(Jonathan): When swapping the index will be zero which is the player and it's bad!
        Utility::Swap(Entity, GameState->Entities[GameState->EntitiesCount-1]);
        --GameState->EntitiesCount;
    }
}

inline void RemoveAllEntities(entity_type Type)
{
    for(u32 EntityIndex = 0; EntityIndex < GameState->EntitiesCount; ++EntityIndex)
    {
        if(GameState->Entities[EntityIndex].Type == Type)
        {
            RemoveEntity(GameState->Entities[EntityIndex]);
            EntityIndex--;
        }
    }
}
