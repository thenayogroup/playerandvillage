#shader vertex

#version 410 core

layout(location = 0) in vec3 Position;
layout(location = 1) in vec3 Normal;

out VertexData
{
    vec3 Position;
    flat vec3 Normal;
    float Diff;
}
OutFragData;

uniform mat4 ProjectionView;
uniform float Time;
uniform float MinHeight;
uniform float MaxHeight;

float HeightTo01(float Value)
{
    float Result = ((Value - MinHeight) / (MaxHeight - MinHeight));
    
    return Result;
}

// TODO(Naor): Remove the + normalized(ViewPosition) there is a bug with the shader with it...
float GetAmbientAndDiffuse()
{
    float T = Time*0.05;
    vec3 Sun = vec3(cos(T), sin(T), cos(T)) * 1000.0;
    
    vec3 LightDir = normalize(Sun - Position);
    //vec3 Color = texture(TerrainColors, vec2(HeightTo01(Position.y), 0.5)).rgb;
    //vec3 Color = texture(TerrainColors, OutFragData.Position.xz*0.001).rgb;
    //vec3 Ambient = 0.1 * Color;
    
    float Diff = max(dot(LightDir, Normal), 0.0) * 0.7;
    Diff = 0.8;
    //vec3 Diffuse = Diff * Color;
    
    //return vec3(Ambient + Diffuse);
    return Diff;
}

void main()
{
    
    gl_Position = ProjectionView *  vec4(Position, 1.0);
    OutFragData.Position = Position;
    OutFragData.Normal = Normal;
    OutFragData.Diff = GetAmbientAndDiffuse();
    //OutFragData.Color.b = HeightTo01(Position.y);
    //OutFragData.Color = Normal;
}

#shader fragment

#version 410 core

in VertexData
{
    vec3 Position;
    flat vec3 Normal;
    float Diff;
}
InFragData;

uniform sampler2D TerrainColors;
out vec4 OutColor;

void main()
{
    
    vec3 Color = texture(TerrainColors, InFragData.Position.xz * 0.07).rgb * InFragData.Diff;
    OutColor = vec4(Color, 1.0);
}

#shader end

