#shader vertex

#version 410 core

layout(location = 0) in vec3 Position;
layout(location = 1) in vec3 Normal;
layout(location = 2) in vec2 TexCoord;
layout(location = 3) in vec3 Positions;

out VertexData
{
    vec3 Position;
    vec3 Normal;
    vec2 TexCoord;
    float Diff;
} OutFragData;

uniform mat4 ProjectionView;
uniform float Time;

float GetDiffuse()
{
    // TODO(Naor): Make the sun configurable through uniforms
    //vec3 Sun = vec3(300.0, 200.0, 300.0) + normalize(ViewPosition);
    float T = Time*0.05;
    //T = 52.5;
    vec3 Sun = vec3(cos(T), sin(T), cos(T)) * 1000.0;
    
    vec3 LightDir = normalize(Sun - Position);
    vec3 CurrNormal = Normal;
    float Diff = 0.0;
    
    Diff = max(dot(LightDir, CurrNormal), 0.0) * 0.7;
    
    return Diff;
}


void main()
{
    gl_Position = ProjectionView * vec4(Positions + Position, 1.0);
    OutFragData.Normal = Normal;
    OutFragData.Position = Positions;
    OutFragData.TexCoord = TexCoord;
    OutFragData.Diff = GetDiffuse();
}

#shader fragment

#version 410 core

in VertexData
{
    vec3 Position;
    vec3 Normal;
    vec2 TexCoord;
    float Diff;
}
InFragData;

uniform sampler2D Texture;
out vec4 OutColor;

void main()
{
    //OutColor = mix(vec4(InFragData.Color, 1), texture(Texture, InFragData.TexCoord), 0.5f);
    OutColor = texture(Texture, InFragData.TexCoord);
    vec3 Ambient = 0.25 * OutColor.rgb;
    vec3 Diffuse = InFragData.Diff * OutColor.rgb;
    
    OutColor.rgb = Ambient + Diffuse;
}

#shader end