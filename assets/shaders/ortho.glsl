#shader vertex

#version 410 core

layout(location = 0) in vec2 Position;
layout(location = 1) in vec2 TexCoords;

out VertexData
{
    vec2 Position;
    vec2 TexCoords;
} OutVert;

uniform mat4 Projection;
uniform mat4 Model;

void main()
{
    gl_Position = Projection * Model * vec4(Position, 1.0f, 1.0f);
    OutVert.Position = gl_Position.xy;
    OutVert.TexCoords = TexCoords;  
}

#shader fragment

#version 410 core

in VertexData   
{
    vec2 Position;
    vec2 TexCoords;
}
InFrag;

uniform sampler2D Texture;

out vec4 OutColor;

void main()
{
    OutColor = texture(Texture, InFrag.TexCoords);
    //if(OutColor.a < 0.1)
    //discard;
    //OutColor = vec4(1.0f);
}

#shader end

