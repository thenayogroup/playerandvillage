//
// Vertex
//
#shader Vertex

#version 410 core

layout(location = 0) in vec3 Position;
layout(location = 1) in vec3 Normal;
layout(location = 2) in vec2 TexCoord;

out VertexData
{
    vec3 Position;
    vec3 Normal;
    vec2 TexCoord;
    float Diff;
} OutFragData;

uniform mat4 ProjectionView;
uniform mat4 Model;
uniform float Time;

float GetDiffuse()
{
    float T = Time*0.05;
    //T = 52.5;
    vec3 Sun = vec3(cos(T), sin(T), cos(T)) * 1000.0;
    vec3 LightDir;
    vec3 CurrNormal = mat3(Model) * Normal;
    
    float Diff = 0.0;
    
    LightDir = normalize(Sun - Position);
    Diff = max(dot(LightDir, CurrNormal), 0.0) * 0.7;
    
    return Diff;
}

void main()
{
    gl_Position = ProjectionView * Model * vec4(Position, 1.0);
    OutFragData.Normal =  Normal;
    OutFragData.TexCoord = TexCoord;
    OutFragData.Diff = GetDiffuse();
}

//
// Fragment
//
#shader Fragment

#version 410 core

in VertexData
{
    vec3 Position;
    vec3 Normal;
    vec2 TexCoord;
    float Diff;
}
InFragData;

uniform sampler2D Texture;
uniform vec4 Color;

out vec4 OutColor;

void main()
{
    //if(Color.x != 0 || Color.y != 0 || Color.z != 0)
    if(any(notEqual(Color.xyz, vec3(0.0))))
    {
        OutColor = mix(vec4(Color.xyz, 1), texture(Texture, InFragData.TexCoord), 0.5f);
    }
    else
    {
        OutColor = texture(Texture, InFragData.TexCoord);
        OutColor.a = Color.a;
    }
    
    
    vec3 Ambient = 0.25 * OutColor.rgb;
    vec3 Diffuse = InFragData.Diff * OutColor.rgb;
    
    OutColor.rgb = Ambient + Diffuse;
}
#shader end
