import re
import urllib2
from HTMLParser import HTMLParser

class MLStripper(HTMLParser):
	def __init__(self):
		self.reset()
		self.fed = []
	def handle_data(self, d):
		self.fed.append(d)
	def get_data(self):
		return ''.join(self.fed)

def strip_tags(html):
	s = MLStripper()
	s.feed(html)
	return s.get_data()

url = 'https://www.quotedb.com/quote/quote.php?action=random_quote'
opener = urllib2.build_opener()
opener.addheaders = [('User-agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A')]
response = opener.open(url).read()
	
matches = re.findall(r'document.write\(\'(.*)\'\)', response)
quote = strip_tags(matches[0])
author = re.search(r'More quotes from (.*)', strip_tags(matches[1]))
if author:
	author = author.group(1)

print '\"' + quote + '\"' + ", " + author + "."